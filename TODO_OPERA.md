# TODO
- opera first in midi chain
- make logs a debug feature, for better speed
- add Patch to control the output channel again
- src/frontend/app.rs: Add visual input_text in frontend like ballet
- src/frontend.rs: Correct channel in Action::CycleInputSource
- optimize path to drumsignal to voice
