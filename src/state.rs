// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

#[cfg(feature = "synth")]
use serde::Deserializer;

use std::path::Path;

use crate::paths;

#[cfg(feature = "synth")]
pub fn ok_or_default<'a, T, D>(deserializer: D) -> Result<T, D::Error>
where
    T: Deserialize<'a> + Default,
    D: Deserializer<'a>,
{
    let v: ron::Value = Deserialize::deserialize(deserializer)?;
    Ok(T::deserialize(v).unwrap_or_default())
}

pub trait State {
    fn new() -> Self;
    fn from_string(s: String) -> Self;

    fn read<'a>(filename: &str) -> Self
    where
        Self: Sized + Deserialize<'a>,
    {
        match std::fs::read_to_string(paths::project_path(filename)) {
            Ok(s) => Self::from_string(s),
            Err(_) => match std::fs::read_to_string(paths::home_path(filename)) {
                Ok(s) => Self::from_string(s),
                Err(_) => Self::new(),
            },
        }
    }

    fn read_global<'a>(filename: &str) -> Self
    where
        Self: Sized + Deserialize<'a>,
    {
        match std::fs::read_to_string(paths::home_path(filename)) {
            Ok(s) => Self::from_string(s),
            Err(_) => Self::new(),
        }
    }

    #[cfg(feature = "ballet")]
    fn read_ballet<'a>(filename: &str) -> Self
    where
        Self: Sized + Deserialize<'a>,
    {
        match std::fs::read_to_string(paths::ballet_path(filename)) {
            Ok(s) => Self::from_string(s),
            Err(_) => Self::new(),
        }
    }

    fn write(&self, filename: &str)
    where
        Self: Serialize,
    {
        let toml = toml::to_string(&self).unwrap();
        let path = paths::project_path(filename);
        let dir = Path::new(&path).parent().unwrap();
        std::fs::create_dir_all(dir).unwrap();
        std::fs::write(&path, toml).unwrap();
    }

    #[cfg(feature = "ballet")]
    fn write_ballet(&self, filename: &str)
    where
        Self: Serialize,
    {
        let ron = ron::to_string(&self).unwrap();
        let path = paths::ballet_path(filename);
        let dir = Path::new(&path).parent().unwrap();
        std::fs::create_dir_all(dir).unwrap();
        std::fs::write(&path, ron).unwrap();
    }

    fn write_global(&self, filename: &str)
    where
        Self: Serialize,
    {
        let toml = toml::to_string(&self).unwrap();
        let path = paths::home_path(filename);
        let dir = Path::new(&path).parent().unwrap();
        std::fs::create_dir_all(dir).unwrap();
        std::fs::write(&path, toml).unwrap();
    }
}
