// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use hwloc::{CpuSet, ObjectType, Topology, CPUBIND_THREAD};
use thread_priority::*;

pub fn set_normal_thread_min_priority() {
    let thread_id = thread_native_id();
    set_thread_priority_and_policy(
        thread_id,
        ThreadPriority::Min,
        ThreadSchedulePolicy::Normal(NormalThreadSchedulePolicy::Normal),
    )
    .unwrap();

    let mut topo = Topology::new();
    let num_cores = topo.objects_with_type(&ObjectType::Core).unwrap().len();

    if num_cores > 1 {
        topo.set_cpubind_for_thread(thread_id, CpuSet::from(0), CPUBIND_THREAD)
            .unwrap();
    }
}

pub fn set_realtime_thread_priority(priority: u32) {
    let thread_id = thread_native_id();
    match set_thread_priority_and_policy(
        thread_id,
        ThreadPriority::Specific(priority),
        ThreadSchedulePolicy::Realtime(RealtimeThreadSchedulePolicy::Fifo),
    )
    .is_ok()
    {
        true => {}
        false => {}
    }

    let mut topo = Topology::new();
    let num_cores = topo.objects_with_type(&ObjectType::Core).unwrap().len();

    if num_cores > 1 {
        // To avoid IRQ interference, real-time threads are bound to cores above 0.
        topo.set_cpubind_for_thread(
            thread_id,
            CpuSet::from_range(1, (num_cores - 1) as i32),
            CPUBIND_THREAD,
        )
        .unwrap();
    }
}
