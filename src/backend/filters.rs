// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use super::frames::{Frame, StereoFrame};
use core::f32::consts::PI;

const DELAYLINE_MAX_LENGTH: usize = 2000; // samples
const SAMPLE_TIME: f32 = 1.0 / 44100.0; // seconds/sample

pub struct Biquad {
    x: [StereoFrame; 2],
    y: [StereoFrame; 2],
}
impl Biquad {
    pub fn new() -> Self {
        Self {
            x: [StereoFrame::zero(); 2],
            y: [StereoFrame::zero(); 2],
        }
    }

    pub fn process(
        &mut self,
        frame: StereoFrame,
        gain: f32,
        zeros: [f32; 2],
        poles: [f32; 2],
    ) -> StereoFrame {
        let num = zeros[0] * self.x[0] + zeros[1] * self.x[1];
        let den = poles[0] * self.y[0] + poles[1] * self.y[1];
        let out = gain * frame + num - den;

        self.x = [frame, self.x[0]];
        self.y = [out, self.y[0]];

        out
    }
}

pub struct LowPass {
    max_freq: f32,
    x: StereoFrame,
}
impl LowPass {
    pub fn new(max_freq: f32) -> Self {
        Self {
            max_freq,
            x: StereoFrame::zero(),
        }
    }
    pub fn process(&mut self, frame: StereoFrame) -> StereoFrame {
        let cutoff = self.max_freq;
        let rc = 1.0 / (2.0 * PI * cutoff);
        let alpha = SAMPLE_TIME / (rc + SAMPLE_TIME);
        let out = alpha * frame + (1.0 - alpha) * self.x;
        self.x = out;

        out
    }
}

pub struct HighPass {
    max_freq: f32,
    x: StereoFrame,
    y: StereoFrame,
}
impl HighPass {
    pub fn new(max_freq: f32) -> Self {
        Self {
            max_freq,
            x: StereoFrame::zero(),
            y: StereoFrame::zero(),
        }
    }
    pub fn process(&mut self, frame: StereoFrame) -> StereoFrame {
        // α := RC / (RC + dt)
        // y[i] := α × y[i−1] + α × (x[i] − x[i−1])
        let cutoff = self.max_freq;
        let rc = 1.0 / (2.0 * PI * cutoff);
        let alpha = rc / (rc + SAMPLE_TIME);
        let out = alpha * self.y + alpha * frame + (-alpha * self.x);
        self.x = frame;
        self.y = out;

        out
    }
}

pub struct FeedBackComb {
    tick: usize,
    delay: usize,
    scaled_delay: usize,
    gain: f32,
    x: [StereoFrame; DELAYLINE_MAX_LENGTH],
}
impl FeedBackComb {
    pub fn new(gain: f32, delay: usize) -> Self {
        assert!(delay <= DELAYLINE_MAX_LENGTH);
        Self {
            tick: 0,
            delay,
            scaled_delay: delay,
            gain,
            x: [StereoFrame::zero(); DELAYLINE_MAX_LENGTH],
        }
    }
    fn recv(&mut self) -> StereoFrame {
        self.x[(self.tick + 1) % self.scaled_delay]
    }
    fn send(&mut self, frame: StereoFrame) {
        self.x[self.tick % self.scaled_delay] = frame + -self.gain * self.recv();
    }
    pub fn process(&mut self, frame: StereoFrame) -> StereoFrame {
        self.send(frame);
        let out = frame + -self.gain * self.recv();
        self.tick += 1;

        out
    }
    pub fn set_scale(&mut self, scale: f32) {
        let scaled_delay = (self.delay as f32 * scale) as usize;
        self.scaled_delay = scaled_delay.min(DELAYLINE_MAX_LENGTH).max(1);
    }
}

pub struct AllPass {
    tick: usize,
    gain: f32,
    delay: usize,
    x: [StereoFrame; DELAYLINE_MAX_LENGTH],
}
impl AllPass {
    pub fn new(gain: f32, delay: usize) -> Self {
        assert!(delay <= DELAYLINE_MAX_LENGTH);
        Self {
            tick: 0,
            delay,
            gain,
            x: [StereoFrame::zero(); DELAYLINE_MAX_LENGTH],
        }
    }
    fn recv(&self) -> StereoFrame {
        self.x[(self.tick + 1) % self.delay]
    }
    fn send(&mut self, frame: StereoFrame) {
        self.x[self.tick % self.delay] = frame + self.gain * self.recv();
    }
    pub fn process(&mut self, frame: StereoFrame) -> StereoFrame {
        self.send(frame);
        let out = -self.gain * frame + (1.0 - self.gain.powf(2.0)) * self.recv();
        self.tick += 1;

        out
    }
}
