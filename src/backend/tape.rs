// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crossbeam::channel;
use crossbeam::channel::{Receiver, Sender};

use super::frames::{Frame, StereoFrame};
use super::io::IOMessage;
use super::wheel::{Wheel, WheelBuffer};

const CHUNK_SIZE: usize = 44100;

/// A Tape is a mono audio object connected to a file on the filesystem.  It follows a
/// `send-process-recv` protocol, for updating the internal state.
pub struct Tape {
    idx: usize,
    wheel: Wheel<StereoFrame>,
    channel_read_rx: Receiver<WheelBuffer<StereoFrame>>,
    channel_request_tx: Sender<IOMessage<StereoFrame>>,
    sample_in: StereoFrame,
    sample_out: StereoFrame,
}

impl Tape {
    pub fn new(
        idx: usize,
        filename: String,
        channel_request_tx: Sender<IOMessage<StereoFrame>>,
    ) -> Self {
        // Set up communication channel to receive buffers that have been read from disk.
        let (channel_read_tx, channel_read_rx) = channel::unbounded::<WheelBuffer<StereoFrame>>();

        channel_request_tx
            .send(IOMessage::Load {
                idx,
                filename,
                channel: channel_read_tx,
            })
            .unwrap();

        // Bootstrap the IO buffers.
        let bootstrap_buffer = WheelBuffer::new(0, CHUNK_SIZE);

        channel_request_tx
            .send(IOMessage::Request {
                idx,
                buffer: bootstrap_buffer,
                t_request: 0,
                flush: false,
            })
            .unwrap();

        Self {
            idx,
            wheel: Wheel::new(CHUNK_SIZE, 120),
            channel_read_rx,
            channel_request_tx,
            sample_in: Frame::zero(),
            sample_out: Frame::zero(),
        }
    }

    /// Pass `sample` to the tape.  The sample will be added to the current working sample.
    /// Several calls to `send` can be made to combine samples from multiple sources, before
    /// calling `process`.
    pub fn send(&mut self, sample: StereoFrame) {
        self.sample_in += sample;
    }

    /// Process the sample at `t`.  This will read from the internal audio buffer and
    /// prepare the working sample for new calls to `send` and `recv`.  If `read` is true,
    /// the active sample in the audio buffer will be added to the working sample.  If
    /// `write` is true, the active sample in the audio buffer will be overwritten by the
    /// working sample.
    ///
    /// Note: if both `read` and `write` are true, the audio buffer will be overdubbed.
    #[allow(clippy::too_many_arguments)]
    pub fn process(
        &mut self,
        t: usize,
        read: bool,
        write: bool,
        lookahead: bool,
        monitor: bool,
        flush: bool,
    ) {
        self.buffer_preload(t, write, lookahead, flush);

        self.sample_out = StereoFrame::zero();

        if monitor {
            self.sample_out += self.sample_in;
        }

        if read {
            let frame = self.buffer_read(t);
            self.sample_out += frame;
        }

        if write {
            self.buffer_write(t, self.sample_out);
        }

        self.sample_in = StereoFrame::zero();
    }

    /// Retrieve the last processed sample.
    pub fn recv(&self) -> StereoFrame {
        self.sample_out
    }

    pub fn preload_status(&self) -> f32 {
        self.wheel.status()
    }

    fn buffer_preload(&mut self, t: usize, write: bool, lookahead: bool, flush: bool) {
        // Set lookahead mode.
        self.wheel.set_lookahead(lookahead);

        // Flush, baby, flush!
        if (flush && !self.wheel.flushed) || self.wheel.is_flushing() {
            if let Ok(buffer) = self.channel_read_rx.try_recv() {
                let buffer = self.wheel.flush(buffer);
                let t_request = buffer.t0;
                self.channel_request_tx
                    .send(IOMessage::Request {
                        idx: self.idx,
                        buffer,
                        t_request,
                        flush,
                    })
                    .unwrap();
            }

            // Don't seek the wheel while flushing.
            return;
        }

        // Seek the wheel forward to the current tick and return if the wheel is already
        // contiguous.
        if self.wheel.seek(t) {
            return;
        }

        if write && !self.wheel.contains(t) {
            self.channel_request_tx
                .send(IOMessage::Overrun(self.idx))
                .unwrap();
        }

        // Load any pending buffers into the wheel.
        if let Ok(buffer) = self.channel_read_rx.try_recv() {
            // Place the new buffer in the wheel.
            let buffer = self.wheel.swap(buffer);
            let t_request = self.wheel.next_request();

            // Send the old buffer off to request new data to be read.
            self.channel_request_tx
                .send(IOMessage::Request {
                    idx: self.idx,
                    buffer,
                    t_request,
                    flush,
                })
                .unwrap();
        }
    }

    fn buffer_read(&mut self, t: usize) -> StereoFrame {
        self.wheel.read(t)
    }

    fn buffer_write(&mut self, t: usize, frame: StereoFrame) {
        self.wheel.write(t, frame);
    }

    pub fn set_loop_points(&mut self, start: usize, stop: usize) {
        self.wheel.set_loop_points(start, stop);
    }
}
