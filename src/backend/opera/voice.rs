// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;

use super::noise::Noise;
use super::oscillator::Oscillator;

use crate::synth::{Modulation};
use crate::opera::{Patch, VoiceParameter, VoiceState};
use crate::messages::{OperaConfig, OperaId};

pub struct Voice {
    pub active: bool,
    pub held: bool,
    pub patch: Patch,
    // Time
    pub t: f32,
    // Frequency
    ids: Vec<OperaId>,
    base_frequency: f32,
    base_frequencies: HashMap<OperaId, f32>,
    current_frequency: f32,
    frequency_idx: usize,
    frequency_count: usize,
    // State
    pub state: VoiceState,
    triggered: bool,
    osc1: Oscillator,
    noise: Noise,
    axes: [f32; 24],
    // Rate reduction,
    counter: usize,
    prev_rate_divider: usize,
    // Filter/gain
    alpha: f32,
    out: f32,
    xs: [f32; 2],
    out_frame: (f32, f32),
    hold_frame: (f32, f32),
}

impl Voice {
    pub fn new() -> Self {
        let patch = Patch::new(0, 1, 255);

        let osc1 = Oscillator::new(patch.scene.osc1.clone());
        let noise = Noise::new(patch.scene.noise.clone());

        let state = patch.scene.voice.clone();

        Self {
            active: false,
            held: false,
            patch,
            // Time
            t: 0.0,
            // Frequency
            ids: Vec::with_capacity(8),
            base_frequency: 0.0,
            base_frequencies: HashMap::new(),
            current_frequency: 0.0,
            frequency_idx: 0,
            frequency_count: 0,
            // State
            state,
            triggered: false,
            osc1,
            noise,
            axes: [0.0; 24],
            // Rate reduction,
            counter: 0,
            prev_rate_divider: 1,
            // A very simple high-pass filter to remove DC offset.  This cuts at roughly 30
            // Hz.  Set `alpha` closer to 1.0 for even lower cutoff values.
            alpha: 0.9958,
            out: 0.0,
            xs: [0.0, 0.0],
            out_frame: (0.0, 0.0),
            hold_frame: (0.0, 0.0),
        }
    }

    pub fn load_patch(&mut self, patch: Patch) {
        self.osc1.state.set_from(&patch.scene.osc1);
        self.noise.state.set_from(&patch.scene.noise);

        self.state.set_from(&patch.scene.voice);

        self.patch = patch;
    }

    pub fn unload_patch(&mut self) {
        self.load_patch(Patch::new(0, 1, 255));
        self.panic();
    }

    pub fn set_param(&mut self, param: VoiceParameter, value: f32) {
        match param {
            VoiceParameter::Voice(dest) => self.state.get_mut(dest).set(value),
            VoiceParameter::Osc1(dest) => self.osc1.state.get_mut(dest).set(value),
            VoiceParameter::Noise(dest) => self.noise.state.get_mut(dest).set(value),
        }
    }

    pub fn set_modulation(&mut self, modulation: Modulation<VoiceParameter>) {
        self.reset_modulations();
        self.patch
            .set_modulation_params(modulation.idx, modulation.destination, modulation.params);
    }

    pub fn set_config(&mut self, dest: OperaConfig, value: usize) {
        self.patch.set_config(dest, value);
    }

    pub fn has_channel(&mut self, channel: u8) -> bool {
        channel == 0 || self.patch.channel == channel
    }

    pub fn has_id(&mut self, channel: u8, id: OperaId) -> bool {
        self.has_channel(channel)
            && (id.group_id == 0
                || self
                    .base_frequencies
                    .keys()
                    .any(|key| key.group_id == id.group_id))
            && (id.voice_id == 0
                || self
                    .base_frequencies
                    .keys()
                    .any(|key| key.voice_id == id.voice_id))
    }

    #[inline(always)]
    pub fn strum(&mut self) {
        if !self.triggered {
            // self.shaper.strum();
        }
    }

    #[inline(always)]
    pub fn strum_decay(&mut self) {
        // self.shaper.strum_decay();
    }

    pub fn trigger(&mut self, id: OperaId, frequency: f32, velocity: f32) {
        self.push_base_frequency(id, frequency);
        self.axes[0] = velocity;
        self.t = 0.0;

        // In order to ensure that we update the state before triggering, in case the delay
        // time, etc. needs to be updated, we schedule the trigger to happen during the next
        // update.
        self.triggered = true;

        if !self.active {
            self.frequency_idx = 0;

            self.osc1.trigger();

            self.xs = [0.0, 0.0];
        }

        self.active = true;
        self.held = true;
    }

    pub fn release(&mut self, id: OperaId, velocity: f32, latch: bool) {
        self.pop_base_frequency(id, latch);

        if velocity > 0.0 {
            self.axes[0] = velocity;
        }

        if self.ids.is_empty() {
            self.held = false;
        }

        self.determine_base_frequency();
    }

    pub fn push_base_frequency(&mut self, id: OperaId, frequency: f32) {
        self.ids.push(id);
        self.base_frequencies.insert(id, frequency);
        self.frequency_count = self.ids.len();
    }

    pub fn pop_base_frequency(&mut self, id: OperaId, latch: bool) {
        let freq = self.base_frequencies.remove(&id);

        self.ids.retain(|&id_| id_ != id);

        self.frequency_count = self.ids.len();
    }

    fn determine_base_frequency(&mut self) {
        self.frequency_idx = self.frequency_count.max(1) - 1;

        self.base_frequency = match self.ids.get(self.frequency_idx) {
            None => self.base_frequency,
            Some(id) => match self.base_frequencies.get(id) {
                None => self.base_frequency,
                Some(freq) => *freq,
            },
        };
    }

    pub fn frequency_distance(&self, frequency: f32) -> f32 {
        if frequency > self.base_frequency {
            frequency / self.base_frequency
        } else {
            self.base_frequency / frequency
        }
    }

    pub fn mute(&mut self) {
        self.ids.clear();
        self.base_frequencies.clear();

        self.held = false;
        self.frequency_count = 0;
    }

    pub fn panic(&mut self) {
        self.noise.panic();
        self.xs = [0.0, 0.0];
        self.out = 0.0;
        self.out_frame = (0.0, 0.0);
        self.active = false;
        self.mute();
    }

    pub fn morph(&mut self, axis: usize, amount: f32) {
        self.axes[axis] = amount;
    }

    pub fn reset_modulations(&mut self) {
        for modulation in self.patch.modulations.iter() {
            match modulation.destination {
                VoiceParameter::Voice(dest) => self.state.get_mut(dest).reset(),
                VoiceParameter::Osc1(dest) => self.osc1.state.get_mut(dest).reset(),
                VoiceParameter::Noise(dest) => self.noise.state.get_mut(dest).reset(),
            }
        }
    }

    pub fn next_state(&mut self) {
        self.reset_modulations();

        for modulation in self.patch.modulations.iter() {
            let amount = modulation.get(&self.axes);

            if amount == 0.0 {
                continue;
            }

            match modulation.destination {
                VoiceParameter::Voice(dest) => self.state.get_mut(dest).modulate(amount),
                VoiceParameter::Osc1(dest) => self.osc1.state.get_mut(dest).modulate(amount),
                VoiceParameter::Noise(dest) => self.noise.state.get_mut(dest).modulate(amount),
            }
        }
    }

    pub fn process(
        &mut self,
        master_axes: &[f32; 24],
        dt: f32,
    ) -> (f32, f32) {
        let rate_divider = self.patch.rate_divider + 1;

        if rate_divider != self.prev_rate_divider {
            self.prev_rate_divider = rate_divider;
        }

        if self.counter == 0 {
            self.hold_frame = self.out_frame;

            let dt = rate_divider as f32 * dt;

            self.next_frame(master_axes, dt);
        }

        let rate_divider = self.patch.rate_divider + 1;
        let a = (self.counter + 1) as f32 / rate_divider as f32;

        let frame = (
            (1.0 - a) * self.hold_frame.0 + a * self.out_frame.0,
            (1.0 - a) * self.hold_frame.1 + a * self.out_frame.1,
        );

        self.counter = (self.counter + 1) % rate_divider;

        frame
    }

    fn next_frame(
        &mut self,
        master_axes: &[f32; 24],
        dt: f32,
    ) {
        self.next_state();

        self.axes[1] = master_axes[1];
        self.axes[2] = master_axes[2];

        if self.triggered {
            self.determine_base_frequency();

            self.triggered = false;
        }

        // `0.0` at f == 27.5, `1.0` at f == 7040.0
        self.axes[16] = (self.base_frequency / 27.5).log2() / 8.0;

        self.current_frequency = self.base_frequency;

        let freq = self.current_frequency;

        let osc1 = self.osc1.process(freq, dt);

        let noise = self.noise.process();

        self.axes[8] = (0.5 + 0.5 * osc1).min(1.0).max(0.0);

        self.axes[11] = (0.5 + 0.5 * noise).min(1.0).max(0.0);

        let mut sample = 0.0;

        sample += self.state.osc1.x * osc1;
        sample += self.state.noise.x * noise;

        // Apply high-pass filter to remove DC offset.
        self.xs[0] = sample;
        self.out = self.alpha * (self.out + self.xs[0] - self.xs[1]);
        self.xs[1] = self.xs[0];

        self.t += dt;

        self.out_frame = (sample, sample);
    }
}
