// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::opera::OscillatorState;

pub struct Oscillator {
    pub state: OscillatorState,
    t: f32,
    x: f32,
}

impl Oscillator {
    pub fn new(state: OscillatorState) -> Self {
        Self {
            state,
            t: 0.0,
            x: 0.0,
        }
    }

    pub fn trigger(&mut self) {
        self.t = 0.0;
        self.x = 0.0;
    }

    pub fn process(&mut self, base_frequency: f32, dt: f32) -> f32 {
        let freq =
            self.state.frequency.x * (1.0 + self.state.track.x * (base_frequency / 440.0 - 1.0));

        if freq == 0.0 {
            return 0.0;
        }

        self.t = (self.t + freq * dt).rem_euclid(1.0);

        let mut t = self.t;

            let x = {

                let pivot = 0.5;

                if t < pivot {
                    -1.0 + 2.0 * t / pivot
                } else {
                    1.0 - 2.0 * (t - pivot) / (1.0 - pivot)
                }

            };

        self.x = x;

        self.x
    }
}
