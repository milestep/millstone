const RATE: f32 = 44100.0;

struct Driver {
    x: f32,
    v: f32,
    a: f32,
    b: f32,
    d: f32,
    gain: f32,
}

impl Driver {
    fn new(stiffness: f32, gain: f32) -> Self {
        Self {
            x: 0.0,                                    // displacement
            v: 0.0,                                    // velocity
            a: stiffness * 0.145 * 10.0_f32.powf(7.0), // stiffness
            b: 10.0_f32.powf(6.0),                     // duffing
            d: 10.0,                                   // damping
            gain,
        }
    }

    pub fn hit(&mut self, velocity: f32) {
        self.v = 10.0_f32.powf(3.0) * velocity;
    }

    fn process(&mut self) -> f32 {
        self.v -= (self.a * self.x + self.b * self.x.powf(3.0) + self.d * self.v) / RATE;
        self.x += self.v / RATE;
        self.gain * self.x * 10.0_f32.powf(6.0)
    }
}

struct Noise {
    x: usize,
    divisor: usize,
}

impl Noise {
    fn new() -> Self {
        Self {
            x: 42,
            divisor: 32768,
        }
    }

    fn process(&mut self) -> f32 {
        self.x = ((self.x * 7621) + 1) % self.divisor;
        self.x as f32 / self.divisor as f32
    }
}

struct Seiding {
    x: f32,
    noise: Noise,
    tension: f32,
    gain: f32,
    radius: f32,
}

impl Seiding {
    fn new(tension: f32, gain: f32) -> Self {
        Self {
            x: 0.0,
            noise: Noise::new(),
            tension,
            gain,
            radius: 0.5,
        }
    }

    pub fn hit(&mut self, velocity: f32) {
        self.x = velocity;
    }

    fn process(&mut self) -> f32 {
        self.x = (self.x - self.tension * (50.0 / RATE)).max(0.0);
        0.1 * self.gain * self.radius * self.x * (self.noise.process() - 0.5)
    }
}

pub struct Drum {
    x: f32,
    v: f32,
    a: f32,
    b: f32,
    d: f32,
    driver: Driver,
    noise: Noise,
    seiding: Seiding,
}

impl Drum {
    pub fn new(
        stiffness: f32,
        damping: f32,
        noise: f32,
        driver_gain: f32,
        driver_stiffness: f32,
    ) -> Self {
        Self {
            x: 0.0,                            // displacement
            v: 0.0,                            // velocity
            a: stiffness * 10.0_f32.powf(7.0), // stiffness
            b: 2.0 * 10.0_f32.powf(6.0),       // duffing
            d: damping * 180.0,                // damping
            driver: Driver::new(driver_stiffness, driver_gain),
            noise: Noise::new(),
            seiding: Seiding::new(stiffness, noise),
        }
    }

    pub fn hit(&mut self, velocity: f32) {
        self.driver.hit(velocity);
        self.seiding.hit(velocity);
        self.v = 10.0_f32.powf(3.0) * velocity;
    }

    pub fn lift(&mut self, velocity: f32) {
        self.seiding.hit(velocity);
    }

    pub fn pressure(&mut self, pressure: f32) {
        self.driver.d = pressure * 180.0;
    }

    pub fn radius(&mut self, radius: f32) {
        self.seiding.radius = (1.0 - radius).clamp(0.2, 0.8);
    }

    pub fn process(&mut self) -> f32 {
        self.v -= (self.a * self.x + self.b * self.x.powf(3.0) + self.d * self.v) / RATE;
        self.v += self.driver.process() * self.noise.process() / RATE;
        self.x += self.v / RATE;
        (self.x + self.seiding.process()).tanh()
    }
}
