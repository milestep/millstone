// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

mod noise;
mod oscillator;
mod voice;

#[cfg(test)]
mod tests;

use crossbeam::channel::{Receiver, RecvError, Sender, TryRecvError};

use crate::synth::{Modulation};
use crate::opera::{MasterParameter, MasterPatch, MasterState, Parameter};
use crate::messages::{Action, OperaMessage};
use crate::perf;

pub fn spawn(
    action_tx: Sender<Action>,
    opera_rx: Receiver<OperaMessage>,
    in_tx: Sender<Vec<f32>>,
    out_rx: Receiver<Vec<f32>>,
) {
    std::thread::spawn(move || {
        let mut synth = Synth::new();
        synth.run(action_tx, opera_rx, in_tx, out_rx);
    });
}

struct Synth {
    pub patch: MasterPatch,
    pub state: MasterState,
    pub voices: Vec<voice::Voice>,
    voice_cycle: usize,
    axes: [f32; 24],
    // Rate reduction
    counter: usize,
    work_frame: [f32; 8],
    hold_frame: [f32; 8],
}

impl Synth {
    pub fn new() -> Self {
        let patch = MasterPatch::new();

        let state = patch.scene.master.clone();

        Self {
            patch,
            state,
            voices: vec![
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
            ],
            voice_cycle: 0,
            axes: [0.0; 24],
            // Rate reduction,
            counter: 0,
            work_frame: [0.0f32; 8],
            hold_frame: [0.0f32; 8],
        }
    }

    pub fn load_patch(&mut self, patch: MasterPatch) {
        self.state.set_from(&patch.scene.master);

        self.patch = patch;
    }

    pub fn set_param(&mut self, param: MasterParameter, value: f32) {
        match param {
            MasterParameter::Master(dest) => self.state.get_mut(dest).set(value),
        }
    }

    pub fn set_modulation(&mut self, modulation: Modulation<MasterParameter>) {
        self.reset_modulations();
        self.patch
            .set_modulation_params(modulation.idx, modulation.destination, modulation.params);
    }

    pub fn morph(&mut self, axis: usize, amount: f32) {
        self.axes[axis] = amount;
    }

    pub fn reset_modulations(&mut self) {
        for modulation in self.patch.modulations.iter() {
            match modulation.destination {
                MasterParameter::Master(dest) => self.state.get_mut(dest).reset(),
            }
        }
    }

    pub fn next_state(&mut self) {
        self.reset_modulations();

        for modulation in self.patch.modulations.iter() {
            let amount = modulation.get(&self.axes);

            if amount == 0.0 {
                continue;
            }

            match modulation.destination {
                MasterParameter::Master(dest) => self.state.get_mut(dest).modulate(amount),
            }
        }
    }

    fn run(
        &mut self,
        action_tx: Sender<Action>,
        opera_rx: Receiver<OperaMessage>,
        in_tx: Sender<Vec<f32>>,
        out_rx: Receiver<Vec<f32>>,
    ) {
        perf::set_realtime_thread_priority(89);

        // Workload measurement
        let mut workload_accumulator = 0;
        let mut workload_max = 0;
        let mut workload_counter = 0;

        let max_panic_frames = 512;
        let mut panic_frames = 0;
        let mut panic_triggered = false;

        loop {
            let mut time_instant = std::time::Instant::now();

            loop {
                match opera_rx.try_recv() {
                    Ok(OperaMessage::Panic) => {
                        panic_frames = max_panic_frames;
                        panic_triggered = true;

                        #[cfg(feature = "opera-debug")]
                        action_tx
                            .send(Action::Log("[opera] Panic!".to_string()))
                            .unwrap();
                    }
                    Ok(OperaMessage::LoadMasterPatch { patch, panic, mute }) => {
                        self.load_patch(patch);

                        if panic {
                            panic_frames = max_panic_frames;
                            panic_triggered = true;
                        } else if mute {
                            self.voices.iter_mut().for_each(|voice| voice.mute());
                        }
                        #[cfg(feature = "opera-debug")]
                        action_tx
                            .send(Action::Log(format!("[opera] LoadMasterPatch {}", panic)))
                            .unwrap();
                    }
                    Ok(OperaMessage::LoadPatch { voice_idx, patch }) => {
                        self.voices[voice_idx].load_patch(patch);
                    }
                    Ok(OperaMessage::UnloadPatch { voice_idx }) => {
                        self.voices[voice_idx].unload_patch();
                    }
                    Ok(OperaMessage::SetParam { param, value }) => {
                        match param {
                            Parameter::Master(dest) => {
                                self.set_param(dest, value);
                            }
                            Parameter::Voice(channel, dest) => {
                                for voice in self.voices.iter_mut() {
                                    if voice.has_channel(channel) {
                                        voice.set_param(dest, value);
                                    }
                                }
                            }
                        }

                        #[cfg(feature = "opera-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[opera] SetParam {:?} {}",
                                param, value
                            )))
                            .unwrap();
                    }
                    Ok(OperaMessage::SetModulation(modulation)) => {
                        match modulation.destination {
                            Parameter::Master(dest) => {
                                self.set_modulation(modulation.with_destination(dest));
                            }
                            Parameter::Voice(channel, dest) => {
                                for voice in self.voices.iter_mut() {
                                    if voice.has_channel(channel) {
                                        voice.set_modulation(modulation.with_destination(dest));
                                    }
                                }
                            }
                        }

                        #[cfg(feature = "opera-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[opera] SetModulation {:?}",
                                modulation
                            )))
                            .unwrap();
                    }
                    Ok(OperaMessage::SetConfig {
                        channel,
                        dest,
                        value,
                    }) => {
                        for voice in self.voices.iter_mut() {
                            if voice.has_channel(channel) {
                                voice.set_config(dest, value);
                            }
                        }

                        #[cfg(feature = "opera-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[opera] SetConfig {} {:?} {}",
                                channel, dest, value
                            )))
                            .unwrap();
                    }
                    Ok(OperaMessage::SetMasterConfig { dest, value }) => {
                        self.patch.set_config(dest, value);

                        #[cfg(feature = "opera-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[opera] SetMasterConfig {:?} {}",
                                dest, value
                            )))
                            .unwrap();
                    }
                    Ok(OperaMessage::Morph {
                        channel,
                        id,
                        axis,
                        amount,
                    }) => {
                        if axis < 7 {
                            if channel == 0 && id.group_id == 0 && id.voice_id == 0 {
                                self.morph(axis, amount);
                            }
                        } else {
                            for voice in self.voices.iter_mut() {
                                if voice.has_id(channel, id) {
                                    voice.morph(axis, amount);
                                }
                            }
                        }

                        #[cfg(feature = "opera-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[opera] Morph {} {:?} {} {}",
                                channel, id, axis, amount
                            )))
                            .unwrap();

                        #[cfg(feature = "opera-debug")]
                        action_tx
                            .send(Action::Log(format!("[opera] Morph {:?}", &self.axes)))
                            .unwrap();
                    }
                    Ok(OperaMessage::NoteOn {
                        channel,
                        id,
                        frequency,
                        velocity,
                        mute,
                        latch,
                    }) => {
                        self.morph(6, ((frequency / 27.5).log2() / 8.0).min(1.0).max(0.0));

                        let mut best_idx = self.voice_cycle;

                        while self.voices[best_idx].patch.channel != channel {
                            best_idx = (best_idx + 1) % self.voices.len();

                            if best_idx == self.voice_cycle {
                                break;
                            }
                        }

                        let mut best_is_active = self.voices[best_idx].active;
                        let mut best_is_held = self.voices[best_idx].held;
                        let mut best_score = self.voices[best_idx].frequency_distance(frequency);

                        for (idx, voice) in self.voices.iter_mut().enumerate() {
                            if voice.patch.channel != channel {
                                continue;
                            }

                            if (mute || latch) {
                                voice.mute();
                            }

                            if best_score == 1.0 {
                                continue;
                            }

                            let score = voice.frequency_distance(frequency);

                            if score > 1.0 && !best_is_active {
                                continue;
                            }

                            if !voice.active
                                || (!voice.held && best_is_held)
                                || (!voice.held && score < best_score)
                            {
                                best_idx = idx;
                                best_is_active = voice.active;
                                best_is_held = voice.held;
                                best_score = score;
                            }
                        }

                        // If the voice we found listens to the right channel, activate!
                        if !mute && (channel == 0 || self.voices[best_idx].patch.channel == channel)
                        {
                            // Because we decay each voice before triggering, this will set
                            // the first strummed note's strum count to 0 when it is
                            // triggered.
                            for voice in self.voices.iter_mut() {
                                if voice.patch.channel == channel {
                                    voice.strum();
                                }
                            }

                            self.voices[best_idx].trigger(id, frequency, velocity);
                            self.voice_cycle = (best_idx + 1) % self.voices.len();

                            #[cfg(feature = "opera-debug")]
                            action_tx
                                .send(Action::Log(format!(
                                    "[opera] NoteOn {} {:?} {} {} {}",
                                    channel, id, frequency, velocity, best_idx
                                )))
                                .unwrap();
                        }
                    }
                    Ok(OperaMessage::NoteOff {
                        channel,
                        id,
                        latch,
                        velocity,
                    }) => {
                        for voice in self.voices.iter_mut() {
                            if voice.has_id(channel, id) {
                                voice.release(id, velocity, latch);
                            }
                        }

                        #[cfg(feature = "opera-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[opera] NoteOff {} {:?} {} {}",
                                channel, id, latch, velocity
                            )))
                            .unwrap();
                    }
                    Err(TryRecvError::Empty) => break,
                    Err(TryRecvError::Disconnected) => panic!("Channel disconnected"),
                }
            }

            if panic_triggered {
                for _ in 0..panic_frames {
                    in_tx.send(vec![0f32; 8]).unwrap();
                }
                panic_triggered = false;
            }

            let mut elapsed = time_instant.elapsed().as_nanos() as i128;

            // Receive a frame from the mixer.
            let frame = match out_rx.recv() {
                Ok(value) => value,
                Err(RecvError) => panic!("channel disconnected"),
            };

            time_instant = std::time::Instant::now();

            // Process and send the frame back to the mixer.
            if panic_frames == 0 {
                in_tx.send(self.process(frame)).unwrap();
            } else {
                let subdiv = 16;

                if (max_panic_frames - panic_frames) % subdiv == 0 {
                    let panic_idx = (max_panic_frames - panic_frames) / subdiv;

                    match panic_idx {
                        0..=15 => self.voices[panic_idx].panic(),
                        24 => self.work_frame.fill(0.0),
                        _ => (),
                    }
                }

                panic_frames -= 1;
            }

            elapsed += time_instant.elapsed().as_nanos() as i128;
            workload_accumulator += elapsed;

            if elapsed > 22675 {
                workload_max += 1;
            }

            workload_counter += 1;

            if workload_counter >= 44100 {
                let percentage = workload_accumulator / 10_000_000;
                let max = workload_max;

                action_tx
                    .send(Action::Workload {
                        identifier: 1,
                        percentage,
                        max,
                    })
                    .unwrap();

                workload_accumulator = 0;
                workload_max = 0;
                workload_counter = 0;
            }
        }
    }

    fn process(&mut self, mut frame: Vec<f32>) -> Vec<f32> {
        let rate_divider = self.patch.rate_divider + 1;

        if self.counter == 0 {
            for (hold_sample, work_sample) in
                self.hold_frame.iter_mut().zip(self.work_frame.iter_mut())
            {
                *hold_sample = *work_sample;
                *work_sample = 0.0;
            }

            let dt = rate_divider as f32 * 1.0 / 44100.0;

            self.next_frame(&frame, dt);
        }

        let a = (self.counter + 1) as f32 / rate_divider as f32;

        for (sample, (work_sample, hold_sample)) in frame
            .iter_mut()
            .zip(self.work_frame.iter().zip(self.hold_frame.iter()))
        {
            *sample = (1.0 - a) * *hold_sample + a * *work_sample;
        }

        self.counter = (self.counter + 1) % rate_divider;

        frame
    }

    fn next_frame(&mut self, frame: &[f32], dt: f32) {
        self.next_state();

        let mut active_channels = [false; 8];

        self.axes[14] = 0.5;

        for voice in self.voices.iter_mut() {
            voice.strum_decay();

            active_channels[voice.patch.output_channel_left] = true;
            active_channels[voice.patch.output_channel_right] = true;

            if !voice.active {
                continue;
            }

            let (left, right) = voice.process(&self.axes, dt);

            self.axes[14] += (0.25 * left + 0.25 * right).min(1.0).max(0.0);

            self.work_frame[voice.patch.output_channel_left] += voice.state.gain.x *  left;
            self.work_frame[voice.patch.output_channel_right] += voice.state.gain.x *  right;
        }

        self.axes[15] = 0.5;

        for sample in self.work_frame.iter_mut() {
            *sample = if self.state.gain.x > self.state.gain.low {
                (*sample * self.state.gain.x).min(1.0).max(-1.0)
            } else {
                0.0
            };

            self.axes[15] += 0.25 * *sample;
        }

        self.axes[15] = self.axes[15].min(1.0).max(0.0);
    }
}
