// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

pub trait Frame {
    fn zero() -> Self;
    fn absmax(&self) -> f32;
    fn get_left(&self) -> f32;
    fn get_right(&self) -> f32;
}

//////////////////////////////////////////////////
// Mono
//////////////////////////////////////////////////

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct MonoFrame {
    pub sample: f32,
}

impl MonoFrame {
    pub fn from(sample: f32) -> Self {
        Self { sample }
    }

    pub fn from_le_bytes(bytes: [u8; 4]) -> Self {
        Self::from(f32::from_le_bytes(bytes))
    }

    #[allow(clippy::wrong_self_convention)]
    pub fn to_le_bytes(&self) -> [u8; 4] {
        self.sample.to_le_bytes()
    }
}

impl Frame for MonoFrame {
    fn zero() -> Self {
        Self { sample: 0.0 }
    }

    fn absmax(&self) -> f32 {
        self.sample.abs()
    }

    fn get_left(&self) -> f32 {
        self.sample
    }

    fn get_right(&self) -> f32 {
        self.sample
    }
}

impl Add<MonoFrame> for MonoFrame {
    type Output = MonoFrame;

    fn add(self, other: Self) -> Self {
        Self {
            sample: self.sample + other.sample,
        }
    }
}

impl AddAssign<MonoFrame> for MonoFrame {
    fn add_assign(&mut self, other: Self) {
        self.sample += other.sample;
    }
}

impl Div<f32> for MonoFrame {
    type Output = MonoFrame;

    fn div(self, other: f32) -> Self {
        Self {
            sample: self.sample / other,
        }
    }
}

impl DivAssign<f32> for MonoFrame {
    fn div_assign(&mut self, other: f32) {
        self.sample /= other;
    }
}

impl Mul<f32> for MonoFrame {
    type Output = MonoFrame;

    fn mul(self, other: f32) -> Self {
        Self {
            sample: self.sample * other,
        }
    }
}

impl MulAssign<f32> for MonoFrame {
    fn mul_assign(&mut self, other: f32) {
        self.sample *= other;
    }
}

impl Neg for MonoFrame {
    type Output = MonoFrame;

    fn neg(self) -> MonoFrame {
        Self {
            sample: -self.sample,
        }
    }
}

impl Sub<MonoFrame> for MonoFrame {
    type Output = MonoFrame;

    fn sub(self, other: Self) -> Self {
        Self {
            sample: self.sample - other.sample,
        }
    }
}

impl SubAssign<MonoFrame> for MonoFrame {
    fn sub_assign(&mut self, other: Self) {
        self.sample -= other.sample;
    }
}

//////////////////////////////////////////////////
// Stereo
//////////////////////////////////////////////////

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct StereoFrame {
    pub left: f32,
    pub right: f32,
}

impl StereoFrame {
    pub fn from(left: f32, right: f32) -> Self {
        Self { left, right }
    }

    pub fn from_le_bytes(bytes: [u8; 8]) -> Self {
        let mut left_bytes = [0u8; 4];
        let mut right_bytes = [0u8; 4];
        for (a, b) in left_bytes.iter_mut().zip(bytes[0..4].iter()) {
            *a = *b;
        }
        for (a, b) in right_bytes.iter_mut().zip(bytes[4..8].iter()) {
            *a = *b;
        }
        Self::from(
            f32::from_le_bytes(left_bytes),
            f32::from_le_bytes(right_bytes),
        )
    }

    #[allow(clippy::wrong_self_convention)]
    pub fn to_le_bytes(&self) -> [u8; 8] {
        let mut res = [0u8; 8];
        let left_bytes = self.left.to_le_bytes();
        let right_bytes = self.right.to_le_bytes();
        res[0] = left_bytes[0];
        res[1] = left_bytes[1];
        res[2] = left_bytes[2];
        res[3] = left_bytes[3];
        res[4] = right_bytes[0];
        res[5] = right_bytes[1];
        res[6] = right_bytes[2];
        res[7] = right_bytes[3];
        res
    }

    pub fn rl_to_ms(&self) -> StereoFrame {
        StereoFrame {
            left: (self.get_left() + self.get_right()) / 2.0_f32.sqrt(),
            right: (self.get_left() - self.get_right()) / 2.0_f32.sqrt(),
        }
    }

    pub fn ms_to_rl(&self) -> StereoFrame {
        StereoFrame {
            left: (self.get_left() + self.get_right()) / 2.0_f32.sqrt(),
            right: (self.get_left() - self.get_right()) / 2.0_f32.sqrt(),
        }
    }
}

impl Frame for StereoFrame {
    fn zero() -> Self {
        Self {
            left: 0.0,
            right: 0.0,
        }
    }

    fn absmax(&self) -> f32 {
        self.left.abs().max(self.right.abs())
    }

    fn get_left(&self) -> f32 {
        self.left
    }

    fn get_right(&self) -> f32 {
        self.right
    }
}

impl Add<StereoFrame> for StereoFrame {
    type Output = StereoFrame;

    fn add(self, other: Self) -> Self {
        Self {
            left: self.left + other.left,
            right: self.right + other.right,
        }
    }
}

impl AddAssign<StereoFrame> for StereoFrame {
    fn add_assign(&mut self, other: Self) {
        self.left += other.left;
        self.right += other.right;
    }
}

impl Add<MonoFrame> for StereoFrame {
    type Output = StereoFrame;

    fn add(self, other: MonoFrame) -> Self {
        Self {
            left: self.left + 0.5 * other.sample,
            right: self.right + 0.5 * other.sample,
        }
    }
}

impl AddAssign<MonoFrame> for StereoFrame {
    fn add_assign(&mut self, other: MonoFrame) {
        self.left += 0.5 * other.sample;
        self.right += 0.5 * other.sample;
    }
}

impl Div<f32> for StereoFrame {
    type Output = StereoFrame;

    fn div(self, other: f32) -> Self {
        Self {
            left: self.left / other,
            right: self.right / other,
        }
    }
}

impl Div<StereoFrame> for f32 {
    type Output = StereoFrame;

    fn div(self, other: StereoFrame) -> StereoFrame {
        StereoFrame {
            left: self / other.left,
            right: self / other.right,
        }
    }
}

impl DivAssign<f32> for StereoFrame {
    fn div_assign(&mut self, other: f32) {
        self.left /= other;
        self.right /= other;
    }
}

impl Mul<f32> for StereoFrame {
    type Output = StereoFrame;

    fn mul(self, other: f32) -> Self {
        Self {
            left: self.left * other,
            right: self.right * other,
        }
    }
}

impl Mul<StereoFrame> for f32 {
    type Output = StereoFrame;

    fn mul(self, other: StereoFrame) -> StereoFrame {
        StereoFrame {
            left: self * other.left,
            right: self * other.right,
        }
    }
}

impl MulAssign<f32> for StereoFrame {
    fn mul_assign(&mut self, other: f32) {
        self.left *= other;
        self.right *= other;
    }
}

impl Neg for StereoFrame {
    type Output = StereoFrame;

    fn neg(self) -> StereoFrame {
        Self {
            left: -self.left,
            right: -self.right,
        }
    }
}

impl Sub<StereoFrame> for StereoFrame {
    type Output = StereoFrame;

    fn sub(self, other: Self) -> Self {
        Self {
            left: self.left - other.left,
            right: self.right - other.right,
        }
    }
}

impl SubAssign<StereoFrame> for StereoFrame {
    fn sub_assign(&mut self, other: Self) {
        self.left -= other.left;
        self.right -= other.right;
    }
}

impl Sub<MonoFrame> for StereoFrame {
    type Output = StereoFrame;

    fn sub(self, other: MonoFrame) -> Self {
        Self {
            left: self.left - 0.5 * other.sample,
            right: self.right - 0.5 * other.sample,
        }
    }
}

impl SubAssign<MonoFrame> for StereoFrame {
    fn sub_assign(&mut self, other: MonoFrame) {
        self.left -= 0.5 * other.sample;
        self.right -= 0.5 * other.sample;
    }
}
