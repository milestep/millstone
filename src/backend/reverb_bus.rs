// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use super::effects;
use super::frames::{Frame, StereoFrame};

pub struct ReverbBus {
    frame_in: StereoFrame,
    frame_out: StereoFrame,
    reverb: effects::Reverb,
}

impl ReverbBus {
    pub fn new() -> Self {
        Self {
            frame_in: Frame::zero(),
            frame_out: Frame::zero(),
            reverb: effects::Reverb::new(0.9, 0.64),
        }
    }

    pub fn send(&mut self, frame: StereoFrame, multiplier: f32) {
        self.frame_in += multiplier * frame;
    }

    pub fn process(&mut self) {
        self.frame_out = self.reverb.process(self.frame_in, 1.0);
        self.frame_in = Frame::zero();
    }

    pub fn recv(&mut self) -> StereoFrame {
        self.frame_out
    }
}
