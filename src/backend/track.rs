// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crossbeam::channel::Sender;

use crate::messages::{Action, Destination};
use crate::paths;

use super::frames::{Frame, StereoFrame};
use super::io::IOMessage;
use super::tape::Tape;

//////////////////////////////////////////////////
// Track
//////////////////////////////////////////////////

pub struct Track {
    pub idx: usize,
    pub physical_left: usize,
    pub physical_right: usize,
    pub monitor: bool,
    pub mute: bool,
    pub solo: bool,
    pub record_arm: bool,
    pub fader: f32,
    pub center: f32,
    pub width: f32,
    pub reverb_send: f32,
    mul_out: f32,
    peak_left: f32,
    peak_right: f32,
    action_tx: Sender<Action>,
    channel_request_tx: Sender<IOMessage<StereoFrame>>,
    tape: Option<Tape>,
}

impl Track {
    pub fn new(
        action_tx: Sender<Action>,
        channel_request_tx: Sender<IOMessage<StereoFrame>>,
        idx: usize,
        left: usize,
        right: usize,
    ) -> Self {
        Self {
            idx,
            physical_left: left,
            physical_right: right,
            monitor: false,
            record_arm: false,
            mute: false,
            solo: false,
            fader: 0.0,
            center: 0.0,
            width: 1.0,
            reverb_send: 0.0,
            mul_out: 0.0,
            peak_left: 0.0,
            peak_right: 0.0,
            action_tx,
            channel_request_tx,
            tape: None,
        }
    }

    pub fn load(&mut self, filename: String) {
        let filename = paths::project_path(&filename);
        let tape = Tape::new(self.idx, filename, self.channel_request_tx.clone());
        self.tape = Some(tape);
    }

    pub fn send(&mut self, frame: StereoFrame) {
        self.peak_left = self.peak_left.max(frame.left);
        self.peak_right = self.peak_right.max(frame.right);

        if let Some(tape) = self.tape.as_mut() {
            tape.send(frame);
        }
    }

    /// Process the current sample at `t`.  If `play` is true, the sample will be mixed with
    /// the audio recorded on the tape.  If both `play` and `record` are true, audio will be
    /// overdubbed on the tape.  If `play` is false, `record` will be ignored.
    pub fn process(&mut self, t: usize, play: bool, record: bool) {
        let read = play && !self.mute;
        let write = play && record && self.record_arm;
        let lookahead = record && self.record_arm;
        let monitor = self.monitor || (self.record_arm && !self.mute && (!play || record));
        let flush = !play;

        self.mul_out = 10.0f32.powf(self.fader / 20.0);

        if let Some(tape) = self.tape.as_mut() {
            tape.process(t, read, write, lookahead, monitor, flush);
        }
    }

    pub fn recv(&self) -> StereoFrame {
        let frame = match &self.tape {
            Some(tape) => self.mul_out * tape.recv(),
            None => StereoFrame::zero(),
        };
        let left = frame.get_left();
        let right = frame.get_right();
        let left_pan = (self.center - self.width).max(-1.0).min(1.0);
        let right_pan = (self.center + self.width).max(-1.0).min(1.0);
        StereoFrame {
            left: (0.5 - 0.5 * left_pan) * left + (0.5 - 0.5 * right_pan) * right,
            right: (0.5 + 0.5 * left_pan) * left + (0.5 + 0.5 * right_pan) * right,
        }
    }

    pub fn update(&mut self, dt: usize) {
        let peak_left_db = 20.0 * self.peak_left.log10();
        let peak_right_db = 20.0 * self.peak_right.log10();

        self.action_tx
            .send(Action::Peak(
                Destination::Track(self.idx),
                dt,
                peak_left_db,
                peak_right_db,
            ))
            .unwrap();

        match self.tape.as_mut() {
            Some(tape) => self
                .action_tx
                .send(Action::LoadStatus(
                    Destination::Track(self.idx),
                    Some(tape.preload_status()),
                ))
                .unwrap(),
            None => self
                .action_tx
                .send(Action::LoadStatus(Destination::Track(self.idx), None))
                .unwrap(),
        }

        self.peak_left = 0.0;
        self.peak_right = 0.0;
    }

    pub fn set_loop_points(&mut self, start: usize, stop: usize) {
        if let Some(tape) = self.tape.as_mut() {
            tape.set_loop_points(start, stop);
        }
    }
}
