// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use super::analyzer;
use super::filters;
use super::frames::{Frame, StereoFrame};
use core::f32::consts::PI;
use rand::Rng;

const SAMPLE_TIME: f32 = 1.0 / 44100.0; // seconds/sample

pub struct WhiteNoise {
    gain: f32,
    rng: rand::rngs::ThreadRng,
}

impl WhiteNoise {
    pub fn new(gain: f32) -> Self {
        Self {
            gain,
            rng: rand::thread_rng(),
        }
    }
    pub fn process(&mut self, frame: StereoFrame, mix: f32) -> StereoFrame {
        if mix == 0.0 {
            return frame;
        }

        let noise = StereoFrame {
            left: self.gain * self.rng.gen::<f32>(),
            right: self.gain * self.rng.gen::<f32>(),
        };

        (1.0 - mix) * frame + mix * noise
    }
}

pub struct Compressor {
    pub gain: f32,  // dB
    signal: f32,    // dB
    velocity: f32,  // dB/seconds
    attack: f32,    // seconds
    release: f32,   // seconds
    threshold: f32, // dB
    knee: f32,      // dB
    ratio: f32,
    rms: analyzer::Rms,
}

impl Compressor {
    pub fn new(attack: f32, release: f32, threshold: f32, knee: f32, ratio: f32) -> Self {
        Self {
            gain: 0.0,
            signal: -f32::INFINITY,
            velocity: 0.0,
            attack,
            release,
            threshold,
            knee,
            ratio,
            rms: analyzer::Rms::new(0.05),
        }
    }

    pub fn process(&mut self, frame: StereoFrame, amount: f32, inertia: f32) -> StereoFrame {
        let rms = self.rms.process(frame);

        if amount <= 0.0 {
            self.gain = 0.0;
            self.signal = -f32::INFINITY;
            self.velocity = 0.0;
            return frame;
        }

        let threshold = self.threshold * amount;
        let ratio = 1.0 + ((self.ratio - 1.0) * amount);
        let knee = self.knee;

        let attack = self.attack;
        let release = self.attack + self.release * inertia;

        self.signal = self.signal.max(20.0 * rms.absmax().log10());

        // Compression stage

        let delta = if self.signal > threshold - knee / 2.0 {
            if self.signal > threshold + knee / 2.0 {
                (self.signal - threshold) * (1.0 - 1.0 / ratio)
            } else {
                let x = (self.signal - (threshold - knee / 2.0)) / (threshold + knee / 2.0);
                (1.0 - x) * (self.signal - threshold) * (1.0 - 1.0 / ratio)
            }
        } else {
            0.0
        };

        self.velocity = self.velocity.max(delta * SAMPLE_TIME);

        if self.gain > -delta {
            self.gain -= self.velocity / attack;
            self.gain = self.gain.max(-delta);
        } else if self.gain < -delta {
            self.gain += self.velocity / release;
            self.gain = self.gain.min(-delta);
        } else {
            self.velocity = 0.0;
        }

        self.signal += self.gain;

        let gain = self.gain - threshold * (1.0 - 1.0 / ratio);

        frame * 10.0f32.powf(gain / 20.0)
    }
}

pub enum SaturationAlgorithm {
    TanhMod,
}

pub struct Saturator {
    algorithm: SaturationAlgorithm,
}
impl Saturator {
    pub fn new(algorithm: SaturationAlgorithm) -> Self {
        Self { algorithm }
    }
    pub fn process(&mut self, frame: StereoFrame, amount: f32) -> StereoFrame {
        if amount == 0.0 {
            return frame;
        }

        let sat = match self.algorithm {
            SaturationAlgorithm::TanhMod => {
                let a = 10.0f32.powf(6.0 / 20.0);
                let n: f32 = 2.0;
                let gain = (n + 1.0).powf(1.0 / n) / (1.0 - (n + 1.0).powf(-1.0));

                let left = (frame.get_left() + a).rem_euclid(2.0 * a) - a;
                let right = (frame.get_right() + a).rem_euclid(2.0 * a) - a;

                StereoFrame {
                    left: (gain * left * (1.0 - (left / a).abs().powf(n))).tanh() / a.tanh(),
                    right: (gain * right * (1.0 - (right / a).abs().powf(n))).tanh() / a.tanh(),
                }
            }
        };

        frame * (1.0 - amount) + sat * amount
    }
}

// Audio EQ Cookbook:
// https://www.w3.org/TR/audio-eq-cookbook/

pub struct LowShelf {
    center_freq: f32, // Hz
    gain: f32,        // dB
    filter: filters::Biquad,
}

impl LowShelf {
    pub fn new(center_freq: f32, gain: f32) -> Self {
        Self {
            center_freq,
            gain,
            filter: filters::Biquad::new(),
        }
    }
    pub fn process(&mut self, frame: StereoFrame, param: f32) -> StereoFrame {
        let freq = self.center_freq;
        let gain = self.gain * 2.0 * ((1.0 - param) - 0.5);

        let a = 10.0f32.powf(gain / 40.0);
        let omega0 = 2.0 * PI * freq * SAMPLE_TIME;
        let sin0 = omega0.sin();
        let cos0 = omega0.cos();

        let k = 2.0f32.powf(1.5) * sin0 * a.sqrt();
        let a0 = (a + 1.0) + (a - 1.0) * cos0 + k;
        let b0 = a * ((a + 1.0) - (a - 1.0) * cos0 + k);

        let gain = b0 / a0;

        let poles = [
            -2.0 / a0 * ((a - 1.0) + (a + 1.0) * cos0),
            ((a + 1.0) + (a - 1.0) * cos0 - k) / a0,
        ];

        let zeros = [
            2.0 * a * ((a - 1.0) - (a + 1.0) * cos0) / a0,
            a * ((a + 1.0) - (a - 1.0) * cos0 - k) / a0,
        ];

        self.filter.process(frame, gain, zeros, poles)
    }
}

pub struct HighShelf {
    center_freq: f32, // Hz
    gain: f32,        // dB
    filter: filters::Biquad,
}

impl HighShelf {
    pub fn new(center_freq: f32, gain: f32) -> Self {
        Self {
            center_freq,
            gain,
            filter: filters::Biquad::new(),
        }
    }
    pub fn process(&mut self, frame: StereoFrame, param: f32) -> StereoFrame {
        let freq = self.center_freq;
        let gain = self.gain * 2.0 * ((1.0 - param) - 0.5);

        let a = 10.0f32.powf(gain / 40.0);
        let omega0 = 2.0 * PI * freq * SAMPLE_TIME;
        let sin0 = omega0.sin();
        let cos0 = omega0.cos();

        let k = 2.0f32.powf(1.5) * sin0 * a.sqrt();
        let a0 = (a + 1.0) - (a - 1.0) * cos0 + k;
        let b0 = a * ((a + 1.0) + (a - 1.0) * cos0 + k);

        let gain = b0 / a0;

        let poles = [
            2.0 / a0 * ((a - 1.0) - (a + 1.0) * cos0),
            ((a + 1.0) - (a - 1.0) * cos0 - k) / a0,
        ];

        let zeros = [
            -2.0 * a * ((a - 1.0) + (a + 1.0) * cos0) / a0,
            a * ((a + 1.0) + (a - 1.0) * cos0 - k) / a0,
        ];

        self.filter.process(frame, gain, zeros, poles)
    }
}

pub struct PeakingEQ {
    center_freq: f32, // Hz
    q: f32,           // freq ratio "Q" = fC / BW = √fHfL / fH – fL
    gain: f32,        // dB
    filter: filters::Biquad,
}

impl PeakingEQ {
    pub fn new(center_freq: f32, q: f32, gain: f32) -> Self {
        Self {
            center_freq,
            q,
            gain,
            filter: filters::Biquad::new(),
        }
    }
    pub fn process(
        &mut self,
        frame: StereoFrame,
        param: f32,
        shift_freq_amount: f32,
    ) -> StereoFrame {
        let freq = self.center_freq + 700.0 * 2.0 * (shift_freq_amount - 0.5);
        let gain = self.gain * 2.0 * ((1.0 - param) - 0.5);

        let a = 10.0f32.powf(gain / 40.0);
        let omega0 = 2.0 * PI * freq * SAMPLE_TIME;
        let sin0 = omega0.sin();
        let cos0 = omega0.cos();
        let alpha = sin0 / (2.0 * self.q);

        let b0 = 1.0 + alpha * a;
        let b1 = -2.0 * cos0;
        let b2 = 1.0 - alpha * a;

        let a0 = 1.0 + alpha / a;
        let a1 = -2.0 * cos0;
        let a2 = 1.0 - alpha / a;

        let gain = b0 / a0;

        let poles = [a1 / a0, a2 / a0];
        let zeros = [b1 / a0, b2 / a0];

        self.filter.process(frame, gain, zeros, poles)
    }
}

// Inspiration from "Freeverb":
// https://ccrma.stanford.edu/~jos/pasp/Freeverb.html

pub struct Reverb {
    fbcf1: filters::FeedBackComb,
    fbcf2: filters::FeedBackComb,
    fbcf3: filters::FeedBackComb,
    fbcf4: filters::FeedBackComb,
    fbcf5: filters::FeedBackComb,
    fbcf6: filters::FeedBackComb,
    fbcf7: filters::FeedBackComb,
    fbcf8: filters::FeedBackComb,
    ap1: filters::AllPass,
    ap2: filters::AllPass,
    ap3: filters::AllPass,
    ap4: filters::AllPass,
    lp: filters::LowPass,  // Hz roll-off
    hp: filters::HighPass, // Hz roll-on
}
impl Reverb {
    pub fn new(cb_gain: f32, ap_gain: f32) -> Self {
        Self {
            fbcf1: filters::FeedBackComb::new(cb_gain, 1557),
            fbcf2: filters::FeedBackComb::new(cb_gain, 1617),
            fbcf3: filters::FeedBackComb::new(cb_gain, 1491),
            fbcf4: filters::FeedBackComb::new(cb_gain, 1422),
            fbcf5: filters::FeedBackComb::new(cb_gain, 1277),
            fbcf6: filters::FeedBackComb::new(cb_gain, 1356),
            fbcf7: filters::FeedBackComb::new(cb_gain, 1188),
            fbcf8: filters::FeedBackComb::new(cb_gain, 1116),
            ap1: filters::AllPass::new(ap_gain, 225),
            ap2: filters::AllPass::new(ap_gain, 556),
            ap3: filters::AllPass::new(ap_gain, 441),
            ap4: filters::AllPass::new(ap_gain, 341),
            lp: filters::LowPass::new(3000.0),
            hp: filters::HighPass::new(100.0),
        }
    }
    pub fn process(&mut self, frame: StereoFrame, mix: f32) -> StereoFrame {
        if mix == 0.0 {
            return frame;
        }

        // Convert to mid-side frame
        let input = frame.rl_to_ms();

        // Parallel comb filter scaling
        let delay_scale = 0.9;
        self.fbcf1.set_scale(delay_scale * mix);
        self.fbcf2.set_scale(delay_scale * mix);
        self.fbcf3.set_scale(delay_scale * mix);
        self.fbcf4.set_scale(delay_scale * mix);
        self.fbcf5.set_scale(delay_scale * mix);
        self.fbcf6.set_scale(delay_scale * mix);
        self.fbcf7.set_scale(delay_scale * mix);
        self.fbcf8.set_scale(delay_scale * mix);

        // Parallel comb filters stage
        let c1 = self.fbcf1.process(input);
        let c2 = self.fbcf2.process(input);
        let c3 = self.fbcf3.process(input);
        let c4 = self.fbcf4.process(input);
        let c5 = self.fbcf5.process(input);
        let c6 = self.fbcf6.process(input);
        let c7 = self.fbcf7.process(input);
        let c8 = self.fbcf8.process(input);

        // Sequencial all-pass filter stage
        let a1 = self.ap1.process(c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8);
        let a2 = self.ap2.process(a1);
        let a3 = self.ap3.process(a2);
        let a4 = self.ap4.process(a3);

        // Convert back to left / right frame
        let lp = self.lp.process(a4.ms_to_rl());
        let out = self.hp.process(lp);

        // Mix with dry signal with gain reduction
        (1.0 - mix) * frame + (mix * out * 0.13)
    }
}

#[cfg(test)]
mod tests {
    extern crate test;

    use super::*;
    use test::black_box;
    use test::Bencher;

    #[bench]
    fn bench_whitenoise(b: &mut Bencher) {
        let mut effect = WhiteNoise::new(black_box(0.5));
        let signal = StereoFrame {
            left: black_box(0.1234),
            right: black_box(0.1234),
        };
        b.iter(|| effect.process(black_box(signal), black_box(0.5)));
    }

    #[bench]
    fn bench_compressor(b: &mut Bencher) {
        let mut effect = Compressor::new(
            black_box(0.5),
            black_box(0.5),
            black_box(0.2),
            black_box(0.4),
            black_box(5.0),
        );
        let signal = StereoFrame {
            left: black_box(0.1234),
            right: black_box(0.1234),
        };
        b.iter(|| effect.process(black_box(signal), black_box(0.5), black_box(0.3)));
    }

    #[bench]
    fn bench_saturator(b: &mut Bencher) {
        let mut effect = Saturator::new(SaturationAlgorithm::TanhMod);
        let signal = StereoFrame {
            left: black_box(0.1234),
            right: black_box(0.1234),
        };
        b.iter(|| effect.process(black_box(signal), black_box(0.5)));
    }

    #[bench]
    fn bench_lowshelf(b: &mut Bencher) {
        let mut effect = LowShelf::new(1000.0, 4.0);
        let signal = StereoFrame {
            left: black_box(0.1234),
            right: black_box(0.1234),
        };
        b.iter(|| effect.process(black_box(signal), black_box(0.5)));
    }

    #[bench]
    fn bench_highshelf(b: &mut Bencher) {
        let mut effect = HighShelf::new(1000.0, 4.0);
        let signal = StereoFrame {
            left: black_box(0.1234),
            right: black_box(0.1234),
        };
        b.iter(|| effect.process(black_box(signal), black_box(0.5)));
    }

    #[bench]
    fn bench_peakingeq(b: &mut Bencher) {
        let mut effect = PeakingEQ::new(black_box(1000.0), black_box(4.0), black_box(30.0));
        let signal = StereoFrame {
            left: black_box(0.1234),
            right: black_box(0.1234),
        };
        b.iter(|| effect.process(black_box(signal), black_box(0.5), black_box(0.3)));
    }

    #[bench]
    fn bench_reverb(b: &mut Bencher) {
        let mut effect = Reverb::new(black_box(0.5), black_box(0.5));
        let signal = StereoFrame {
            left: black_box(0.1234),
            right: black_box(0.1234),
        };
        b.iter(|| effect.process(black_box(signal), black_box(0.5)));
    }
}
