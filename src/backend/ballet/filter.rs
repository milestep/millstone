// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use core::f32::consts::PI;

use crate::ballet::FilterState;

pub struct Filter {
    pub state: FilterState,
    x: f32,
}

impl Filter {
    pub fn new(state: FilterState) -> Self {
        Self { state, x: 1.0 }
    }

    pub fn trigger(&mut self) {
        self.x = 1.0;
    }

    pub fn panic(&mut self) {
        self.x = 1.0;
    }

    pub fn prepare(&mut self, dt: f32, base_frequency: f32) -> ([f32; 3], [f32; 3], f32) {
        let freq =
            (1.0 + self.state.track.x * (base_frequency / 440.0 - 1.0)) * self.state.frequency.x;

        let omega0 = (PI * 2.0 * freq * dt).min(0.91 * PI);
        let sin0 = omega0.sin();
        let cos0 = omega0.cos();

        let alpha = (1.0 - self.state.resonance.x).powf(2.0) * sin0;
        let lp = self.state.lowpass.x * (1.0 - cos0);
        let bp = self.state.bandpass.x * sin0;
        let hp = self.state.highpass.x * (1.0 + cos0);
        let k = lp + hp;

        (
            [1.0 + alpha, -2.0 * cos0, 1.0 - alpha],
            [(k - bp) / 2.0, lp - hp, (k + bp) / 2.0],
            freq,
        )
    }

    pub fn process(
        &mut self,
        x: &mut [f32; 2],
        y: &mut [f32; 2],
        a: &[f32; 3],
        b: &[f32; 3],
        frame: f32,
        dt: f32,
    ) -> f32 {
        let out = (b[0] * frame + b[1] * x[0] - a[1] * y[0] + b[2] * x[1] - a[2] * y[1]) / a[0];

        *x = [frame, x[0]];
        *y = [out.max(-(2.0f32.powf(16.0))).min(2.0f32.powf(16.0)), y[0]];

        self.x = 1.0 + (1.0 - (dt / self.state.decay.x).min(1.0)) * (self.x - 1.0);
        self.x = self.x.max(out.abs() / self.state.headroom.x);

        out / self.x
    }
}
