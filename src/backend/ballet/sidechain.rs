// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::ballet::SidechainState;

pub struct Sidechain {
    pub state: SidechainState,
    t: f32,
    x: f32,
    y0: f32,
}

impl Sidechain {
    pub fn new(state: SidechainState) -> Sidechain {
        Self {
            state,
            t: 0.0,
            x: 0.0,
            y0: 0.0,
        }
    }

    pub fn trigger(&mut self, velocity: f32) {
        self.t = 0.0;
        self.y0 = self.state.amount.x * (1.0 + self.state.sense.x * (velocity - 1.0));
    }

    pub fn process(&mut self, dt: f32) -> f32 {
        self.t += dt;

        let b = 8.0;

        let t1 = self.state.attack.x;
        let t2 = t1 + self.state.hold.x;
        let t3 = t2 + self.state.decay.x;

        if self.t <= t2 {
            let a = 1.0 - (b * dt / self.state.attack.x).min(1.0);
            self.x = a * self.x + (1.0 - a);
        } else if self.t <= t3 {
            self.x *= 1.0 - (b * dt / self.state.decay.x).min(1.0);
        } else {
            self.x = 0.0;
        }

        (self.y0 * self.x).min(1.0).max(0.0)
    }
}
