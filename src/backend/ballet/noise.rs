// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use rand::rngs::SmallRng;
use rand::{Rng, SeedableRng};

use crate::ballet::NoiseState;

pub struct Noise {
    pub state: NoiseState,
    rng: SmallRng,
    prev_sample: f32,
    sum: f32,
}

impl Noise {
    pub fn new(state: NoiseState) -> Self {
        Self {
            state,
            rng: SmallRng::from_entropy(),
            prev_sample: 0.0,
            sum: 0.0,
        }
    }

    pub fn panic(&mut self) {
        self.prev_sample = 0.0;
        self.sum = 0.0;
    }

    pub fn process(&mut self) -> f32 {
        let mut sample = 1.0 - 2.0 * self.rng.gen::<f32>();

        self.sum = (self.sum + 0.5 * sample).max(-1.0).min(1.0);

        let difference = 0.5 * (sample - self.prev_sample);

        self.prev_sample = sample;

        sample *= 1.0 - self.state.balance.x.abs();

        if self.state.balance.x > 0.0 {
            sample += self.state.balance.x.abs() * difference;
        } else if self.state.balance.x < 0.0 {
            sample += self.state.balance.x.abs() * self.sum;
        }

        sample
    }
}
