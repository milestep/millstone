// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

extern crate test;

use test::{black_box, Bencher};

use crate::ballet::{MasterScene, OscillatorDestination, Scene, VoiceParameter};
use crate::messages::BalletId;
use crate::synth::{Modulation, ModulationParams};

use super::accumulator::Accumulator;
use super::delay::Delay;
use super::envelope::Envelope;
use super::filter::Filter;
use super::lfo::Lfo;
use super::oscillator::Oscillator;
use super::shaper::Shaper;
use super::short_delay::ShortDelay;
use super::tuned_delay::TunedDelay;
use super::voice::Voice;
use super::waveshaper::Waveshaper;
use super::{get_freq_correction, Synth};

#[bench]
fn bench_shaper(b: &mut Bencher) {
    let scene = Scene::new();
    let mut shaper = black_box(Shaper::new(scene.shaper));

    shaper.trigger();

    b.iter(|| {
        shaper.process(black_box(0.5), black_box(1.0 / 44100.0), black_box(1.0));
    });
}

#[bench]
fn bench_oscillator(b: &mut Bencher) {
    let scene = Scene::new();
    let mut osc = black_box(Oscillator::new(scene.osc1));
    let base_frequency = black_box(440.0);
    let phase = black_box(0.0);

    b.iter(|| osc.process(base_frequency, phase));
}

#[bench]
fn bench_oscillator_max(b: &mut Bencher) {
    let mut scene = Scene::new();
    scene.osc1.delay.x = 0.5;
    scene.osc1.width.x = 0.5;
    scene.osc1.gap.x = 0.5;
    scene.osc1.cushion.x = 0.5;
    scene.osc1.melt.x = 0.5;
    let mut osc = black_box(Oscillator::new(scene.osc1));
    let base_frequency = black_box(440.0);
    let phase = black_box(0.0);

    b.iter(|| osc.process(base_frequency, phase));
}

#[bench]
fn bench_tuned_delay(b: &mut Bencher) {
    let scene = Scene::new();
    let mut tuned_delay = black_box(TunedDelay::new(scene.tuned_delay));
    let base_frequency = black_box(440.0);
    let dt = black_box(1.0 / 44100.0);

    b.iter(|| tuned_delay.process(base_frequency, dt, 0.0, 0.0, 0.0, 0.0, 0.0));
}

#[bench]
fn bench_filter(b: &mut Bencher) {
    let scene = Scene::new();
    let mut flt = Filter::new(scene.flt1);
    let base_frequency = black_box(440.0);
    let mut x = black_box([0.0, 0.0]);
    let mut y = black_box([0.0, 0.0]);
    let frame = black_box(0.0);
    let dt = black_box(1.0 / 44100.0);

    b.iter(|| {
        let (a, b, _) = flt.prepare(dt, base_frequency);
        flt.process(&mut x, &mut y, &a, &b, frame, dt);
    });
}

#[bench]
fn bench_waveshaper(b: &mut Bencher) {
    let mut scene = Scene::new();
    scene.wshp.asymmetry.x = 0.5;
    scene.wshp.cushion.x = 0.5;
    scene.wshp.crossover.x = 0.0;
    scene.wshp.crush.x = 0.5;
    let mut wshp = Waveshaper::new(scene.wshp);
    let sample = black_box(0.5);

    b.iter(|| wshp.process(sample));
}

#[bench]
fn bench_accumulator(b: &mut Bencher) {
    let scene = Scene::new();
    let mut acc = black_box(Accumulator::new(scene.acc1));

    let dt = black_box(1.0 / 44100.0);

    b.iter(|| acc.process(dt));
}

#[bench]
fn bench_envelope(b: &mut Bencher) {
    let scene = Scene::new();
    let mut env = black_box(Envelope::new(scene.env1));

    let t = black_box(0.5);
    let dt = black_box(1.0 / 44100.0);

    env.trigger(black_box(0.0));

    b.iter(|| env.process(t, dt));
}

#[bench]
fn bench_envelope_with_echo(b: &mut Bencher) {
    let mut scene = Scene::new();
    scene.env1.echo.x = 0.5;
    let mut env = black_box(Envelope::new(scene.env1));

    let t = black_box(0.5);
    let dt = black_box(1.0 / 44100.0);

    env.trigger(black_box(0.0));

    b.iter(|| env.process(t, dt));
}

#[bench]
fn bench_lfo(b: &mut Bencher) {
    let scene = Scene::new();
    let mut lfo = black_box(Lfo::new(scene.lfo1));
    let dt = black_box(1.0 / 44100.0);

    b.iter(|| lfo.process(dt));
}

#[bench]
fn bench_delay(b: &mut Bencher) {
    let master_scene = MasterScene::new();
    let mut delay_effect = black_box(Delay::new(master_scene.delay));

    let active_channels = black_box([true, true, false, false, false, false, false, false]);
    let in_frame = black_box([0.0f32; 8]);
    let mut out_frame = black_box([0.0f32; 8]);

    b.iter(|| delay_effect.process(1.0 / 44100.0, &active_channels, &in_frame, &mut out_frame));
}

#[bench]
fn bench_short_delay(b: &mut Bencher) {
    let mut delay_effect = black_box(ShortDelay::new());

    let abs_delay = black_box(0.5);
    let sample = black_box(0.0);

    b.iter(|| delay_effect.process(abs_delay, sample));
}

#[bench]
fn bench_voice(b: &mut Bencher) {
    let mut voice = black_box(Voice::new());
    voice.trigger(
        BalletId {
            group_id: 0,
            voice_id: 0,
            note_id: 0,
        },
        440.0,
        1.0,
    );

    let master_axes = black_box([0.0f32; 24]);
    let scale = black_box([true; 12]);
    let dt = black_box(1.0 / 44100.0);

    b.iter(|| voice.process(&master_axes, &scale, 0, 0, 0.0, dt));
}

#[bench]
fn bench_voice_all_disabled(b: &mut Bencher) {
    let mut voice = black_box(Voice::new());

    voice.patch.disable_osc1 = 1;
    voice.patch.disable_osc2 = 1;
    voice.patch.disable_tuned_delay = 1;
    voice.patch.disable_flt1 = 1;
    voice.patch.disable_flt2 = 1;
    voice.patch.disable_wshp = 1;
    voice.patch.disable_delay = 1;
    voice.patch.disable_env1 = 1;
    voice.patch.disable_env2 = 1;
    voice.patch.disable_lfo1 = 1;
    voice.patch.disable_lfo2 = 1;

    voice.trigger(
        BalletId {
            group_id: 0,
            voice_id: 0,
            note_id: 0,
        },
        440.0,
        1.0,
    );

    let master_axes = black_box([0.0f32; 24]);
    let scale = black_box([true; 12]);
    let dt = black_box(1.0 / 44100.0);

    voice.process(&master_axes, &scale, 0, 0, 0.0, dt);

    b.iter(|| voice.process(&master_axes, &scale, 0, 0, 0.0, dt));
}

#[bench]
fn bench_vocal_processor(b: &mut Bencher) {
    let mut synth = black_box(Synth::new());

    let dt = black_box(1.0 / 44100.0);

    let mut input_sample = black_box(0.0f32);

    b.iter(|| {
        synth.input_rms.process(&[input_sample], dt);
        let rms = synth.input_rms.mean_square[0].sqrt();
        synth.vocal_processor.process(&mut input_sample, rms, dt);
    });
}

#[bench]
fn bench_freq_correction(b: &mut Bencher) {
    let freq = black_box(556.0);
    let notes = black_box([true; 12]);
    let root_note = black_box(0);
    let tuning = black_box(1);

    b.iter(|| {
        black_box(get_freq_correction(freq, &notes, root_note, tuning));
    });
}

#[bench]
fn bench_compressor(b: &mut Bencher) {
    let mut synth = black_box(Synth::new());

    let active_channels = black_box([true, true, false, false, false, false, false, false]);

    let dt = black_box(1.0 / 44100.0);

    b.iter(|| {
        synth
            .compressor
            .process(&active_channels, &mut synth.work_frame, dt);
    });
}

#[bench]
fn bench_master_filter(b: &mut Bencher) {
    let mut synth = black_box(Synth::new());

    let active_channels = black_box([true, true, false, false, false, false, false, false]);

    let dt = black_box(1.0 / 44100.0);

    b.iter(|| {
        synth.master_filter.process(
            black_box(-0.05),
            &active_channels,
            &mut synth.work_frame,
            dt,
        );
    });
}

#[bench]
fn bench_synth_1_voice(b: &mut Bencher) {
    let mut synth = black_box(Synth::new());

    synth.voices[0].trigger(
        BalletId {
            group_id: 0,
            voice_id: 0,
            note_id: 0,
        },
        440.0,
        1.0,
    );

    b.iter(|| synth.process(black_box(vec![0.0f32; 8])));
}

#[bench]
fn bench_synth_1_voice_2_modulations(b: &mut Bencher) {
    let mut synth = black_box(Synth::new());

    synth.voices[0].trigger(
        BalletId {
            group_id: 0,
            voice_id: 0,
            note_id: 0,
        },
        440.0,
        1.0,
    );

    synth.voices[0].set_modulation(Modulation {
        idx: [1, 6, 2, 8],
        destination: VoiceParameter::Osc1(OscillatorDestination::Asymmetry),
        params: ModulationParams {
            amount: 1.0,
            lower: 0.0,
            upper: 1.0,
            offset: 1.0,
        },
    });

    synth.voices[0].set_modulation(Modulation {
        idx: [2, 3, 4, 5],
        destination: VoiceParameter::Osc2(OscillatorDestination::Asymmetry),
        params: ModulationParams {
            amount: 1.0,
            lower: 0.0,
            upper: 1.0,
            offset: 1.0,
        },
    });

    for axis in 0..15 {
        synth.voices[0].morph(axis, black_box(0.1));
    }

    b.iter(|| synth.process(black_box(vec![0.0f32; 8])));
}

#[bench]
fn bench_synth_1_voice_all_disabled(b: &mut Bencher) {
    let mut synth = black_box(Synth::new());

    synth.patch.disable_vocal = 1;
    synth.patch.disable_compressor = 1;
    synth.patch.disable_delay = 1;
    synth.patch.disable_flt = 1;
    synth.voices[0].patch.disable_osc1 = 1;
    synth.voices[0].patch.disable_osc2 = 1;
    synth.voices[0].patch.disable_tuned_delay = 1;
    synth.voices[0].patch.disable_flt1 = 1;
    synth.voices[0].patch.disable_flt2 = 1;
    synth.voices[0].patch.disable_wshp = 1;
    synth.voices[0].patch.disable_delay = 1;
    synth.voices[0].patch.disable_env1 = 1;
    synth.voices[0].patch.disable_env2 = 1;
    synth.voices[0].patch.disable_lfo1 = 1;
    synth.voices[0].patch.disable_lfo2 = 1;

    synth.voices[0].trigger(
        BalletId {
            group_id: 0,
            voice_id: 0,
            note_id: 0,
        },
        440.0,
        1.0,
    );

    for axis in 0..15 {
        synth.voices[0].morph(axis, black_box(0.1));
    }

    b.iter(|| synth.process(black_box(vec![0.0f32; 8])));
}

#[bench]
fn bench_synth_8_voices(b: &mut Bencher) {
    let mut synth = black_box(Synth::new());

    for idx in 0..8 {
        synth.voices[idx].trigger(
            BalletId {
                group_id: 0,
                voice_id: 0,
                note_id: 0,
            },
            440.0,
            1.0,
        );
    }

    b.iter(|| synth.process(black_box(vec![0.0f32; 8])));
}

#[bench]
fn bench_synth_16_voices(b: &mut Bencher) {
    let mut synth = black_box(Synth::new());

    for idx in 0..16 {
        synth.voices[idx].trigger(
            BalletId {
                group_id: 0,
                voice_id: 0,
                note_id: 0,
            },
            440.0,
            1.0,
        );
    }

    b.iter(|| synth.process(black_box(vec![0.0f32; 8])));
}

#[bench]
fn bench_synth_16_voices_2_modulations(b: &mut Bencher) {
    let mut synth = black_box(Synth::new());

    for idx in 0..16 {
        synth.voices[idx].trigger(
            BalletId {
                group_id: 0,
                voice_id: 0,
                note_id: 0,
            },
            440.0,
            1.0,
        );

        synth.voices[idx].set_modulation(Modulation {
            idx: [1, 6, 2, 8],
            destination: VoiceParameter::Osc1(OscillatorDestination::Asymmetry),
            params: ModulationParams {
                amount: 1.0,
                lower: 0.0,
                upper: 1.0,
                offset: 1.0,
            },
        });

        synth.voices[idx].set_modulation(Modulation {
            idx: [2, 3, 4, 5],
            destination: VoiceParameter::Osc2(OscillatorDestination::Asymmetry),
            params: ModulationParams {
                amount: 1.0,
                lower: 0.0,
                upper: 1.0,
                offset: 1.0,
            },
        });

        for axis in 0..15 {
            synth.voices[idx].morph(axis, black_box(0.1));
        }
    }

    b.iter(|| synth.process(black_box(vec![0.0f32; 8])));
}

#[bench]
fn bench_synth_16_voices_voice_disabled(b: &mut Bencher) {
    let mut synth = black_box(Synth::new());

    for voice in synth.voices.iter_mut() {
        voice.patch.disable_osc1 = 1;
        voice.patch.disable_osc2 = 1;
        voice.patch.disable_tuned_delay = 1;
        voice.patch.disable_flt1 = 1;
        voice.patch.disable_flt2 = 1;
        voice.patch.disable_wshp = 1;
        voice.patch.disable_delay = 1;
        voice.patch.disable_env1 = 1;
        voice.patch.disable_env2 = 1;
        voice.patch.disable_lfo1 = 1;
        voice.patch.disable_lfo2 = 1;

        voice.trigger(
            BalletId {
                group_id: 0,
                voice_id: 0,
                note_id: 0,
            },
            440.0,
            1.0,
        );

        for axis in 0..15 {
            voice.morph(axis, black_box(0.1));
        }
    }

    b.iter(|| synth.process(black_box(vec![0.0f32; 8])));
}

#[bench]
fn bench_synth_16_voices_all_disabled(b: &mut Bencher) {
    let mut synth = black_box(Synth::new());

    synth.patch.disable_vocal = 1;
    synth.patch.disable_compressor = 1;
    synth.patch.disable_delay = 1;
    synth.patch.disable_flt = 1;

    for voice in synth.voices.iter_mut() {
        voice.patch.disable_osc1 = 1;
        voice.patch.disable_osc2 = 1;
        voice.patch.disable_tuned_delay = 1;
        voice.patch.disable_flt1 = 1;
        voice.patch.disable_flt2 = 1;
        voice.patch.disable_wshp = 1;
        voice.patch.disable_delay = 1;
        voice.patch.disable_env1 = 1;
        voice.patch.disable_env2 = 1;
        voice.patch.disable_lfo1 = 1;
        voice.patch.disable_lfo2 = 1;

        voice.trigger(
            BalletId {
                group_id: 0,
                voice_id: 0,
                note_id: 0,
            },
            440.0,
            1.0,
        );

        for axis in 0..15 {
            voice.morph(axis, black_box(0.1));
        }
    }

    b.iter(|| synth.process(black_box(vec![0.0f32; 8])));
}
