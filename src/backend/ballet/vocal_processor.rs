// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use core::f32::consts::PI;

use crate::ballet::VocalProcessorState;

pub struct VocalProcessor {
    pub state: VocalProcessorState,
    pub gain: f32,   // dB
    pub signal: f32, // dB
    filter: VocalFilter,
    vocal_delay: VocalDelay,
}

impl VocalProcessor {
    pub fn new(state: VocalProcessorState) -> Self {
        Self {
            state,
            gain: 0.0,
            signal: -f32::INFINITY,
            filter: VocalFilter::new(),
            vocal_delay: VocalDelay::new(),
        }
    }

    pub fn panic(&mut self) {
        self.vocal_delay.panic();
    }

    pub fn process(&mut self, sample: &mut f32, rms: f32, dt: f32) -> (f32, f32, f32) {
        *sample = self
            .filter
            .process(self.state.lowpass.x, self.state.highpass.x, *sample, dt);

        let gate = self.state.gate.x;
        let threshold = self.state.threshold.x;

        self.signal = 20.0 * rms.log10();

        // Compression stage.

        let mut delta = 0.0;
        let mut a = 1.0 - (dt / 0.05).min(1.0);

        if gate > self.state.gate.low && self.signal < gate {
            delta += ((self.signal - gate) * 0.875).max(-36.0);

            a = if self.gain > delta {
                1.0 - (dt / 0.1).min(1.0)
            } else {
                1.0 - (dt / 0.02).min(1.0)
            };
        }

        if threshold < self.state.threshold.high && self.signal > threshold {
            delta += (threshold - self.signal) * 0.875;

            a = if self.gain > delta {
                1.0 - (dt / 0.02).min(1.0)
            } else {
                1.0 - (dt / 0.1).min(1.0)
            };
        }

        self.gain = a * self.gain + (1.0 - a) * delta;
        self.signal += self.gain;

        let k = 10.0f32.powf(self.state.gain.x / 20.0);

        // Do parallel compression
        *sample *= 1.0 + k * (10.0f32.powf((self.gain - threshold * 0.875) / 20.0) - 1.0);

        let (left, right) = if self.state.time.x == self.state.time.low {
            (*sample, *sample)
        } else {
            let spread = if self.state.feedback.x > 0.0 {
                0.0
            } else {
                0.5
            };

            self.vocal_delay.process(
                self.state.feedback.x.abs(),
                self.state.time.x,
                spread,
                *sample,
                dt,
            )
        };

        (left, right, *sample)
    }
}

pub struct VocalFilter {
    lowpass: Filter,
    highpass: Filter,
}

impl VocalFilter {
    pub fn new() -> Self {
        Self {
            lowpass: Filter::new(),
            highpass: Filter::new(),
        }
    }

    pub fn process(&mut self, lowpass: f32, highpass: f32, sample: f32, dt: f32) -> f32 {
        self.highpass
            .process(highpass, self.lowpass.process(lowpass, sample, dt), dt)
    }
}

pub struct Filter {
    x: [f32; 2],
    y: [f32; 2],
}

impl Filter {
    pub fn new() -> Self {
        Self {
            x: [0.0; 2],
            y: [0.0; 2],
        }
    }

    pub fn process(&mut self, value: f32, sample: f32, dt: f32) -> f32 {
        let freq = if value >= 0.0 {
            50.0f32.powf(1.0 - value) * 10000.0f32.powf(value)
        } else {
            50.0f32.powf(-value) * 10000.0f32.powf(1.0 + value)
        };

        let omega0 = PI * freq * dt / 2.0;
        let sin0 = omega0.sin().abs();
        let cos0 = omega0.cos();

        let alpha = std::f32::consts::FRAC_1_SQRT_2 * sin0;

        let a0 = 1.0 + alpha;
        let a1 = -2.0 * cos0;
        let a2 = 1.0 - alpha;

        let k = 1.0 + value.signum() * cos0;

        let b0 = k / 2.0;
        let b1 = -(value.signum() + cos0);
        let b2 = k / 2.0;

        let num = b1 / a0 * self.x[0] + b2 / a0 * self.x[1];
        let den = a1 / a0 * self.y[0] + a2 / a0 * self.y[1];

        self.x = [sample, self.x[0]];

        let out = b0 / a0 * sample + num - den;
        self.y = [out, self.y[0]];

        out
    }
}

struct VocalDelay {
    t: usize,
    delay_left: f32,
    delay_right: f32,
    buffer: Vec<[f32; 2]>,
}

impl VocalDelay {
    pub fn new() -> Self {
        Self {
            t: 0,
            delay_left: 0.0,
            delay_right: 0.0,
            buffer: vec![[0.0; 2]; 3 * 44100],
        }
    }

    pub fn panic(&mut self) {
        self.buffer.fill([0.0; 2]);
    }

    pub fn process(
        &mut self,
        feedback: f32,
        delay: f32,
        spread: f32,
        input: f32,
        dt: f32,
    ) -> (f32, f32) {
        for sample in self.buffer[self.t].iter_mut() {
            *sample = input;
        }

        let length = self.buffer.len();
        let sample_delay = delay / dt / (length as f32);

        let delay_left = sample_delay * (1.0 + spread).max(1.0);
        let delay_right = sample_delay * (1.0 - spread).max(1.0);

        self.delay_left = if delay_left > self.delay_left {
            delay_left.min(self.delay_left + 1e-5)
        } else if delay_left < self.delay_left {
            delay_left.max(self.delay_left - 1e-5)
        } else {
            delay_left
        };

        self.delay_right = if delay_right > self.delay_right {
            delay_right.min(self.delay_right + 1e-5)
        } else if delay_right < self.delay_right {
            delay_right.max(self.delay_right - 1e-5)
        } else {
            delay_right
        };

        let delay_left = length as f32 - (self.delay_left * (length as f32 - 1.0));
        let delay_right = length as f32 - (self.delay_right * (length as f32 - 1.0));

        let delayed_idx_left = (self.t + delay_left.round() as usize) % length;
        let delayed_idx_right = (self.t + delay_right.round() as usize) % length;

        let x0 = self.buffer[delayed_idx_left][0];
        let left = input + feedback * x0;
        self.buffer[self.t][0] += feedback * x0;

        let x0 = self.buffer[delayed_idx_right][1];
        let right = input + feedback * x0;
        self.buffer[self.t][1] += feedback * x0;

        self.t = (self.t + 1) % length;

        (left, right)
    }
}
