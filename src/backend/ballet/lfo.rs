// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::ballet::LfoState;

pub struct Lfo {
    pub state: LfoState,
    t: f32,
    x: f32,
    x0: f32,
}

impl Lfo {
    pub fn new(state: LfoState) -> Self {
        Self {
            state,
            t: 0.0,
            x: 0.0,
            x0: 0.0,
        }
    }

    pub fn panic(&mut self) {
        self.t = 0.0;
        self.x = 0.0;
        self.x0 = 0.0;
    }

    pub fn trigger(&mut self) {
        self.t = 0.0;
        self.x = 0.0;
    }

    pub fn process(&mut self, dt: f32) -> f32 {
        let freq = if self.state.frequency.x == self.state.frequency.low {
            0.0
        } else {
            self.state.frequency.x
        };

        self.t = (self.t + freq * dt).rem_euclid(1.0);
        let t = (self.t + self.state.phase.x).rem_euclid(1.0);

        let mut x = {
            let pivot = 0.5 * (1.0 + self.state.asymmetry.x);

            if t < pivot {
                -1.0 + 2.0 * t / pivot
            } else {
                1.0 - 2.0 * (t - pivot) / (1.0 - pivot)
            }
        };

        if self.state.cushion.x > 0.0 {
            x += self.state.cushion.x * ((x * std::f32::consts::FRAC_PI_2).sin() - x);
        }

        self.x0 = x;

        self.x = if self.state.slew.x > self.state.slew.low {
            let a = 1.0 - (dt / (self.state.slew.x)).min(1.0);
            a * self.x + (1.0 - a) * self.x0
        } else {
            self.x0
        };

        self.state.amount.x * 0.5 * (self.x + 1.0)
    }
}
