// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;

use super::accumulator::Accumulator;
use super::envelope::Envelope;
use super::filter::Filter;
use super::lfo::Lfo;
use super::noise::Noise;
use super::oscillator::Oscillator;
use super::shaper::Shaper;
use super::short_delay::ShortDelay;
use super::tuned_delay::TunedDelay;
use super::waveshaper::Waveshaper;

use crate::ballet::{Patch, VoiceParameter, VoiceState};
use crate::messages::{BalletConfig, BalletId};
use crate::synth::Modulation;

pub struct Voice {
    pub active: bool,
    pub held: bool,
    pub patch: Patch,
    // Time
    pub t: f32,
    // Frequency
    ids: Vec<BalletId>,
    base_frequency: f32,
    base_frequencies: HashMap<BalletId, f32>,
    current_frequency: f32,
    frequency_idx: usize,
    frequency_count: usize,
    arp_tick: f32,
    latched: Vec<f32>,
    // State
    pub state: VoiceState,
    osc1: Oscillator,
    osc2: Oscillator,
    osc3: Oscillator,
    noise: Noise,
    tuned_delay: TunedDelay,
    flt1: Filter,
    flt2: Filter,
    wshp: Waveshaper,
    flt_x: [[f32; 2]; 2],
    flt_y: [[f32; 2]; 2],
    env1: Envelope,
    env2: Envelope,
    lfo1: Lfo,
    lfo2: Lfo,
    acc1: Accumulator,
    acc2: Accumulator,
    shaper: Shaper,
    // Modulation
    input_axes: [f32; 24],
    axes: [f32; 24],
    // Rate reduction,
    counter: usize,
    // Filter/gain
    cutoff: f32,
    out: f32,
    xs: [f32; 2],
    short_delay: ShortDelay,
    out_frame: (f32, f32),
    hold_frame: (f32, f32),
}

impl Voice {
    pub fn new() -> Self {
        let patch = Patch::new(0, 1, 255);

        let osc1 = Oscillator::new(patch.scene.osc1.clone());
        let osc2 = Oscillator::new(patch.scene.osc2.clone());
        let osc3 = Oscillator::new(patch.scene.osc3.clone());
        let noise = Noise::new(patch.scene.noise.clone());
        let tuned_delay = TunedDelay::new(patch.scene.tuned_delay.clone());

        let flt1 = Filter::new(patch.scene.flt1.clone());
        let flt2 = Filter::new(patch.scene.flt2.clone());
        let wshp = Waveshaper::new(patch.scene.wshp.clone());

        let env1 = Envelope::new(patch.scene.env1.clone());
        let env2 = Envelope::new(patch.scene.env2.clone());

        let lfo1 = Lfo::new(patch.scene.lfo1.clone());
        let lfo2 = Lfo::new(patch.scene.lfo2.clone());

        let acc1 = Accumulator::new(patch.scene.acc1.clone());
        let acc2 = Accumulator::new(patch.scene.acc2.clone());

        let shaper = Shaper::new(patch.scene.shaper.clone());

        let state = patch.scene.voice.clone();

        Self {
            active: false,
            held: false,
            patch,
            // Time
            t: 0.0,
            // Frequency
            ids: Vec::with_capacity(8),
            base_frequency: 0.0,
            base_frequencies: HashMap::new(),
            current_frequency: 0.0,
            frequency_idx: 0,
            frequency_count: 0,
            arp_tick: 0.0,
            latched: Vec::with_capacity(8),
            // State
            state,
            osc1,
            osc2,
            osc3,
            noise,
            tuned_delay,
            flt1,
            flt2,
            wshp,
            flt_x: [[0.0; 2]; 2],
            flt_y: [[0.0; 2]; 2],
            env1,
            env2,
            lfo1,
            lfo2,
            acc1,
            acc2,
            shaper,
            // Modulation
            input_axes: [0.0; 24],
            axes: [0.0; 24],
            // Rate reduction,
            counter: 0,
            // A very simple high-pass filter to remove DC offset.
            cutoff: 30.0, // Hz
            out: 0.0,
            xs: [0.0, 0.0],
            short_delay: ShortDelay::new(),
            out_frame: (0.0, 0.0),
            hold_frame: (0.0, 0.0),
        }
    }

    pub fn load_patch(&mut self, patch: Patch) {
        self.reset_modulations();

        self.osc1.state.set_from(&patch.scene.osc1);
        self.osc2.state.set_from(&patch.scene.osc2);
        self.osc3.state.set_from(&patch.scene.osc3);
        self.noise.state.set_from(&patch.scene.noise);
        self.tuned_delay.state.set_from(&patch.scene.tuned_delay);

        self.flt1.state.set_from(&patch.scene.flt1);
        self.flt2.state.set_from(&patch.scene.flt2);
        self.wshp.state.set_from(&patch.scene.wshp);

        self.env1.state.set_from(&patch.scene.env1);
        self.env2.state.set_from(&patch.scene.env2);

        self.lfo1.state.set_from(&patch.scene.lfo1);
        self.lfo2.state.set_from(&patch.scene.lfo2);

        self.acc1.state.set_from(&patch.scene.acc1);
        self.acc2.state.set_from(&patch.scene.acc2);

        self.shaper.state.set_from(&patch.scene.shaper);

        self.state.set_from(&patch.scene.voice);

        self.patch = patch;
    }

    pub fn unload_patch(&mut self) {
        self.load_patch(Patch::new(0, 1, 255));
        self.panic();
    }

    pub fn set_param(&mut self, param: VoiceParameter, value: f32) {
        match param {
            VoiceParameter::Voice(dest) => self.state.get_mut(dest).set(value),
            VoiceParameter::Osc1(dest) => self.osc1.state.get_mut(dest).set(value),
            VoiceParameter::Osc2(dest) => self.osc2.state.get_mut(dest).set(value),
            VoiceParameter::Osc3(dest) => self.osc3.state.get_mut(dest).set(value),
            VoiceParameter::Noise(dest) => self.noise.state.get_mut(dest).set(value),
            VoiceParameter::TunedDelay(dest) => self.tuned_delay.state.get_mut(dest).set(value),
            VoiceParameter::Flt1(dest) => self.flt1.state.get_mut(dest).set(value),
            VoiceParameter::Flt2(dest) => self.flt2.state.get_mut(dest).set(value),
            VoiceParameter::Wshp(dest) => self.wshp.state.get_mut(dest).set(value),
            VoiceParameter::Env1(dest) => self.env1.state.get_mut(dest).set(value),
            VoiceParameter::Env2(dest) => self.env2.state.get_mut(dest).set(value),
            VoiceParameter::Lfo1(dest) => self.lfo1.state.get_mut(dest).set(value),
            VoiceParameter::Lfo2(dest) => self.lfo2.state.get_mut(dest).set(value),
            VoiceParameter::Acc1(dest) => self.acc1.state.get_mut(dest).set(value),
            VoiceParameter::Acc2(dest) => self.acc2.state.get_mut(dest).set(value),
            VoiceParameter::Shaper(dest) => self.shaper.state.get_mut(dest).set(value),
        }
    }

    pub fn set_modulation(&mut self, modulation: Modulation<VoiceParameter>) {
        self.reset_modulations();
        self.patch
            .set_modulation_params(modulation.idx, modulation.destination, modulation.params);
    }

    pub fn set_config(&mut self, dest: BalletConfig, value: usize) {
        self.patch.set_config(dest, value);
    }

    pub fn has_channel(&mut self, channel: u8) -> bool {
        channel == 0 || self.patch.channel == channel
    }

    pub fn has_id(&mut self, channel: u8, id: BalletId) -> bool {
        self.has_channel(channel)
            && (id.group_id == 0
                || self
                    .base_frequencies
                    .keys()
                    .any(|key| key.group_id == id.group_id))
            && (id.voice_id == 0
                || self
                    .base_frequencies
                    .keys()
                    .any(|key| key.voice_id == id.voice_id))
    }

    pub fn is_latched(&self) -> bool {
        !self.latched.is_empty()
    }

    #[inline(always)]
    pub fn strum(&mut self) {
        self.shaper.strum();
    }

    #[inline(always)]
    pub fn strum_decay(&mut self) {
        self.shaper.strum_decay();
    }

    pub fn trigger(&mut self, id: BalletId, frequency: f32, velocity: f32) {
        self.push_base_frequency(id, frequency);
        self.axes[0] = velocity;

        // In order to ensure that we update the state before triggering, in case the delay
        // time, etc. needs to be updated, we schedule the trigger to happen during the next
        // update.
        self.shaper.triggered = true;

        if !self.active {
            self.frequency_idx = 0;

            if self.patch.disable_osc1 == 0 {
                self.osc1.trigger();
            }

            if self.patch.disable_osc2 == 0 {
                self.osc2.trigger();
                self.osc3.trigger();
            }

            if self.patch.disable_tuned_delay == 0 {
                self.tuned_delay.reset();
            }

            if self.patch.disable_flt1 == 0 {
                self.flt1.trigger();
            }

            if self.patch.disable_flt2 == 0 {
                self.flt2.trigger();
            }

            if self.patch.disable_lfo1 == 0 {
                self.lfo1.trigger();
            }

            if self.patch.disable_lfo2 == 0 {
                self.lfo2.trigger();
            }

            self.xs = [0.0, 0.0];
        }

        self.active = true;
        self.held = true;

        self.determine_base_frequency();
    }

    pub fn release(&mut self, id: BalletId, velocity: f32, latch: bool) {
        self.pop_base_frequency(id, latch);

        if velocity > 0.0 {
            self.axes[0] = velocity;
        }

        if self.latched.is_empty() && self.ids.is_empty() {
            self.shaper.release(self.t);
            self.env1.release(self.t);
            self.env2.release(self.t);

            self.held = false;
        }

        self.determine_base_frequency();
    }

    pub fn push_base_frequency(&mut self, id: BalletId, frequency: f32) {
        self.ids.push(id);
        self.base_frequencies.insert(id, frequency);
        self.frequency_count = self.ids.len() + self.latched.len();
    }

    pub fn pop_base_frequency(&mut self, id: BalletId, latch: bool) {
        let freq = self.base_frequencies.remove(&id);

        if latch {
            if let Some(freq) = freq {
                self.latched.push(freq);
            }
        }

        self.ids.retain(|&id_| id_ != id);

        self.frequency_count = self.ids.len() + self.latched.len();
    }

    fn determine_base_frequency(&mut self) {
        if self.state.arp.x == self.state.arp.low {
            self.frequency_idx = self.frequency_count.max(1) - 1;
        }

        self.base_frequency = match self.ids.get(self.frequency_idx - self.latched.len()) {
            None => match self.latched.get(self.frequency_idx) {
                None => self.base_frequency,
                Some(freq) => *freq,
            },
            Some(id) => match self.base_frequencies.get(id) {
                None => self.base_frequency,
                Some(freq) => *freq,
            },
        };
    }

    pub fn frequency_distance(&self, frequency: f32) -> f32 {
        if frequency > self.base_frequency {
            frequency / self.base_frequency
        } else {
            self.base_frequency / frequency
        }
    }

    pub fn mute(&mut self) {
        self.ids.clear();
        self.base_frequencies.clear();
        self.latched.clear();

        self.shaper.release(self.t);
        self.env1.release(self.t);
        self.env2.release(self.t);

        self.held = false;
        self.frequency_count = 0;
    }

    pub fn panic(&mut self) {
        self.input_axes.fill(0.0);
        self.axes.fill(0.0);
        self.flt_x[0].fill(0.0);
        self.flt_x[1].fill(0.0);
        self.flt_y[0].fill(0.0);
        self.flt_y[1].fill(0.0);
        self.shaper.panic();
        self.noise.panic();
        self.tuned_delay.panic();
        self.flt1.panic();
        self.flt2.panic();
        self.wshp.panic();
        self.env1.panic();
        self.env2.panic();
        self.lfo1.panic();
        self.lfo2.panic();
        self.acc1.panic();
        self.acc2.panic();
        self.short_delay.panic();
        self.xs = [0.0, 0.0];
        self.out = 0.0;
        self.out_frame = (0.0, 0.0);
        self.active = false;
        self.mute();
    }

    pub fn morph(&mut self, axis: usize, amount: f32) {
        self.input_axes[axis] = amount;
    }

    pub fn reset_modulations(&mut self) {
        for modulation in self.patch.modulations.iter() {
            match modulation.destination {
                VoiceParameter::Voice(dest) => self.state.get_mut(dest).reset(),
                VoiceParameter::Osc1(dest) => self.osc1.state.get_mut(dest).reset(),
                VoiceParameter::Osc2(dest) => self.osc2.state.get_mut(dest).reset(),
                VoiceParameter::Osc3(dest) => self.osc3.state.get_mut(dest).reset(),
                VoiceParameter::Noise(dest) => self.noise.state.get_mut(dest).reset(),
                VoiceParameter::TunedDelay(dest) => self.tuned_delay.state.get_mut(dest).reset(),
                VoiceParameter::Flt1(dest) => self.flt1.state.get_mut(dest).reset(),
                VoiceParameter::Flt2(dest) => self.flt2.state.get_mut(dest).reset(),
                VoiceParameter::Wshp(dest) => self.wshp.state.get_mut(dest).reset(),
                VoiceParameter::Env1(dest) => self.env1.state.get_mut(dest).reset(),
                VoiceParameter::Env2(dest) => self.env2.state.get_mut(dest).reset(),
                VoiceParameter::Lfo1(dest) => self.lfo1.state.get_mut(dest).reset(),
                VoiceParameter::Lfo2(dest) => self.lfo2.state.get_mut(dest).reset(),
                VoiceParameter::Acc1(dest) => self.acc1.state.get_mut(dest).reset(),
                VoiceParameter::Acc2(dest) => self.acc2.state.get_mut(dest).reset(),
                VoiceParameter::Shaper(dest) => self.shaper.state.get_mut(dest).reset(),
            }
        }
    }

    pub fn next_state(&mut self) {
        self.reset_modulations();

        for modulation in self.patch.modulations.iter() {
            let amount = modulation.get(&self.axes);

            if amount == 0.0 {
                continue;
            }

            match modulation.destination {
                VoiceParameter::Voice(dest) => self.state.get_mut(dest).modulate(amount),
                VoiceParameter::Osc1(dest) => self.osc1.state.get_mut(dest).modulate(amount),
                VoiceParameter::Osc2(dest) => self.osc2.state.get_mut(dest).modulate(amount),
                VoiceParameter::Osc3(dest) => self.osc3.state.get_mut(dest).modulate(amount),
                VoiceParameter::Noise(dest) => self.noise.state.get_mut(dest).modulate(amount),
                VoiceParameter::TunedDelay(dest) => {
                    self.tuned_delay.state.get_mut(dest).modulate(amount)
                }
                VoiceParameter::Flt1(dest) => self.flt1.state.get_mut(dest).modulate(amount),
                VoiceParameter::Flt2(dest) => self.flt2.state.get_mut(dest).modulate(amount),
                VoiceParameter::Wshp(dest) => self.wshp.state.get_mut(dest).modulate(amount),
                VoiceParameter::Env1(dest) => self.env1.state.get_mut(dest).modulate(amount),
                VoiceParameter::Env2(dest) => self.env2.state.get_mut(dest).modulate(amount),
                VoiceParameter::Lfo1(dest) => self.lfo1.state.get_mut(dest).modulate(amount),
                VoiceParameter::Lfo2(dest) => self.lfo2.state.get_mut(dest).modulate(amount),
                VoiceParameter::Acc1(dest) => self.acc1.state.get_mut(dest).modulate(amount),
                VoiceParameter::Acc2(dest) => self.acc2.state.get_mut(dest).modulate(amount),
                VoiceParameter::Shaper(dest) => self.shaper.state.get_mut(dest).modulate(amount),
            }
        }
    }

    pub fn process(
        &mut self,
        master_axes: &[f32; 24],
        scale: &[bool; 12],
        root_note: usize,
        tuning: usize,
        input_sample: f32,
        dt: f32,
    ) -> (f32, f32) {
        let rate_divider = self.patch.rate_divider + 1;

        if self.counter == 0 {
            self.hold_frame = self.out_frame;

            let dt = rate_divider as f32 * dt;

            self.next_frame(master_axes, scale, root_note, tuning, input_sample, dt);
        }

        let a = (self.counter + 1) as f32 / rate_divider as f32;

        let frame = (
            (1.0 - a) * self.hold_frame.0 + a * self.out_frame.0,
            (1.0 - a) * self.hold_frame.1 + a * self.out_frame.1,
        );

        self.counter = (self.counter + 1) % rate_divider;

        frame
    }

    fn next_frame(
        &mut self,
        master_axes: &[f32; 24],
        scale: &[bool; 12],
        root_note: usize,
        tuning: usize,
        input_sample: f32,
        dt: f32,
    ) {
        let alpha = 1.0 - (dt / 0.001).min(1.0);
        self.axes[1] = alpha * self.axes[1] + (1.0 - alpha) * self.input_axes[1];

        self.axes[2] = master_axes[2];
        self.axes[3] = master_axes[3];
        self.axes[4] = master_axes[4];
        self.axes[5] = master_axes[5];
        self.axes[18] = master_axes[18];
        self.axes[19] = master_axes[19];
        self.axes[20] = master_axes[20];
        self.axes[21] = master_axes[21];
        self.axes[22] = master_axes[22];
        self.axes[23] = master_axes[23];

        self.next_state();

        self.axes[6] = self.acc1.process(dt);
        self.axes[7] = self.acc2.process(dt);

        if self.shaper.triggered {
            self.determine_base_frequency();

            self.shaper.trigger();
            self.tuned_delay.trigger();
            self.env1.trigger(self.shaper.delay);
            self.env2.trigger(self.shaper.delay);
            self.arp_tick = 0.0;
            self.t = 0.0;
        }

        self.axes[12] = if self.patch.disable_env1 > 0 {
            self.shaper.x
        } else {
            self.env1.process(self.t, dt)
        };

        self.axes[13] = if self.patch.disable_env2 > 0 {
            self.shaper.x
        } else {
            self.env2.process(self.t, dt)
        };

        self.axes[14] = if self.patch.disable_lfo1 > 0 {
            master_axes[14]
        } else {
            self.lfo1.process(dt)
        };

        self.axes[15] = if self.patch.disable_lfo2 > 0 {
            master_axes[15]
        } else {
            self.lfo2.process(dt)
        };

        if self.state.arp.x > self.state.arp.low {
            self.arp_tick += dt;

            if self.arp_tick > self.state.arp.x {
                self.arp_tick -= self.state.arp.x;
                self.frequency_idx += 1;
                if self.frequency_idx >= self.frequency_count {
                    self.frequency_idx = 0;
                };
                self.determine_base_frequency();
            }
        }

        self.current_frequency = if self.t >= self.shaper.delay {
            if self.state.portamento.x > self.state.portamento.low && self.current_frequency > 0.0 {
                let a = 1.0 - (dt / self.state.portamento.x).min(1.0);
                a * self.current_frequency + (1.0 - a) * self.base_frequency
            } else {
                self.base_frequency
            }
        } else {
            self.current_frequency
        };

        // `0.0` at f == 27.5, `1.0` at f == 7040.0
        self.axes[16] = (self.current_frequency / 27.5).log2() / 8.0;

        let freq = self.state.frequency.x * self.current_frequency / 440.0;

        let correction = super::get_freq_correction(freq, scale, root_note, tuning);
        self.axes[17] = (correction / 12.0 + 0.5).min(1.0).max(0.0);

        let osc1 = if self.patch.disable_osc1 > 0 {
            input_sample
        } else {
            self.osc1.process(freq, dt)
        };

        let osc2 = if self.patch.disable_osc2 > 0 {
            input_sample
        } else {
            self.osc2.process(freq, dt)
        };

        let osc3 = if self.patch.disable_osc2 > 0 {
            input_sample
        } else {
            self.osc3.process(freq, dt)
        };

        let noise = self.noise.process();

        self.axes[8] = if self.patch.disable_osc1 > 0 {
            master_axes[8]
        } else {
            (0.5 + 0.5 * osc1).min(1.0).max(0.0)
        };

        self.axes[9] = if self.patch.disable_osc2 > 0 {
            master_axes[9]
        } else {
            (0.5 + 0.5 * osc2).min(1.0).max(0.0)
        };

        self.axes[10] = if self.patch.disable_osc2 > 0 {
            master_axes[10]
        } else {
            (0.5 + 0.5 * osc3).min(1.0).max(0.0)
        };

        self.axes[11] = (0.5 + 0.5 * noise).min(1.0).max(0.0);

        let tuned_delay = if self.patch.disable_tuned_delay > 0 {
            input_sample
        } else {
            self.tuned_delay
                .process(freq, dt, osc1, osc2, osc3, noise, input_sample)
        };

        let mut sample = 0.0;

        sample += self.state.osc1.x * osc1;
        sample += self.state.osc2.x * osc2;
        sample += self.state.osc3.x * osc3;
        sample += self.state.noise.x * noise;
        sample += self.state.tuned_delay.x * tuned_delay;

        if self.state.gain.x > self.state.gain.low {
            sample *= self.state.gain.x;
        } else {
            sample = 0.0;
        }

        let (f1, freq1) = if self.patch.disable_flt1 > 0 {
            (sample, freq)
        } else {
            let (a, b, freq1) = self.flt1.prepare(dt, freq);
            let f1 = self
                .flt1
                .process(&mut self.flt_x[0], &mut self.flt_y[0], &a, &b, sample, dt);
            (f1, freq1)
        };

        let f2 = if self.patch.disable_flt2 > 0 {
            f1
        } else {
            let (a, b, _) = self.flt2.prepare(dt, freq1);
            self.flt2.process(
                &mut self.flt_x[1],
                &mut self.flt_y[1],
                &a,
                &b,
                (1.0 - self.state.flt2.x) * f1 + self.state.flt2.x * sample,
                dt,
            )
        };

        sample = if self.patch.disable_wshp > 0 {
            self.state.flt2.x * f1 + f2
        } else {
            self.wshp
                .process(0.5 * (self.state.flt2.x * f1 + (2.0 - self.state.flt2.x) * f2))
        };

        // Apply high-pass filter to remove DC offset.
        let alpha = 1.0 / (2.0 * self.cutoff * std::f32::consts::PI * dt + 1.0);

        self.xs[0] = sample;
        self.out = alpha * (self.out + self.xs[0] - self.xs[1]);
        self.xs[1] = self.xs[0];

        self.t += dt;

        let gain = self.shaper.process(self.t, dt, self.axes[0]);

        let (left, right) = if self.patch.disable_delay > 0 {
            (gain * self.out, gain * self.out)
        } else {
            let (sample, delayed_sample) = self
                .short_delay
                .process(self.state.delay.x.abs(), gain * self.out);

            if self.state.delay.x >= 0.0 {
                (sample, delayed_sample)
            } else {
                (delayed_sample, sample)
            }
        };

        // To prevent clicks, let the voice continue until the delayed signal is empty.
        if self.shaper.is_done()
            && (left == 0.0 || !left.is_finite())
            && (right == 0.0 || !right.is_finite())
        {
            self.xs = [0.0, 0.0];
            self.out = 0.0;
            self.out_frame = (0.0, 0.0);
            self.active = false;
        }

        let left_pan = (self.state.pan.x - self.state.width.x).max(-1.0).min(1.0);
        let right_pan = (self.state.pan.x + self.state.width.x).max(-1.0).min(1.0);

        self.out_frame = (
            (0.5 - 0.5 * left_pan) * left + (0.5 - 0.5 * right_pan) * right,
            (0.5 + 0.5 * left_pan) * left + (0.5 + 0.5 * right_pan) * right,
        );
    }
}
