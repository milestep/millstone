// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use core::f32::consts::FRAC_PI_2;

use crate::ballet::CompressorState;

use super::rms::Rms;

pub struct Compressor {
    pub state: CompressorState,
    pub gain: f32, // dB
    signal: f32,   // dB
    velocity: f32, // dB/seconds
    rms: Rms<8>,
    sum_frame: [usize; 8],
}

impl Compressor {
    pub fn new(state: CompressorState) -> Self {
        Self {
            state,
            gain: 0.0,
            signal: -f32::INFINITY,
            velocity: 0.0,
            rms: Rms::new(0.01),
            sum_frame: [0; 8],
        }
    }

    pub fn panic(&mut self) {
        self.gain = 0.0;
        self.signal = -f32::INFINITY;
        self.velocity = 0.0;
        self.sum_frame.fill(0);
        self.rms.panic();
    }

    pub fn process(&mut self, active_channels: &[bool; 8], frame: &mut [f32; 8], dt: f32) {
        let drive = (1.0 - self.state.drive.x).powf(2.0).max(0.001);

        for (sample, active) in frame.iter_mut().zip(active_channels.iter()) {
            if !active {
                continue;
            }

            *sample = (1.0 - self.state.drive.x) * (*sample).min(1.0).max(-1.0)
                + self.state.drive.x * (*sample / drive).atan() / FRAC_PI_2;
        }

        let threshold = self.state.threshold.x;
        let knee = self.state.knee.x;
        let inv_ratio = if self.state.ratio.x == self.state.ratio.high {
            0.0
        } else {
            1.0 / self.state.ratio.x
        };

        self.rms.process(frame, dt);
        let max_rms = self
            .rms
            .mean_square
            .into_iter()
            .reduce(f32::max)
            .unwrap()
            .sqrt();
        self.signal = self.signal.max(20.0 * max_rms.log10());

        // Compression stage

        let delta = if self.signal > threshold - 0.5 * knee {
            let x = if self.signal < threshold + 0.5 * knee {
                (self.signal - (threshold - 0.5 * knee)) / knee
            } else {
                1.0
            };

            x * (threshold - self.signal) * (1.0 - inv_ratio)
        } else {
            0.0
        };

        let a = if self.gain > delta {
            1.0 - (dt / self.state.attack.x).min(1.0)
        } else {
            1.0 - (dt / self.state.release.x).min(1.0)
        };

        self.gain = a * self.gain + (1.0 - a) * delta;
        self.signal += self.gain;

        let makeup_gain = if threshold + 0.5 * knee > 0.0 {
            (0.5 - threshold / knee) * threshold * (1.0 - inv_ratio)
        } else {
            threshold * (1.0 - inv_ratio)
        };

        let k = 10.0f32.powf(self.state.gain.x / 20.0);
        let mut gain = 1.0 + k * (10.0f32.powf((self.gain - makeup_gain) / 20.0) - 1.0);
        gain *= 10.0f32.powf(self.state.post_gain.x / 20.0);

        for sample in frame.iter_mut() {
            *sample *= gain;
        }
    }
}
