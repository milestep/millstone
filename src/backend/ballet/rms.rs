// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

pub struct Rms<const S: usize> {
    c: f32,
    pub mean_square: [f32; S],
}

impl<const S: usize> Rms<S> {
    pub fn new(c: f32) -> Self {
        Self {
            c,
            mean_square: [0.0; S],
        }
    }

    pub fn panic(&mut self) {
        self.mean_square.fill(0.0);
    }

    pub fn process(&mut self, frame: &[f32; S], dt: f32) {
        let alpha = 1.0 - (dt / self.c).min(1.0);

        for (sum, sample) in self.mean_square.iter_mut().zip(frame.iter()) {
            *sum = alpha * *sum + (1.0 - alpha) * (*sample).powi(2);
        }
    }
}
