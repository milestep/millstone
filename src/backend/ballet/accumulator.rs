// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::ballet::AccumulatorState;

pub struct Accumulator {
    pub state: AccumulatorState,
    sum: f32,
    x0: f32,
    tick: f32,
}

impl Accumulator {
    pub fn new(state: AccumulatorState) -> Accumulator {
        Self {
            state,
            sum: 0.0,
            x0: 0.0,
            tick: 0.0,
        }
    }

    pub fn panic(&mut self) {
        self.sum = 0.0;
        self.x0 = 0.0;
        self.tick = 0.0;
    }

    pub fn process(&mut self, dt: f32) -> f32 {
        let a = 1.0 - (dt / self.state.slew.x).min(1.0);
        self.sum = a * self.sum + (1.0 - a) * self.state.value.x;

        let mut hold = false;

        if self.state.rate.x < self.state.rate.high {
            self.tick += dt * self.state.rate.x;

            if self.tick < 1.0 {
                hold = true;
            } else {
                self.tick -= 1.0;
            }
        }

        if !hold {
            self.x0 = self.sum;
        }

        if self.state.depth.x > 0.0 {
            let f = 2f32.powf(16.0 * (1.0 - self.state.depth.x));
            (self.x0 * f).round() / f
        } else {
            self.x0
        }
    }
}
