// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::ballet::WaveshaperState;

pub struct Waveshaper {
    pub state: WaveshaperState,
    x: f32,
    tick: f32,
}

impl Waveshaper {
    pub fn new(state: WaveshaperState) -> Self {
        Self {
            state,
            x: 0.0,
            tick: 0.0,
        }
    }

    pub fn panic(&mut self) {
        self.x = 0.0;
        self.tick = 0.0;
    }

    pub fn process(&mut self, sample: f32) -> f32 {
        if self.state.sampling.x < self.state.sampling.high {
            self.tick += self.state.sampling.x;

            if self.tick < 1.0 {
                return self.x;
            }

            self.tick -= 1.0;
        }

        let mut x = self.state.pregain.x * sample;

        if (self.state.asymmetry.x > 0.0 && x > 0.0) || (self.state.asymmetry.x < 0.0 && x < 0.0) {
            x *= 2.0f32.powf(self.state.asymmetry.x.abs());
        }

        if x.abs() <= self.state.gate.x {
            x = 0.0;
        }

        if self.state.cushion.x > 0.0 {
            x += self.state.cushion.x * ((x * std::f32::consts::FRAC_PI_2).sin() - x);
        }

        x = x.min(self.state.hardclip.x).max(-self.state.hardclip.x);

        if self.state.crossover.x > self.state.crossover.low {
            x = if self.state.crossover.x < 0.0 {
                2.0 / (1.0 - self.state.crossover.x) * x.max(self.state.crossover.x)
                    - (1.0 + self.state.crossover.x) / 4.0
            } else {
                2.0 * x.max(-self.state.crossover.x).abs() - 0.25
            }
        }

        if self.state.crush.x > 0.0 {
            let f = 2f32.powf(16.0 * (1.0 - self.state.crush.x));
            x = (x * f).round() / f;
        }

        self.x = x;

        self.x
    }
}
