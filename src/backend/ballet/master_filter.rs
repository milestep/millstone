// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use core::f32::consts::PI;

pub struct MasterFilter {
    dctrap: DCTrap,
    filter1: Filter,
    filter2: Filter,
}

impl MasterFilter {
    pub fn new() -> Self {
        Self {
            dctrap: DCTrap::new(),
            filter1: Filter::new(),
            filter2: Filter::new(),
        }
    }

    pub fn panic(&mut self) {
        self.filter1.panic();
        self.filter2.panic();
        self.dctrap.panic();
    }

    pub fn process(
        &mut self,
        value: f32,
        active_channels: &[bool; 8],
        frame: &mut [f32; 8],
        dt: f32,
    ) {
        let value = value.max(-1.0).min(1.0);

        let freq = if value >= 0.0 {
            25.0f32.powf(1.0 - value) * 22050.0f32.powf(value)
        } else {
            5.0f32.powf(-value) * 22050.0f32.powf(1.0 + value)
        };

        let omega0 = (PI * 2.0 * freq * dt).min(0.999 * PI);
        let sin0 = omega0.sin().abs();
        let cos0 = omega0.cos();

        let alpha = sin0 / 1.2;

        let a0 = 1.0 + alpha;
        let a1 = -2.0 * cos0;
        let a2 = 1.0 - alpha;

        let k = 1.0 + value.signum() * cos0;

        let b0 = k / 2.0;
        let b1 = -(value.signum() + cos0);
        let b2 = k / 2.0;

        let interp = (2.0 * value.abs()).min(1.0).max(0.0);

        self.filter1
            .process(a0, a1, a2, b0, b1, b2, interp, active_channels, frame);
        self.filter2
            .process(a0, a1, a2, b0, b1, b2, interp, active_channels, frame);
        self.dctrap.process(active_channels, frame);
    }
}

struct DCTrap {
    alpha: f32,
    xs: [[f32; 2]; 8],
    out: [f32; 8],
}

impl DCTrap {
    pub fn new() -> Self {
        Self {
            alpha: 0.9958,
            xs: [[0.0; 2]; 8],
            out: [0.0; 8],
        }
    }

    pub fn panic(&mut self) {
        for idx in 0..8 {
            self.xs[idx].fill(0.0);
            self.out[idx] = 0.0;
        }
    }

    pub fn process(&mut self, active_channels: &[bool; 8], frame: &mut [f32; 8]) {
        for (idx, active) in active_channels.iter().enumerate() {
            if !active {
                continue;
            }

            self.xs[idx][0] = frame[idx];
            self.out[idx] = self.alpha * (self.out[idx] + self.xs[idx][0] - self.xs[idx][1]);
            self.xs[idx][1] = self.xs[idx][0];
        }
    }
}

struct Filter {
    x: [[f32; 2]; 8],
    y: [[f32; 2]; 8],
}

impl Filter {
    pub fn new() -> Self {
        Self {
            x: [[0.0; 2]; 8],
            y: [[0.0; 2]; 8],
        }
    }

    pub fn panic(&mut self) {
        for idx in 0..8 {
            self.x[idx].fill(0.0);
            self.y[idx].fill(0.0);
        }
    }

    #[allow(clippy::too_many_arguments)]
    pub fn process(
        &mut self,
        a0: f32,
        a1: f32,
        a2: f32,
        b0: f32,
        b1: f32,
        b2: f32,
        interp: f32,
        active_channels: &[bool; 8],
        frame: &mut [f32; 8],
    ) {
        for (idx, active) in active_channels.iter().enumerate() {
            if !active {
                continue;
            }

            let num = b1 / a0 * self.x[idx][0] + b2 / a0 * self.x[idx][1];
            let den = a1 / a0 * self.y[idx][0] + a2 / a0 * self.y[idx][1];

            self.x[idx] = [frame[idx], self.x[idx][0]];
            let filtered_frame = b0 / a0 * frame[idx] + num - den;
            self.y[idx] = [filtered_frame, self.y[idx][0]];

            frame[idx] = (1.0 - interp) * frame[idx] + interp * filtered_frame;
        }
    }
}
