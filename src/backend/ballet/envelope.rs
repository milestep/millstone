// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use rand::rngs::SmallRng;
use rand::{Rng, SeedableRng};

use crate::ballet::EnvelopeState;

pub struct Envelope {
    pub state: EnvelopeState,
    rng: SmallRng,
    delay: f32,
    t_release: Option<f32>,
    t_s: f32,
    n: f32,
    x: f32,
    x0: f32,
    x0_trigger: f32,
    y0: f32,
    r0: f32,
    sustain: f32,
    sustain_stage: bool,
}

impl Envelope {
    pub fn new(state: EnvelopeState) -> Envelope {
        Self {
            state,
            rng: SmallRng::from_entropy(),
            delay: 0.0,
            t_release: None,
            t_s: 0.0,
            n: 0.0,
            x: 0.0,
            x0: 0.0,
            x0_trigger: 0.0,
            y0: 0.0,
            r0: 0.0,
            sustain: 0.0,
            sustain_stage: false,
        }
    }

    pub fn panic(&mut self) {
        self.delay = 0.0;
        self.t_release = None;
        self.t_s = 0.0;
        self.n = 0.0;
        self.x = 0.0;
        self.x0 = 0.0;
        self.x0_trigger = 0.0;
        self.y0 = 0.0;
        self.r0 = 0.0;
        self.sustain = 0.0;
        self.sustain_stage = false;
    }

    pub fn trigger(&mut self, delay: f32) {
        self.t_release = None;
        self.delay = delay;
        self.sustain_stage = false;

        self.x0 *= self.y0;
        self.x0_trigger = self.x0;

        self.t_s = 0.0;
        self.n = 0.0;
        self.x = 0.0;
        self.r0 = if self.state.scatter.x > 0.0 {
            self.state.scatter.x * self.rng.gen_range(0.0..1.0)
        } else {
            0.0
        };
        self.y0 = 1.0;
    }

    pub fn release(&mut self, t: f32) {
        self.t_release = Some(t);
    }

    pub fn process(&mut self, t: f32, dt: f32) -> f32 {
        let t1 = self.delay;
        let t2 = if self.state.attack.x > self.state.attack.low {
            t1 + self.state.attack.x
        } else {
            t1
        };
        let t3 = if self.state.hold.x > self.state.hold.low {
            t2 + self.state.hold.x
        } else {
            t2
        };
        let t4 = if self.state.decay.x > self.state.decay.low {
            t3 + self.state.decay.x
        } else {
            t3
        };

        self.sustain = if self.sustain_stage {
            if self.state.slope.x > 0.0 {
                (self.state.sustain.x - (t - t4) * (1.0 - self.state.slope.x).log2())
                    .max(0.0)
                    .min(1.0)
            } else if self.state.slope.x < 0.0 {
                (self.state.sustain.x + (t - t4) * (1.0 + self.state.slope.x).log2())
                    .max(0.0)
                    .min(1.0)
            } else {
                self.state.sustain.x
            }
        } else {
            self.state.sustain.x
        };

        if let Some(t_release) = self.t_release {
            if (t - t_release) > self.state.release.x {
                self.x = 0.0;
                return 0.0;
            }

            self.y0 *= 1.0 - (dt / self.state.release.x * 8.0).min(1.0);
        } else if t >= t1 {
            if self.t_s > t4 - t1 {
                self.n += 1.0;
                self.t_s -= t4 - t1;

                if self.state.scatter.x > 0.0 {
                    self.r0 = self.state.scatter.x * self.rng.gen_range(0.0..1.0);
                }
            }

            if t <= t2 {
                let a = (t2 - t) / self.state.attack.x;
                self.x0 = a * self.x0_trigger + (1.0 - a) * (1.0 - self.r0);
            } else if t <= t3.max(t2 + dt) {
                self.x0 = 1.0 - self.r0;
            } else if t > t3 {
                let a = 1.0 - (dt / self.state.decay.x * 8.0).min(1.0);
                self.x0 = a * self.x0 + (1.0 - a) * self.sustain * (1.0 - self.r0);
            }

            if t > t4 {
                self.sustain_stage = true;
            }
        }

        if self.sustain_stage {
            if self.state.echo.x > self.state.echo.low {
                if self.t_s <= t2 - t1 {
                    let a = self.t_s / self.state.attack.x;
                    self.x = a * self.state.echo.x.powf(self.n) * (1.0 - self.r0);
                } else if self.t_s <= (t3 - t1).max(t2 - t1 + dt) {
                    self.x = self.state.echo.x.powf(self.n) * (1.0 - self.r0);
                } else if self.t_s > t3 - t1 {
                    self.x *= 1.0 - (dt / self.state.decay.x * 8.0).min(1.0);
                }
            } else {
                self.x = 0.0;
            }
        } else {
            self.x = 0.0;
        }

        if t >= t1 {
            self.t_s += dt;
        }

        (self.y0 * (self.x0 + self.x * (1.0 - self.x0)))
            .min(1.0)
            .max(0.0)
    }
}
