// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::ballet::TunedDelayState;

#[derive(PartialEq)]
enum SampleMode {
    Sample,
    Hold,
}

pub struct TunedDelay {
    pub state: TunedDelayState,
    t: f32,
    period: f32,
    period_size: f32,
    tick: f32,
    input_buffer: Vec<f32>,
    sample_mode: SampleMode,
    buffer: Vec<f32>,
    buffer_idx: usize,
    input_idx: usize,
    hold_idx: usize,
    alpha: f32,
    out: f32,
    xs: [f32; 2],
}

impl TunedDelay {
    pub fn new(state: TunedDelayState) -> Self {
        Self {
            state,
            t: 0.0,
            period: 0.0,
            period_size: 0.0,
            tick: 0.0,
            input_buffer: Vec::with_capacity(44100),
            sample_mode: SampleMode::Sample,
            buffer: Vec::with_capacity(16 * 44100),
            buffer_idx: 0,
            input_idx: 0,
            hold_idx: 0,
            alpha: 0.9998,
            out: 0.0,
            xs: [0.0, 0.0],
        }
    }

    pub fn trigger(&mut self) {
        self.period_size = 0.0;
    }

    pub fn reset(&mut self) {
        self.t = 0.0;
        self.period = 0.0;
        self.tick = 0.0;
        self.sample_mode = SampleMode::Sample;
        // We set the buffer index to the last index, so that, on the
        // next processing step, this will be set to zero.
        self.buffer.clear();
        self.buffer_idx = 0;
        self.input_idx = 0;
        self.out = 0.0;
        self.period_size = 0.0;
        self.xs = [0.0, 0.0];
    }

    pub fn panic(&mut self) {
        self.input_buffer.clear();
        self.reset();
        self.hold_idx = 0;
        self.t = 0.0;
        self.tick = 0.0;
        self.period = 0.0;
        self.period_size = 0.0;
    }

    #[allow(clippy::too_many_arguments)]
    pub fn process(
        &mut self,
        base_frequency: f32,
        dt: f32,
        osc1: f32,
        osc2: f32,
        osc3: f32,
        noise: f32,
        voice: f32,
    ) -> f32 {
        let mut sample = self.state.osc1.x * osc1
            + self.state.osc2.x * osc2
            + self.state.osc3.x * osc3
            + self.state.noise.x * noise
            + self.state.voice.x * voice;

        let mut freq = self.state.frequency.x
            * ((1.0 - self.state.track.x) / 27.5 + self.state.track.x * base_frequency / 440.0);
        if freq < 1.0 {
            freq = 1.0;
        }

        let length = self.buffer.capacity();
        let input_length = self.input_buffer.capacity();

        while self.buffer_idx >= self.buffer.len() {
            self.buffer.push(0.0);
        }

        while self.input_idx >= self.input_buffer.len() {
            self.input_buffer.push(0.0);
        }

        self.period_size = if self.period_size == 0.0 {
            1.0 / (dt * freq)
        } else {
            1.0 + (1.0 - (dt * freq).min(1.0)) * self.period_size
        };

        self.period += 1.0 / self.period_size;

        if self.period >= 1.0 {
            if self.sample_mode == SampleMode::Sample {
                // Store the final sample for smooth interpolation
                // near the end.
                self.input_buffer[self.input_idx] = sample;
                self.hold_idx = self.input_idx;
                self.sample_mode = SampleMode::Hold;
            }

            self.period -= 1.0;

            if self.state.sampling.x > self.state.sampling.low {
                self.tick += self.state.sampling.x;
            }

            if self.tick >= 1.0 {
                self.sample_mode = SampleMode::Sample;
                self.tick -= 1.0;
            }
        }

        match self.sample_mode {
            SampleMode::Sample => {
                self.input_buffer[self.input_idx] = sample;
            }
            SampleMode::Hold => {
                let idx = self.period * self.period_size;
                let hold_idx = input_length + self.hold_idx - self.period_size.ceil() as usize;
                let idx_lo = (hold_idx + idx.floor() as usize) % input_length;
                let idx_hi = (idx_lo + 1) % input_length;

                let a = idx - idx.floor();
                sample = (1.0 - a) * self.input_buffer.get(idx_lo).map_or(0.0, |x| *x)
                    + a * self.input_buffer.get(idx_hi).map_or(0.0, |x| *x);
            }
        }

        // Increment `input_idx` after we're done using it.
        self.input_idx = (self.input_idx + 1) % input_length;

        self.buffer[self.buffer_idx] = sample;

        if self.state.feedback.x != 0.0 {
            let delay = length as f32 - self.period_size;
            let delayed_idx_lo = (self.buffer_idx + delay.floor() as usize) % length;
            let delayed_idx_hi = (delayed_idx_lo + 1) % length;
            let a = delay - delay.floor();

            // To avoid issues with feedback at even fractions of the Nyquist frequency,
            // interpolate between the midpoints. This is effectively a kind of low-pass
            // filter.
            let x0 = self.buffer.get(delayed_idx_lo).map_or(0.0, |x| *x);
            let x1 = self.buffer.get(delayed_idx_hi).map_or(0.0, |x| *x);
            let x = (1.0 - a) * x0 + a * x1;

            let delayed_sample =
                if self.state.filter_frequency.x == self.state.filter_frequency.high {
                    x
                } else {
                    let k = dt * self.state.filter_frequency.x;
                    let b = k / (1.0 + k);
                    self.out + b * (x - self.out)
                };

            // Apply high-pass filter to remove DC offset.
            self.xs[0] = sample + self.state.feedback.x * delayed_sample;
            self.out = self.alpha * (self.out + self.xs[0] - self.xs[1]);
            self.xs[1] = self.xs[0];

            self.buffer[self.buffer_idx] = self.out;
        }

        self.t = (self.t + (self.state.speed.x - 1.0) / self.period_size).rem_euclid(1.0);

        let delay = length as f32 - self.period_size + self.t * self.period_size;
        let delayed_idx_lo_1 =
            (self.buffer_idx + (delay - self.period_size).floor() as usize) % length;
        let delayed_idx_lo_2 = (self.buffer_idx + delay.floor() as usize) % length;
        let delayed_idx_hi_1 = (delayed_idx_lo_1 + 1) % length;
        let delayed_idx_hi_2 = (delayed_idx_lo_2 + 1) % length;

        // Increment `buffer_idx` after we're done using it.
        self.buffer_idx = (self.buffer_idx + 1) % length;

        let a_1 = (delay - self.period_size) - (delay - self.period_size).floor();
        let a_2 = delay - delay.floor();

        let x_1 = (1.0 - a_1) * self.buffer.get(delayed_idx_lo_1).map_or(0.0, |x| *x)
            + a_1 * self.buffer.get(delayed_idx_hi_1).map_or(0.0, |x| *x);
        let x_2 = (1.0 - a_2) * self.buffer.get(delayed_idx_lo_2).map_or(0.0, |x| *x)
            + a_2 * self.buffer.get(delayed_idx_hi_2).map_or(0.0, |x| *x);

        let blend = if self.state.blend.x == self.state.blend.low {
            self.t
        } else {
            let c = (-self.state.blend.x).exp();
            ((self.state.blend.x * (self.t - 1.0)).exp() - c) / (1.0 - c)
        };

        blend * x_1 + (1.0 - blend) * x_2
    }
}
