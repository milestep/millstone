// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::ballet::ShaperState;

pub struct Shaper {
    pub state: ShaperState,
    t_release: Option<f32>,
    pub x: f32,
    x_trigger: f32,
    pub delay: f32,
    strums: usize,
    can_trigger: bool,
    pub triggered: bool,
}

impl Shaper {
    pub fn new(state: ShaperState) -> Shaper {
        Self {
            state,
            t_release: None,
            x: 0.0,
            x_trigger: 0.0,
            delay: 0.0,
            strums: 0,
            can_trigger: false,
            triggered: false,
        }
    }

    pub fn panic(&mut self) {
        self.t_release = None;
        self.x = 0.0;
        self.x_trigger = 0.0;
        self.delay = 0.0;
        self.strums = 0;
        self.triggered = false;
        self.can_trigger = false;
    }

    pub fn is_done(&self) -> bool {
        self.t_release.is_some() && self.x == 0.0
    }

    pub fn strum(&mut self) {
        if !self.triggered {
            self.strums += 1;
        }
    }

    pub fn strum_decay(&mut self) {
        self.strums = 0;
    }

    pub fn trigger(&mut self) {
        self.can_trigger = false;
        self.triggered = false;
        self.t_release = None;
        self.x_trigger = self.x;

        self.delay = if self.state.delay.x > self.state.delay.low {
            self.strums as f32 * self.state.delay.x
        } else {
            0.0
        };

        self.strums = 0;
    }

    pub fn release(&mut self, t: f32) {
        self.t_release = Some(t);
        self.delay = 0.0;
        self.can_trigger = false;
        self.triggered = false;
    }

    pub fn process(&mut self, t: f32, dt: f32, velocity: f32) -> f32 {
        let t1 = self.delay;
        let t2 = t1 + self.state.attack.x;
        let t3 = t2 + self.state.hold.x;

        let x0 = if self.state.sense.x > 0.0 {
            1.0 + self.state.sense.x * (velocity - 1.0)
        } else {
            1.0
        };

        if let Some(t_release) = self.t_release {
            if t - t_release > self.state.release.x {
                self.x = 0.0;
                return 0.0;
            }

            self.x *= 1.0 - (dt / self.state.release.x * 8.0).min(1.0);
        } else if t >= t1 {
            if self.state.trigger.x <= self.state.trigger.low {
                self.can_trigger = true;
            }

            if self.can_trigger && self.state.trigger.x >= self.state.trigger.high {
                self.triggered = true;
            }

            if t <= t2 {
                let a = (t2 - t) / self.state.attack.x;
                self.x = a * self.x_trigger + (1.0 - a) * x0;
            } else if t > t3 {
                let a = 1.0 - (dt / self.state.decay.x * 8.0).min(1.0);
                self.x = a * self.x + (1.0 - a) * self.state.sustain.x * x0;
            }
        } else {
            self.x *= 1.0 - (dt / self.state.release.x * 8.0).min(1.0);
        }

        self.x
    }
}
