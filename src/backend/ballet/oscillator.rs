// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::ballet::OscillatorState;

pub struct Oscillator {
    pub state: OscillatorState,
    t: f32,
    x: f32,
}

impl Oscillator {
    pub fn new(state: OscillatorState) -> Self {
        Self {
            state,
            t: 0.0,
            x: 0.0,
        }
    }

    pub fn trigger(&mut self) {
        self.t = 0.0;
        self.x = 0.0;
    }

    pub fn process(&mut self, base_frequency: f32, dt: f32) -> f32 {
        let freq =
            self.state.frequency.x * (1.0 + self.state.track.x * (base_frequency / 440.0 - 1.0));

        if freq == 0.0 {
            return 0.0;
        }

        self.t = (self.t + freq * dt) % 1.0;

        let mut fs = [0.0, 0.0, 0.0, 0.0, 0.0];
        let mut t = self.t + self.state.phase.x + self.state.delay.x;

        let pivot = 0.5 * (1.0 + self.state.asymmetry.x);
        let k = 1.0 / (1.0 - self.state.width.x);

        for f in fs.iter_mut() {
            t = (t - self.state.delay.x).rem_euclid(1.0);

            *f = if t < self.state.width.x {
                -1.0
            } else {
                if self.state.width.x > self.state.width.low {
                    t = (k * (t - self.state.width.x)) % 1.0;
                }

                let x = if t <= pivot {
                    -1.0 + 2.0 * t / pivot
                } else {
                    1.0 - 2.0 * (t - pivot) / (1.0 - pivot)
                };

                self.state.gap.x * x.signum() + (1.0 - self.state.gap.x) * x
            };

            if self.state.delay.x == self.state.delay.low {
                break;
            }
        }

        let mut x = if self.state.delay.x == self.state.delay.low {
            fs[0]
        } else {
            fs[1] + 0.5 * (fs[0] - fs[2]) - 0.25 * (fs[1] - fs[3]) + 0.125 * (fs[2] - fs[4])
        };

        if self.state.melt.x > self.state.melt.low {
            x = (1.0 - self.state.melt.x) * x + self.state.melt.x * (2.0 * x.abs() - 1.0);
        }

        if self.state.cushion.x > self.state.cushion.low {
            x += self.state.cushion.x * ((x * std::f32::consts::FRAC_PI_2).sin() - x);
        }

        self.x = x;

        self.x
    }
}
