// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

mod accumulator;
mod compressor;
mod delay;
mod envelope;
mod filter;
mod lfo;
mod master_filter;
mod noise;
mod oscillator;
mod pitch_tracker;
mod rms;
mod shaper;
mod short_delay;
mod sidechain;
mod tuned_delay;
mod vocal_processor;
mod voice;
mod waveshaper;

#[cfg(test)]
mod tests;

use crossbeam::channel::{Receiver, RecvError, Sender, TryRecvError};

use crate::ballet::{MasterParameter, MasterPatch, MasterState, Parameter};
use crate::messages::{Action, BalletMessage};
use crate::perf;
use crate::synth::Modulation;

pub fn spawn(
    action_tx: Sender<Action>,
    ballet_rx: Receiver<BalletMessage>,
    in_tx: Sender<Vec<f32>>,
    out_rx: Receiver<Vec<f32>>,
) {
    std::thread::spawn(move || {
        let mut synth = Synth::new();
        synth.run(action_tx, ballet_rx, in_tx, out_rx);
    });
}

// Tunings are:
// 0 - equal temperament
// 1 - just intonation 1
// 2.. not implemented
pub fn get_freq_correction(freq: f32, notes: &[bool; 12], root_note: usize, tuning: usize) -> f32 {
    let note = (12.0 * (freq / 440.0).log2() - 3.0).rem_euclid(12.0);
    let mut correction = 12.0f32;

    let detuning = match tuning {
        1 => [
            0.0, 0.11731, 0.0391, 0.15641, -0.13686, -0.01955, -0.09776, 0.01955, 0.13686,
            -0.15641, -0.0391, -0.11731,
        ],
        2 => [
            0.0, -0.0978, 0.0391, -0.0587, 0.0782, -0.01955, -0.1173, 0.01955, -0.0782, 0.0587,
            -0.0391, 0.0978,
        ],
        3 => [
            0.0, 0.0496, 0.0391, -0.0247, -0.1369, -0.2922, 0.2827, 0.01955, -0.2737, 0.05865,
            -0.3117, -0.1173,
        ],
        4 => [0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0],
        5 => [
            0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, -1.0,
        ],
        6 => [0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0, 0.0, -1.0, 1.0, 0.0],
        7 => [0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0],
        _ => [0.0; 12],
    };

    for (patch_note, &enabled) in notes.iter().enumerate() {
        if enabled {
            let detune = detuning[(patch_note + 12 - root_note) % 12];

            if (patch_note as f32 - note).abs() < correction.abs() {
                correction = patch_note as f32 - note + detune;
            } else if (12.0 + patch_note as f32 - note).abs() < correction.abs() {
                correction = 12.0 + patch_note as f32 - note + detune;
            }
        }
    }

    if correction.abs() > 6.0 {
        correction = 0.0;
    }

    correction
}

struct Synth {
    pub patch: MasterPatch,
    pub state: MasterState,
    pub compressor: compressor::Compressor,
    pub delay: delay::Delay,
    pub voices: Vec<voice::Voice>,
    voice_cycle: usize,
    input_axes: [f32; 24],
    axes: [f32; 24],
    // Rate reduction
    counter: usize,
    time_scale: f32,
    // External input
    input_rms: rms::Rms<1>,
    talk: bool,
    vocal_processor: vocal_processor::VocalProcessor,
    pitch_tracker: pitch_tracker::PitchTracker<3, 6>,
    // Effects
    sidechain: sidechain::Sidechain,
    lfo1: lfo::Lfo,
    lfo2: lfo::Lfo,
    acc1: accumulator::Accumulator,
    acc2: accumulator::Accumulator,
    master_filter: master_filter::MasterFilter,
    tick: f32,
    work_frame: [f32; 8],
    hold_frame: [f32; 8],
    delay_in_frame: [f32; 8],
    delay_out_frame: [f32; 8],
}

impl Synth {
    pub fn new() -> Self {
        let patch = MasterPatch::new();

        let state = patch.scene.master.clone();
        let delay = delay::Delay::new(patch.scene.delay.clone());
        let compressor = compressor::Compressor::new(patch.scene.compressor.clone());

        let sidechain = sidechain::Sidechain::new(patch.scene.sc.clone());
        let lfo1 = lfo::Lfo::new(patch.scene.lfo1.clone());
        let lfo2 = lfo::Lfo::new(patch.scene.lfo2.clone());
        let acc1 = accumulator::Accumulator::new(patch.scene.acc1.clone());
        let acc2 = accumulator::Accumulator::new(patch.scene.acc2.clone());

        let vocal_processor =
            vocal_processor::VocalProcessor::new(patch.scene.vocal_processor.clone());
        let pitch_tracker = pitch_tracker::PitchTracker::new(patch.scene.pitch_tracker.clone());

        Self {
            patch,
            state,
            compressor,
            delay,
            voices: vec![
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
                voice::Voice::new(),
            ],
            voice_cycle: 0,
            input_axes: [0.0; 24],
            axes: [0.0; 24],
            // Rate reduction,
            counter: 0,
            time_scale: 0.0,
            // External input
            input_rms: rms::Rms::new(0.01),
            talk: false,
            vocal_processor,
            pitch_tracker,
            // Effects
            sidechain,
            lfo1,
            lfo2,
            acc1,
            acc2,
            master_filter: master_filter::MasterFilter::new(),
            tick: 0.0,
            work_frame: [0.0f32; 8],
            hold_frame: [0.0f32; 8],
            delay_in_frame: [0.0f32; 8],
            delay_out_frame: [0.0f32; 8],
        }
    }

    pub fn load_patch(&mut self, patch: MasterPatch) {
        self.reset_modulations();

        self.compressor.state.set_from(&patch.scene.compressor);
        self.delay.state.set_from(&patch.scene.delay);
        self.sidechain.state.set_from(&patch.scene.sc);
        self.lfo1.state.set_from(&patch.scene.lfo1);
        self.lfo2.state.set_from(&patch.scene.lfo2);
        self.vocal_processor
            .state
            .set_from(&patch.scene.vocal_processor);

        self.state.set_from(&patch.scene.master);

        self.patch = patch;
    }

    pub fn set_param(&mut self, param: MasterParameter, value: f32) {
        match param {
            MasterParameter::Master(dest) => self.state.get_mut(dest).set(value),
            MasterParameter::Compressor(dest) => self.compressor.state.get_mut(dest).set(value),
            MasterParameter::Delay(dest) => self.delay.state.get_mut(dest).set(value),
            MasterParameter::Sidechain(dest) => self.sidechain.state.get_mut(dest).set(value),
            MasterParameter::Lfo1(dest) => self.lfo1.state.get_mut(dest).set(value),
            MasterParameter::Lfo2(dest) => self.lfo2.state.get_mut(dest).set(value),
            MasterParameter::Acc1(dest) => self.acc1.state.get_mut(dest).set(value),
            MasterParameter::Acc2(dest) => self.acc2.state.get_mut(dest).set(value),
            MasterParameter::VocalProcessor(dest) => {
                self.vocal_processor.state.get_mut(dest).set(value)
            }
            MasterParameter::PitchTracker(dest) => {
                self.pitch_tracker.state.get_mut(dest).set(value)
            }
        }
    }

    pub fn set_modulation(&mut self, modulation: Modulation<MasterParameter>) {
        self.reset_modulations();
        self.patch
            .set_modulation_params(modulation.idx, modulation.destination, modulation.params);
    }

    pub fn morph(&mut self, axis: usize, amount: f32) {
        self.input_axes[axis] = amount;
    }

    pub fn trigger_sidechain(&mut self, channel: usize, velocity: f32) {
        if self.patch.sidechain_channel == channel {
            self.sidechain.trigger(velocity);
        }
    }

    pub fn reset_modulations(&mut self) {
        for modulation in self.patch.modulations.iter() {
            match modulation.destination {
                MasterParameter::Master(dest) => self.state.get_mut(dest).reset(),
                MasterParameter::Compressor(dest) => self.compressor.state.get_mut(dest).reset(),
                MasterParameter::Delay(dest) => self.delay.state.get_mut(dest).reset(),
                MasterParameter::Sidechain(dest) => self.sidechain.state.get_mut(dest).reset(),
                MasterParameter::Lfo1(dest) => self.lfo1.state.get_mut(dest).reset(),
                MasterParameter::Lfo2(dest) => self.lfo2.state.get_mut(dest).reset(),
                MasterParameter::Acc1(dest) => self.acc1.state.get_mut(dest).reset(),
                MasterParameter::Acc2(dest) => self.acc2.state.get_mut(dest).reset(),
                MasterParameter::VocalProcessor(dest) => {
                    self.vocal_processor.state.get_mut(dest).reset()
                }
                MasterParameter::PitchTracker(dest) => {
                    self.pitch_tracker.state.get_mut(dest).reset()
                }
            }
        }
    }

    pub fn next_state(&mut self) {
        self.reset_modulations();

        for modulation in self.patch.modulations.iter() {
            let amount = modulation.get(&self.axes);

            if amount == 0.0 {
                continue;
            }

            match modulation.destination {
                MasterParameter::Master(dest) => self.state.get_mut(dest).modulate(amount),
                MasterParameter::Compressor(dest) => {
                    self.compressor.state.get_mut(dest).modulate(amount)
                }
                MasterParameter::Delay(dest) => self.delay.state.get_mut(dest).modulate(amount),
                MasterParameter::Sidechain(dest) => {
                    self.sidechain.state.get_mut(dest).modulate(amount)
                }
                MasterParameter::Lfo1(dest) => self.lfo1.state.get_mut(dest).modulate(amount),
                MasterParameter::Lfo2(dest) => self.lfo2.state.get_mut(dest).modulate(amount),
                MasterParameter::Acc1(dest) => self.acc1.state.get_mut(dest).modulate(amount),
                MasterParameter::Acc2(dest) => self.acc2.state.get_mut(dest).modulate(amount),
                MasterParameter::VocalProcessor(dest) => {
                    self.vocal_processor.state.get_mut(dest).modulate(amount)
                }
                MasterParameter::PitchTracker(dest) => {
                    self.pitch_tracker.state.get_mut(dest).modulate(amount)
                }
            }
        }
    }

    fn run(
        &mut self,
        action_tx: Sender<Action>,
        ballet_rx: Receiver<BalletMessage>,
        in_tx: Sender<Vec<f32>>,
        out_rx: Receiver<Vec<f32>>,
    ) {
        perf::set_realtime_thread_priority(89);

        // Workload measurement
        let mut workload_accumulator = 0;
        let mut workload_max = 0;
        let mut workload_counter = 0;

        let max_panic_frames = 512;
        let mut panic_frames = 0;
        let mut panic_triggered = false;

        self.lfo1.trigger();
        self.lfo2.trigger();

        loop {
            let mut time_instant = std::time::Instant::now();

            loop {
                match ballet_rx.try_recv() {
                    Ok(BalletMessage::Panic) => {
                        panic_frames = max_panic_frames;
                        panic_triggered = true;

                        #[cfg(feature = "ballet-debug")]
                        action_tx
                            .send(Action::Log("[ballet] Panic!".to_string()))
                            .unwrap();
                    }
                    Ok(BalletMessage::LoadMasterPatch { patch, panic, mute }) => {
                        self.load_patch(*patch);

                        if panic {
                            panic_frames = max_panic_frames;
                            panic_triggered = true;
                        } else if mute {
                            self.voices.iter_mut().for_each(|voice| voice.mute());
                        }
                        #[cfg(feature = "ballet-debug")]
                        action_tx
                            .send(Action::Log(format!("[ballet] LoadMasterPatch {}", panic)))
                            .unwrap();
                    }
                    Ok(BalletMessage::LoadPatch { voice_idx, patch }) => {
                        self.voices[voice_idx].load_patch(*patch);
                    }
                    Ok(BalletMessage::UnloadPatch { voice_idx }) => {
                        self.voices[voice_idx].unload_patch();
                    }
                    Ok(BalletMessage::SetParam { param, value }) => {
                        match param {
                            Parameter::Master(dest) => {
                                self.set_param(dest, value);
                            }
                            Parameter::Voice(channel, dest) => {
                                for voice in self.voices.iter_mut() {
                                    if voice.has_channel(channel) {
                                        voice.set_param(dest, value);
                                    }
                                }
                            }
                        }

                        #[cfg(feature = "ballet-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[ballet] SetParam {:?} {}",
                                param, value
                            )))
                            .unwrap();
                    }
                    Ok(BalletMessage::SetNote { note, value }) => {
                        if self.patch.get_note(note) != value {
                            self.patch.toggle_note(note);
                        }

                        #[cfg(feature = "ballet-debug")]
                        action_tx
                            .send(Action::Log(format!("[ballet] SetNote {} {}", note, value)))
                            .unwrap();
                    }
                    Ok(BalletMessage::SetModulation(modulation)) => {
                        match modulation.destination {
                            Parameter::Master(dest) => {
                                self.set_modulation(modulation.with_destination(dest));

                                #[cfg(feature = "ballet-debug")]
                                action_tx
                                    .send(Action::Log(format!(
                                        "[ballet] Length of modulations: {:?}",
                                        self.patch.modulations.len(),
                                    )))
                                    .unwrap();
                            }
                            Parameter::Voice(channel, dest) => {
                                for voice in self.voices.iter_mut() {
                                    if voice.has_channel(channel) {
                                        voice.set_modulation(modulation.with_destination(dest));
                                    }
                                }
                            }
                        }

                        #[cfg(feature = "ballet-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[ballet] SetModulation {:?}",
                                modulation
                            )))
                            .unwrap();
                    }
                    Ok(BalletMessage::SetConfig {
                        channel,
                        dest,
                        value,
                    }) => {
                        for voice in self.voices.iter_mut() {
                            if voice.has_channel(channel) {
                                voice.set_config(dest, value);
                            }
                        }

                        #[cfg(feature = "ballet-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[ballet] SetConfig {} {:?} {}",
                                channel, dest, value
                            )))
                            .unwrap();
                    }
                    Ok(BalletMessage::SetMasterConfig { dest, value }) => {
                        self.patch.set_config(dest, value);

                        #[cfg(feature = "ballet-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[ballet] SetMasterConfig {:?} {}",
                                dest, value
                            )))
                            .unwrap();
                    }
                    Ok(BalletMessage::SetTalk(talk)) => {
                        self.talk = talk;
                    }
                    Ok(BalletMessage::Morph {
                        channel,
                        id,
                        axis,
                        amount,
                    }) => {
                        if axis < 7 {
                            if channel == 0 && id.group_id == 0 && id.voice_id == 0 {
                                self.morph(axis, amount);
                            }
                        } else {
                            for voice in self.voices.iter_mut() {
                                if voice.has_id(channel, id) {
                                    voice.morph(axis, amount);
                                }
                            }
                        }

                        #[cfg(feature = "ballet-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[ballet] Morph {} {:?} {} {}",
                                channel, id, axis, amount
                            )))
                            .unwrap();

                        #[cfg(feature = "ballet-debug")]
                        action_tx
                            .send(Action::Log(format!("[ballet] Morph {:?}", &self.axes)))
                            .unwrap();
                    }
                    Ok(BalletMessage::NoteOn {
                        channel,
                        id,
                        frequency,
                        velocity,
                        mute,
                        latch,
                    }) => {
                        self.morph(6, ((frequency / 27.5).log2() / 8.0).min(1.0).max(0.0));

                        let mut best_idx = self.voice_cycle;

                        while self.voices[best_idx].patch.channel != channel {
                            best_idx = (best_idx + 1) % self.voices.len();

                            if best_idx == self.voice_cycle {
                                break;
                            }
                        }

                        let mut best_is_active = self.voices[best_idx].active;
                        let mut best_is_held = self.voices[best_idx].held;
                        let mut best_score = self.voices[best_idx].frequency_distance(frequency);

                        for (idx, voice) in self.voices.iter_mut().enumerate() {
                            if voice.patch.channel != channel {
                                continue;
                            }

                            if (mute || latch) && voice.is_latched() {
                                voice.mute();
                            }

                            if best_score == 1.0 {
                                continue;
                            }

                            let score = voice.frequency_distance(frequency);

                            if score > 1.0 && !best_is_active {
                                continue;
                            }

                            if !voice.active
                                || (!voice.held && best_is_held)
                                || (!voice.held && score < best_score)
                            {
                                best_idx = idx;
                                best_is_active = voice.active;
                                best_is_held = voice.held;
                                best_score = score;
                            }
                        }

                        // If the voice we found listens to the right channel, activate!
                        if !mute && (channel == 0 || self.voices[best_idx].patch.channel == channel)
                        {
                            self.voices[best_idx].trigger(id, frequency, velocity);
                            self.voice_cycle = (best_idx + 1) % self.voices.len();

                            for voice in self.voices.iter_mut() {
                                if voice.patch.channel == channel {
                                    voice.strum();
                                }
                            }

                            #[cfg(feature = "ballet-debug")]
                            action_tx
                                .send(Action::Log(format!(
                                    "[ballet] NoteOn {} {:?} {} {} {}",
                                    channel, id, frequency, velocity, best_idx
                                )))
                                .unwrap();
                        }
                    }
                    Ok(BalletMessage::NoteOff {
                        channel,
                        id,
                        latch,
                        velocity,
                    }) => {
                        for voice in self.voices.iter_mut() {
                            if voice.has_id(channel, id) {
                                voice.release(id, velocity, latch);
                            }
                        }

                        #[cfg(feature = "ballet-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[ballet] NoteOff {} {:?} {} {}",
                                channel, id, latch, velocity
                            )))
                            .unwrap();
                    }
                    Ok(BalletMessage::Sidechain { channel, velocity }) => {
                        self.trigger_sidechain(channel, velocity);

                        #[cfg(feature = "ballet-debug")]
                        action_tx
                            .send(Action::Log(format!(
                                "[ballet] Sidechain {} {}",
                                channel, velocity
                            )))
                            .unwrap();
                    }
                    Err(TryRecvError::Empty) => break,
                    Err(TryRecvError::Disconnected) => panic!("Channel disconnected"),
                }
            }

            if panic_triggered {
                for _ in 0..panic_frames {
                    in_tx.send(vec![0f32; 8]).unwrap();
                }
                panic_triggered = false;
            }

            let mut elapsed = time_instant.elapsed().as_nanos() as i128;

            // Receive a frame from the mixer.
            let frame = match out_rx.recv() {
                Ok(value) => value,
                Err(RecvError) => panic!("channel disconnected"),
            };

            time_instant = std::time::Instant::now();

            // Process and send the frame back to the mixer.
            if panic_frames == 0 {
                in_tx.send(self.process(frame)).unwrap();
            } else {
                let subdiv = 16;

                if (max_panic_frames - panic_frames) % subdiv == 0 {
                    let panic_idx = (max_panic_frames - panic_frames) / subdiv;

                    match panic_idx {
                        0..=15 => self.voices[panic_idx].panic(),
                        16..=19 => {
                            self.delay.diffusers[panic_idx - 16].panic();
                            self.delay.delay_lines[panic_idx - 16].panic();
                        }
                        20 => {
                            self.vocal_processor.panic();
                            self.pitch_tracker.panic();
                        }
                        21 => self.compressor.panic(),
                        22 => {
                            self.acc1.panic();
                            self.acc2.panic();
                        }
                        23 => self.master_filter.panic(),
                        24 => self.work_frame.fill(0.0),
                        25 => self.delay_in_frame.fill(0.0),
                        26 => self.delay_out_frame.fill(0.0),
                        28 => {
                            self.tick = 0.0;
                            self.input_axes.fill(0.0);
                            self.axes.fill(0.0);
                        }
                        29 => self.lfo1.panic(),
                        30 => self.lfo2.panic(),
                        _ => (),
                    }
                }

                panic_frames -= 1;
            }

            elapsed += time_instant.elapsed().as_nanos() as i128;
            workload_accumulator += elapsed;

            if elapsed > 22675 {
                workload_max += 1;
            }

            workload_counter += 1;

            if workload_counter >= 44100 {
                let percentage = workload_accumulator / 10_000_000;
                let max = workload_max;

                action_tx
                    .send(Action::Workload {
                        identifier: 1,
                        percentage,
                        max,
                    })
                    .unwrap();

                workload_accumulator = 0;
                workload_max = 0;
                workload_counter = 0;
            }
        }
    }

    fn process(&mut self, mut frame: Vec<f32>) -> Vec<f32> {
        let rate_divider = self.patch.rate_divider + 1;

        if self.counter == 0 {
            for (hold_sample, work_sample) in
                self.hold_frame.iter_mut().zip(self.work_frame.iter_mut())
            {
                *hold_sample = *work_sample;
                *work_sample = 0.0;
            }

            // Smoothing factor of 5 ms
            let alpha = 200.0 / 44100.0;
            self.time_scale = (1.0 - alpha) * self.time_scale + alpha * self.state.time_scale.x;

            let dt = self.time_scale * rate_divider as f32 * 1.0 / 44100.0;

            self.next_frame(&frame, dt);
        }

        let a = (self.counter + 1) as f32 / rate_divider as f32;

        for (sample, (work_sample, hold_sample)) in frame
            .iter_mut()
            .zip(self.work_frame.iter().zip(self.hold_frame.iter()))
        {
            *sample = (1.0 - a) * *hold_sample + a * *work_sample;
        }

        self.counter = (self.counter + 1) % rate_divider;

        frame
    }

    fn next_frame(&mut self, frame: &[f32], dt: f32) {
        let alpha = 1.0 - (dt / 0.001).min(1.0);
        self.axes[2] = alpha * self.axes[2] + (1.0 - alpha) * self.input_axes[2];
        self.axes[3] = alpha * self.axes[3] + (1.0 - alpha) * self.input_axes[3];
        self.axes[4] = alpha * self.axes[4] + (1.0 - alpha) * self.input_axes[4];
        self.axes[5] = alpha * self.axes[5] + (1.0 - alpha) * self.input_axes[5];

        self.next_state();

        let mut active_channels = [false; 8];
        let (vocal_left_channel, vocal_right_channel) = (6, 7);

        self.axes[6] = self.acc1.process(dt);
        self.axes[7] = self.acc2.process(dt);

        self.axes[23] = self.sidechain.process(dt);

        self.axes[14] = self.lfo1.process(dt);
        self.axes[15] = self.lfo2.process(dt);

        let mut input_sample = frame[self.patch.input_source];

        let (vocal_left, vocal_right) = if self.talk {
            let (left, right) = (input_sample, input_sample);
            input_sample = 0.0;
            (left, right)
        } else if self.patch.disable_vocal == 0 {
            // Process the input sample using a pre-allocated buffer for the RMS.
            self.input_rms.process(&[input_sample], dt);

            let rms = self.input_rms.mean_square[0].sqrt();

            let (freq, affinity) = if self.patch.disable_pitch_tracker == 0 {
                self.pitch_tracker.process(input_sample, dt)
            } else {
                (27.5, 1.0)
            };

            let correction = get_freq_correction(
                freq,
                &self.patch.scale,
                self.patch.root_note,
                self.patch.tuning,
            );

            // `0.0` at f == 27.5, `1.0` at f == 7040.0
            self.axes[18] = (freq / 27.5).log2() / 8.0;
            self.axes[19] = (correction / 12.0 + 0.5).min(1.0).max(0.0);
            self.axes[20] = affinity;

            let (left, right, dry) = self.vocal_processor.process(&mut input_sample, rms, dt);

            self.axes[21] = (dry + 0.5).max(0.0).min(1.0);

            self.axes[22] = 10.0f32
                .powf(self.vocal_processor.signal / 20.0)
                .min(1.0)
                .max(0.0);

            let delay = if self.vocal_processor.state.delay_send.x
                > self.vocal_processor.state.delay_send.low
            {
                self.vocal_processor.state.delay_send.x
            } else {
                0.0
            };

            if delay > self.vocal_processor.state.delay_send.low {
                self.delay_in_frame[vocal_left_channel] += delay * left;
                self.delay_in_frame[vocal_right_channel] += delay * right;
            }

            active_channels[vocal_left_channel] = true;
            active_channels[vocal_right_channel] = true;

            if self.state.vocal.x > self.state.vocal.low {
                (self.state.vocal.x * left, self.state.vocal.x * right)
            } else {
                (0.0, 0.0)
            }
        } else {
            self.axes[18] = 0.0;
            self.axes[19] = 0.0;
            self.axes[20] = 0.0;
            self.axes[21] = 0.0;
            self.axes[22] = 0.0;
            (0.0, 0.0)
        };

        self.axes[8] = 0.5;

        for voice in self.voices.iter_mut() {
            active_channels[voice.patch.output_channel_left] = true;
            active_channels[voice.patch.output_channel_right] = true;

            if !voice.active {
                voice.strum_decay();
                continue;
            }

            let (left, right) = voice.process(
                &self.axes,
                &self.patch.scale,
                self.patch.root_note,
                self.patch.tuning,
                input_sample,
                dt,
            );

            self.axes[8] += (0.25 * left + 0.25 * right).min(1.0).max(0.0);

            let clean = voice.state.send.x;
            let delay = voice.state.delay_send.x;

            if self.state.clean.x > self.state.clean.low {
                self.work_frame[voice.patch.output_channel_left] +=
                    self.state.clean.x * clean * left;
                self.work_frame[voice.patch.output_channel_right] +=
                    self.state.clean.x * clean * right;
            }

            if delay > voice.state.delay_send.low {
                self.delay_in_frame[voice.patch.output_channel_left] += delay * left;
                self.delay_in_frame[voice.patch.output_channel_right] += delay * right;
            }
        }

        if self.patch.disable_delay == 0 {
            self.delay.process(
                dt,
                &active_channels,
                &self.delay_in_frame,
                &mut self.delay_out_frame,
            );

            if self.state.delay.x > self.state.delay.low {
                for (delay_sample, sample) in
                    self.delay_out_frame.iter().zip(self.work_frame.iter_mut())
                {
                    *sample += self.state.delay.x * *delay_sample;
                }
            }

            self.delay_in_frame.fill(0.0);
        }

        if self.patch.disable_compressor == 0 {
            self.compressor
                .process(&active_channels, &mut self.work_frame, dt);
        }

        if self.patch.disable_flt == 0 {
            self.master_filter.process(
                self.state.filter.x,
                &active_channels,
                &mut self.work_frame,
                dt,
            );
        }

        self.axes[9] = 0.5;

        for sample in self.work_frame.iter_mut() {
            *sample = if self.state.gain.x > self.state.gain.low {
                (*sample * self.state.gain.x).min(1.0).max(-1.0)
            } else {
                0.0
            };

            self.axes[9] += 0.25 * *sample;
        }

        self.axes[9] = self.axes[9].min(1.0).max(0.0);

        // Mix in the vocal post-filter.
        self.work_frame[vocal_left_channel] += vocal_left;
        self.work_frame[vocal_right_channel] += vocal_right;
    }
}
