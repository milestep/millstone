// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use std::f32::consts::PI;

use crate::ballet::PitchTrackerState;

pub struct PitchTracker<const N: usize, const M: usize> {
    pub state: PitchTrackerState,
    pub freq: f32,
    pub candidate_freq: f32,
    pub affinity: f32,
    pub variance: f32,
    integrators: [Integrator; M],
    interval_tracker: IntervalTracker<N, M>,
    rate_reducer: RateReducer,
    integrator_order: [usize; M],
}

impl<const N: usize, const M: usize> PitchTracker<N, M> {
    pub fn new(state: PitchTrackerState) -> Self {
        Self {
            state,
            freq: 440.0,
            candidate_freq: 440.0,
            affinity: 0.0,
            variance: 0.0,
            integrators: core::array::from_fn(|_| Integrator::new()),
            interval_tracker: IntervalTracker::new(),
            rate_reducer: RateReducer::new(),
            integrator_order: core::array::from_fn(|i| i),
        }
    }

    pub fn panic(&mut self) {
        self.freq = 440.0;
        self.candidate_freq = 440.0;
        self.affinity = 0.0;

        for integrator in self.integrators.iter_mut() {
            integrator.panic()
        }

        self.interval_tracker.panic();
        self.rate_reducer.panic();
    }

    pub fn process(&mut self, sample: f32, dt: f32) -> (f32, f32) {
        let (sample, rate_divider, skip) = self.rate_reducer.process(
            sample,
            dt,
            self.state.max_freq.x,
            self.state.freq_low.x,
            self.state.freq_high.x,
        );

        if skip {
            return (self.freq, self.affinity);
        }

        let reduced_dt = rate_divider * dt;

        if self.interval_tracker.process(
            sample,
            reduced_dt,
            self.state.decay.x,
            self.state.signal_threshold.x,
        ) {
            self.integrator_order.sort_unstable_by(|&a, &b| {
                self.integrators[b]
                    .affinity
                    .partial_cmp(&self.integrators[a].affinity)
                    .unwrap()
            });

            let mut taken_interval_indices = [usize::MAX; M];

            for &integrator_idx in self.integrator_order.iter() {
                let integrator_interval = 1.0 / self.integrators[integrator_idx].freq;

                let mut best_interval = std::f32::INFINITY;
                let mut best_idx = 0;

                for (idx, &interval) in self.interval_tracker.cumulated_intervals.iter().enumerate()
                {
                    if taken_interval_indices.contains(&idx) {
                        continue;
                    }

                    if (integrator_interval - interval).abs()
                        < (integrator_interval - best_interval).abs()
                    {
                        best_interval = interval;
                        best_idx = idx;
                    }
                }

                taken_interval_indices[integrator_idx] = best_idx;
                self.integrators[integrator_idx].set_target_freq(
                    1.0 / best_interval,
                    self.state.min_freq.x,
                    self.state.max_freq.x,
                );
            }
        }

        let alpha = 1.0 - reduced_dt * self.freq;

        self.affinity *= alpha;

        let prev_candidate_freq = self.candidate_freq;

        for integrator in self.integrators.iter_mut() {
            let (freq, affinity) = integrator.process(
                sample,
                reduced_dt,
                self.state.noise_level.x,
                self.state.relative_freq_tolerance.x,
            );

            if affinity > self.affinity {
                self.affinity = affinity;

                if affinity > self.state.threshold.x {
                    self.candidate_freq = freq;
                }
            }
        }

        let freq_tolerance =
            self.freq.max(self.candidate_freq) * self.state.relative_freq_tolerance.x;

        let variance = (self.candidate_freq - prev_candidate_freq).abs();
        self.variance = (alpha * self.variance).max(variance);

        if self.variance < freq_tolerance {
            self.freq = self.candidate_freq;
        }

        (
            self.freq,
            if self.state.threshold.x == self.state.threshold.low {
                1.0
            } else {
                (self.affinity / self.state.threshold.x).min(1.0).max(0.0)
            },
        )
    }
}

struct Integrator {
    abs: f32,
    sin: f32,
    cos: f32,
    freq: f32,
    affinity: f32,
    target_freq: f32,
    x: f32,
}

impl Integrator {
    pub fn new() -> Self {
        Self {
            abs: 1.0,
            sin: 0.0,
            cos: 0.0,
            freq: 440.0,
            affinity: 0.0,
            target_freq: 440.0,
            x: 0.0,
        }
    }

    pub fn panic(&mut self) {
        self.abs = 1.0;
        self.sin = 0.0;
        self.cos = 0.0;
        self.freq = 440.0;
        self.affinity = 0.0;
        self.target_freq = 440.0;
        self.x = 0.0;
    }

    pub fn set_target_freq(&mut self, mut freq: f32, min_freq: f32, max_freq: f32) {
        if freq < min_freq {
            for k in 0..4 {
                if freq * (k as f32) >= min_freq {
                    freq *= k as f32;
                    break;
                }
            }
        }

        if freq > max_freq {
            for k in 0..4 {
                if freq / (k as f32) <= max_freq {
                    freq /= k as f32;
                    break;
                }
            }
        }

        self.target_freq = freq.max(min_freq).min(max_freq);
    }

    pub fn process(
        &mut self,
        sample: f32,
        dt: f32,
        noise_level: f32,
        relative_freq_tolerance: f32,
    ) -> (f32, f32) {
        self.x = (self.x + self.target_freq * dt) % 1.0;

        let alpha = 1.0 - (dt * self.target_freq).min(1.0);

        self.abs = alpha * self.abs + (1.0 - alpha) * sample.abs();
        self.cos = alpha * self.cos + (1.0 - alpha) * sample * (2.0 * PI * self.x).cos();
        self.sin = alpha * self.sin + (1.0 - alpha) * sample * (2.0 * PI * self.x).cos();
        self.freq = alpha * self.freq + (1.0 - alpha) * self.target_freq;

        let scale = 0.6337 * (self.abs.powi(2) + 10.0f32.powf(noise_level / 20.0));
        let affinity = (self.sin.powi(2) + self.cos.powi(2)) / scale;
        self.affinity = alpha * self.affinity + (1.0 - alpha) * affinity;

        let freq_tolerance = self.target_freq.min(self.freq) * relative_freq_tolerance;

        if (self.target_freq - self.freq).abs() < freq_tolerance {
            (self.freq, self.affinity)
        } else {
            (self.freq, 0.0)
        }
    }
}

struct IntervalTracker<const N: usize, const M: usize> {
    prev_sample: f32,
    t: f32,
    t0: f32,
    intervals: [f32; N],
    cumulated_intervals: [f32; M],
    idx: usize,
}

impl<const N: usize, const M: usize> IntervalTracker<N, M> {
    pub fn new() -> Self {
        Self {
            prev_sample: 0.0,
            t: 1.0,
            t0: 0.0,
            intervals: [1.0 / 40.0; N],
            cumulated_intervals: [1.0; M],
            idx: 0,
        }
    }

    pub fn panic(&mut self) {
        self.prev_sample = 0.0;
        self.t = 1.0;
        self.t0 = 0.0;
        self.intervals.fill(1.0 / 40.0);
        self.cumulated_intervals.fill(1.0);
        self.idx = 0;
    }

    pub fn process(&mut self, sample: f32, dt: f32, decay: f32, threshold: f32) -> bool {
        if self.prev_sample < 0.0 && sample >= 0.0 {
            self.t0 = self.t - dt * sample / (sample - self.prev_sample);

            if self.t0 < decay {
                self.t0 = 0.0
            }
        }

        self.prev_sample = sample;

        let n = self.intervals.len();
        let mut trigger = false;

        if self.t0 > 0.0 && sample > threshold {
            trigger = true;

            self.intervals[self.idx] = self.t0;
            self.t -= self.t0;
            self.t0 = 0.0;

            let mut cumulated_idx = 0;

            for i in 0..n {
                for j in i..n {
                    let interval_idx = (n - j + self.idx) % n;

                    self.cumulated_intervals[cumulated_idx] = if i == j {
                        self.intervals[interval_idx]
                    } else {
                        self.intervals[interval_idx] + self.cumulated_intervals[cumulated_idx - 1]
                    };

                    cumulated_idx += 1;
                }
            }
        }

        self.idx = (self.idx + 1) % n;
        self.t += dt;

        trigger
    }
}

struct RateReducer {
    pub counter: usize,
    sum: f32,
    x0: f32,
    x1: f32,
    lowpass_y: f32,
    highpass_y: f32,
    highpass_x: f32,
}

impl RateReducer {
    pub fn new() -> Self {
        Self {
            counter: 0,
            sum: 0.0,
            x0: 0.0,
            x1: 0.0,
            lowpass_y: 0.0,
            highpass_y: 0.0,
            highpass_x: 0.0,
        }
    }

    pub fn panic(&mut self) {
        self.counter = 0;
        self.sum = 0.0;
        self.x0 = 0.0;
        self.x1 = 0.0;
        self.lowpass_y = 0.0;
        self.highpass_y = 0.0;
        self.highpass_x = 0.0;
    }

    pub fn process(
        &mut self,
        sample: f32,
        dt: f32,
        max_freq: f32,
        freq_low: f32,
        freq_high: f32,
    ) -> (f32, f32, bool) {
        let lowpass_alpha = 2.0 * PI * dt * freq_high / (2.0 * PI * dt * freq_high + 1.0);
        let highpass_alpha = 1.0 / (2.0 * PI * dt * freq_low + 1.0);

        self.highpass_y = highpass_alpha * (self.highpass_y + sample - self.highpass_x);
        self.highpass_x = sample;
        self.lowpass_y = self.lowpass_y + lowpass_alpha * (self.highpass_y - self.lowpass_y);
        self.sum += self.lowpass_y;

        let rate_divider = (1.0 / (4.0 * max_freq * dt)).floor().max(1.0);
        let mut skip = true;

        if self.counter == 0 {
            skip = false;
            self.x0 = self.x1;
            self.x1 = self.sum / rate_divider;
            self.sum = 0.0;
        }

        let a = (self.counter + 1) as f32 / rate_divider;

        self.counter = (self.counter + 1) % rate_divider as usize;

        ((1.0 - a) * self.x0 + a * self.x1, rate_divider, skip)
    }
}
