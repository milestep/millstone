// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

pub struct ShortDelay {
    t: usize,
    buffer: Vec<f32>,
}

impl ShortDelay {
    pub fn new() -> Self {
        Self {
            t: 0,
            buffer: vec![0.0; 2 * 441],
        }
    }

    pub fn panic(&mut self) {
        self.buffer.fill(0.0);
    }

    pub fn process(&mut self, abs_delay: f32, sample: f32) -> (f32, f32) {
        let length = self.buffer.len();
        self.t = (self.t + 1) % length;
        self.buffer[self.t] = sample;

        let delay = length as f32 - (abs_delay * (length as f32 - 1.0));
        let delayed_idx_lo = (self.t + delay.floor() as usize) % length;
        let delayed_idx_hi = (delayed_idx_lo + 1) % length;
        let a = delay - delay.floor();
        let delayed_sample =
            (1.0 - a) * self.buffer[delayed_idx_lo] + a * self.buffer[delayed_idx_hi];

        if abs_delay == 0.0 {
            (sample, sample)
        } else {
            (sample, delayed_sample)
        }
    }
}
