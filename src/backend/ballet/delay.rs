// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::ballet::DelayState;

pub struct Delay {
    pub state: DelayState,
    pub diffusers: Vec<Diffuser>,
    pub delay_lines: Vec<DelayLine>,
    diffusion_constants: [f32; 4],
    work_frame: [f32; 8],
}

impl Delay {
    pub fn new(state: DelayState) -> Delay {
        Self {
            state,
            diffusers: vec![
                Diffuser::new(),
                Diffuser::new(),
                Diffuser::new(),
                Diffuser::new(),
            ],
            delay_lines: vec![
                DelayLine::new(),
                DelayLine::new(),
                DelayLine::new(),
                DelayLine::new(),
            ],
            diffusion_constants: [0.54, 0.41, 0.99, 0.65],
            work_frame: [0.0; 8],
        }
    }

    pub fn process(
        &mut self,
        dt: f32,
        active_channels: &[bool; 8],
        in_frame: &[f32; 8],
        out_frame: &mut [f32; 8],
    ) {
        let diffusion = if self.state.diffusion.x > self.state.diffusion.low {
            self.state.diffusion.x
        } else {
            0.0
        };

        let shimmer = if self.state.shimmer.x > self.state.shimmer.low {
            self.state.shimmer.x
        } else {
            0.0
        };

        for (sample, work_sample) in in_frame.iter().zip(self.work_frame.iter_mut()) {
            *work_sample = *sample;
        }

        for (sample, work_sample) in out_frame.iter_mut().zip(self.work_frame.iter_mut()) {
            *work_sample += self.state.feedback.x * *sample;
            *sample = 0.0;
        }

        if diffusion > 0.0 {
            for (idx, diffuser) in self.diffusers.iter_mut().enumerate() {
                diffuser.process(
                    self.state.diffusion_length.x * self.diffusion_constants[idx],
                    active_channels,
                    &mut self.work_frame,
                    dt,
                );
            }

            for (sample, work_sample) in in_frame.iter().zip(self.work_frame.iter_mut()) {
                *work_sample *= diffusion;
                *work_sample += (1.0 - diffusion) * *sample;
            }
        }

        let wet1 = 1.0;
        let wet2 = self.state.balance.x;
        let wet3 = (-1.0 + 2.0 * wet2).max(0.0).min(1.0);
        let wet4 = (-2.0 + 3.0 * wet2).max(0.0).min(1.0);
        let s = wet1 + wet2 + wet3 + wet4;

        self.delay_lines[0].process(
            DelayParams {
                wet: wet1 / s,
                feedback: self.state.delay1.x,
                delay: self.state.time1.x,
                spread: self.state.spread.x,
                cutoff: self.state.cutoff.x,
                shimmer,
                shimmer_shift: self.state.shimmer_shift.x,
                speed: 1.0,
            },
            active_channels,
            &self.work_frame,
            out_frame,
            dt,
        );

        if wet2 > 0.0 {
            self.delay_lines[1].process(
                DelayParams {
                    wet: wet2 / s,
                    feedback: self.state.delay2.x,
                    delay: self.state.time2.x,
                    spread: -self.state.spread.x,
                    cutoff: self.state.cutoff.x,
                    shimmer,
                    shimmer_shift: self.state.shimmer_shift.x,
                    speed: -1.0,
                },
                active_channels,
                &self.work_frame,
                out_frame,
                dt,
            );
        }

        if wet3 > 0.0 {
            self.delay_lines[2].process(
                DelayParams {
                    wet: wet3 / s,
                    feedback: self.state.delay3.x,
                    delay: self.state.time3.x,
                    spread: self.state.spread.x,
                    cutoff: self.state.cutoff.x,
                    shimmer,
                    shimmer_shift: self.state.shimmer_shift.x,
                    speed: 1.0,
                },
                active_channels,
                &self.work_frame,
                out_frame,
                dt,
            );
        }

        if wet4 > 0.0 {
            self.delay_lines[3].process(
                DelayParams {
                    wet: wet4 / s,
                    feedback: self.state.delay4.x,
                    delay: self.state.time4.x,
                    spread: -self.state.spread.x,
                    cutoff: self.state.cutoff.x,
                    shimmer,
                    shimmer_shift: self.state.shimmer_shift.x,
                    speed: -1.0,
                },
                active_channels,
                &self.work_frame,
                out_frame,
                dt,
            );
        }
    }
}

struct DelayParams {
    wet: f32,
    feedback: f32,
    delay: f32,
    spread: f32,
    cutoff: f32,
    shimmer: f32,
    shimmer_shift: f32,
    speed: f32,
}

pub struct DelayLine {
    t: usize,
    shimmer_t: f32,
    input_buffer: Vec<[f32; 8]>,
    buffer: Vec<[f32; 8]>,
    out: [f32; 8],
    xs: [[f32; 2]; 8],
}

impl DelayLine {
    fn new() -> Self {
        Self {
            t: 0,
            shimmer_t: 0.0,
            input_buffer: Vec::with_capacity(10 * 44100),
            buffer: Vec::with_capacity(10 * 44100),
            out: [0.0; 8],
            xs: [[0.0; 2]; 8],
        }
    }

    pub fn panic(&mut self) {
        self.t = 0;
        self.shimmer_t = 0.0;

        self.buffer.clear();
        self.input_buffer.clear();
        self.out.fill(0.0);

        for x in self.xs.iter_mut() {
            x.fill(0.0);
        }
    }

    fn process(
        &mut self,
        params: DelayParams,
        active_channels: &[bool; 8],
        in_frame: &[f32; 8],
        out_frame: &mut [f32; 8],
        dt: f32,
    ) {
        if self.t >= self.buffer.len() {
            self.buffer.push([0.0; 8]);
        }

        if self.t >= self.input_buffer.len() {
            self.input_buffer.push([0.0; 8]);
        }

        for (sample, input) in self.input_buffer[self.t].iter_mut().zip(in_frame.iter()) {
            *sample = *input;
        }

        let length = self.buffer.capacity();
        let sample_delay = params.delay / dt;
        let shimmer_delay = sample_delay.floor() as usize;

        let delay = 2.0 * length as f32 - sample_delay * (1.0 + params.spread.abs());

        let delayed_idx_lo = self.t + delay.floor() as usize;
        let delayed_idx_hi = delayed_idx_lo + 1;
        let a = delay - delay.floor();

        let input_delay = [params.spread, -params.spread]
            .map(|x| length - ((1.0 + x).max(1.0) * sample_delay).floor() as usize);
        let input_t = input_delay.map(|x| self.t + x);

        for (idx, active) in active_channels.iter().enumerate() {
            if !active {
                continue;
            }

            self.buffer[self.t][idx] = match self.input_buffer.get(input_t[idx % 2] % length) {
                Some(value) => value[idx],
                None => 0.0,
            };

            if params.feedback > 0.0 {
                let x0_lo = match self.buffer.get(delayed_idx_lo % length) {
                    Some(value) => value[idx],
                    None => 0.0,
                };

                let x0_hi = match self.buffer.get(delayed_idx_hi % length) {
                    Some(value) => value[idx],
                    None => 0.0,
                };

                self.buffer[self.t][idx] += params.feedback * ((1.0 - a) * x0_lo + a * x0_hi);
            }

            if params.shimmer > 0.0 {
                let t0 =
                    input_t[idx % 2] + length - shimmer_delay + self.shimmer_t.floor() as usize;
                let t1 =
                    input_t[idx % 2] + length - 2 * shimmer_delay + self.shimmer_t.floor() as usize;

                let x0_lo = match self.input_buffer.get(t0 % length) {
                    Some(value) => value[idx],
                    None => 0.0,
                };

                let x0_hi = match self.input_buffer.get((t0 + 1) % length) {
                    Some(value) => value[idx],
                    None => 0.0,
                };

                let x1_lo = match self.input_buffer.get(t1 % length) {
                    Some(value) => value[idx],
                    None => 0.0,
                };

                let x1_hi = match self.input_buffer.get((t1 + 1) % length) {
                    Some(value) => value[idx],
                    None => 0.0,
                };

                let x0 = 0.5 * (x0_lo + x0_hi);
                let x1 = 0.5 * (x1_lo + x1_hi);
                let a = self.shimmer_t / shimmer_delay as f32;

                self.buffer[self.t][idx] += params.shimmer * ((1.0 - a) * x0 + a * x1);
            }

            // Apply high-pass filter to remove DC offset.
            self.xs[idx][0] = self.buffer[self.t][idx];

            self.out[idx] = params.cutoff * ((self.out[idx] - self.xs[idx][1]) + self.xs[idx][0]);
            self.xs[idx][1] = self.xs[idx][0];
        }

        for (out_sample, sample) in out_frame.iter_mut().zip(self.out.iter()) {
            *out_sample += params.wet * sample;
        }

        self.t = (self.t + 1) % length;
        self.shimmer_t = (self.shimmer_t + (params.speed * params.shimmer_shift - 1.0))
            .rem_euclid(shimmer_delay as f32);
    }
}

pub struct Diffuser {
    t: usize,
    input_buffer: Vec<[f32; 8]>,
    buffer: Vec<[f32; 8]>,
}

impl Diffuser {
    fn new() -> Self {
        Self {
            t: 0,
            input_buffer: Vec::with_capacity(44100),
            buffer: Vec::with_capacity(44100),
        }
    }

    pub fn panic(&mut self) {
        self.t = 0;
        self.buffer.clear();
        self.input_buffer.clear();
    }

    fn process(
        &mut self,
        delay: f32,
        active_channels: &[bool; 8],
        work_frame: &mut [f32; 8],
        dt: f32,
    ) {
        if self.t >= self.buffer.len() {
            self.buffer.push([0.0; 8]);
        }

        if self.t >= self.input_buffer.len() {
            self.input_buffer.push([0.0; 8]);
        }

        for (sample, input) in self.input_buffer[self.t].iter_mut().zip(work_frame.iter()) {
            *sample = *input;
        }

        let length = self.buffer.capacity();
        let diffusion_delay = delay / dt;

        let idx_lo = self.t + length - diffusion_delay.floor() as usize;
        let idx_hi = idx_lo + 1;
        let a = diffusion_delay - diffusion_delay.floor();

        for (idx, active) in active_channels.iter().enumerate() {
            if !active {
                continue;
            }

            let x_lo = match self.input_buffer.get(idx_lo % length) {
                Some(value) => value[idx],
                None => 0.0,
            };

            let x_hi = match self.input_buffer.get(idx_hi % length) {
                Some(value) => value[idx],
                None => 0.0,
            };

            let y_lo = match self.buffer.get(idx_lo % length) {
                Some(value) => value[idx],
                None => 0.0,
            };

            let y_hi = match self.buffer.get(idx_hi % length) {
                Some(value) => value[idx],
                None => 0.0,
            };

            let x = (1.0 - a) * x_lo + a * x_hi;
            let y = (1.0 - a) * y_lo + a * y_hi;

            self.buffer[self.t][idx] = x + 0.5 * (self.input_buffer[self.t][idx] - y);
            work_frame[idx] = self.buffer[self.t][idx];
        }

        self.t = (self.t + 1) % length;
    }
}
