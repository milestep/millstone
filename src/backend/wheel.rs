// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use super::frames::Frame;

#[derive(Clone)]
pub struct WheelBuffer<T> {
    pub data: Vec<T>,
    pub t0: usize,
    pub modified: bool,
}

impl<T: Frame + std::clone::Clone + std::marker::Copy> WheelBuffer<T> {
    pub fn new(t0: usize, chunk_size: usize) -> Self {
        Self {
            data: vec![Frame::zero(); chunk_size],
            t0,
            modified: false,
        }
    }

    pub fn contains(&self, t: usize) -> bool {
        t >= self.t0 && t < self.end()
    }

    pub fn end(&self) -> usize {
        self.t0 + self.data.len()
    }

    pub fn read(&self, t: usize) -> T {
        if self.contains(t) {
            return self.data[t - self.t0];
        }
        Frame::zero()
    }

    pub fn write(&mut self, t: usize, sample: T) {
        if self.contains(t) {
            self.data[t - self.t0] = sample;
            self.modified = true;
        }
    }
}

pub struct Wheel<T> {
    data: Vec<WheelBuffer<T>>,
    holdover: Option<WheelBuffer<T>>,
    cursor: usize,
    chunk_size: usize,
    wheel_size: usize,
    contiguous: bool,
    pub flushed: bool,
    lookahead: bool,
    loop_start: usize,
    loop_stop: usize,
}

impl<T: Frame + std::clone::Clone + std::marker::Copy> Wheel<T> {
    pub fn new(chunk_size: usize, wheel_size: usize) -> Self {
        Self {
            data: vec![WheelBuffer::new(0, chunk_size); wheel_size],
            holdover: None,
            cursor: 0,
            chunk_size,
            wheel_size,
            contiguous: false,
            flushed: true,
            lookahead: false,
            loop_start: 0,
            loop_stop: 0,
        }
    }

    pub fn is_flushing(&self) -> bool {
        self.holdover.is_some()
    }

    pub fn set_lookahead(&mut self, lookahead: bool) {
        if self.lookahead != lookahead {
            self.contiguous = false;
        }
        self.lookahead = lookahead;
    }

    pub fn set_loop_points(&mut self, start: usize, stop: usize) {
        self.loop_start = start;
        self.loop_stop = stop;
    }

    pub fn status(&self) -> f32 {
        let nvalid: f32 = (0..self.wheel_size)
            .map(|idx| (self.validate(idx) as u8) as f32)
            .sum();
        nvalid / self.wheel_size as f32
    }

    pub fn seek(&mut self, t: usize) -> bool {
        let prev_cursor = self.cursor;
        self.cursor = self.tick_to_region(t);

        if self.loop_start < self.loop_stop {
            let wrapped_t = (t - self.loop_stop) % (self.loop_stop - self.loop_start);
            if self.contains(self.loop_start + wrapped_t) {
                return true;
            }
        }

        if self.cursor != prev_cursor {
            self.contiguous = false;
        }
        self.contiguous
    }

    pub fn contains(&mut self, t: usize) -> bool {
        let idx = self.tick_to_index(t);
        self.data[idx].contains(t)
    }

    pub fn read(&mut self, t: usize) -> T {
        let idx = self.tick_to_index(t);
        self.data[idx].read(t)
    }

    pub fn write(&mut self, t: usize, frame: T) {
        let idx = self.tick_to_index(t);
        self.data[idx].write(t, frame);
        self.flushed = false;
    }

    pub fn next_request(&mut self) -> usize {
        for i in 0..self.wheel_size {
            let idx = if self.cursor + i < self.end() {
                (self.cursor + i) % self.wheel_size
            } else {
                (self.end() - (1 + i)) % self.wheel_size
            };

            if !self.validate(idx) {
                return self.region_to_tick(self.index_to_region(idx));
            }
        }
        self.contiguous = true;
        self.region_to_tick(self.end())
    }

    pub fn hold(&mut self, buffer: WheelBuffer<T>) {
        self.holdover = Some(buffer);
    }

    pub fn swap(&mut self, new_buffer: WheelBuffer<T>) -> WheelBuffer<T> {
        // Validate that the new buffer belongs in the wheel, otherwise, simply return the
        // same buffer back again, since we have no use for it.
        let region = self.tick_to_region(new_buffer.t0);
        if region < self.start() || region >= self.end() {
            return new_buffer;
        }

        // The buffer fits into the wheel, so we actually swap it out with the old buffer.
        let idx = self.tick_to_index(new_buffer.t0);
        self.data.push(new_buffer);
        self.data.swap_remove(idx)
    }

    pub fn flush(&mut self, buffer: WheelBuffer<T>) -> WheelBuffer<T> {
        // If we have no holdover, the incoming buffer is the first as part of the flush
        // cycle and needs to be held until we are done flushing.
        match self.holdover {
            Some(_) => {
                let buffer = self.swap(buffer);
                self.data.push(buffer);
            }
            None => self.hold(buffer),
        }

        // Find the next modified buffer and return it.
        for idx in 0..self.wheel_size {
            if self.data[idx].modified {
                return self.data.swap_remove(idx);
            }
        }

        // None of the buffers in data are modified, so we are done flushing and return the
        // holdover.
        self.flushed = true;
        self.holdover.take().unwrap()
    }

    fn tick_to_region(&self, t: usize) -> usize {
        t / self.chunk_size
    }

    fn tick_to_index(&self, t: usize) -> usize {
        self.tick_to_region(t) % self.wheel_size
    }

    fn index_to_region(&self, idx: usize) -> usize {
        let region = if idx < self.end() % self.wheel_size {
            self.end()
        } else {
            self.start()
        };
        idx + (region / self.wheel_size) * self.wheel_size
    }

    fn region_to_tick(&self, region: usize) -> usize {
        region * self.chunk_size
    }

    //////////////////////////////////////////////////
    // For a wheel size of 6, we have the following scenarios:
    //
    //      v
    //  0 1 2 3 4 5 6 7 8 9
    // |   valid   |
    //  0 1 2 3 4 5 0 1 2 3
    //
    //          v
    //  0 1 2 3 4 5 6 7 8 9
    //   |   valid   |
    //  0 1 2 3 4 5 0 1 2 3
    //
    //              v
    //  0 1 2 3 4 5 6 7 8 9
    //       |   valid   |
    //  0 1 2 3 4 5 0 1 2 3
    //
    //////////////////////////////////////////////////

    fn start(&self) -> usize {
        if self.lookahead {
            self.cursor
        } else if self.cursor > self.wheel_size / 2 {
            self.cursor - self.wheel_size / 2
        } else {
            0
        }
    }

    fn end(&self) -> usize {
        if self.lookahead {
            self.cursor + self.wheel_size
        } else if self.cursor > self.wheel_size / 2 {
            self.cursor + self.wheel_size / 2
        } else {
            self.wheel_size
        }
    }

    fn validate(&self, idx: usize) -> bool {
        match self.data.get(idx) {
            Some(buffer) => buffer.t0 / self.chunk_size == self.index_to_region(idx),
            None => false,
        }
    }
}
