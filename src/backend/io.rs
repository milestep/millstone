// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crossbeam::channel::{Receiver, RecvError, Sender};

use std::fs::File;
use std::fs::OpenOptions;
use std::io::{BufReader, BufWriter, Read, Seek, SeekFrom, Write};
use std::path::Path;
use std::thread;

use crate::messages::{Action, Destination};

use super::frames::{Frame, StereoFrame};
use super::wheel::WheelBuffer;
use crate::perf;

pub fn spawn(action_tx: Sender<Action>, channel_request_rx: Receiver<IOMessage<StereoFrame>>) {
    thread::spawn(move || {
        let io_handler = IOHandler::new(action_tx, 8);
        io_handler.run(channel_request_rx);
    });
}

pub enum IOMessage<T> {
    Load {
        idx: usize,
        filename: String,
        channel: Sender<WheelBuffer<T>>,
    },
    Request {
        idx: usize,
        buffer: WheelBuffer<T>,
        t_request: usize,
        flush: bool,
    },
    Overrun(usize),
}

pub struct IOHandler {
    action_tx: Sender<Action>,
    ios: Vec<Option<IO>>,
}

impl IOHandler {
    pub fn new(action_tx: Sender<Action>, size: usize) -> Self {
        Self {
            action_tx,
            ios: Vec::with_capacity(size),
        }
    }

    pub fn run(mut self, channel_request_rx: Receiver<IOMessage<StereoFrame>>) {
        perf::set_normal_thread_min_priority();

        loop {
            // Receive buffer from tape.
            let msg = match channel_request_rx.recv() {
                Ok(msg) => msg,
                Err(RecvError) => panic!("Channel disconnected"),
            };

            match msg {
                IOMessage::Load {
                    idx,
                    filename,
                    channel,
                } => {
                    // Create a new IO object.
                    let io = IO::new(idx, &filename, channel);

                    // Inform the frontend about the file size.
                    io.summarize_data_size(&self.action_tx);

                    // Set the IO object as the handler for track no. `idx`.
                    while idx >= self.ios.len() {
                        self.ios.push(None);
                    }
                    self.ios[idx] = Some(io);
                }
                IOMessage::Request {
                    idx,
                    buffer,
                    t_request,
                    flush,
                } => {
                    let mut connected = true;
                    if let Some(io) = &mut self.ios[idx] {
                        connected = io.process(&self.action_tx, buffer, t_request, flush);
                    }
                    if !connected {
                        self.ios[idx] = None;
                    }
                }
                IOMessage::Overrun(channel) => {
                    self.action_tx
                        .send(Action::Log(format!("IO overrun (channel: {})", channel)))
                        .unwrap();
                }
            }
        }
    }
}

pub struct IO {
    idx: usize,
    reader: BufReader<File>,
    writer: BufWriter<File>,
    visited: Vec<bool>,
    channel_tx: Sender<WheelBuffer<StereoFrame>>,
    sample_rate: usize,
    offset: usize,
    data_size: usize,
}

impl IO {
    pub fn new(idx: usize, filename: &str, channel_tx: Sender<WheelBuffer<StereoFrame>>) -> Self {
        std::fs::create_dir_all(Path::new(&filename).parent().unwrap()).unwrap();

        let mut writer = BufWriter::with_capacity(
            8 * 1024 * 1024,
            OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .truncate(false)
                .open(filename)
                .unwrap(),
        );

        let mut reader = BufReader::new(OpenOptions::new().read(true).open(filename).unwrap());

        // Size of the data section.
        let mut data_size: usize = 0;

        // Offset from start of file stream to the actual data.
        let offset: usize = match read_header(&mut reader, &mut data_size).unwrap() {
            0 => write_header(&mut writer, 0, data_size, 44100).unwrap(),
            offset => offset,
        };

        let visited = vec![false; data_size / 44100];

        // Flush write out.
        writer.flush().unwrap();

        Self {
            idx,
            reader,
            writer,
            visited,
            channel_tx,
            sample_rate: 44100,
            offset,
            data_size,
        }
    }

    pub fn process(
        &mut self,
        action_tx: &Sender<Action>,
        mut buffer: WheelBuffer<StereoFrame>,
        t_request: usize,
        flush: bool,
    ) -> bool {
        // Re-usable memory for use when decoding floating point values.
        let mut bytes = [0u8; 8];

        // If the buffer has been modified, write changes to disk.
        if buffer.modified {
            // Seek to the time indicated by the frame.
            self.writer
                .seek(SeekFrom::Start((self.offset + 8 * buffer.t0) as u64))
                .unwrap();

            for frame in buffer.data.iter() {
                // Write out the frame.
                match self.writer.write(&frame.to_le_bytes()) {
                    Ok(n) => match n {
                        8 => (),
                        _ => action_tx
                            .send(Action::Log(format!(
                                "Write failed, only wrote {} out of 8 bytes",
                                n
                            )))
                            .unwrap(),
                    },
                    Err(_) => panic!("Failed to write bytes"),
                }
            }

            // If we need to grow the file size, update the header.
            if 8 * buffer.end() > self.data_size {
                self.data_size = 8 * buffer.end();
                write_header(
                    &mut self.writer,
                    self.offset,
                    self.data_size,
                    self.sample_rate,
                )
                .unwrap();

                // The file size has changed, so we need to inform the frontend.
                self.summarize_data_size(action_tx);
            }

            // Flush the writer.
            if flush {
                self.writer.flush().unwrap();
            }

            self.summarize_buffer(action_tx, &buffer, true);
        }

        // If the buffer was modified (and therefore just written out) and it already
        // matches the requested time, we do not need to read it back from disk again.
        if !(buffer.modified && buffer.t0 == t_request) {
            // Read data from the file into the buffer to be passed back.
            if 8 * t_request < self.data_size {
                self.reader
                    .seek(SeekFrom::Start((self.offset + 8 * t_request) as u64))
                    .unwrap();
            }

            for (dt, frame) in buffer.data.iter_mut().enumerate() {
                if 8 * (t_request + dt) < self.data_size {
                    *frame = match self.reader.read(&mut bytes) {
                        Ok(n) => match n {
                            0 => Frame::zero(),
                            8 => StereoFrame::from_le_bytes(bytes),
                            _ => {
                                action_tx
                                    .send(Action::Log(format!(
                                        "Malformed floating point value ({} bytes)",
                                        n
                                    )))
                                    .unwrap();
                                Frame::zero()
                            }
                        },
                        Err(_) => panic!("Failed to read bytes"),
                    };
                } else {
                    *frame = Frame::zero();
                }
            }

            buffer.t0 = t_request;

            self.summarize_buffer(action_tx, &buffer, false);
        }

        buffer.modified = false;

        // Send the buffer downstream and return a boolean indicating whether the buffer
        // could be passed on.
        self.channel_tx.send(buffer).is_ok()
    }

    pub fn summarize_data_size(&self, action_tx: &Sender<Action>) {
        action_tx
            .send(Action::FileSize(
                Destination::Track(self.idx),
                self.data_size / 8,
            ))
            .unwrap();
    }

    pub fn summarize_buffer(
        &mut self,
        action_tx: &Sender<Action>,
        buffer: &WheelBuffer<StereoFrame>,
        force: bool,
    ) {
        for (i, chunk) in buffer.data.chunks(44100).enumerate() {
            let t = buffer.t0 + i * 44100;

            let idx = t / 44100;
            while idx >= self.visited.len() {
                self.visited.push(false);
            }

            if !force && self.visited[idx] {
                continue;
            }

            let rms = chunk
                .iter()
                .map(|x| x.absmax())
                .max_by(|x, y| x.partial_cmp(y).unwrap())
                .unwrap();
            let length = 44100;

            action_tx
                .send(Action::LoudnessRegion(
                    Destination::Track(self.idx),
                    t,
                    length,
                    rms,
                ))
                .unwrap();

            self.visited[idx] = true;
        }
    }
}

/// Locate data section, read size and return offset in bytes.
pub fn read_header(
    reader: &mut BufReader<File>,
    data_size: &mut usize,
) -> Result<usize, Box<dyn std::error::Error + 'static>> {
    reader.seek(SeekFrom::Start(0))?;
    let mut offset = 0;
    let mut bytes = [0u8; 4];

    // Locate the data segment.
    loop {
        offset += 4;
        match reader.read(&mut bytes) {
            Ok(n) => match n {
                4 => match bytes {
                    [b'd', b'a', b't', b'a'] => break,
                    _ => continue,
                },
                _ => return Ok(0),
            },
            Err(_) => panic!(),
        }
    }

    // Read the size of the data segment.
    let size = match reader.read(&mut bytes) {
        Ok(n) => match n {
            4 => u32::from_le_bytes(bytes),
            _ => 0,
        },
        Err(_) => panic!(),
    };

    *data_size = size as usize;

    // Shift offset
    offset += 4;

    Ok(offset)
}

/// Generate a WAV header for the file.
fn write_header(
    writer: &mut BufWriter<File>,
    offset: usize,
    data_size: usize,
    sample_rate: usize,
) -> Result<usize, Box<dyn std::error::Error + 'static>> {
    // Seek to the beginning of the file.
    writer.seek(SeekFrom::Start(0))?;

    let mut bytes_written = 0;

    // Write the header.
    bytes_written += writer.write(b"RIFF")?; // RIFF
    bytes_written += writer.write(&((offset + data_size) as u32).to_le_bytes())?; // File size
    bytes_written += writer.write(b"WAVE")?; // WAVE
    bytes_written += writer.write(b"fmt ")?; // fmt
    bytes_written += writer.write(&(16_u32).to_le_bytes())?; // Size of fmt chunk
    bytes_written += writer.write(&(3_u16).to_le_bytes())?; // Format type (3 = float)
    bytes_written += writer.write(&(2_u16).to_le_bytes())?; // Number of channels
    bytes_written += writer.write(&(sample_rate as u32).to_le_bytes())?; // Sample rate
    bytes_written += writer.write(&((8 * sample_rate) as u32).to_le_bytes())?; // Data rate (bytes/sec)
    bytes_written += writer.write(&(8_u16).to_le_bytes())?; // Frame size (bytes)
    bytes_written += writer.write(&(32_u16).to_le_bytes())?; // Bits per sample
    bytes_written += writer.write(b"data")?; // data
    bytes_written += writer.write(&(data_size as u32).to_le_bytes())?; // data

    Ok(bytes_written)
}
