// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crossbeam::channel::Sender;

use crate::messages::{Action, Destination};

use super::analyzer;
#[cfg(feature = "master_effects")]
use super::effects;
use super::frames::{Frame, StereoFrame};

pub struct Bus {
    pub mute: bool,
    pub solo: bool,
    pub fader: f32,
    pub pan: f32,
    destination: Option<Destination>,
    action_tx: Sender<Action>,
    frame_in: StereoFrame,
    frame_out: StereoFrame,
    mul_out: f32,
    rms_value_left: f32,
    rms_value_right: f32,
    peak_value_left: f32,
    peak_value_right: f32,
    rms: analyzer::Rms,
    #[cfg(feature = "master_effects")]
    lowshelf: effects::LowShelf,
    #[cfg(feature = "master_effects")]
    highshelf: effects::HighShelf,
    #[cfg(feature = "master_effects")]
    eq: effects::PeakingEQ,
    #[cfg(feature = "master_effects")]
    compressor: effects::Compressor,
    #[cfg(feature = "master_effects")]
    saturator: effects::Saturator,
    #[cfg(feature = "master_effects")]
    whitenoise: effects::WhiteNoise,
    #[cfg(feature = "master_effects")]
    pub effects: [f32; 8],
}

impl Bus {
    pub fn new(action_tx: Sender<Action>) -> Self {
        Self {
            mute: false,
            solo: false,
            fader: 0.0,
            pan: 0.0,
            destination: None,
            action_tx,
            frame_in: Frame::zero(),
            frame_out: Frame::zero(),
            mul_out: 0.0,
            rms_value_left: 0.0,
            rms_value_right: 0.0,
            peak_value_left: 0.0,
            peak_value_right: 0.0,
            rms: analyzer::Rms::new(0.5),
            #[cfg(feature = "master_effects")]
            lowshelf: effects::LowShelf::new(80.0, -36.0),
            #[cfg(feature = "master_effects")]
            highshelf: effects::HighShelf::new(8000.0, -36.0),
            #[cfg(feature = "master_effects")]
            eq: effects::PeakingEQ::new(1200.0, 1.0, -36.0),
            #[cfg(feature = "master_effects")]
            compressor: effects::Compressor::new(0.005, 0.3, -24.0, 6.0, 4.0),
            #[cfg(feature = "master_effects")]
            saturator: effects::Saturator::new(effects::SaturationAlgorithm::TanhMod),
            #[cfg(feature = "master_effects")]
            whitenoise: effects::WhiteNoise::new(0.125), // rms = 1/8x peak noise for limited bw
            #[cfg(feature = "master_effects")]
            effects: [0.0; 8],
        }
    }

    pub fn set_destination(&mut self, destination: Destination) {
        self.destination = Some(destination);
    }

    pub fn send(&mut self, frame: StereoFrame) {
        self.frame_in += frame;
    }

    pub fn process(&mut self, _t: usize, _play: bool) {
        self.mul_out = 10.0f32.powf(self.fader / 20.0);

        // Effects 0-7
        #[allow(unused_mut)]
        let mut frame = 1.0 * self.frame_in;

        #[cfg(feature = "master_effects")]
        {
            frame = self.whitenoise.process(frame, self.effects[4]);
            frame = self.lowshelf.process(frame, self.effects[0]);
            frame = self.eq.process(frame, self.effects[1], self.effects[2]);
            frame = self.highshelf.process(frame, self.effects[3]);
            frame = self
                .compressor
                .process(frame, self.effects[5], self.effects[6]);
            frame = self.saturator.process(frame, self.effects[7]);
        }

        if self.mute {
            self.rms_value_left = 0.0;
            self.rms_value_right = 0.0;
            self.peak_value_left = 0.0;
            self.peak_value_right = 0.0;
        } else {
            let rms = self.rms.process(self.mul_out * frame);
            self.rms_value_left = self.rms_value_left.max(rms.left.abs());
            self.rms_value_right = self.rms_value_right.max(rms.right.abs());
            self.peak_value_left = self.peak_value_left.max(self.mul_out * frame.left.abs());
            self.peak_value_right = self.peak_value_right.max(self.mul_out * frame.right.abs());
        }

        // Update input output
        self.frame_out = frame;
        self.frame_in = Frame::zero();
    }

    pub fn recv(&mut self) -> StereoFrame {
        if self.mute {
            StereoFrame {
                left: 0.0,
                right: 0.0,
            }
        } else {
            self.mul_out * self.frame_out
        }
    }

    pub fn update(&mut self, dt: usize) {
        let peak_db_left = 20.0 * self.peak_value_left.log10();
        let peak_db_right = 20.0 * self.peak_value_right.log10();
        let rms_db_left = 20.0 * self.rms_value_left.log10();
        let rms_db_right = 20.0 * self.rms_value_right.log10();

        if let Some(destination) = self.destination {
            self.action_tx
                .send(Action::Peak(destination, dt, peak_db_left, peak_db_right))
                .unwrap();
            self.action_tx
                .send(Action::Rms(destination, dt, rms_db_left, rms_db_right))
                .unwrap();
            #[cfg(feature = "master_effects")]
            self.action_tx
                .send(Action::CompressorState(destination, self.compressor.gain))
                .unwrap();
        }

        self.rms_value_left = 0.0;
        self.rms_value_right = 0.0;
        self.peak_value_left = 0.0;
        self.peak_value_right = 0.0;
    }
}
