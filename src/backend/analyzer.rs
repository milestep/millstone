// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crossbeam::channel::{Receiver, RecvError, Sender};
use rustfft::{num_complex::Complex, Fft, FftPlanner};
use std::sync::Arc;

use super::frames::{Frame, StereoFrame};
use crate::messages::{Action, Destination};
use crate::perf;

// Constant factor when converting a f32 to usize for fixed-point calculations.
const FLOAT_TO_INT: f32 = 16777216.0;

pub fn spawn(action_tx: Sender<Action>, data_rx: Receiver<StereoFrame>, size: usize) {
    std::thread::spawn(move || {
        let mut analyzer = Analyzer::new(size);
        analyzer.run(action_tx, data_rx);
    });
}

pub struct Rms {
    size: usize,
    buffer_left: Vec<usize>,
    buffer_right: Vec<usize>,
    sum_left: usize,
    sum_right: usize,
    tick: usize,
}

impl Rms {
    pub fn new(size: f32) -> Self {
        let size = (size * 44100.0) as usize;
        Self {
            size,
            buffer_left: vec![0; size],
            buffer_right: vec![0; size],
            sum_left: 0,
            sum_right: 0,
            tick: 0,
        }
    }
    pub fn process(&mut self, frame: StereoFrame) -> StereoFrame {
        // To avoid having to do the sum of the whole buffer, we keep track of the sum of
        // the squares by only adding and subtracting the square of the samples going in and
        // out.  Due to the low precision of f32, this can lead to issues with drifting of
        // the number over several such cycles, so we store the sum as a "fixed-point" u32
        // with a constant multiplier instead, and convert back to f32 at each step.  We
        // choose a multiplier large enough, that overflow is unlikely.
        self.tick = (self.tick + 1) % self.size;

        self.sum_left -= self.buffer_left[self.tick];
        self.sum_right -= self.buffer_right[self.tick];

        self.buffer_left[self.tick] =
            (frame.get_left().powi(2) * FLOAT_TO_INT) as usize / self.size;
        self.buffer_right[self.tick] =
            (frame.get_right().powi(2) * FLOAT_TO_INT) as usize / self.size;

        self.sum_left += self.buffer_left[self.tick];
        self.sum_right += self.buffer_right[self.tick];

        StereoFrame {
            left: (self.sum_left as f32).sqrt() / FLOAT_TO_INT.sqrt(),
            right: (self.sum_right as f32).sqrt() / FLOAT_TO_INT.sqrt(),
        }
    }
}

pub struct Analyzer {
    size: usize,
    fft_left: Arc<dyn Fft<f32>>,
    fft_right: Arc<dyn Fft<f32>>,
}

impl Analyzer {
    pub fn new(size: usize) -> Self {
        let mut planner = FftPlanner::new();
        let fft_left = planner.plan_fft_forward(2 * size);
        let fft_right = planner.plan_fft_forward(2 * size);

        Self {
            size,
            fft_left,
            fft_right,
        }
    }

    pub fn run(&mut self, action_tx: Sender<Action>, data_rx: Receiver<StereoFrame>) {
        perf::set_realtime_thread_priority(20);

        let mut counter: usize = 0;

        // We allocate buffers twice the size of size, and use a
        // rolling window that includes the data from the last buffer.
        let mut buffer_left = DualBuffer::new(self.size);
        let mut buffer_right = DualBuffer::new(self.size);

        loop {
            let frame = match data_rx.recv() {
                Ok(frame) => frame,
                Err(RecvError) => panic!("Channel disconnected"),
            };

            buffer_left.write(counter, frame.get_left());
            buffer_right.write(counter, frame.get_right());

            counter += 1;

            // When the buffer is full, process.
            if counter >= self.size {
                self.fft_left.process(buffer_left.get_mut_buffer());
                self.fft_right.process(buffer_right.get_mut_buffer());

                let mut real_buffer = Vec::with_capacity(self.size);
                for idx in 0..self.size {
                    let left_norm = buffer_left.get(idx) / (self.size as f32).sqrt();
                    let left_db = 20.0 * left_norm.log10();

                    let right_norm = buffer_right.get(idx) / (self.size as f32).sqrt();
                    let right_db = 20.0 * right_norm.log10();

                    real_buffer.push((left_db + right_db) / 2.0);
                }

                // Send the spectrum analysis to the frontend.
                action_tx
                    .send(Action::SpectrumAnalysis(
                        Destination::Master,
                        self.size,
                        real_buffer,
                    ))
                    .unwrap();

                buffer_left.cycle_buffers();
                buffer_right.cycle_buffers();

                // Finally, reset the counter.
                counter = 0;
            }
        }
    }
}

struct DualBuffer {
    idx: usize,
    size: usize,
    buffers: [Vec<Complex<f32>>; 2],
}

impl DualBuffer {
    fn new(size: usize) -> Self {
        let buffer_1 = vec![
            Complex {
                re: 0.0f32,
                im: 0.0f32
            };
            2 * size
        ];

        let buffer_2 = vec![
            Complex {
                re: 0.0f32,
                im: 0.0f32
            };
            2 * size
        ];

        Self {
            idx: 0,
            size,
            buffers: [buffer_1, buffer_2],
        }
    }

    fn write(&mut self, idx: usize, value: f32) {
        self.buffers[self.idx][idx + self.size].re = value;
        self.buffers[self.idx][idx + self.size].im = 0.0;
        self.buffers[(self.idx + 1) % 2][idx].re = value;
        self.buffers[(self.idx + 1) % 2][idx].im = 0.0;
    }

    fn get(&self, idx: usize) -> f32 {
        self.buffers[self.idx][idx].norm()
    }

    fn get_mut_buffer(&mut self) -> &mut Vec<Complex<f32>> {
        &mut self.buffers[self.idx]
    }

    fn cycle_buffers(&mut self) {
        self.idx = (self.idx + 1) % 2;
    }
}
