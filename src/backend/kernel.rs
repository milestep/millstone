// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

//! The kernel module is responsible for handling connections to the soundcard and starting
//! related threads for audio I/O.
//!
//! The kernel runs in its own thread where it monitors the available soundcards and MIDI
//! hardware.  If a configured soundcard appears, the kernel will attempt to connect to it,
//! set its configured parameters, start the input and output worker threads, and connect
//! them to the mixer.  If the soundcard is disconnected or an unrecoverable state is
//! encountered, the kernel will gracefully disconnect the workers from the mixer and
//! attempt to recover by reconnecting to the soundcard again, when available.

use std::sync::{Arc, Mutex};

use std::time::Duration;

use alsa::pcm::PCM;
use crossbeam::channel;
use crossbeam::channel::{Receiver, RecvError, RecvTimeoutError, Sender, TryRecvError};
use parking_lot::RwLock;

use crate::global_settings::GlobalSettings;
use crate::logger::Logger;
use crate::messages::Action;

use crate::perf;

pub enum KernelMessage {
    MountInput {
        main_rx: Receiver<Vec<f32>>,
        recycle_tx: Sender<Vec<f32>>,
        nchannels: usize,
    },
    MountOutput {
        main_tx: Sender<Vec<f32>>,
        recycle_rx: Receiver<Vec<f32>>,
        nchannels: usize,
        period_size: usize,
    },
}

pub fn spawn(
    logger: Arc<RwLock<Logger>>,
    global_settings: Arc<RwLock<GlobalSettings>>,
    action_tx: Sender<Action>,
    kernel_tx: Sender<KernelMessage>,
) {
    let sample_format_in = match &global_settings.read().sample_format_in[..] {
        "S16_LE" => alsa::pcm::Format::S16LE,
        "S24_3LE" => alsa::pcm::Format::S243LE,
        "S32_LE" => alsa::pcm::Format::S32LE,
        format => panic!("Invalid sample format: {}", format),
    };
    let sample_format_out = match &global_settings.read().sample_format_out[..] {
        "S16_LE" => alsa::pcm::Format::S16LE,
        "S24_3LE" => alsa::pcm::Format::S243LE,
        "S32_LE" => alsa::pcm::Format::S32LE,
        format => panic!("Invalid sample format: {}", format),
    };

    let rate = global_settings.read().rate;
    let period_size = global_settings.read().period_size;
    let periods = global_settings.read().periods;

    let device_lock_in = Arc::new(Mutex::new(()));
    let device_lock_out = Arc::clone(&device_lock_in);

    let logger_in = Arc::clone(&logger);
    let logger_out = Arc::clone(&logger);

    let device_in_pattern = String::from(&global_settings.read().device_in);
    let device_out_pattern = String::from(&global_settings.read().device_out);

    let nchannels_in = global_settings.read().nchannels_in;
    let nchannels_out = global_settings.read().nchannels_out;

    let action_tx_out = action_tx.clone();
    let action_tx_in = action_tx;

    let kernel_tx_out = kernel_tx.clone();
    let kernel_tx_in = kernel_tx;

    std::thread::spawn(move || {
        perf::set_normal_thread_min_priority();

        let action_tx = action_tx_out;
        let kernel_tx = kernel_tx_out;

        loop {
            let audio_output_worker = loop {
                let device_out = loop {
                    if let Some(name) = hw_device_from_pattern(&device_out_pattern) {
                        break name;
                    }
                    std::thread::sleep(Duration::from_secs(1));
                };

                // Lock the device so we only attempt to talk it one at a
                // time (ALSA is not std::thread-safe in this regard).
                let _guard = device_lock_out.lock().unwrap();

                match AudioOutputWorker::new(
                    &logger_out,
                    &device_out,
                    rate,
                    period_size,
                    periods,
                    nchannels_out,
                    sample_format_out,
                )
                .map_err(|e| (e, e.errno()))
                {
                    Ok(res) => break res,
                    Err((_, nix::errno::Errno::EBUSY)) => action_tx
                        .send(Action::Log("[kernel] Output device: Busy".to_string()))
                        .unwrap(),
                    Err((_, nix::errno::Errno::EINVAL)) => action_tx
                        .send(Action::Log(
                            "[kernel] Output device: Unsupported parameters".to_string(),
                        ))
                        .unwrap(),
                    Err(_) => action_tx
                        .send(Action::Log(
                            "[kernel] Output device: Unhandled error".to_string(),
                        ))
                        .unwrap(),
                }
                // Sleep and retry.
                std::thread::sleep(Duration::from_secs(1));
            };

            let (main_tx, main_rx) = channel::unbounded::<Vec<f32>>();
            let (recycle_tx, recycle_rx) = channel::unbounded::<Vec<f32>>();

            kernel_tx
                .send(KernelMessage::MountOutput {
                    main_tx,
                    recycle_rx,
                    nchannels: nchannels_out,
                    period_size,
                })
                .unwrap();

            let audio_output_handle = {
                let action_tx = action_tx.clone();
                std::thread::spawn(move || audio_output_worker.run(action_tx, recycle_tx, main_rx))
            };
            audio_output_handle.join().unwrap();

            action_tx
                .send(Action::Log("[kernel] Output stopped".to_string()))
                .unwrap();
        }
    });

    std::thread::spawn(move || {
        perf::set_normal_thread_min_priority();

        let action_tx = action_tx_in;
        let kernel_tx = kernel_tx_in;

        loop {
            let audio_input_worker = loop {
                let device_in = loop {
                    if let Some(name) = hw_device_from_pattern(&device_in_pattern) {
                        break name;
                    }
                    std::thread::sleep(Duration::from_secs(1));
                };

                // Lock the device so we only attempt to talk it one at a
                // time (ALSA is not std::thread-safe in this regard).
                let _guard = device_lock_in.lock().unwrap();

                match AudioInputWorker::new(
                    &logger_in,
                    &device_in,
                    rate,
                    period_size,
                    periods,
                    nchannels_in,
                    sample_format_in,
                )
                .map_err(|e| (e, e.errno()))
                {
                    Ok(res) => break res,
                    Err((_, nix::errno::Errno::EBUSY)) => action_tx
                        .send(Action::Log("[kernel] Input device: Busy".to_string()))
                        .unwrap(),
                    Err((_, nix::errno::Errno::EINVAL)) => action_tx
                        .send(Action::Log(
                            "[kernel] Input device: Unsupported parameters".to_string(),
                        ))
                        .unwrap(),
                    Err(_) => action_tx
                        .send(Action::Log(
                            "[kernel] Input device: Unhandled error".to_string(),
                        ))
                        .unwrap(),
                }
                // Sleep and retry.
                std::thread::sleep(Duration::from_secs(1));
            };

            let (main_tx, main_rx) = channel::unbounded::<Vec<f32>>();
            let (recycle_tx, recycle_rx) = channel::unbounded::<Vec<f32>>();

            kernel_tx
                .send(KernelMessage::MountInput {
                    main_rx,
                    recycle_tx,
                    nchannels: nchannels_in,
                })
                .unwrap();

            let audio_input_handle = {
                let action_tx = action_tx.clone();
                std::thread::spawn(move || audio_input_worker.run(action_tx, main_tx, recycle_rx))
            };

            audio_input_handle.join().unwrap();

            action_tx
                .send(Action::Log("[kernel] Input stopped".to_string()))
                .unwrap();
        }
    });
}

fn hw_device_from_pattern(pattern: &str) -> Option<String> {
    if pattern == "default" {
        return Some(String::from("default"));
    }
    for card in alsa::card::Iter::new().map(|x| x.unwrap()) {
        if card.get_name().unwrap().contains(pattern) {
            return Some(std::format!("hw:{},0", card.get_index()));
        }
    }
    None
}

//////////////////////////////////////////////////
// AudioInputWorker
//////////////////////////////////////////////////

pub struct AudioInputWorker {
    pcm: PCM,
    period_size: usize,
    nchannels: usize,
    sample_format: alsa::pcm::Format,
}

impl AudioInputWorker {
    fn new(
        logger: &Arc<RwLock<Logger>>,
        device_name: &str,
        rate: usize,
        period_size: usize,
        periods: usize,
        nchannels: usize,
        sample_format: alsa::pcm::Format,
    ) -> Result<AudioInputWorker, alsa::Error> {
        use alsa::pcm::{Access, HwParams};
        use alsa::{Direction, ValueOr};

        let pcm = match PCM::new(device_name, Direction::Capture, false).map_err(|e| (e, e.errno()))
        {
            Ok(pcm) => pcm,
            Err((e, nix::errno::Errno::EINVAL)) => {
                logger.write().log("Input device: failed to open device");
                return Err(e);
            }
            Err((e, _)) => return Err(e),
        };

        {
            let hwp = match HwParams::any(&pcm).map_err(|e| (e, e.errno())) {
                Ok(hwp) => hwp,
                Err((e, nix::errno::Errno::EINVAL)) => {
                    logger
                        .write()
                        .log("Input device: failed to get hardware parameters");
                    return Err(e);
                }
                Err((e, _)) => return Err(e),
            };
            match hwp
                .set_channels(nchannels as u32)
                .map_err(|e| (e, e.errno()))
            {
                Ok(()) => (),
                Err((_, nix::errno::Errno::EINVAL)) => {
                    logger
                        .write()
                        .log("Input device: unsupported number of channels");
                }
                Err((e, _)) => return Err(e),
            };
            match hwp
                .set_rate(rate as u32, ValueOr::Nearest)
                .map_err(|e| (e, e.errno()))
            {
                Ok(()) => (),
                Err((_, nix::errno::Errno::EINVAL)) => {
                    logger.write().log("Input device: unsupported sample rate");
                }
                Err((e, _)) => return Err(e),
            };
            match hwp.set_format(sample_format).map_err(|e| (e, e.errno())) {
                Ok(()) => (),
                Err((_, nix::errno::Errno::EINVAL)) => {
                    logger
                        .write()
                        .log("Input device: unsupported sample format");
                }
                Err((e, _)) => return Err(e),
            };
            hwp.set_access(Access::RWInterleaved)?;
            match hwp
                .set_period_size(period_size as alsa::pcm::Frames, ValueOr::Nearest)
                .map_err(|e| (e, e.errno()))
            {
                Ok(()) => (),
                Err((_, nix::errno::Errno::EINVAL)) => {
                    logger.write().log("Input device: unsupported period size");
                }
                Err((e, _)) => return Err(e),
            };
            match hwp
                .set_periods(periods as u32, ValueOr::Nearest)
                .map_err(|e| (e, e.errno()))
            {
                Ok(()) => (),
                Err((_, nix::errno::Errno::EINVAL)) => {
                    logger
                        .write()
                        .log("Input device: unsupported number of periods");
                }
                Err((e, _)) => return Err(e),
            };

            pcm.hw_params(&hwp)?;
            pcm.prepare()?;

            let hwp = pcm.hw_params_current()?;
            assert_eq!(hwp.get_period_size()?, period_size as alsa::pcm::Frames);
        }

        Ok(AudioInputWorker {
            pcm,
            period_size,
            nchannels,
            sample_format,
        })
    }

    fn run(
        &self,
        action_tx: Sender<Action>,
        tx: Sender<Vec<f32>>,
        rx: Receiver<Vec<f32>>,
    ) -> (Sender<Vec<f32>>, Receiver<Vec<f32>>) {
        // Bind this thread to a single CPU to avoid IRQ interference.
        perf::set_realtime_thread_priority(90);

        match self.sample_format {
            alsa::pcm::Format::S16LE => self.run_impl(action_tx, tx, rx, 2),
            alsa::pcm::Format::S243LE => self.run_impl(action_tx, tx, rx, 3),
            alsa::pcm::Format::S32LE => self.run_impl(action_tx, tx, rx, 4),
            _ => panic!(),
        }
    }

    fn run_impl(
        &self,
        action_tx: Sender<Action>,
        tx: Sender<Vec<f32>>,
        rx: Receiver<Vec<f32>>,
        bytes_per_sample: usize,
    ) -> (Sender<Vec<f32>>, Receiver<Vec<f32>>) {
        let io = self.pcm.io_bytes();
        // let mut buf = vec![num::zero::<T>(); self.nchannels * self.period_size];
        let mut buf = vec![0u8; bytes_per_sample * self.nchannels * self.period_size];
        let normalisation_factor = 2f32.powi(31) - 1.0;

        // Await first frame before starting input stream.
        match rx.recv() {
            Ok(_) => {
                action_tx
                    .send(Action::Log("[kernel] Input started".to_string()))
                    .unwrap();
            }
            Err(RecvError) => panic!("channel disconnected"),
        }

        let mut overruns = 0;

        // Continuously read input from the PCM device and slice it up into f32 frames to be
        // passed to the channel.
        loop {
            // Loop until the read was successful.  Hopefully, we never get an overrun.
            loop {
                match io.readi(&mut buf) {
                    Ok(_) => break,
                    Err(_) => match self.pcm.prepare() {
                        Ok(_) => {
                            overruns += 1;
                            action_tx.send(Action::Overrun(overruns)).unwrap();
                            continue;
                        }
                        Err(_) => return (tx, rx),
                    }, // overrun -> recover
                }
            }

            // Chop the buffer up into frame chunks and process them one by one.
            for frame in buf.chunks(bytes_per_sample * self.nchannels) {
                // Take a frame from the recycling channel or create a new one.
                let mut frame_f32 = match rx.try_recv() {
                    Ok(value) => value,
                    Err(TryRecvError::Empty) => vec![0f32; self.nchannels],
                    Err(TryRecvError::Disconnected) => panic!("channel disconnected"),
                };
                // Convert the input frame to f32, normalize it and write it to the recycled
                // frame.
                for (sample, sample_f32) in frame.chunks(bytes_per_sample).zip(&mut frame_f32) {
                    let mut full_sample_bytes = [0u8; 4];
                    for (fsample, sample) in full_sample_bytes.iter_mut().zip(sample) {
                        *fsample = *sample;
                    }
                    let sample_i32 =
                        i32::from_le_bytes(full_sample_bytes) << (8 * (4 - bytes_per_sample));
                    *sample_f32 = sample_i32 as f32 / normalisation_factor;
                }
                // Pass the recycled frame to the main channel.
                tx.send(frame_f32).unwrap();
            }
        }
    }
}

//////////////////////////////////////////////////
// AudioOutputWorker
//////////////////////////////////////////////////

pub struct AudioOutputWorker {
    pcm: PCM,
    period_size: usize,
    nchannels: usize,
    sample_format: alsa::pcm::Format,
}

impl AudioOutputWorker {
    fn new(
        logger: &Arc<RwLock<Logger>>,
        device_name: &str,
        rate: usize,
        period_size: usize,
        periods: usize,
        nchannels: usize,
        sample_format: alsa::pcm::Format,
    ) -> Result<AudioOutputWorker, alsa::Error> {
        use alsa::pcm::{Access, HwParams};
        use alsa::{Direction, ValueOr};

        let pcm =
            match PCM::new(device_name, Direction::Playback, false).map_err(|e| (e, e.errno())) {
                Ok(pcm) => pcm,
                Err((e, nix::errno::Errno::EINVAL)) => {
                    logger.write().log("Output device: failed to open device");
                    return Err(e);
                }
                Err((e, _)) => return Err(e),
            };

        {
            let hwp = match HwParams::any(&pcm).map_err(|e| (e, e.errno())) {
                Ok(hwp) => hwp,
                Err((e, nix::errno::Errno::EINVAL)) => {
                    logger
                        .write()
                        .log("Output device: failed to get hardware parameters");
                    return Err(e);
                }
                Err((e, _)) => return Err(e),
            };
            match hwp
                .set_channels(nchannels as u32)
                .map_err(|e| (e, e.errno()))
            {
                Ok(()) => (),
                Err((_, nix::errno::Errno::EINVAL)) => {
                    logger
                        .write()
                        .log("Output device: unsupported number of channels");
                }
                Err((e, _)) => return Err(e),
            };
            match hwp
                .set_rate(rate as u32, ValueOr::Nearest)
                .map_err(|e| (e, e.errno()))
            {
                Ok(()) => (),
                Err((_, nix::errno::Errno::EINVAL)) => {
                    logger.write().log("Output device: unsupported sample rate");
                }
                Err((e, _)) => return Err(e),
            };
            match hwp.set_format(sample_format).map_err(|e| (e, e.errno())) {
                Ok(()) => (),
                Err((_, nix::errno::Errno::EINVAL)) => {
                    logger
                        .write()
                        .log("Output device: unsupported sample format");
                }
                Err((e, _)) => return Err(e),
            };
            hwp.set_access(Access::RWInterleaved)?;
            match hwp
                .set_period_size(period_size as alsa::pcm::Frames, ValueOr::Nearest)
                .map_err(|e| (e, e.errno()))
            {
                Ok(()) => (),
                Err((_, nix::errno::Errno::EINVAL)) => {
                    logger.write().log("Output device: unsupported period size");
                }
                Err((e, _)) => return Err(e),
            };
            match hwp
                .set_periods(periods as u32, ValueOr::Nearest)
                .map_err(|e| (e, e.errno()))
            {
                Ok(()) => (),
                Err((_, nix::errno::Errno::EINVAL)) => {
                    logger
                        .write()
                        .log("Output device: unsupported number of periods");
                }
                Err((e, _)) => return Err(e),
            };

            pcm.hw_params(&hwp)?;
            pcm.prepare()?;

            let hwp = pcm.hw_params_current()?;
            assert_eq!(hwp.get_period_size()?, period_size as alsa::pcm::Frames);
        }

        Ok(AudioOutputWorker {
            pcm,
            period_size,
            nchannels,
            sample_format,
        })
    }

    fn run(
        &self,
        action_tx: Sender<Action>,
        tx: Sender<Vec<f32>>,
        rx: Receiver<Vec<f32>>,
    ) -> (Sender<Vec<f32>>, Receiver<Vec<f32>>) {
        perf::set_realtime_thread_priority(86);

        match self.sample_format {
            alsa::pcm::Format::S16LE => self.run_impl(action_tx, tx, rx, 2),
            alsa::pcm::Format::S243LE => self.run_impl(action_tx, tx, rx, 3),
            alsa::pcm::Format::S32LE => self.run_impl(action_tx, tx, rx, 4),
            _ => panic!(),
        }
    }

    fn run_impl(
        &self,
        action_tx: Sender<Action>,
        tx: Sender<Vec<f32>>,
        rx: Receiver<Vec<f32>>,
        bytes_per_sample: usize,
    ) -> (Sender<Vec<f32>>, channel::Receiver<Vec<f32>>) {
        let io = self.pcm.io_bytes();
        let mut buf = vec![0u8; bytes_per_sample * self.nchannels * self.period_size];
        let normalisation_factor = 2f32.powi((8 * bytes_per_sample - 1) as i32) - 1.0;
        let mut underruns = 0;

        action_tx
            .send(Action::Log("[kernel] Output started".to_string()))
            .unwrap();

        loop {
            // Read frames one by one, and insert them into the output buffer.
            for frame in buf.chunks_mut(bytes_per_sample * self.nchannels) {
                // Take a frame from the main channel.
                let frame_f32 = match rx.recv_timeout(std::time::Duration::from_secs(1)) {
                    Ok(value) => value,
                    Err(RecvTimeoutError::Timeout) => return (tx, rx),
                    Err(RecvTimeoutError::Disconnected) => panic!("channel disconnected"),
                };

                // Normalize the frame, convert it to i32, and write it to the buffer.
                for (sample, sample_f32) in frame.chunks_mut(bytes_per_sample).zip(&frame_f32) {
                    let full_sample = (sample_f32.max(-1.0).min(1.0) * normalisation_factor) as i32;
                    for (sample, fsample) in sample.iter_mut().zip(&full_sample.to_le_bytes()) {
                        *sample = *fsample;
                    }
                }
                // Pass the frame back through the recycling channel.
                tx.send(frame_f32).unwrap();
            }

            loop {
                match io.writei(&buf) {
                    Ok(_) => break,
                    Err(_) => match self.pcm.prepare() {
                        Ok(_) => {
                            if underruns > 0 {
                                action_tx.send(Action::Underrun(underruns)).unwrap();
                            }
                            underruns += 1;
                            continue;
                        }
                        Err(_) => return (tx, rx),
                    }, // underrun -> recover
                }
            }
        }
    }
}
