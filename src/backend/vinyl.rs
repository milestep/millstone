// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use std::fs::File;
use std::fs::OpenOptions;
use std::io::{BufWriter, Seek, SeekFrom, Write};
use std::path::Path;

use super::frames::StereoFrame;

pub struct Vinyl {
    writer: BufWriter<File>,
    sample_rate: usize,
    offset: usize,
    data_size: usize,
}

impl Vinyl {
    pub fn new(filename: &str, sample_rate: usize) -> Self {
        std::fs::create_dir_all(Path::new(&filename).parent().unwrap()).unwrap();

        let file = match OpenOptions::new()
            .write(true)
            .read(true)
            .create(true)
            .truncate(false)
            .open(filename)
        {
            Ok(file) => file,
            Err(_) => panic!(),
        };

        let writer = BufWriter::new(file);

        Self {
            writer,
            sample_rate,
            offset: 0,
            data_size: 0,
        }
    }

    pub fn initialize(&mut self) {
        self.offset = self.write_wav_header().unwrap();
    }

    pub fn write(&mut self, frame: StereoFrame) {
        match self.writer.write(&frame.to_le_bytes()) {
            Ok(n) => {
                match n {
                    8 => (),
                    _ => panic!("Write failed, only wrote {} out of 8 bytes", n),
                }
                self.data_size += n;
            }
            Err(_) => panic!("Failed to write bytes"),
        }
    }

    pub fn finalize(&mut self) {
        self.write_wav_header().unwrap();
        self.writer.flush().unwrap();
    }

    fn write_wav_header(&mut self) -> Result<usize, Box<dyn std::error::Error + 'static>> {
        let offset = self.offset;
        let data_size = self.data_size;
        let sample_rate = self.sample_rate;

        // Seek to the beginning of the file.
        self.writer.seek(SeekFrom::Start(0))?;

        let mut bytes_written = 0;

        // Write the header.
        bytes_written += self.writer.write(b"RIFF")?; // RIFF
        bytes_written += self
            .writer
            .write(&((offset + data_size) as u32).to_le_bytes())?; // File size
        bytes_written += self.writer.write(b"WAVE")?; // WAVE
        bytes_written += self.writer.write(b"fmt ")?; // fmt
        bytes_written += self.writer.write(&(16_u32).to_le_bytes())?; // Size of fmt chunk
        bytes_written += self.writer.write(&(3_u16).to_le_bytes())?; // Format type (3 = float)
        bytes_written += self.writer.write(&(2_u16).to_le_bytes())?; // Number of channels
        bytes_written += self.writer.write(&(sample_rate as u32).to_le_bytes())?; // Sample rate
        bytes_written += self
            .writer
            .write(&((8 * sample_rate) as u32).to_le_bytes())?; // Data rate (bytes/sec)
        bytes_written += self.writer.write(&(8_u16).to_le_bytes())?; // Frame size (bytes)
        bytes_written += self.writer.write(&(32_u16).to_le_bytes())?; // Bits per sample
        bytes_written += self.writer.write(b"data")?; // data
        bytes_written += self.writer.write(&(data_size as u32).to_le_bytes())?; // data

        Ok(bytes_written)
    }
}
