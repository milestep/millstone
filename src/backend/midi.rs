// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use alsa::seq;
use alsa::seq::{Addr, PortCap};

use crossbeam::channel::{Receiver, Sender, TryRecvError};

use serde::{Deserialize, Serialize};

use std::error;
use std::thread;
use std::time::Duration;

use crate::messages::{Action, MidiMessage};

use crate::perf;

#[derive(Clone, Deserialize, Serialize)]
pub enum DevicePattern {
    DeviceName(String),
    DeviceNameAndPort(String, i32),
}

pub struct AddrNamePair {
    pub addr: Addr,
    pub name: String,
}

pub fn spawn(
    blacklist_str: &[String],
    action_tx: Sender<Action>,
    inbound_tx: Sender<MidiMessage>,
    outbound_rx: Receiver<MidiMessage>,
) {
    let mut blacklist = vec![];

    for item in blacklist_str.iter() {
        let terms: Vec<&str> = item.split("::").collect();
        match terms.len() {
            1 => blacklist.push(DevicePattern::DeviceName(terms[0].to_string())),
            2 => blacklist.push(DevicePattern::DeviceNameAndPort(
                terms[0].to_string(),
                terms[1].parse::<i32>().unwrap(),
            )),
            _ => (),
        };
    }

    thread::spawn(move || {
        perf::set_normal_thread_min_priority();

        loop {
            let mut result =
                Kernel::new(blacklist.clone(), inbound_tx.clone(), outbound_rx.clone());
            loop {
                if let Ok(ref mut kernel) = result {
                    match kernel.run() {
                        Ok(()) => break,
                        Err(e) => {
                            action_tx
                                .send(Action::Log(format!("[midi] MIDI disconnected: {}", e)))
                                .unwrap();
                            break;
                        }
                    }
                }
            }

            std::thread::sleep(Duration::from_secs(1));
        }
    });
}

pub struct Kernel {
    input_seq: seq::Seq,
    output_seq: seq::Seq,
    input_port: i32,
    output_port: i32,
    blacklist: Vec<DevicePattern>,
    inbound_tx: Sender<MidiMessage>,
    outbound_rx: Receiver<MidiMessage>,
    input_addrs: Vec<Addr>,
    output_addrs: Vec<Addr>,
    addr_name_pairs: Vec<AddrNamePair>,
}

impl Kernel {
    pub fn new(
        blacklist: Vec<DevicePattern>,
        inbound_tx: Sender<MidiMessage>,
        outbound_rx: Receiver<MidiMessage>,
    ) -> Result<Self, Box<dyn error::Error>> {
        let input_seq = seq::Seq::open(None, Some(alsa::Direction::Capture), true)?;
        let dinfo = seq::PortInfo::empty()?;
        dinfo.set_capability(PortCap::WRITE | PortCap::SUBS_WRITE);
        input_seq.create_port(&dinfo)?;
        let input_port = dinfo.get_port();

        let output_seq = seq::Seq::open(None, Some(alsa::Direction::Playback), true)?;
        let dinfo = seq::PortInfo::empty()?;
        output_seq.create_port(&dinfo)?;
        let output_port = dinfo.get_port();

        Ok(Self {
            input_seq,
            output_seq,
            input_port,
            output_port,
            blacklist,
            inbound_tx,
            outbound_rx,
            addr_name_pairs: vec![],
            input_addrs: vec![],
            output_addrs: vec![],
        })
    }

    pub fn run(&mut self) -> Result<(), Box<dyn error::Error>> {
        let mut t0 = std::time::SystemTime::now();

        let mut step = 0;
        let step_interval = std::time::Duration::from_millis(1);

        let probe_interval = 1000;

        loop {
            if step % probe_interval == 0 {
                self.probe()?;
            }

            self.listen_inbound()?;
            self.listen_outbound()?;

            match t0.elapsed() {
                Ok(elapsed) => {
                    step += 1;
                    if step * step_interval > elapsed {
                        std::thread::sleep(step * step_interval - elapsed);
                    }
                }
                Err(_) => {
                    // System time has changed, so we re-calibrate the clock.
                    t0 = std::time::SystemTime::now();
                    step = 0;
                }
            }
        }
    }

    fn probe(&mut self) -> Result<(), Box<dyn error::Error>> {
        for client_info in seq::ClientIter::new(&self.input_seq) {
            for port in seq::PortIter::new(&self.input_seq, client_info.get_client()) {
                if port.get_capability().contains(PortCap::SUBS_READ)
                    && !port.get_capability().contains(PortCap::NO_EXPORT)
                {
                    let sender = Addr {
                        client: port.get_client(),
                        port: port.get_port(),
                    };

                    // Skip if already connected to this client.
                    if self.input_addrs.iter().any(|addr| *addr == sender) {
                        continue;
                    }

                    let info = &self.input_seq.get_any_client_info(sender.client);
                    let name = String::from(info.as_ref().unwrap().get_name()?);

                    if self.blacklist.iter().any(|pattern| match pattern {
                        DevicePattern::DeviceName(pattern_name) => name.contains(pattern_name),
                        DevicePattern::DeviceNameAndPort(pattern_name, pattern_port) => {
                            name.contains(pattern_name) && sender.port == *pattern_port
                        }
                    }) {
                        continue;
                    }

                    // Try connecting to target midi device
                    let subs = seq::PortSubscribe::empty()?;
                    subs.set_sender(sender);
                    subs.set_dest(Addr {
                        client: self.input_seq.client_id()?,
                        port: self.input_port,
                    });
                    self.input_seq.subscribe_port(&subs)?;

                    self.input_addrs.push(sender);
                    self.addr_name_pairs.push(AddrNamePair {
                        addr: sender,
                        name: String::from(&name),
                    });

                    self.inbound_tx
                        .send(MidiMessage::Connect { addr: sender, name })
                        .unwrap();
                }

                if port.get_capability().contains(PortCap::SUBS_WRITE)
                    && !port.get_capability().contains(PortCap::NO_EXPORT)
                {
                    let receiver = Addr {
                        client: port.get_client(),
                        port: port.get_port(),
                    };

                    // Skip if already connected to this client.
                    if self.output_addrs.iter().any(|addr| *addr == receiver) {
                        continue;
                    }

                    let info = &self.output_seq.get_any_client_info(receiver.client);
                    let name = String::from(info.as_ref().unwrap().get_name()?);

                    if self.blacklist.iter().any(|pattern| match pattern {
                        DevicePattern::DeviceName(pattern_name) => name.contains(pattern_name),
                        DevicePattern::DeviceNameAndPort(pattern_name, pattern_port) => {
                            name.contains(pattern_name) && receiver.port == *pattern_port
                        }
                    }) {
                        continue;
                    }

                    // Try connecting to target midi device
                    let subs = seq::PortSubscribe::empty()?;
                    subs.set_sender(Addr {
                        client: self.output_seq.client_id()?,
                        port: self.output_port,
                    });
                    subs.set_dest(receiver);
                    self.output_seq.subscribe_port(&subs)?;
                    self.output_addrs.push(receiver);
                }
            }
        }

        Ok(())
    }

    fn listen_inbound(&mut self) -> Result<(), Box<dyn error::Error>> {
        loop {
            match self.input_seq.input().event_input_pending(true) {
                Ok(nevents) => {
                    if nevents == 0 {
                        break;
                    }
                }
                _ => break,
            }

            let input = &mut self.input_seq.input();
            let ev = input.event_input()?;

            match ev.get_type() {
                seq::EventType::PortSubscribed => (),
                seq::EventType::PortUnsubscribed => {
                    let data: seq::Connect =
                        ev.get_data().ok_or("Error resolving event data").unwrap();
                    let addr = data.sender;

                    let name = match self.name_from_addr(addr) {
                        Some(name) => name,
                        None => format!("{}:{}", addr.client, addr.port),
                    };

                    self.inbound_tx
                        .send(MidiMessage::Disconnect { addr, name })
                        .unwrap();

                    self.input_addrs.retain(|input_addr| *input_addr != addr);
                    self.output_addrs.retain(|output_addr| *output_addr != addr);
                    self.addr_name_pairs
                        .retain(|addr_name_pair| addr_name_pair.addr != addr);
                }
                seq::EventType::Noteon => {
                    let data: seq::EvNote =
                        ev.get_data().ok_or("Error resolving event data").unwrap();
                    let addr = ev.get_source();

                    self.inbound_tx
                        .send(MidiMessage::NoteOn {
                            addr,
                            channel: data.channel,
                            note: data.note,
                            velocity: data.velocity,
                        })
                        .unwrap();
                }
                seq::EventType::Noteoff => {
                    let data: seq::EvNote =
                        ev.get_data().ok_or("Error resolving event data").unwrap();
                    let addr = ev.get_source();

                    self.inbound_tx
                        .send(MidiMessage::NoteOff {
                            addr,
                            channel: data.channel,
                            note: data.note,
                            velocity: data.velocity,
                        })
                        .unwrap();
                }
                seq::EventType::Controller => {
                    let data: seq::EvCtrl =
                        ev.get_data().ok_or("Error resolving event data").unwrap();
                    let addr = ev.get_source();

                    self.inbound_tx
                        .send(MidiMessage::CC {
                            addr,
                            channel: data.channel,
                            param: data.param as u8,
                            value: data.value as u8,
                        })
                        .unwrap();
                }
                seq::EventType::Pitchbend => {
                    let data: seq::EvCtrl =
                        ev.get_data().ok_or("Error resolving event data").unwrap();
                    let addr = ev.get_source();

                    self.inbound_tx
                        .send(MidiMessage::Pitchbend {
                            addr,
                            channel: data.channel,
                            value: data.value as i16,
                        })
                        .unwrap();
                }
                seq::EventType::Keypress => {
                    let data: seq::EvNote =
                        ev.get_data().ok_or("Error resolving event data").unwrap();
                    let addr = ev.get_source();

                    self.inbound_tx
                        .send(MidiMessage::KeyPressure {
                            addr,
                            channel: data.channel,
                            note: data.note,
                            velocity: data.velocity,
                        })
                        .unwrap();
                }
                seq::EventType::Chanpress => {
                    let _data: seq::EvCtrl =
                        ev.get_data().ok_or("Error resolving event data").unwrap();
                    let _addr = ev.get_source();

                    // println!(
                    //     "[{}] CHANNEL PRESSURE: (channel: {}, note: {}, velocity: {})",
                    //     tag, data.channel, data.param, data.value,
                    // );
                }
                seq::EventType::Bounce => (),
                seq::EventType::ClientChange => (),
                seq::EventType::ClientExit => (),
                seq::EventType::ClientStart => (),
                seq::EventType::Clock => (),
                seq::EventType::Continue => (),
                seq::EventType::Control14 => (),
                seq::EventType::Echo => (),
                seq::EventType::Keysign => (),
                seq::EventType::None => (),
                seq::EventType::Nonregparam => (),
                seq::EventType::Note => (),
                seq::EventType::Oss => (),
                seq::EventType::Pgmchange => (),
                seq::EventType::PortChange => (),
                seq::EventType::PortExit => (),
                seq::EventType::PortStart => (),
                seq::EventType::Qframe => (),
                seq::EventType::QueueSkew => (),
                seq::EventType::Regparam => (),
                seq::EventType::Reset => (),
                seq::EventType::Result => (),
                seq::EventType::Sensing => (),
                seq::EventType::SetposTick => (),
                seq::EventType::SetposTime => (),
                seq::EventType::Songpos => (),
                seq::EventType::Songsel => (),
                seq::EventType::Start => (),
                seq::EventType::Stop => (),
                seq::EventType::SyncPos => (),
                seq::EventType::Sysex => {
                    let data: Vec<u8> = ev
                        .get_ext()
                        .ok_or("Error resolving event data")
                        .unwrap()
                        .to_vec();
                    let addr = ev.get_source();

                    self.inbound_tx
                        .send(MidiMessage::Sysex { addr, data })
                        .unwrap();
                }
                seq::EventType::System => (),
                seq::EventType::Tempo => (),
                seq::EventType::Tick => (),
                seq::EventType::Timesign => (),
                seq::EventType::TuneRequest => (),
                seq::EventType::Usr0 => (),
                seq::EventType::Usr1 => (),
                seq::EventType::Usr2 => (),
                seq::EventType::Usr3 => (),
                seq::EventType::Usr4 => (),
                seq::EventType::Usr5 => (),
                seq::EventType::Usr6 => (),
                seq::EventType::Usr7 => (),
                seq::EventType::Usr8 => (),
                seq::EventType::Usr9 => (),
                seq::EventType::UsrVar0 => (),
                seq::EventType::UsrVar1 => (),
                seq::EventType::UsrVar2 => (),
                seq::EventType::UsrVar3 => (),
                seq::EventType::UsrVar4 => (),
            }
        }
        Ok(())
    }

    fn listen_outbound(&mut self) -> Result<(), Box<dyn error::Error>> {
        loop {
            let msg = match self.outbound_rx.try_recv() {
                Err(TryRecvError::Empty) => break,
                Err(TryRecvError::Disconnected) => panic!("Channel disconnected"),
                Ok(msg) => msg,
            };

            match msg {
                MidiMessage::Connect { addr: _, name: _ } => (),
                MidiMessage::Disconnect { addr: _, name: _ } => (),
                MidiMessage::NoteOn {
                    addr,
                    channel,
                    note,
                    velocity,
                } => self.note_on(addr, channel, note, velocity)?,
                MidiMessage::NoteOff {
                    addr,
                    channel,
                    note,
                    velocity,
                } => self.note_off(addr, channel, note, velocity)?,
                MidiMessage::CC {
                    addr,
                    channel,
                    param,
                    value,
                } => self.cc(addr, channel, param.into(), value.into())?,
                MidiMessage::Pitchbend {
                    addr,
                    channel,
                    value,
                } => self.pitchbend(addr, channel, value.into())?,
                MidiMessage::KeyPressure {
                    addr,
                    channel,
                    note,
                    velocity,
                } => self.key_pressure(addr, channel, note, velocity)?,
                MidiMessage::Sysex { addr, data } => self.sysex(addr, data)?,
            };
        }
        Ok(())
    }

    fn name_from_addr(&self, addr: Addr) -> Option<String> {
        self.addr_name_pairs
            .iter()
            .find(|pair| pair.addr == addr)
            .map(|pair| String::from(&pair.name))
    }

    fn note_on(
        &self,
        dest: Addr,
        channel: u8,
        note: u8,
        velocity: u8,
    ) -> Result<(), Box<dyn error::Error>> {
        let data = seq::EvNote {
            channel,
            note,
            velocity,
            off_velocity: 0,
            duration: 0,
        };
        let mut ev = seq::Event::new(seq::EventType::Noteon, &data);
        ev.set_dest(dest);
        ev.set_direct();
        self.output_seq.event_output_direct(&mut ev)?;
        Ok(())
    }

    fn note_off(
        &self,
        dest: Addr,
        channel: u8,
        note: u8,
        velocity: u8,
    ) -> Result<(), Box<dyn error::Error>> {
        let data = seq::EvNote {
            channel,
            note,
            velocity,
            off_velocity: 0,
            duration: 0,
        };
        let mut ev = seq::Event::new(seq::EventType::Noteoff, &data);
        ev.set_dest(dest);
        ev.set_direct();
        self.output_seq.event_output_direct(&mut ev)?;
        Ok(())
    }

    fn cc(
        &self,
        dest: Addr,
        channel: u8,
        param: u32,
        value: i32,
    ) -> Result<(), Box<dyn error::Error>> {
        let data = seq::EvCtrl {
            channel,
            param,
            value,
        };
        let mut ev = seq::Event::new(seq::EventType::Controller, &data);
        ev.set_dest(dest);
        ev.set_direct();
        self.output_seq.event_output_direct(&mut ev)?;
        Ok(())
    }

    fn pitchbend(&self, dest: Addr, channel: u8, value: i32) -> Result<(), Box<dyn error::Error>> {
        let data = seq::EvCtrl {
            channel,
            param: 0,
            value,
        };
        let mut ev = seq::Event::new(seq::EventType::Pitchbend, &data);
        ev.set_dest(dest);
        ev.set_direct();
        self.output_seq.event_output_direct(&mut ev)?;
        Ok(())
    }

    fn key_pressure(
        &self,
        dest: Addr,
        channel: u8,
        note: u8,
        velocity: u8,
    ) -> Result<(), Box<dyn error::Error>> {
        let data = seq::EvNote {
            channel,
            note,
            velocity,
            off_velocity: 0,
            duration: 0,
        };
        let mut ev = seq::Event::new(seq::EventType::Keypress, &data);
        ev.set_dest(dest);
        ev.set_direct();
        self.output_seq.event_output_direct(&mut ev)?;
        Ok(())
    }

    fn sysex(&self, dest: Addr, data: Vec<u8>) -> Result<(), Box<dyn error::Error>> {
        let mut ev = seq::Event::new_ext(seq::EventType::Sysex, data);
        ev.set_dest(dest);
        ev.set_direct();
        self.output_seq.event_output_direct(&mut ev)?;
        Ok(())
    }
}
