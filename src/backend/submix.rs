// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::messages::Destination;

use super::frames::{Frame, StereoFrame};

pub struct Submix {
    pub master_gain: f32,
    pub master_pan: f32,
    pub track_gains: [f32; 8],
    pub track_pans: [f32; 8],
    pub track_mutes: [bool; 8],
    pub track_solos: [bool; 8],
    pub fader: f32,
    frame_in: StereoFrame,
    frame_out: StereoFrame,
    mul_out: f32,
}

impl Submix {
    pub fn new() -> Self {
        Self {
            master_gain: 0.0,
            master_pan: 0.0,
            track_gains: [-f32::INFINITY; 8],
            track_pans: [0.0; 8],
            track_mutes: [false; 8],
            track_solos: [false; 8],
            fader: 0.0,
            frame_in: Frame::zero(),
            frame_out: Frame::zero(),
            mul_out: 1.0,
        }
    }

    pub fn send(&mut self, source: Destination, frame: StereoFrame) {
        // If any tracks are marked as `solo`, we are in solo mode:
        let solo_mode = self.track_solos.iter().any(|x| *x);

        if solo_mode {
            if let Destination::Track(idx) = source {
                if !self.track_solos[idx] {
                    return;
                }
            } else {
                return;
            }
        }

        if let Destination::Track(idx) = source {
            if self.track_mutes[idx] {
                return;
            }
        }

        let left = frame.get_left();
        let right = frame.get_right();

        let gain = match source {
            Destination::Master => 10.0f32.powf(self.master_gain / 20.0),
            Destination::Track(idx) => 10.0f32.powf(self.track_gains[idx] / 20.0),
        };

        let pan = match source {
            Destination::Master => self.master_pan,
            Destination::Track(idx) => self.track_pans[idx],
        };

        let left_pan = (pan + pan.abs() - 1.0).max(-1.0).min(1.0);
        let right_pan = (pan - pan.abs() + 1.0).max(-1.0).min(1.0);

        self.frame_in += StereoFrame {
            left: gain * ((0.5 - 0.5 * left_pan) * left + (0.5 - 0.5 * right_pan) * right),
            right: gain * ((0.5 + 0.5 * left_pan) * left + (0.5 + 0.5 * right_pan) * right),
        };
    }

    pub fn process(&mut self) {
        self.mul_out = 10.0f32.powf(self.fader / 20.0);
        self.frame_out = self.frame_in;
        self.frame_in = Frame::zero();
    }

    pub fn recv(&self) -> StereoFrame {
        self.mul_out * self.frame_out
    }
}
