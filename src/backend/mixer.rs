// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crossbeam::channel::{Receiver, RecvTimeoutError, SendError, Sender, TryRecvError};

use std::time::Duration;

use crate::messages::{Action, Destination, Request};
use crate::perf;

use super::bus::Bus;
use super::frames::{Frame, StereoFrame};
use super::io::IOMessage;
use super::kernel::KernelMessage;
#[cfg(feature = "master_effects")]
use super::reverb_bus::ReverbBus;
use super::submix::Submix;
use super::track::Track;
use super::vinyl::Vinyl;

#[cfg(feature = "ballet")]
use super::ballet;

#[cfg(feature = "opera")]
use super::opera;

//////////////////////////////////////////////////
// Mixer
//////////////////////////////////////////////////

pub fn spawn(
    action_tx: Sender<Action>,
    io_request_tx: Sender<IOMessage<StereoFrame>>,
    analyzer_tx: Sender<StereoFrame>,
    request_rx: Receiver<Request>,
    kernel_rx: Receiver<KernelMessage>,
) {
    std::thread::spawn(move || {
        perf::set_realtime_thread_priority(88);

        let mut mixer = Mixer::new(action_tx, io_request_tx, analyzer_tx);
        mixer.run(request_rx, kernel_rx);
    });
}

pub struct Mixer {
    action_tx: Sender<Action>,
    analyzer_tx: Sender<StereoFrame>,
    master_bus: Bus,
    #[cfg(feature = "master_effects")]
    reverb_bus: ReverbBus,
    submixes: Vec<Submix>,
    tracks: Vec<Track>,
}

impl Mixer {
    pub fn new(
        action_tx: Sender<Action>,
        io_request_tx: Sender<IOMessage<StereoFrame>>,
        analyzer_tx: Sender<StereoFrame>,
    ) -> Self {
        let mut master_bus = Bus::new(action_tx.clone());
        master_bus.set_destination(Destination::Master);

        #[cfg(feature = "master_effects")]
        let reverb_bus = ReverbBus::new();

        let mut submixes: Vec<Submix> = Vec::with_capacity(8);
        for _ in 0..8 {
            submixes.push(Submix::new());
        }

        let mut tracks: Vec<Track> = Vec::with_capacity(8);
        for idx in 0..8 {
            tracks.push(Track::new(
                action_tx.clone(),
                io_request_tx.clone(),
                idx,
                1,
                2,
            ));
        }

        Self {
            action_tx,
            analyzer_tx,
            master_bus,
            #[cfg(feature = "master_effects")]
            reverb_bus,
            submixes,
            tracks,
        }
    }

    pub fn run(&mut self, request_rx: Receiver<Request>, kernel_rx: Receiver<KernelMessage>) {
        // We set these to zero to begin with, and await for the kernel to report the values
        // to be used.
        let mut nchannels_in = 0;
        let mut nchannels_out = 0;

        // Counter to allow frame skipping in the output stream, if we are falling behind.
        let mut output_frame_balance: usize = 0;
        let mut max_out_frames = 0;

        // Set up internal state.
        let mut t = 0;
        let mut t_return = 0;
        let mut t_stop = 0;
        let mut play = false;
        let mut record = false;
        let mut auto_return = false;

        // Loop points and looping
        let mut looping = false;
        let mut t_loop_start = 0;
        let mut t_loop_stop = 0;

        // Vinyl for exporting
        let mut vinyl: Option<Vinyl> = None;

        // Counters and interval values for streaming data to the frontend.
        let mut update_frontend_counter = 0;
        let mut update_frontend_interval = 44100;

        let mut in_tx: Option<Sender<Vec<f32>>> = None;
        let mut in_rx: Option<Receiver<Vec<f32>>> = None;
        let mut out_tx: Option<Sender<Vec<f32>>> = None;
        let mut out_rx: Option<Receiver<Vec<f32>>> = None;

        // Allocate buffers of frames.
        let mut track_frames = [StereoFrame::zero(); 8];
        let mut submix_frames = [StereoFrame::zero(); 8];

        // Analyzer
        let mut analyzer_active = false;

        // Workload measurement
        let mut workload_accumulator = 0;
        let mut workload_max = 0;
        let mut workload_counter = 0;

        // Ballet
        #[cfg(feature = "ballet")]
        let mut ballet_tx: Option<Sender<Vec<f32>>> = None;

        #[cfg(feature = "ballet")]
        let mut ballet_rx: Option<Receiver<Vec<f32>>> = None;

        #[cfg(feature = "ballet")]
        let mut ballet_is_seeded: bool = false;

        #[cfg(feature = "ballet")]
        let mut ballet_input_frame: Vec<f32> = vec![0.0; 8];

        // Opera
        #[cfg(feature = "opera")]
        let mut opera_tx: Option<Sender<Vec<f32>>> = None;

        #[cfg(feature = "opera")]
        let mut opera_rx: Option<Receiver<Vec<f32>>> = None;

        #[cfg(feature = "opera")]
        let mut opera_frame_balance: usize = 0;

        #[cfg(feature = "opera")]
        let mut opera_input_frame: Vec<f32> = vec![0.0; 8];

        loop {
            let mut time_instant = std::time::Instant::now();

            // If we have no input or output stream, slow down.
            if in_rx.is_none() || out_tx.is_none() {
                std::thread::sleep(Duration::from_millis(
                    (update_frontend_interval as u64 * 1000) / 44100,
                ));
            }

            // Receive and handle kernel messages.
            loop {
                match kernel_rx.try_recv() {
                    Ok(KernelMessage::MountInput {
                        main_rx,
                        recycle_tx,
                        nchannels,
                    }) => {
                        in_tx = Some(recycle_tx);
                        in_rx = Some(main_rx);
                        nchannels_in = nchannels;

                        // Keep track of whether or not the input is still working.
                        let mut input_disconnected = false;

                        // Send input frame to kernel to activate stream.
                        if let Some(in_tx) = &in_tx {
                            match in_tx.send(vec![0f32; nchannels_in]) {
                                Ok(()) => (),
                                Err(SendError(_)) => input_disconnected = true,
                            }
                        }

                        // Something went wrong when setting up the input stream, so we
                        // discard it.
                        if input_disconnected {
                            in_tx = None;
                            in_rx = None;
                        } else {
                            self.action_tx
                                .send(Action::Log("[mixer] Input mounted".to_string()))
                                .unwrap();
                        }
                    }
                    Ok(KernelMessage::MountOutput {
                        main_tx,
                        recycle_rx,
                        nchannels,
                        period_size,
                    }) => {
                        out_tx = Some(main_tx);
                        out_rx = Some(recycle_rx);
                        nchannels_out = nchannels;
                        max_out_frames = period_size;

                        // Reset the frame balance.
                        output_frame_balance = 0;

                        self.action_tx
                            .send(Action::Log("[mixer] Output mounted".to_string()))
                            .unwrap();
                    }
                    Err(TryRecvError::Empty) => break,
                    Err(TryRecvError::Disconnected) => panic!("Channel disconnected"),
                }
            }

            // Receive and handle request messages.
            loop {
                match request_rx.try_recv() {
                    Ok(Request::PlayState(state)) => {
                        play = state;
                        if play {
                            t_return = t;
                        } else if auto_return {
                            t = t_return;
                        }
                    }
                    Ok(Request::RecordState(state)) => {
                        record = state;
                    }
                    Ok(Request::AutoReturn(state)) => {
                        auto_return = state;
                    }
                    Ok(Request::LoopState(state)) => {
                        looping = state;
                        for track in self.tracks.iter_mut() {
                            if looping {
                                track.set_loop_points(t_loop_start, t_loop_stop);
                            } else {
                                track.set_loop_points(0, 0);
                            }
                        }
                    }
                    Ok(Request::SetLoopStart(value)) => {
                        t_loop_start = value;
                    }
                    Ok(Request::SetLoopStop(value)) => {
                        t_loop_stop = t_loop_start.max(value);
                    }
                    Ok(Request::Export(filename, start, stop)) => {
                        let stop = stop.max(start);
                        t = start;
                        t_return = t;
                        t_stop = stop;
                        vinyl = Some(Vinyl::new(&filename, 44100));
                        if let Some(v) = vinyl.as_mut() {
                            v.initialize();
                        }
                    }
                    Ok(Request::AbsoluteTime(t_)) => {
                        t = t_;
                    }
                    Ok(Request::ForwardTimeShift(dt)) => {
                        t += dt;
                    }
                    Ok(Request::BackwardTimeShift(dt)) => {
                        if dt < t {
                            t -= dt;
                        } else {
                            t = 0;
                        }
                    }
                    Ok(Request::Load(Destination::Track(channel), filename)) => {
                        if let Some(track) = self.tracks.get_mut(channel) {
                            track.load(filename);
                        }
                    }
                    Ok(Request::Load(Destination::Master, _)) => {}
                    Ok(Request::Monitor(Destination::Track(channel), value)) => {
                        if let Some(track) = self.tracks.get_mut(channel) {
                            track.monitor = value;
                        }
                    }
                    Ok(Request::Monitor(Destination::Master, _)) => {}
                    Ok(Request::Mute(Destination::Track(channel), value)) => {
                        if let Some(track) = self.tracks.get_mut(channel) {
                            track.mute = value;
                        }
                    }
                    Ok(Request::Mute(Destination::Master, value)) => {
                        self.master_bus.mute = value;
                    }
                    Ok(Request::Solo(Destination::Track(channel), value)) => {
                        if let Some(track) = self.tracks.get_mut(channel) {
                            track.solo = value;
                        }
                    }
                    Ok(Request::Solo(Destination::Master, value)) => {
                        self.master_bus.solo = value;
                    }
                    Ok(Request::RecordArm(Destination::Track(channel), value)) => {
                        if let Some(track) = self.tracks.get_mut(channel) {
                            track.record_arm = value;
                        }
                    }
                    Ok(Request::RecordArm(Destination::Master, _)) => {}
                    Ok(Request::Fader(Destination::Track(channel), value)) => {
                        if let Some(track) = self.tracks.get_mut(channel) {
                            track.fader = value;
                        }
                    }
                    Ok(Request::Fader(Destination::Master, value)) => {
                        self.master_bus.fader = value;
                    }
                    Ok(Request::Pan(Destination::Track(channel), value)) => {
                        if let Some(track) = self.tracks.get_mut(channel) {
                            track.center = value;
                            track.width = 1.0 - value.abs();
                        }
                    }
                    Ok(Request::Pan(Destination::Master, value)) => {
                        self.master_bus.pan = value;
                    }
                    Ok(Request::ReverbSend(Destination::Track(channel), value)) => {
                        if let Some(track) = self.tracks.get_mut(channel) {
                            track.reverb_send = value;
                        }
                    }
                    Ok(Request::ReverbSend(Destination::Master, _value)) => {}
                    Ok(Request::SubmixFader(idx, value)) => {
                        if let Some(submix) = self.submixes.get_mut(idx) {
                            submix.fader = value;
                        }
                    }
                    Ok(Request::SubmixGain(Destination::Track(channel), idx, value)) => {
                        if let Some(submix) = self.submixes.get_mut(idx) {
                            submix.track_gains[channel] = value;
                        }
                    }
                    Ok(Request::SubmixGain(Destination::Master, idx, value)) => {
                        if let Some(submix) = self.submixes.get_mut(idx) {
                            submix.master_gain = value;
                        }
                    }
                    Ok(Request::SubmixPan(Destination::Track(channel), idx, value)) => {
                        if let Some(submix) = self.submixes.get_mut(idx) {
                            submix.track_pans[channel] = value;
                        }
                    }
                    Ok(Request::SubmixPan(Destination::Master, idx, value)) => {
                        if let Some(submix) = self.submixes.get_mut(idx) {
                            submix.master_pan = value;
                        }
                    }
                    Ok(Request::SubmixSolo(Destination::Track(channel), idx, value)) => {
                        if let Some(submix) = self.submixes.get_mut(idx) {
                            submix.track_solos[channel] = value;
                        }
                    }
                    Ok(Request::SubmixSolo(Destination::Master, _, _)) => (),
                    Ok(Request::SubmixMute(Destination::Track(channel), idx, value)) => {
                        if let Some(submix) = self.submixes.get_mut(idx) {
                            submix.track_mutes[channel] = value;
                        }
                    }
                    Ok(Request::SubmixMute(Destination::Master, _, _)) => (),
                    #[cfg(feature = "master_effects")]
                    Ok(Request::MasterEffect(effects)) => {
                        self.master_bus.effects = effects;
                    }
                    Ok(Request::InputSourceLeft(Destination::Track(channel), value)) => {
                        if let Some(track) = self.tracks.get_mut(channel) {
                            track.physical_left = value;
                        }
                    }
                    Ok(Request::InputSourceRight(Destination::Track(channel), value)) => {
                        if let Some(track) = self.tracks.get_mut(channel) {
                            track.physical_right = value;
                        }
                    }
                    Ok(Request::InputSourceLeft(Destination::Master, _)) => {}
                    Ok(Request::InputSourceRight(Destination::Master, _)) => {}
                    Ok(Request::SetAnalyzerState(state)) => {
                        analyzer_active = state;
                    }
                    Ok(Request::SetUpdateInterval(interval)) => {
                        update_frontend_interval = interval;
                    }

                    #[cfg(feature = "ballet")]
                    Ok(Request::BalletSetup(ballet_msg_rx)) => {
                        let (ballet_in_tx, ballet_in_rx) =
                            crossbeam::channel::unbounded::<Vec<f32>>();
                        let (ballet_out_tx, ballet_out_rx) =
                            crossbeam::channel::unbounded::<Vec<f32>>();

                        ballet::spawn(
                            self.action_tx.clone(),
                            ballet_msg_rx,
                            ballet_in_tx,
                            ballet_out_rx,
                        );

                        ballet_tx = Some(ballet_out_tx);
                        ballet_rx = Some(ballet_in_rx);
                    }

                    #[cfg(feature = "opera")]
                    Ok(Request::OperaSetup(opera_msg_rx)) => {
                        let (opera_in_tx, opera_in_rx) =
                            crossbeam::channel::unbounded::<Vec<f32>>();
                        let (opera_out_tx, opera_out_rx) =
                            crossbeam::channel::unbounded::<Vec<f32>>();

                        opera::spawn(
                            self.action_tx.clone(),
                            opera_msg_rx,
                            opera_in_tx,
                            opera_out_rx,
                        );

                        opera_tx = Some(opera_out_tx);
                        opera_rx = Some(opera_in_rx);

                        let mut disconnected = false;

                        if let Some(in_tx) = &opera_tx {
                            for _ in 0..16 {
                                match in_tx.send(vec![0f32; 8]) {
                                    Ok(()) => (),
                                    Err(SendError(_)) => disconnected = true,
                                };
                            }
                        }

                        if disconnected {
                            opera_tx = None;
                            opera_rx = None;
                        }
                    }

                    Err(TryRecvError::Empty) => break,
                    Err(TryRecvError::Disconnected) => panic!("Channel disconnected"),
                }
            }

            // Increment frontend counter.
            update_frontend_counter += 1;

            let mut elapsed = time_instant.elapsed().as_nanos() as i128;

            // Keep track of whether or not the input is still working.
            let mut input_disconnected = false;

            match vinyl {
                // If we have a vinyl ready, drain the input.
                Some(_) => {
                    time_instant = std::time::Instant::now();

                    while let Some(in_rx) = &in_rx {
                        match in_rx.try_recv() {
                            Ok(_) => (),
                            Err(TryRecvError::Empty) => break,
                            Err(TryRecvError::Disconnected) => {
                                input_disconnected = true;
                                break;
                            }
                        }
                    }
                }
                // Otherwise, read from the input.
                None => {
                    if let Some(in_rx) = &in_rx {
                        let in_frame = match in_rx.recv_timeout(Duration::from_millis(500)) {
                            Ok(frame) => frame,
                            Err(RecvTimeoutError::Timeout) => vec![0f32; nchannels_in],
                            Err(RecvTimeoutError::Disconnected) => {
                                input_disconnected = true;
                                vec![0f32; nchannels_in]
                            }
                        };

                        time_instant = std::time::Instant::now();

                        for (channel, sample) in in_frame.iter().enumerate() {
                            for track in self.tracks.iter_mut() {
                                if track.physical_left == channel && track.physical_right == channel
                                {
                                    track.send(StereoFrame::from(0.5 * *sample, 0.5 * *sample));
                                } else if track.physical_left == channel {
                                    track.send(StereoFrame::from(*sample, 0.0));
                                } else if track.physical_right == channel {
                                    track.send(StereoFrame::from(0.0, *sample));
                                }
                            }

                            #[cfg(feature = "ballet")]
                            if channel < 8 {
                                ballet_input_frame[channel] = *sample;
                            }

                            #[cfg(feature = "opera")]
                            if channel < 8 {
                                opera_input_frame[channel] = *sample;
                            }
                        }

                        // Recycle the input frame.
                        if let Some(in_tx) = &in_tx {
                            match in_tx.send(in_frame) {
                                Ok(()) => (),
                                Err(SendError(_)) => input_disconnected = true,
                            };
                        }
                    }
                }
            }

            // If we have detected a disconnect from the input, unset the channels.
            if input_disconnected {
                in_tx = None;
                in_rx = None;
            }

            #[cfg(feature = "ballet")]
            if vinyl.is_none() {
                if ballet_is_seeded {
                    // Keep track of whether or not Ballet is still working.
                    let mut ballet_disconnected = false;
                    let mut has_frame = true;

                    // If we are connected to the synth, try to receive a frame.
                    if let Some(in_rx) = &ballet_rx {
                        let mut in_frame = match in_rx.recv_timeout(Duration::from_millis(500)) {
                            Ok(frame) => frame,
                            Err(RecvTimeoutError::Timeout) => {
                                has_frame = false;
                                vec![0f32; 8]
                            }
                            Err(RecvTimeoutError::Disconnected) => {
                                ballet_disconnected = true;
                                has_frame = false;
                                vec![0f32; 8]
                            }
                        };

                        // Send the output from the synthesizer to subscribed tracks.  To avoid
                        // aliasing, we set the "physical" inputs of the synthesizer at `256 +
                        // channel`.
                        for (channel, sample) in in_frame.iter_mut().enumerate() {
                            for track in self.tracks.iter_mut() {
                                if track.physical_left == 256 + channel
                                    && track.physical_right == 256 + channel
                                {
                                    track.send(StereoFrame::from(0.5 * *sample, 0.5 * *sample));
                                } else if track.physical_left == 256 + channel {
                                    track.send(StereoFrame::from(*sample, 0.0));
                                } else if track.physical_right == 256 + channel {
                                    track.send(StereoFrame::from(0.0, *sample));
                                }
                            }

                            *sample = ballet_input_frame[channel];
                        }

                        if has_frame {
                            // Recycle the frame.
                            if let Some(in_tx) = &ballet_tx {
                                match in_tx.send(in_frame) {
                                    Ok(()) => (),
                                    Err(SendError(_)) => ballet_disconnected = true,
                                };
                            }
                        }
                    }

                    if ballet_disconnected {
                        ballet_tx = None;
                        ballet_rx = None;
                    }
                } else if out_rx.is_some() {
                    if let Some(in_tx) = &ballet_tx {
                        (0..max_out_frames)
                            .for_each(|_| in_tx.send(vec![0f32; 8]).unwrap_or_default());

                        ballet_is_seeded = true;
                    }
                }
            }

            #[cfg(feature = "opera")]
            {
                // Keep track of whether or not Opera is still working.
                let mut opera_disconnected = false;

                // If we are connected to the synth, try to receive a frame.
                if let Some(in_rx) = &opera_rx {
                    let mut in_frame = match in_rx.recv_timeout(Duration::from_millis(500)) {
                        Ok(frame) => frame,
                        Err(RecvTimeoutError::Timeout) => {
                            opera_frame_balance += 1;
                            vec![0f32; 8]
                        }
                        Err(RecvTimeoutError::Disconnected) => {
                            opera_disconnected = true;
                            opera_frame_balance += 1;
                            vec![0f32; 8]
                        }
                    };

                    // Send the output from the synthesizer to subscribed tracks.  To avoid
                    // aliasing, we set the "physical" inputs of the synthesizer at `256 +
                    // channel`.
                    for (channel, sample) in in_frame.iter_mut().enumerate() {
                        for track in self.tracks.iter_mut() {
                            if track.physical_left == 256 + channel
                                && track.physical_right == 256 + channel
                            {
                                track.send(StereoFrame::from(0.5 * *sample, 0.5 * *sample));
                            } else if track.physical_left == 256 + channel {
                                track.send(StereoFrame::from(*sample, 0.0));
                            } else if track.physical_right == 256 + channel {
                                track.send(StereoFrame::from(0.0, *sample));
                            }
                        }

                        *sample = opera_input_frame[channel];
                    }

                    if opera_frame_balance <= max_out_frames {
                        // Recycle the frame.
                        if let Some(in_tx) = &opera_tx {
                            match in_tx.send(in_frame) {
                                Ok(()) => (),
                                Err(SendError(_)) => opera_disconnected = true,
                            };
                        }
                    } else {
                        // Skip and destroy the frame.
                        opera_frame_balance = opera_frame_balance.saturating_sub(1);
                    }
                }

                if opera_disconnected {
                    opera_tx = None;
                    opera_rx = None;
                }
            }

            // Process each track.
            match vinyl {
                Some(_) => {
                    for track in self.tracks.iter_mut() {
                        track.process(t, true, false);
                    }
                }
                None => {
                    for track in self.tracks.iter_mut() {
                        track.process(t, play, record);
                    }
                }
            }

            // If any tracks are marked as `solo`, we are in solo mode:
            let solo_mode = self.tracks.iter().any(|track| track.solo);

            // Retrieve frames from each track.
            for (idx, track) in self.tracks.iter().enumerate() {
                // If solo mode is active, exit early if track is not in solo mode.
                if solo_mode && !track.solo {
                    continue;
                }
                track_frames[idx] = track.recv();
            }

            // Send frames to the reverb bus.
            #[cfg(feature = "master_effects")]
            for (frame, track) in track_frames.iter().zip(self.tracks.iter()) {
                self.reverb_bus.send(*frame, track.reverb_send);
            }
            #[cfg(feature = "master_effects")]
            self.reverb_bus.process();

            // Send frames to the master bus.
            for frame in track_frames.iter() {
                self.master_bus.send(*frame);
            }
            #[cfg(feature = "master_effects")]
            self.master_bus.send(self.reverb_bus.recv());

            // Process the master bus.
            match vinyl {
                Some(_) => self.master_bus.process(t, true),
                None => self.master_bus.process(t, play),
            }

            // Retrieve the master output frame.
            let master_out_frame = self.master_bus.recv();

            // Keep track of whether or not the output is still working.
            let mut output_disconnected = false;

            match vinyl.as_mut() {
                Some(v) => {
                    v.write(master_out_frame);
                }
                None => {
                    if let Some(out_rx) = &out_rx {
                        // Retrieve a recycled output frame.
                        let mut out_frame = match out_rx.try_recv() {
                            Ok(value) => value,
                            Err(TryRecvError::Empty) => {
                                output_frame_balance += 1;
                                vec![0f32; nchannels_out]
                            }
                            Err(TryRecvError::Disconnected) => {
                                output_disconnected = true;
                                output_frame_balance += 1;
                                vec![0f32; nchannels_out]
                            }
                        };

                        // Process each submix.
                        for (idx, submix) in self.submixes.iter_mut().enumerate() {
                            // Only process submixes for which outputs exist.
                            if idx > nchannels_out / 2 {
                                break;
                            }

                            // Send each of the track frames to the submix.
                            for (track_idx, frame) in track_frames.iter().enumerate() {
                                submix.send(Destination::Track(track_idx), *frame);
                            }

                            // Send the master bus output to the submix.
                            submix.send(Destination::Master, master_out_frame);

                            submix.process();

                            submix_frames[idx] = submix.recv();
                        }

                        // Write the master frame to the output frame to be passed to the
                        // output stream.
                        for (channel, sample) in out_frame.iter_mut().enumerate() {
                            let idx = channel / 2;
                            let submix_frame = match submix_frames.get(idx) {
                                Some(submix_frame) => *submix_frame,
                                None => StereoFrame::zero(),
                            };

                            *sample = match channel % 2 {
                                0 => submix_frame.get_left(),
                                1 => submix_frame.get_right(),
                                _ => 0.0,
                            };
                        }

                        // Pass the output frame downstream.
                        if output_frame_balance <= max_out_frames {
                            if let Some(out_tx) = &out_tx {
                                match out_tx.send(out_frame) {
                                    Ok(()) => (),
                                    Err(SendError(_)) => output_disconnected = true,
                                }
                            }
                        } else {
                            // Skip and destroy the frame.
                            output_frame_balance = output_frame_balance.saturating_sub(1);
                        }
                    }

                    // Pass the master frame to the analyzer.
                    if analyzer_active {
                        self.analyzer_tx.send(master_out_frame).unwrap();
                    }
                }
            }

            // If we have detected a disconnect from the output, unset the channels.
            if output_disconnected {
                out_tx = None;
                out_rx = None;
            }

            match vinyl.as_mut() {
                Some(v) => {
                    t += 1;
                    if t > t_stop {
                        v.finalize();
                        self.action_tx.send(Action::ExportCompleted).unwrap();
                        vinyl = None;
                        if auto_return {
                            t = t_return;
                        }
                    }
                }
                None => {
                    if play {
                        // Advance the cursor.
                        t += 1;

                        if looping
                            && t_loop_stop > t_loop_start
                            && (t > t_loop_stop || t < t_loop_start)
                        {
                            t = t_loop_start;
                        }
                    }
                }
            }

            if update_frontend_counter >= update_frontend_interval
                || in_rx.is_none()
                || out_tx.is_none()
            {
                // Send the current time to the frontend.
                self.action_tx.send(Action::Time(t)).unwrap();

                // Publish the state of the master bus and tracks to the frontend.
                self.master_bus.update(update_frontend_interval);
                for track in self.tracks.iter_mut() {
                    track.update(update_frontend_interval);
                }

                update_frontend_counter = 0;
            }

            elapsed += time_instant.elapsed().as_nanos() as i128;
            workload_accumulator += elapsed;

            if elapsed > 22675 {
                workload_max += 1;
            }

            workload_counter += 1;

            if workload_counter >= 44100 {
                let percentage = workload_accumulator / 10_000_000;
                let max = workload_max;

                self.action_tx
                    .send(Action::Workload {
                        identifier: 0,
                        percentage,
                        max,
                    })
                    .unwrap();

                workload_accumulator = 0;
                workload_max = 0;
                workload_counter = 0;
            }
        }
    }
}
