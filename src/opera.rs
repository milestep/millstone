// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use crate::messages::{OperaConfig, OperaMasterConfig};
use crate::state::State;

use crate::synth::{Modulation, ModulationParams, Value};

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize)]
pub enum Parameter {
    Master(MasterParameter),
    Voice(u8, VoiceParameter),
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize)]
pub enum MasterParameter {
    Master(MasterDestination),
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize)]
pub enum VoiceParameter {
    Voice(VoiceDestination),
    Osc1(OscillatorDestination),
    Noise(NoiseDestination),
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct MasterPatch {
    pub scene: MasterScene,
    #[serde(default)]
    pub modulations: Vec<Modulation<MasterParameter>>,

    #[serde(default)]
    pub rate_divider: usize,

    #[serde(default)]
    pub input_source: usize,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct MasterScene {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub master: MasterState,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum MasterDestination {
    Gain,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct MasterState {
    pub gain: Value,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Patch {
    #[serde(default)]
    pub output_channel_left: usize,
    #[serde(default)]
    pub output_channel_right: usize,
    #[serde(default)]
    pub channel: u8,

    #[serde(default)]
    pub rate_divider: usize,

    pub scene: Scene,
    #[serde(default)]
    pub modulations: Vec<Modulation<VoiceParameter>>,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Scene {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub voice: VoiceState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub osc1: OscillatorState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub noise: NoiseState,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum VoiceDestination {
    Osc1,
    Gain,
    Noise,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct VoiceState {
    pub gain: Value,
    pub osc1: Value,
    pub noise: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum OscillatorDestination {
    Frequency,
    Track,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct OscillatorState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub frequency: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub track: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum NoiseDestination {
    Balance,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct NoiseState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub balance: Value,
}

impl Default for MasterPatch {
    fn default() -> Self {
        Self::new()
    }
}

impl MasterPatch {
    pub fn new() -> Self {
        Self {
            scene: MasterScene::new(),
            modulations: Vec::with_capacity(32),
            rate_divider: 0,
            input_source: 1,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        self.scene.set_from(&other.scene);

        for modulation in other.modulations.iter() {
            self.set_modulation_params(modulation.idx, modulation.destination, modulation.params);
        }

        self.rate_divider = other.rate_divider;

        self.input_source = other.input_source;
    }

    pub fn get_modulation_params(
        &self,
        idx: [usize; 4],
        destination: MasterParameter,
    ) -> ModulationParams {
        for modulation in self.modulations.iter() {
            if modulation.idx == idx && modulation.destination == destination {
                return modulation.params;
            }
        }

        ModulationParams::new()
    }

    pub fn set_modulation_params(
        &mut self,
        idx: [usize; 4],
        destination: MasterParameter,
        params: ModulationParams,
    ) {
        for modulation in self.modulations.iter_mut() {
            if modulation.idx == idx && modulation.destination == destination {
                modulation.params = params;
                return;
            }
        }

        self.modulations.push(Modulation {
            idx,
            destination,
            params,
        });
    }

    pub fn shift_param(&mut self, dest: MasterParameter, amount: f32) {
        match dest {
            MasterParameter::Master(dest) => self.scene.master.get_mut(dest).shift_relative(amount),
        }
    }

    pub fn get_param(&self, dest: MasterParameter) -> Option<&Value> {
        match dest {
            MasterParameter::Master(dest) => Some(self.scene.master.get(dest)),
        }
    }

    pub fn set_param(&mut self, dest: MasterParameter, value: f32) {
        match dest {
            MasterParameter::Master(dest) => self.scene.master.get_mut(dest).set(value),
        };
    }

    pub fn get_config(&self, dest: OperaMasterConfig) -> usize {
        match dest {
            OperaMasterConfig::InputSource => self.input_source,
            OperaMasterConfig::RateDivider => self.rate_divider,
        }
    }

    pub fn set_config(&mut self, dest: OperaMasterConfig, value: usize) {
        match dest {
            OperaMasterConfig::InputSource => {
                self.input_source = value;
            }
            OperaMasterConfig::RateDivider => {
                self.rate_divider = value;
            }
        }
    }
}

impl MasterScene {
    pub fn new() -> Self {
        Self {
            master: MasterState::default(),
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        self.master.set_from(&other.master);
    }
}

impl Default for MasterState {
    fn default() -> Self {
        Self {
            gain: Value::logarithmic(1.0, 1.0, 2.0f32.powf(-7.0), 2.0, 0),
        }
    }
}

impl MasterState {
    pub fn get(&self, destination: MasterDestination) -> &Value {
        match destination {
            MasterDestination::Gain => &self.gain,
        }
    }

    pub fn get_mut(&mut self, destination: MasterDestination) -> &mut Value {
        match destination {
            MasterDestination::Gain => &mut self.gain,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in MasterDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for Patch {
    fn default() -> Self {
        Self::new(0, 1, 255)
    }
}

impl State for Patch {
    fn new() -> Self {
        Patch::new(0, 1, 1)
    }

    fn from_string(s: String) -> Self {
        match ron::from_str(&s) {
            Ok(state) => state,
            Err(_) => Self::new(0, 1, 1),
        }
    }
}

impl Patch {
    pub fn new(output_channel_left: usize, output_channel_right: usize, channel: u8) -> Self {
        Self {
            output_channel_left,
            output_channel_right,
            channel,
            scene: Scene::new(),
            modulations: Vec::with_capacity(32),
            rate_divider: 0,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        self.scene.set_from(&other.scene);

        for modulation in other.modulations.iter() {
            self.set_modulation_params(modulation.idx, modulation.destination, modulation.params);
        }

        self.output_channel_left = other.output_channel_left;
        self.output_channel_right = other.output_channel_right;
        self.channel = other.channel;

        self.rate_divider = other.rate_divider;
    }

    pub fn get_modulation_params(
        &self,
        idx: [usize; 4],
        destination: VoiceParameter,
    ) -> ModulationParams {
        for modulation in self.modulations.iter() {
            if modulation.idx == idx && modulation.destination == destination {
                return modulation.params;
            }
        }

        ModulationParams::new()
    }

    pub fn set_modulation_params(
        &mut self,
        idx: [usize; 4],
        destination: VoiceParameter,
        params: ModulationParams,
    ) {
        for modulation in self.modulations.iter_mut() {
            if modulation.idx == idx && modulation.destination == destination {
                modulation.params = params;
                return;
            }
        }

        self.modulations.push(Modulation {
            idx,
            destination,
            params,
        });
    }

    pub fn shift_param(&mut self, dest: VoiceParameter, amount: f32) {
        match dest {
            VoiceParameter::Voice(dest) => self.scene.voice.get_mut(dest).shift_relative(amount),
            VoiceParameter::Osc1(dest) => self.scene.osc1.get_mut(dest).shift_relative(amount),
            VoiceParameter::Noise(dest) => self.scene.noise.get_mut(dest).shift_relative(amount),
        }
    }

    pub fn get_param(&self, dest: VoiceParameter) -> Option<&Value> {
        match dest {
            VoiceParameter::Voice(dest) => Some(self.scene.voice.get(dest)),
            VoiceParameter::Osc1(dest) => Some(self.scene.osc1.get(dest)),
            VoiceParameter::Noise(dest) => Some(self.scene.noise.get(dest)),
        }
    }

    pub fn set_param(&mut self, dest: VoiceParameter, value: f32) {
        match dest {
            VoiceParameter::Voice(dest) => self.scene.voice.get_mut(dest).set(value),
            VoiceParameter::Osc1(dest) => self.scene.osc1.get_mut(dest).set(value),
            VoiceParameter::Noise(dest) => self.scene.noise.get_mut(dest).set(value),
        };
    }

    pub fn get_config(&self, dest: OperaConfig) -> usize {
        match dest {
            OperaConfig::OutputChannelLeft => self.output_channel_left,
            OperaConfig::OutputChannelRight => self.output_channel_right,
            OperaConfig::RateDivider => self.rate_divider,
        }
    }

    pub fn set_config(&mut self, dest: OperaConfig, value: usize) {
        match dest {
            OperaConfig::OutputChannelLeft => {
                self.output_channel_left = value;
            }
            OperaConfig::OutputChannelRight => {
                self.output_channel_right = value;
            }
            OperaConfig::RateDivider => {
                self.rate_divider = value;
            }
        }
    }
}

impl Scene {
    pub fn new() -> Self {
        Self {
            voice: VoiceState::default(),
            osc1: OscillatorState::default(),
            noise: NoiseState::default(),
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        self.voice.set_from(&other.voice);
        self.osc1.set_from(&other.osc1);
        self.noise.set_from(&other.noise);
    }
}

impl Default for VoiceState {
    fn default() -> Self {
        Self {
            osc1: Value::linear(1.0, 0.0, -1.0, 1.0, 0),
            noise: Value::linear(0.0, 0.0, -1.0, 1.0, 0),
            gain: Value::logarithmic(0.25, 1.0, 2.0f32.powf(-7.0), 2.0, 4),
        }
    }
}

impl VoiceState {
    pub fn get(&self, destination: VoiceDestination) -> &Value {
        match destination {
            VoiceDestination::Osc1 => &self.osc1,
            VoiceDestination::Noise => &self.noise,
            VoiceDestination::Gain => &self.gain,
        }
    }

    pub fn get_mut(&mut self, destination: VoiceDestination) -> &mut Value {
        match destination {
            VoiceDestination::Osc1 => &mut self.osc1,
            VoiceDestination::Noise => &mut self.noise,
            VoiceDestination::Gain => &mut self.gain,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in VoiceDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for OscillatorState {
    fn default() -> Self {
        Self {
            frequency: Value::logarithmic(440.0, 440.0, 27.5, 7040.0, 0),
            track: Value::linear(1.0, 1.0, 0.0, 1.0, 0),
        }
    }
}

impl OscillatorState {
    pub fn get(&self, destination: OscillatorDestination) -> &Value {
        match destination {
            OscillatorDestination::Frequency => &self.frequency,
            OscillatorDestination::Track => &self.track,
        }
    }

    pub fn get_mut(&mut self, destination: OscillatorDestination) -> &mut Value {
        match destination {
            OscillatorDestination::Frequency => &mut self.frequency,
            OscillatorDestination::Track => &mut self.track,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in OscillatorDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for NoiseState {
    fn default() -> Self {
        Self {
            balance: Value::linear(0.0, 0.0, -1.0, 1.0, 0),
        }
    }
}

impl NoiseState {
    pub fn get(&self, destination: NoiseDestination) -> &Value {
        match destination {
            NoiseDestination::Balance => &self.balance,
        }
    }

    pub fn get_mut(&mut self, destination: NoiseDestination) -> &mut Value {
        match destination {
            NoiseDestination::Balance => &mut self.balance,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in NoiseDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}
