// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

use std::fs;
use std::path::Path;

use dircpy::*;

use crate::paths;

#[derive(Deserialize, Serialize)]
pub struct Depot {
    current_project: String,

    #[serde(default = "default_trash")]
    trash: Vec<String>,
}

fn default_trash() -> Vec<String> {
    vec![]
}

impl Depot {
    fn new() -> Self {
        Self {
            current_project: new_project_impl(),
            trash: default_trash(),
        }
    }

    pub fn get_current_project(&self) -> String {
        String::from(&self.current_project)
    }

    pub fn get_current_project_index(&self) -> usize {
        let current_project = self.get_current_project();
        let all_projects = self.get_projects();
        all_projects
            .iter()
            .position(|x| *x == current_project)
            .unwrap_or(0)
    }

    pub fn get_projects(&self) -> Vec<String> {
        let mut entries = std::fs::read_dir(paths::depot_path("."))
            .unwrap()
            .map(|res| res.map(|e| e.path()).unwrap())
            .filter(|e| e.is_dir())
            .collect::<Vec<_>>();
        entries.sort();
        entries.dedup();
        let mut entries = entries
            .iter()
            .map(|e| {
                e.as_path()
                    .file_name()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .to_string()
            })
            .filter(|e| !e.starts_with('.'))
            .collect::<Vec<String>>();
        for project_name in self.trash.iter() {
            entries.retain(|x| *x != *project_name);
        }

        entries
    }

    pub fn get_trashed_projects(&self) -> Vec<String> {
        self.trash.clone()
    }

    pub fn set_current_project(&mut self, name: String) {
        self.current_project = name;
    }

    pub fn trash_project(&mut self, name: &str) {
        self.trash.push(name.to_string());
        self.trash.dedup();
    }

    pub fn rename_project(&self, source: &str, destination: &str) {
        let source = &paths::depot_path(source);
        let destination = &paths::depot_path(destination);
        fs::rename(source, destination).unwrap();
    }

    pub fn restore_project(&mut self, name: &str) {
        self.trash.retain(|x| *x != name);
    }

    pub fn new_project(&mut self) {
        self.current_project = new_project_impl();
    }

    pub fn copy_project(&self, project_name: &str) {
        let source = &paths::depot_path(project_name);
        let destination = paths::depot_path(&copy_project_impl(project_name, "C"));
        copy_dir(source, destination).unwrap();
    }

    pub fn new_project_from(&mut self, project_name: &str) {
        let source = &paths::depot_path(project_name);
        let new_name = new_project_impl();
        let destination = paths::depot_path(&new_name);
        CopyBuilder::new(source, destination)
            .overwrite_if_newer(false)
            .overwrite_if_size_differs(false)
            .with_exclude_filter(".wav")
            .with_exclude_filter("markers.toml")
            .run()
            .unwrap();
        self.set_current_project(new_name);
    }

    pub fn get_scripts(&self) -> Vec<String> {
        let scripts_path = paths::depot_path(".scripts");
        let scripts_dir = Path::new(&scripts_path);
        std::fs::create_dir_all(scripts_dir).unwrap();

        let mut entries = std::fs::read_dir(scripts_path)
            .unwrap()
            .map(|res| res.map(|e| e.path()).unwrap())
            .filter(|e| e.is_file())
            .collect::<Vec<_>>();
        entries.sort();
        entries.dedup();
        entries
            .iter()
            .map(|e| {
                e.as_path()
                    .file_name()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .to_string()
            })
            .collect::<Vec<String>>()
    }

    fn from_string(s: String) -> Self {
        match toml::from_str(&s) {
            Ok(state) => state,
            Err(_) => Self::new(),
        }
    }

    pub fn read<'a>(filename: &str) -> Self
    where
        Self: Sized + Deserialize<'a>,
    {
        match std::fs::read_to_string(paths::depot_path(filename)) {
            Ok(s) => Self::from_string(s),
            Err(_) => {
                let depot_settings = Self::new();
                depot_settings.write(filename);
                depot_settings
            }
        }
    }

    pub fn write(&self, filename: &str)
    where
        Self: Serialize,
    {
        let toml = toml::to_string(&self).unwrap();
        let path = paths::depot_path(filename);
        let dir = Path::new(&path).parent().unwrap();
        std::fs::create_dir_all(dir).unwrap();
        std::fs::write(&path, toml).unwrap();
    }
}

fn new_project_impl() -> String {
    let date = chrono::Local::now().format("%Y-%m-%d");
    for idx in 1..100 {
        let name = format!("{}_{:02}", date, idx);
        if !paths::is_dir(&paths::depot_path(&name)) {
            return name;
        }
    }
    format!("{}_XX", date)
}

fn copy_project_impl(source_name: &str, keyword: &str) -> String {
    for idx in 1..100 {
        let name = format!("{}_{}{}", source_name, keyword, idx);
        if !paths::is_dir(&paths::depot_path(&name)) {
            return name;
        }
    }
    format!("{}_XX", source_name)
}
