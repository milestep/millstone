// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use crate::messages::{BalletConfig, BalletMasterConfig};
use crate::state::State;

use crate::synth::{Modulation, ModulationParams, Value};

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize)]
pub enum Parameter {
    Master(MasterParameter),
    Voice(u8, VoiceParameter),
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize)]
pub enum MasterParameter {
    Master(MasterDestination),
    Compressor(CompressorDestination),
    Delay(DelayDestination),
    Sidechain(SidechainDestination),
    Lfo1(LfoDestination),
    Lfo2(LfoDestination),
    Acc1(AccumulatorDestination),
    Acc2(AccumulatorDestination),
    VocalProcessor(VocalProcessorDestination),
    PitchTracker(PitchTrackerDestination),
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize)]
pub enum VoiceParameter {
    Voice(VoiceDestination),
    Osc1(OscillatorDestination),
    Osc2(OscillatorDestination),
    Osc3(OscillatorDestination),
    Noise(NoiseDestination),
    TunedDelay(TunedDelayDestination),
    Flt1(FilterDestination),
    Flt2(FilterDestination),
    Wshp(WaveshaperDestination),
    Env1(EnvelopeDestination),
    Env2(EnvelopeDestination),
    Acc1(AccumulatorDestination),
    Acc2(AccumulatorDestination),
    Lfo1(LfoDestination),
    Lfo2(LfoDestination),
    Shaper(ShaperDestination),
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct MasterPatch {
    pub scene: MasterScene,
    #[serde(default)]
    pub modulations: Vec<Modulation<MasterParameter>>,

    #[serde(default)]
    pub scale: [bool; 12],
    #[serde(default)]
    pub root_note: usize,
    #[serde(default)]
    pub tuning: usize,

    #[serde(default)]
    pub rate_divider: usize,

    #[serde(default)]
    pub input_source: usize,
    #[serde(default)]
    pub sidechain_channel: usize,
    #[serde(default)]
    pub disable_vocal: usize,
    #[serde(default)]
    pub disable_pitch_tracker: usize,
    #[serde(default)]
    pub disable_delay: usize,
    #[serde(default)]
    pub disable_compressor: usize,
    #[serde(default)]
    pub disable_flt: usize,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct MasterScene {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub delay: DelayState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub master: MasterState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub compressor: CompressorState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub sc: SidechainState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub lfo1: LfoState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub lfo2: LfoState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub acc1: AccumulatorState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub acc2: AccumulatorState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub vocal_processor: VocalProcessorState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub pitch_tracker: PitchTrackerState,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum MasterDestination {
    Vocal,
    Clean,
    Delay,
    TimeScale,
    Filter,
    Gain,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct MasterState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub vocal: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub clean: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub delay: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub time_scale: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub filter: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub gain: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum CompressorDestination {
    Drive,
    Attack,
    Release,
    Threshold,
    Gain,
    Knee,
    Ratio,
    PostGain,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct CompressorState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub drive: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub attack: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub release: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub threshold: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub gain: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub knee: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub ratio: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub post_gain: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum DelayDestination {
    Balance,
    Spread,
    Diffusion,
    DiffusionLength,
    Shimmer,
    ShimmerShift,
    Grains,
    Feedback,
    Cutoff,
    Delay1,
    Time1,
    Delay2,
    Time2,
    Delay3,
    Time3,
    Delay4,
    Time4,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct DelayState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub balance: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub spread: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub diffusion: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub diffusion_length: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub shimmer: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub shimmer_shift: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub grains: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub feedback: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub cutoff: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub delay1: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub time1: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub delay2: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub time2: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub delay3: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub time3: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub delay4: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub time4: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum SidechainDestination {
    Amount,
    Attack,
    Hold,
    Decay,
    Sense,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct SidechainState {
    pub amount: Value,
    pub attack: Value,
    pub hold: Value,
    pub decay: Value,
    pub sense: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum VocalProcessorDestination {
    Gain,
    Lowpass,
    Highpass,
    Gate,
    Threshold,
    Feedback,
    Time,
    DelaySend,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct VocalProcessorState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub gain: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub lowpass: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub highpass: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub gate: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub threshold: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub feedback: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub time: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub delay_send: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum PitchTrackerDestination {
    Threshold,
    MinFreq,
    MaxFreq,
    NoiseLevel,
    RelativeFreqTolerance,
    Decay,
    SignalThreshold,
    FreqLow,
    FreqHigh,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct PitchTrackerState {
    pub threshold: Value,
    pub min_freq: Value,
    pub max_freq: Value,
    pub noise_level: Value,
    pub relative_freq_tolerance: Value,
    pub decay: Value,
    pub signal_threshold: Value,
    pub freq_low: Value,
    pub freq_high: Value,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Patch {
    #[serde(default)]
    pub output_channel_left: usize,
    #[serde(default)]
    pub output_channel_right: usize,
    #[serde(default)]
    pub channel: u8,

    #[serde(default)]
    pub rate_divider: usize,

    #[serde(default)]
    pub disable_osc1: usize,
    #[serde(default)]
    pub disable_osc2: usize,
    #[serde(default)]
    pub disable_tuned_delay: usize,
    #[serde(default)]
    pub disable_flt1: usize,
    #[serde(default)]
    pub disable_flt2: usize,
    #[serde(default)]
    pub disable_wshp: usize,
    #[serde(default)]
    pub disable_delay: usize,
    #[serde(default)]
    pub disable_env1: usize,
    #[serde(default)]
    pub disable_env2: usize,
    #[serde(default)]
    pub disable_lfo1: usize,
    #[serde(default)]
    pub disable_lfo2: usize,

    pub scene: Scene,
    #[serde(default)]
    pub modulations: Vec<Modulation<VoiceParameter>>,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Scene {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub voice: VoiceState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub shaper: ShaperState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub osc1: OscillatorState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub osc2: OscillatorState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub osc3: OscillatorState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub noise: NoiseState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub tuned_delay: TunedDelayState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub flt1: FilterState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub flt2: FilterState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub wshp: WaveshaperState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub env1: EnvelopeState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub env2: EnvelopeState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub lfo1: LfoState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub lfo2: LfoState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub acc1: AccumulatorState,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub acc2: AccumulatorState,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum VoiceDestination {
    Osc1,
    Osc2,
    Osc3,
    Noise,
    TunedDelay,
    Flt2,
    Gain,
    Frequency,
    Portamento,
    Arp,
    Delay,
    Pan,
    Width,
    Send,
    DelaySend,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct VoiceState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub osc1: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub osc2: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub osc3: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub noise: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub tuned_delay: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub flt2: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub gain: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub frequency: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub portamento: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub arp: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub delay: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub pan: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub width: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub send: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub delay_send: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum OscillatorDestination {
    Frequency,
    Phase,
    Delay,
    Track,
    Asymmetry,
    Width,
    Gap,
    Cushion,
    Melt,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct OscillatorState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub frequency: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub phase: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub delay: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub track: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub asymmetry: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub width: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub gap: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub cushion: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub melt: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum NoiseDestination {
    Balance,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct NoiseState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub balance: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum TunedDelayDestination {
    Input,
    Frequency,
    Phase,
    Track,
    Speed,
    Feedback,
    FilterFrequency,
    Sampling,
    Blend,
    Osc1,
    Osc2,
    Osc3,
    Noise,
    Voice,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct TunedDelayState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub input: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub frequency: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub phase: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub track: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub speed: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub feedback: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub filter_frequency: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub sampling: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub blend: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub osc1: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub osc2: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub osc3: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub noise: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub voice: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum FilterDestination {
    Frequency,
    Lowpass,
    Highpass,
    Bandpass,
    Resonance,
    Headroom,
    Decay,
    Track,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct FilterState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub frequency: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub lowpass: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub highpass: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub bandpass: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub resonance: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub headroom: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub decay: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub track: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum WaveshaperDestination {
    Pregain,
    Asymmetry,
    Gate,
    Cushion,
    Hardclip,
    Crossover,
    Crush,
    Sampling,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct WaveshaperState {
    pub pregain: Value,
    pub asymmetry: Value,
    pub gate: Value,
    pub cushion: Value,
    pub hardclip: Value,
    pub crossover: Value,
    pub crush: Value,
    pub sampling: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum EnvelopeDestination {
    Amount,
    Attack,
    Hold,
    Decay,
    Sustain,
    Slope,
    Scatter,
    Echo,
    Release,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct EnvelopeState {
    pub amount: Value,
    pub attack: Value,
    pub hold: Value,
    pub decay: Value,
    pub sustain: Value,
    pub slope: Value,
    pub scatter: Value,
    pub echo: Value,
    pub release: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum LfoDestination {
    Amount,
    Frequency,
    Phase,
    Asymmetry,
    Cushion,
    Noise,
    Sampling,
    Slew,
    Track,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct LfoState {
    pub amount: Value,
    pub frequency: Value,
    pub phase: Value,
    pub asymmetry: Value,
    pub cushion: Value,
    pub noise: Value,
    pub sampling: Value,
    pub slew: Value,
    pub track: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum AccumulatorDestination {
    Value,
    Slew,
    Depth,
    Rate,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct AccumulatorState {
    pub value: Value,
    pub slew: Value,
    pub depth: Value,
    pub rate: Value,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize, EnumIter)]
pub enum ShaperDestination {
    Trigger,
    Delay,
    Attack,
    Hold,
    Decay,
    Sustain,
    Release,
    Sense,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct ShaperState {
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub trigger: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub delay: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub attack: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub hold: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub decay: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub sustain: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub release: Value,
    #[serde(default)]
    #[serde(deserialize_with = "crate::state::ok_or_default")]
    pub sense: Value,
}

impl Default for MasterPatch {
    fn default() -> Self {
        Self::new()
    }
}

impl MasterPatch {
    pub fn new() -> Self {
        Self {
            scene: MasterScene::new(),
            modulations: Vec::with_capacity(32),
            scale: [true; 12],
            rate_divider: 0,
            input_source: 1,
            root_note: 0,
            tuning: 0,
            sidechain_channel: 9,
            disable_vocal: 0,
            disable_pitch_tracker: 0,
            disable_delay: 0,
            disable_compressor: 0,
            disable_flt: 0,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        self.scene.set_from(&other.scene);

        self.modulations.clear();

        for modulation in other.modulations.iter() {
            self.set_modulation_params(modulation.idx, modulation.destination, modulation.params);
        }

        for (other_note, note) in other.scale.iter().zip(self.scale.iter_mut()) {
            *note = *other_note;
        }

        self.rate_divider = other.rate_divider;

        self.input_source = other.input_source;
        self.sidechain_channel = other.sidechain_channel;

        self.disable_vocal = other.disable_vocal;
        self.disable_pitch_tracker = other.disable_pitch_tracker;
        self.disable_delay = other.disable_delay;
        self.disable_compressor = other.disable_compressor;
        self.disable_flt = other.disable_flt;
    }

    pub fn get_note(&self, idx: i16) -> bool {
        self.scale[idx.rem_euclid(12) as usize]
    }

    pub fn toggle_note(&mut self, idx: i16) -> bool {
        let note = idx.rem_euclid(12) as usize;
        self.scale[note] = !self.scale[note];
        self.scale[note]
    }

    pub fn get_modulation_params(
        &self,
        idx: [usize; 4],
        destination: MasterParameter,
    ) -> ModulationParams {
        for modulation in self.modulations.iter() {
            if modulation.idx == idx && modulation.destination == destination {
                return modulation.params;
            }
        }

        ModulationParams::new()
    }

    pub fn set_modulation_params(
        &mut self,
        idx: [usize; 4],
        destination: MasterParameter,
        params: ModulationParams,
    ) {
        let mut remove_idx = None;

        for (modulation_idx, modulation) in self.modulations.iter_mut().enumerate() {
            if modulation.idx == idx && modulation.destination == destination {
                if params.amount == 0.0
                    && params.lower == 0.0
                    && params.upper == 1.0
                    && params.offset == 1.0
                {
                    remove_idx = Some(modulation_idx);
                    break;
                }

                modulation.params = params;
                return;
            }
        }

        if let Some(idx) = remove_idx {
            self.modulations.swap_remove(idx);
            return;
        }

        self.modulations.push(Modulation {
            idx,
            destination,
            params,
        });
    }

    pub fn shift_param(&mut self, dest: MasterParameter, amount: f32) {
        match dest {
            MasterParameter::Master(dest) => self.scene.master.get_mut(dest).shift_relative(amount),
            MasterParameter::Compressor(dest) => {
                self.scene.compressor.get_mut(dest).shift_relative(amount)
            }
            MasterParameter::Delay(dest) => self.scene.delay.get_mut(dest).shift_relative(amount),
            MasterParameter::Sidechain(dest) => self.scene.sc.get_mut(dest).shift_relative(amount),
            MasterParameter::Lfo1(dest) => self.scene.lfo1.get_mut(dest).shift_relative(amount),
            MasterParameter::Lfo2(dest) => self.scene.lfo2.get_mut(dest).shift_relative(amount),
            MasterParameter::Acc1(dest) => self.scene.acc1.get_mut(dest).shift_relative(amount),
            MasterParameter::Acc2(dest) => self.scene.acc2.get_mut(dest).shift_relative(amount),
            MasterParameter::VocalProcessor(dest) => self
                .scene
                .vocal_processor
                .get_mut(dest)
                .shift_relative(amount),
            MasterParameter::PitchTracker(dest) => self
                .scene
                .pitch_tracker
                .get_mut(dest)
                .shift_relative(amount),
        }
    }

    pub fn get_param(&self, dest: MasterParameter) -> Option<&Value> {
        match dest {
            MasterParameter::Master(dest) => Some(self.scene.master.get(dest)),
            MasterParameter::Compressor(dest) => Some(self.scene.compressor.get(dest)),
            MasterParameter::Delay(dest) => Some(self.scene.delay.get(dest)),
            MasterParameter::Sidechain(dest) => Some(self.scene.sc.get(dest)),
            MasterParameter::Lfo1(dest) => Some(self.scene.lfo1.get(dest)),
            MasterParameter::Lfo2(dest) => Some(self.scene.lfo2.get(dest)),
            MasterParameter::Acc1(dest) => Some(self.scene.acc1.get(dest)),
            MasterParameter::Acc2(dest) => Some(self.scene.acc2.get(dest)),
            MasterParameter::VocalProcessor(dest) => Some(self.scene.vocal_processor.get(dest)),
            MasterParameter::PitchTracker(dest) => Some(self.scene.pitch_tracker.get(dest)),
        }
    }

    pub fn set_param(&mut self, dest: MasterParameter, value: f32) {
        match dest {
            MasterParameter::Master(dest) => self.scene.master.get_mut(dest).set(value),
            MasterParameter::Compressor(dest) => self.scene.compressor.get_mut(dest).set(value),
            MasterParameter::Delay(dest) => self.scene.delay.get_mut(dest).set(value),
            MasterParameter::Sidechain(dest) => self.scene.sc.get_mut(dest).set(value),
            MasterParameter::Lfo1(dest) => self.scene.lfo1.get_mut(dest).set(value),
            MasterParameter::Lfo2(dest) => self.scene.lfo2.get_mut(dest).set(value),
            MasterParameter::Acc1(dest) => self.scene.acc1.get_mut(dest).set(value),
            MasterParameter::Acc2(dest) => self.scene.acc2.get_mut(dest).set(value),
            MasterParameter::VocalProcessor(dest) => {
                self.scene.vocal_processor.get_mut(dest).set(value)
            }
            MasterParameter::PitchTracker(dest) => {
                self.scene.pitch_tracker.get_mut(dest).set(value)
            }
        };
    }

    pub fn get_config(&self, dest: BalletMasterConfig) -> usize {
        match dest {
            BalletMasterConfig::RootNote => self.root_note,
            BalletMasterConfig::Tuning => self.tuning,
            BalletMasterConfig::InputSource => self.input_source,
            BalletMasterConfig::SidechainChannel => self.sidechain_channel,
            BalletMasterConfig::RateDivider => self.rate_divider,
            BalletMasterConfig::DisableVocal => self.disable_vocal,
            BalletMasterConfig::DisablePitchTracker => self.disable_pitch_tracker,
            BalletMasterConfig::DisableDelay => self.disable_delay,
            BalletMasterConfig::DisableCompressor => self.disable_compressor,
            BalletMasterConfig::DisableFilter => self.disable_flt,
        }
    }

    pub fn set_config(&mut self, dest: BalletMasterConfig, value: usize) {
        match dest {
            BalletMasterConfig::RootNote => {
                self.root_note = value;
            }
            BalletMasterConfig::Tuning => {
                self.tuning = value;
            }
            BalletMasterConfig::SidechainChannel => {
                self.sidechain_channel = value;
            }
            BalletMasterConfig::InputSource => {
                self.input_source = value;
            }
            BalletMasterConfig::RateDivider => {
                self.rate_divider = value;
            }
            BalletMasterConfig::DisableVocal => {
                self.disable_vocal = value;
            }
            BalletMasterConfig::DisablePitchTracker => {
                self.disable_pitch_tracker = value;
            }
            BalletMasterConfig::DisableDelay => {
                self.disable_delay = value;
            }
            BalletMasterConfig::DisableCompressor => {
                self.disable_compressor = value;
            }
            BalletMasterConfig::DisableFilter => {
                self.disable_flt = value;
            }
        }
    }
}

impl MasterScene {
    pub fn new() -> Self {
        Self {
            delay: DelayState::default(),
            master: MasterState::default(),
            compressor: CompressorState::default(),
            sc: SidechainState::default(),
            lfo1: LfoState::default(),
            lfo2: LfoState::default(),
            acc1: AccumulatorState::default(),
            acc2: AccumulatorState::default(),
            vocal_processor: VocalProcessorState::default(),
            pitch_tracker: PitchTrackerState::default(),
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        self.delay.set_from(&other.delay);
        self.master.set_from(&other.master);
        self.compressor.set_from(&other.compressor);
        self.sc.set_from(&other.sc);
        self.lfo1.set_from(&other.lfo1);
        self.lfo2.set_from(&other.lfo2);
        self.acc1.set_from(&other.acc1);
        self.acc2.set_from(&other.acc2);
        self.vocal_processor.set_from(&other.vocal_processor);
        self.pitch_tracker.set_from(&other.pitch_tracker);
    }
}

impl Default for MasterState {
    fn default() -> Self {
        Self {
            vocal: Value::logarithmic(2.0f32.powf(-7.0), 1.0, 2.0f32.powf(-7.0), 2.0, 0),
            clean: Value::logarithmic(1.0, 1.0, 2.0f32.powf(-7.0), 2.0, 0),
            delay: Value::logarithmic(0.5, 1.0, 2.0f32.powf(-7.0), 2.0, 1),
            time_scale: Value::logarithmic(1.0, 1.0, 2.0f32.powf(-3.0), 2.0f32.powf(3.0), 3),
            filter: Value::linear(0.0, 0.0, -1.0, 1.0, 3),
            gain: Value::logarithmic(1.0, 1.0, 2.0f32.powf(-7.0), 2.0, 0),
        }
    }
}

impl MasterState {
    #[inline(always)]
    pub fn get(&self, destination: MasterDestination) -> &Value {
        match destination {
            MasterDestination::Vocal => &self.vocal,
            MasterDestination::Clean => &self.clean,
            MasterDestination::Delay => &self.delay,
            MasterDestination::TimeScale => &self.time_scale,
            MasterDestination::Filter => &self.filter,
            MasterDestination::Gain => &self.gain,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: MasterDestination) -> &mut Value {
        match destination {
            MasterDestination::Vocal => &mut self.vocal,
            MasterDestination::Clean => &mut self.clean,
            MasterDestination::Delay => &mut self.delay,
            MasterDestination::TimeScale => &mut self.time_scale,
            MasterDestination::Filter => &mut self.filter,
            MasterDestination::Gain => &mut self.gain,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in MasterDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for CompressorState {
    fn default() -> Self {
        Self {
            drive: Value::linear(0.0, 1.0, 0.0, 1.0, 0),
            attack: Value::logarithmic(0.005, 1.0, 0.001, 0.5, 1),
            release: Value::logarithmic(0.05, 1.0, 0.001, 0.5, 1),
            threshold: Value::linear(0.0, 0.0, -48.0, 0.0, 2),
            gain: Value::linear(0.0, 0.0, -48.0, 0.0, 2),
            knee: Value::linear(3.0, 3.0, 0.0, 18.0, 2),
            ratio: Value::logarithmic(2.0, 2.0, 1.0, 20.0, 3),
            post_gain: Value::linear(0.0, 0.0, -48.0, 0.0, 0),
        }
    }
}

impl CompressorState {
    #[inline(always)]
    pub fn get(&self, destination: CompressorDestination) -> &Value {
        match destination {
            CompressorDestination::Drive => &self.drive,
            CompressorDestination::Attack => &self.attack,
            CompressorDestination::Release => &self.release,
            CompressorDestination::Threshold => &self.threshold,
            CompressorDestination::Gain => &self.gain,
            CompressorDestination::Knee => &self.knee,
            CompressorDestination::Ratio => &self.ratio,
            CompressorDestination::PostGain => &self.post_gain,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: CompressorDestination) -> &mut Value {
        match destination {
            CompressorDestination::Drive => &mut self.drive,
            CompressorDestination::Attack => &mut self.attack,
            CompressorDestination::Release => &mut self.release,
            CompressorDestination::Threshold => &mut self.threshold,
            CompressorDestination::Gain => &mut self.gain,
            CompressorDestination::Knee => &mut self.knee,
            CompressorDestination::Ratio => &mut self.ratio,
            CompressorDestination::PostGain => &mut self.post_gain,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in CompressorDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for DelayState {
    fn default() -> Self {
        Self {
            balance: Value::linear(1.0, 1.0, 0.0, 1.0, 0),
            spread: Value::linear(0.0, 0.0, -1.0, 1.0, 0),
            diffusion: Value::linear(0.0001, 0.5, 0.0001, 0.9999, 1),
            diffusion_length: Value::logarithmic(0.05, 0.1, 0.001, 1.0, 1),
            shimmer: Value::logarithmic(2.0f32.powf(-6.0), 1.0, 2.0f32.powf(-6.0), 4.0, 2),
            shimmer_shift: Value::logarithmic(2.0, 2.0, 2.0f32.powf(-4.0), 2.0f32.powf(4.0), 2),
            grains: Value::linear(1.0, 1.0, 1.0, 9.0, 2),
            feedback: Value::linear(0.0, 1.0, 0.0, 1.0, 3),
            cutoff: Value::linear(0.99, 0.99, 0.98, 1.0, 3),
            delay1: Value::linear(0.52, 1.0, 0.0, 1.0, 0),
            time1: Value::logarithmic(0.04, 0.0, 0.001, 10.0, 0),
            delay2: Value::linear(0.48, 1.0, 0.0, 1.0, 1),
            time2: Value::logarithmic(0.007, 0.0, 0.001, 10.0, 1),
            delay3: Value::linear(0.45, 1.0, 0.0, 1.0, 2),
            time3: Value::logarithmic(0.012, 0.0, 0.001, 10.0, 2),
            delay4: Value::linear(0.55, 1.0, 0.0, 1.0, 2),
            time4: Value::logarithmic(0.01, 0.0, 0.001, 10.0, 2),
        }
    }
}

impl DelayState {
    #[inline(always)]
    pub fn get(&self, destination: DelayDestination) -> &Value {
        match destination {
            DelayDestination::Balance => &self.balance,
            DelayDestination::Spread => &self.spread,
            DelayDestination::Diffusion => &self.diffusion,
            DelayDestination::DiffusionLength => &self.diffusion_length,
            DelayDestination::Shimmer => &self.shimmer,
            DelayDestination::ShimmerShift => &self.shimmer_shift,
            DelayDestination::Grains => &self.grains,
            DelayDestination::Feedback => &self.feedback,
            DelayDestination::Cutoff => &self.cutoff,
            DelayDestination::Delay1 => &self.delay1,
            DelayDestination::Time1 => &self.time1,
            DelayDestination::Delay2 => &self.delay2,
            DelayDestination::Time2 => &self.time2,
            DelayDestination::Delay3 => &self.delay3,
            DelayDestination::Time3 => &self.time3,
            DelayDestination::Delay4 => &self.delay4,
            DelayDestination::Time4 => &self.time4,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: DelayDestination) -> &mut Value {
        match destination {
            DelayDestination::Balance => &mut self.balance,
            DelayDestination::Spread => &mut self.spread,
            DelayDestination::Diffusion => &mut self.diffusion,
            DelayDestination::DiffusionLength => &mut self.diffusion_length,
            DelayDestination::Shimmer => &mut self.shimmer,
            DelayDestination::ShimmerShift => &mut self.shimmer_shift,
            DelayDestination::Grains => &mut self.grains,
            DelayDestination::Feedback => &mut self.feedback,
            DelayDestination::Cutoff => &mut self.cutoff,
            DelayDestination::Delay1 => &mut self.delay1,
            DelayDestination::Time1 => &mut self.time1,
            DelayDestination::Delay2 => &mut self.delay2,
            DelayDestination::Time2 => &mut self.time2,
            DelayDestination::Delay3 => &mut self.delay3,
            DelayDestination::Time3 => &mut self.time3,
            DelayDestination::Delay4 => &mut self.delay4,
            DelayDestination::Time4 => &mut self.time4,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in DelayDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for SidechainState {
    fn default() -> Self {
        Self {
            amount: Value::linear(1.0, 1.0, 0.0, 1.0, 0),
            attack: Value::logarithmic(0.01, 1.0, 0.003, 30.0, 1),
            hold: Value::logarithmic(0.1, 1.0, 0.0005, 5.0, 1),
            decay: Value::logarithmic(0.5, 1.0, 0.003, 30.0, 1),
            sense: Value::linear(0.0, 1.0, 0.0, 1.0, 2),
        }
    }
}

impl SidechainState {
    #[inline(always)]
    pub fn get(&self, destination: SidechainDestination) -> &Value {
        match destination {
            SidechainDestination::Amount => &self.amount,
            SidechainDestination::Attack => &self.attack,
            SidechainDestination::Hold => &self.hold,
            SidechainDestination::Decay => &self.decay,
            SidechainDestination::Sense => &self.sense,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: SidechainDestination) -> &mut Value {
        match destination {
            SidechainDestination::Amount => &mut self.amount,
            SidechainDestination::Attack => &mut self.attack,
            SidechainDestination::Hold => &mut self.hold,
            SidechainDestination::Decay => &mut self.decay,
            SidechainDestination::Sense => &mut self.sense,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in SidechainDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for VocalProcessorState {
    fn default() -> Self {
        Self {
            lowpass: Value::linear(0.0, 0.0, -0.5, 0.0, 0),
            highpass: Value::linear(0.0, 0.0, 0.0, 0.5, 0),
            gate: Value::linear(-36.0, -36.0, -48.0, 0.0, 1),
            threshold: Value::linear(0.0, 0.0, -48.0, 0.0, 1),
            gain: Value::linear(0.0, 0.0, -48.0, 0.0, 1),
            feedback: Value::linear(0.0, 0.0, -1.0, 1.0, 2),
            time: Value::logarithmic(0.003, 0.5, 0.003, 3.0, 2),
            delay_send: Value::logarithmic(2.0f32.powf(-7.0), 1.0, 2.0f32.powf(-7.0), 2.0, 3),
        }
    }
}

impl VocalProcessorState {
    #[inline(always)]
    pub fn get(&self, destination: VocalProcessorDestination) -> &Value {
        match destination {
            VocalProcessorDestination::Gain => &self.gain,
            VocalProcessorDestination::Lowpass => &self.lowpass,
            VocalProcessorDestination::Highpass => &self.highpass,
            VocalProcessorDestination::Gate => &self.gate,
            VocalProcessorDestination::Threshold => &self.threshold,
            VocalProcessorDestination::Feedback => &self.feedback,
            VocalProcessorDestination::Time => &self.time,
            VocalProcessorDestination::DelaySend => &self.delay_send,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: VocalProcessorDestination) -> &mut Value {
        match destination {
            VocalProcessorDestination::Gain => &mut self.gain,
            VocalProcessorDestination::Lowpass => &mut self.lowpass,
            VocalProcessorDestination::Highpass => &mut self.highpass,
            VocalProcessorDestination::Gate => &mut self.gate,
            VocalProcessorDestination::Threshold => &mut self.threshold,
            VocalProcessorDestination::Feedback => &mut self.feedback,
            VocalProcessorDestination::Time => &mut self.time,
            VocalProcessorDestination::DelaySend => &mut self.delay_send,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in VocalProcessorDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for PitchTrackerState {
    fn default() -> Self {
        Self {
            threshold: Value::linear(0.36, 0.36, 0.0, 1.0, 0),
            min_freq: Value::logarithmic(55.0, 55.0, 27.5, 7040.0, 1),
            max_freq: Value::logarithmic(1760.0, 1760.0, 27.5, 7040.0, 1),
            noise_level: Value::linear(-90.0, -90.0, -120.0, -40.0, 2),
            relative_freq_tolerance: Value::linear(0.03, 0.03, 0.0, 0.2, 2),
            decay: Value::linear(0.0005, 0.0005, 0.0, 0.005, 2),
            signal_threshold: Value::logarithmic(0.003, 0.5, 0.003, 3.0, 2),
            freq_low: Value::logarithmic(200.0, 200.0, 50.0, 800.0, 1),
            freq_high: Value::logarithmic(400.0, 400.0, 50.5, 800.0, 1),
        }
    }
}

impl PitchTrackerState {
    #[inline(always)]
    pub fn get(&self, destination: PitchTrackerDestination) -> &Value {
        match destination {
            PitchTrackerDestination::Threshold => &self.threshold,
            PitchTrackerDestination::MinFreq => &self.min_freq,
            PitchTrackerDestination::MaxFreq => &self.max_freq,
            PitchTrackerDestination::NoiseLevel => &self.noise_level,
            PitchTrackerDestination::RelativeFreqTolerance => &self.relative_freq_tolerance,
            PitchTrackerDestination::Decay => &self.decay,
            PitchTrackerDestination::SignalThreshold => &self.signal_threshold,
            PitchTrackerDestination::FreqHigh => &self.freq_low,
            PitchTrackerDestination::FreqLow => &self.freq_high,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: PitchTrackerDestination) -> &mut Value {
        match destination {
            PitchTrackerDestination::Threshold => &mut self.threshold,
            PitchTrackerDestination::MinFreq => &mut self.min_freq,
            PitchTrackerDestination::MaxFreq => &mut self.max_freq,
            PitchTrackerDestination::NoiseLevel => &mut self.noise_level,
            PitchTrackerDestination::RelativeFreqTolerance => &mut self.relative_freq_tolerance,
            PitchTrackerDestination::Decay => &mut self.decay,
            PitchTrackerDestination::SignalThreshold => &mut self.signal_threshold,
            PitchTrackerDestination::FreqHigh => &mut self.freq_low,
            PitchTrackerDestination::FreqLow => &mut self.freq_high,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in PitchTrackerDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for Patch {
    fn default() -> Self {
        Self::new(0, 1, 255)
    }
}

impl State for Patch {
    fn new() -> Self {
        Patch::new(0, 1, 1)
    }

    fn from_string(s: String) -> Self {
        match ron::from_str(&s) {
            Ok(state) => state,
            Err(_) => Self::new(0, 1, 1),
        }
    }
}

impl Patch {
    pub fn new(output_channel_left: usize, output_channel_right: usize, channel: u8) -> Self {
        Self {
            output_channel_left,
            output_channel_right,
            channel,
            scene: Scene::new(),
            modulations: Vec::with_capacity(32),
            rate_divider: 0,
            disable_osc1: 0,
            disable_osc2: 0,
            disable_tuned_delay: 0,
            disable_flt1: 0,
            disable_flt2: 0,
            disable_wshp: 0,
            disable_delay: 0,
            disable_env1: 0,
            disable_env2: 0,
            disable_lfo1: 0,
            disable_lfo2: 0,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        self.scene.set_from(&other.scene);

        self.modulations.clear();

        for modulation in other.modulations.iter() {
            self.set_modulation_params(modulation.idx, modulation.destination, modulation.params);
        }

        self.output_channel_left = other.output_channel_left;
        self.output_channel_right = other.output_channel_right;
        self.channel = other.channel;

        self.rate_divider = other.rate_divider;

        self.disable_osc1 = other.disable_osc1;
        self.disable_osc2 = other.disable_osc2;
        self.disable_tuned_delay = other.disable_tuned_delay;
        self.disable_flt1 = other.disable_flt1;
        self.disable_flt2 = other.disable_flt2;
        self.disable_wshp = other.disable_wshp;
        self.disable_delay = other.disable_delay;
        self.disable_env1 = other.disable_env1;
        self.disable_env2 = other.disable_env2;
        self.disable_lfo1 = other.disable_lfo1;
        self.disable_lfo2 = other.disable_lfo2;
    }

    pub fn get_modulation_params(
        &self,
        idx: [usize; 4],
        destination: VoiceParameter,
    ) -> ModulationParams {
        for modulation in self.modulations.iter() {
            if modulation.idx == idx && modulation.destination == destination {
                return modulation.params;
            }
        }

        ModulationParams::new()
    }

    pub fn set_modulation_params(
        &mut self,
        idx: [usize; 4],
        destination: VoiceParameter,
        params: ModulationParams,
    ) {
        let mut remove_idx = None;

        for (modulation_idx, modulation) in self.modulations.iter_mut().enumerate() {
            if modulation.idx == idx && modulation.destination == destination {
                if params.amount == 0.0
                    && params.lower == 0.0
                    && params.upper == 1.0
                    && params.offset == 1.0
                {
                    remove_idx = Some(modulation_idx);
                    break;
                }

                modulation.params = params;
                return;
            }
        }

        if let Some(idx) = remove_idx {
            self.modulations.swap_remove(idx);
            return;
        }

        self.modulations.push(Modulation {
            idx,
            destination,
            params,
        });
    }

    pub fn shift_param(&mut self, dest: VoiceParameter, amount: f32) {
        match dest {
            VoiceParameter::Voice(dest) => self.scene.voice.get_mut(dest).shift_relative(amount),
            VoiceParameter::Osc1(dest) => self.scene.osc1.get_mut(dest).shift_relative(amount),
            VoiceParameter::Osc2(dest) => self.scene.osc2.get_mut(dest).shift_relative(amount),
            VoiceParameter::Osc3(dest) => self.scene.osc3.get_mut(dest).shift_relative(amount),
            VoiceParameter::Noise(dest) => self.scene.noise.get_mut(dest).shift_relative(amount),
            VoiceParameter::TunedDelay(dest) => {
                self.scene.tuned_delay.get_mut(dest).shift_relative(amount)
            }
            VoiceParameter::Flt1(dest) => self.scene.flt1.get_mut(dest).shift_relative(amount),
            VoiceParameter::Flt2(dest) => self.scene.flt2.get_mut(dest).shift_relative(amount),
            VoiceParameter::Wshp(dest) => self.scene.wshp.get_mut(dest).shift_relative(amount),
            VoiceParameter::Env1(dest) => self.scene.env1.get_mut(dest).shift_relative(amount),
            VoiceParameter::Env2(dest) => self.scene.env2.get_mut(dest).shift_relative(amount),
            VoiceParameter::Lfo1(dest) => self.scene.lfo1.get_mut(dest).shift_relative(amount),
            VoiceParameter::Lfo2(dest) => self.scene.lfo2.get_mut(dest).shift_relative(amount),
            VoiceParameter::Acc1(dest) => self.scene.acc1.get_mut(dest).shift_relative(amount),
            VoiceParameter::Acc2(dest) => self.scene.acc2.get_mut(dest).shift_relative(amount),
            VoiceParameter::Shaper(dest) => self.scene.shaper.get_mut(dest).shift_relative(amount),
        }
    }

    pub fn get_param(&self, dest: VoiceParameter) -> Option<&Value> {
        match dest {
            VoiceParameter::Voice(dest) => Some(self.scene.voice.get(dest)),
            VoiceParameter::Osc1(dest) => Some(self.scene.osc1.get(dest)),
            VoiceParameter::Osc2(dest) => Some(self.scene.osc2.get(dest)),
            VoiceParameter::Osc3(dest) => Some(self.scene.osc3.get(dest)),
            VoiceParameter::Noise(dest) => Some(self.scene.noise.get(dest)),
            VoiceParameter::TunedDelay(dest) => Some(self.scene.tuned_delay.get(dest)),
            VoiceParameter::Flt1(dest) => Some(self.scene.flt1.get(dest)),
            VoiceParameter::Flt2(dest) => Some(self.scene.flt2.get(dest)),
            VoiceParameter::Wshp(dest) => Some(self.scene.wshp.get(dest)),
            VoiceParameter::Env1(dest) => Some(self.scene.env1.get(dest)),
            VoiceParameter::Env2(dest) => Some(self.scene.env2.get(dest)),
            VoiceParameter::Lfo1(dest) => Some(self.scene.lfo1.get(dest)),
            VoiceParameter::Lfo2(dest) => Some(self.scene.lfo2.get(dest)),
            VoiceParameter::Acc1(dest) => Some(self.scene.acc1.get(dest)),
            VoiceParameter::Acc2(dest) => Some(self.scene.acc2.get(dest)),
            VoiceParameter::Shaper(dest) => Some(self.scene.shaper.get(dest)),
        }
    }

    pub fn set_param(&mut self, dest: VoiceParameter, value: f32) {
        match dest {
            VoiceParameter::Voice(dest) => self.scene.voice.get_mut(dest).set(value),
            VoiceParameter::Osc1(dest) => self.scene.osc1.get_mut(dest).set(value),
            VoiceParameter::Osc2(dest) => self.scene.osc2.get_mut(dest).set(value),
            VoiceParameter::Osc3(dest) => self.scene.osc3.get_mut(dest).set(value),
            VoiceParameter::Noise(dest) => self.scene.noise.get_mut(dest).set(value),
            VoiceParameter::TunedDelay(dest) => self.scene.tuned_delay.get_mut(dest).set(value),
            VoiceParameter::Flt1(dest) => self.scene.flt1.get_mut(dest).set(value),
            VoiceParameter::Flt2(dest) => self.scene.flt2.get_mut(dest).set(value),
            VoiceParameter::Wshp(dest) => self.scene.wshp.get_mut(dest).set(value),
            VoiceParameter::Env1(dest) => self.scene.env1.get_mut(dest).set(value),
            VoiceParameter::Env2(dest) => self.scene.env2.get_mut(dest).set(value),
            VoiceParameter::Lfo1(dest) => self.scene.lfo1.get_mut(dest).set(value),
            VoiceParameter::Lfo2(dest) => self.scene.lfo2.get_mut(dest).set(value),
            VoiceParameter::Acc1(dest) => self.scene.acc1.get_mut(dest).set(value),
            VoiceParameter::Acc2(dest) => self.scene.acc2.get_mut(dest).set(value),
            VoiceParameter::Shaper(dest) => self.scene.shaper.get_mut(dest).set(value),
        };
    }

    pub fn get_config(&self, dest: BalletConfig) -> usize {
        match dest {
            BalletConfig::OutputChannelLeft => self.output_channel_left,
            BalletConfig::OutputChannelRight => self.output_channel_right,
            BalletConfig::RateDivider => self.rate_divider,
            BalletConfig::DisableOsc1 => self.disable_osc1,
            BalletConfig::DisableOsc2 => self.disable_osc2,
            BalletConfig::DisableTunedDelay => self.disable_tuned_delay,
            BalletConfig::DisableFlt1 => self.disable_flt1,
            BalletConfig::DisableFlt2 => self.disable_flt2,
            BalletConfig::DisableWshp => self.disable_wshp,
            BalletConfig::DisableDelay => self.disable_delay,
            BalletConfig::DisableEnv1 => self.disable_env1,
            BalletConfig::DisableEnv2 => self.disable_env2,
            BalletConfig::DisableLfo1 => self.disable_lfo1,
            BalletConfig::DisableLfo2 => self.disable_lfo2,
        }
    }

    pub fn set_config(&mut self, dest: BalletConfig, value: usize) {
        match dest {
            BalletConfig::OutputChannelLeft => {
                self.output_channel_left = value;
            }
            BalletConfig::OutputChannelRight => {
                self.output_channel_right = value;
            }
            BalletConfig::RateDivider => {
                self.rate_divider = value;
            }
            BalletConfig::DisableOsc1 => {
                self.disable_osc1 = value;
            }
            BalletConfig::DisableOsc2 => {
                self.disable_osc2 = value;
            }
            BalletConfig::DisableTunedDelay => {
                self.disable_tuned_delay = value;
            }
            BalletConfig::DisableFlt1 => {
                self.disable_flt1 = value;
            }
            BalletConfig::DisableFlt2 => {
                self.disable_flt2 = value;
            }
            BalletConfig::DisableWshp => {
                self.disable_wshp = value;
            }
            BalletConfig::DisableDelay => {
                self.disable_delay = value;
            }
            BalletConfig::DisableEnv1 => {
                self.disable_env1 = value;
            }
            BalletConfig::DisableEnv2 => {
                self.disable_env2 = value;
            }
            BalletConfig::DisableLfo1 => {
                self.disable_lfo1 = value;
            }
            BalletConfig::DisableLfo2 => {
                self.disable_lfo2 = value;
            }
        }
    }
}

impl Scene {
    pub fn new() -> Self {
        Self {
            voice: VoiceState::default(),
            shaper: ShaperState::default(),
            osc1: OscillatorState::default(),
            osc2: OscillatorState::default(),
            osc3: OscillatorState::default(),
            noise: NoiseState::default(),
            tuned_delay: TunedDelayState::default(),
            flt1: FilterState::with_frequency(440.0 * 2.0f32.powf(5.0 / 4.0)),
            flt2: FilterState::with_frequency(440.0),
            wshp: WaveshaperState::default(),
            env1: EnvelopeState::default(),
            env2: EnvelopeState::default(),
            lfo1: LfoState::default(),
            lfo2: LfoState::default(),
            acc1: AccumulatorState::default(),
            acc2: AccumulatorState::default(),
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        self.voice.set_from(&other.voice);
        self.shaper.set_from(&other.shaper);
        self.osc1.set_from(&other.osc1);
        self.osc2.set_from(&other.osc2);
        self.osc3.set_from(&other.osc3);
        self.noise.set_from(&other.noise);
        self.tuned_delay.set_from(&other.tuned_delay);
        self.flt1.set_from(&other.flt1);
        self.flt2.set_from(&other.flt2);
        self.wshp.set_from(&other.wshp);
        self.env1.set_from(&other.env1);
        self.env2.set_from(&other.env2);
        self.lfo1.set_from(&other.lfo1);
        self.lfo2.set_from(&other.lfo2);
        self.acc1.set_from(&other.acc1);
        self.acc2.set_from(&other.acc2);
        self.acc1.set_from(&other.acc1);
        self.acc2.set_from(&other.acc2);
    }
}

impl Default for VoiceState {
    fn default() -> Self {
        Self {
            osc1: Value::linear(1.0, 0.0, -1.0, 1.0, 0),
            osc2: Value::linear(0.0, 0.0, -1.0, 1.0, 0),
            osc3: Value::linear(0.0, 0.0, -1.0, 1.0, 0),
            noise: Value::linear(0.0, 0.0, -1.0, 1.0, 0),
            tuned_delay: Value::linear(0.0, 0.0, -1.0, 1.0, 0),
            flt2: Value::linear(0.0, 1.0, 0.0, 1.0, 3),
            gain: Value::logarithmic(0.25, 1.0, 2.0f32.powf(-7.0), 2.0, 4),
            frequency: Value::logarithmic(440.0, 440.0, 27.5, 7040.0, 0),
            portamento: Value::logarithmic(0.0005, 1.0, 0.0005, 5.0, 0),
            arp: Value::logarithmic(0.003, 1.0, 0.003, 30.0, 1),
            delay: Value::linear(0.0, 0.0, -1.0, 1.0, 2),
            pan: Value::linear(0.0, 0.0, -1.0, 1.0, 2),
            width: Value::linear(1.0, 0.0, 0.0, 1.0, 2),
            send: Value::logarithmic(0.25, 1.0, 2.0f32.powf(-7.0), 2.0, 3),
            delay_send: Value::logarithmic(2.0f32.powf(-7.0), 1.0, 2.0f32.powf(-7.0), 2.0, 3),
        }
    }
}

impl VoiceState {
    #[inline(always)]
    pub fn get(&self, destination: VoiceDestination) -> &Value {
        match destination {
            VoiceDestination::Osc1 => &self.osc1,
            VoiceDestination::Osc2 => &self.osc2,
            VoiceDestination::Osc3 => &self.osc3,
            VoiceDestination::Noise => &self.noise,
            VoiceDestination::TunedDelay => &self.tuned_delay,
            VoiceDestination::Flt2 => &self.flt2,
            VoiceDestination::Gain => &self.gain,
            VoiceDestination::Frequency => &self.frequency,
            VoiceDestination::Portamento => &self.portamento,
            VoiceDestination::Arp => &self.arp,
            VoiceDestination::Delay => &self.delay,
            VoiceDestination::Pan => &self.pan,
            VoiceDestination::Width => &self.width,
            VoiceDestination::Send => &self.send,
            VoiceDestination::DelaySend => &self.delay_send,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: VoiceDestination) -> &mut Value {
        match destination {
            VoiceDestination::Osc1 => &mut self.osc1,
            VoiceDestination::Osc2 => &mut self.osc2,
            VoiceDestination::Osc3 => &mut self.osc3,
            VoiceDestination::Noise => &mut self.noise,
            VoiceDestination::TunedDelay => &mut self.tuned_delay,
            VoiceDestination::Flt2 => &mut self.flt2,
            VoiceDestination::Gain => &mut self.gain,
            VoiceDestination::Frequency => &mut self.frequency,
            VoiceDestination::Portamento => &mut self.portamento,
            VoiceDestination::Arp => &mut self.arp,
            VoiceDestination::Delay => &mut self.delay,
            VoiceDestination::Pan => &mut self.pan,
            VoiceDestination::Width => &mut self.width,
            VoiceDestination::Send => &mut self.send,
            VoiceDestination::DelaySend => &mut self.delay_send,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in VoiceDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for OscillatorState {
    fn default() -> Self {
        Self {
            frequency: Value::logarithmic(440.0, 440.0, 27.5, 7040.0, 0),
            phase: Value::linear(0.0, 0.0, -1.0, 1.0, 0),
            track: Value::linear(1.0, 1.0, 0.0, 1.0, 0),
            delay: Value::linear(0.0, 0.0, 0.0, 1.0, 1),
            asymmetry: Value::linear(1.0, 1.0, 0.0, 1.0, 1),
            width: Value::linear(0.0, 1.0, 0.0, 1.0, 1),
            gap: Value::linear(0.0, 1.0, 0.0, 1.0, 1),
            cushion: Value::linear(0.0, 1.0, 0.0, 1.0, 2),
            melt: Value::linear(0.0, 1.0, 0.0, 1.0, 2),
        }
    }
}

impl OscillatorState {
    #[inline(always)]
    pub fn get(&self, destination: OscillatorDestination) -> &Value {
        match destination {
            OscillatorDestination::Frequency => &self.frequency,
            OscillatorDestination::Phase => &self.phase,
            OscillatorDestination::Track => &self.track,
            OscillatorDestination::Delay => &self.delay,
            OscillatorDestination::Asymmetry => &self.asymmetry,
            OscillatorDestination::Width => &self.width,
            OscillatorDestination::Gap => &self.gap,
            OscillatorDestination::Cushion => &self.cushion,
            OscillatorDestination::Melt => &self.melt,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: OscillatorDestination) -> &mut Value {
        match destination {
            OscillatorDestination::Frequency => &mut self.frequency,
            OscillatorDestination::Phase => &mut self.phase,
            OscillatorDestination::Track => &mut self.track,
            OscillatorDestination::Delay => &mut self.delay,
            OscillatorDestination::Asymmetry => &mut self.asymmetry,
            OscillatorDestination::Width => &mut self.width,
            OscillatorDestination::Gap => &mut self.gap,
            OscillatorDestination::Cushion => &mut self.cushion,
            OscillatorDestination::Melt => &mut self.melt,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in OscillatorDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for NoiseState {
    fn default() -> Self {
        Self {
            balance: Value::linear(0.0, 0.0, -1.0, 1.0, 0),
        }
    }
}

impl NoiseState {
    #[inline(always)]
    pub fn get(&self, destination: NoiseDestination) -> &Value {
        match destination {
            NoiseDestination::Balance => &self.balance,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: NoiseDestination) -> &mut Value {
        match destination {
            NoiseDestination::Balance => &mut self.balance,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in NoiseDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for TunedDelayState {
    fn default() -> Self {
        Self {
            input: Value::linear(0.0, 0.0, -1.0, 1.0, 3),
            frequency: Value::logarithmic(440.0, 440.0, 27.5, 7040.0, 0),
            phase: Value::linear(0.0, 0.0, -1.0, 1.0, 0),
            track: Value::linear(1.0, 1.0, 0.0, 1.0, 0),
            speed: Value::logarithmic(1.0, 1.0, 0.0625, 16.0, 0),
            feedback: Value::linear(0.0, 0.0, -1.0, 1.0, 1),
            filter_frequency: Value::logarithmic(22100.0, 22100.0, 1381.25, 353600.0, 1),
            sampling: Value::logarithmic(1.0, 1.0, 2.0f32.powf(-8.0), 1.0, 2),
            blend: Value::logarithmic(0.5, 0.5, 0.5, 2.0f32.powf(7.0), 2),
            osc1: Value::linear(0.0, 0.0, 0.0, 1.0, 0),
            osc2: Value::linear(0.0, 0.0, 0.0, 1.0, 0),
            osc3: Value::linear(0.0, 0.0, 0.0, 1.0, 0),
            noise: Value::linear(0.0, 0.0, 0.0, 1.0, 1),
            voice: Value::linear(0.0, 0.0, 0.0, 1.0, 2),
        }
    }
}

impl TunedDelayState {
    #[inline(always)]
    pub fn get(&self, destination: TunedDelayDestination) -> &Value {
        match destination {
            TunedDelayDestination::Input => &self.input,
            TunedDelayDestination::Frequency => &self.frequency,
            TunedDelayDestination::Phase => &self.phase,
            TunedDelayDestination::Track => &self.track,
            TunedDelayDestination::Speed => &self.speed,
            TunedDelayDestination::Feedback => &self.feedback,
            TunedDelayDestination::FilterFrequency => &self.filter_frequency,
            TunedDelayDestination::Sampling => &self.sampling,
            TunedDelayDestination::Blend => &self.blend,
            TunedDelayDestination::Osc1 => &self.osc1,
            TunedDelayDestination::Osc2 => &self.osc2,
            TunedDelayDestination::Osc3 => &self.osc3,
            TunedDelayDestination::Noise => &self.noise,
            TunedDelayDestination::Voice => &self.voice,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: TunedDelayDestination) -> &mut Value {
        match destination {
            TunedDelayDestination::Input => &mut self.input,
            TunedDelayDestination::Frequency => &mut self.frequency,
            TunedDelayDestination::Phase => &mut self.phase,
            TunedDelayDestination::Track => &mut self.track,
            TunedDelayDestination::Speed => &mut self.speed,
            TunedDelayDestination::Feedback => &mut self.feedback,
            TunedDelayDestination::FilterFrequency => &mut self.filter_frequency,
            TunedDelayDestination::Sampling => &mut self.sampling,
            TunedDelayDestination::Blend => &mut self.blend,
            TunedDelayDestination::Osc1 => &mut self.osc1,
            TunedDelayDestination::Osc2 => &mut self.osc2,
            TunedDelayDestination::Osc3 => &mut self.osc3,
            TunedDelayDestination::Noise => &mut self.noise,
            TunedDelayDestination::Voice => &mut self.voice,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in TunedDelayDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for FilterState {
    fn default() -> Self {
        Self {
            frequency: Value::logarithmic(440.0, 440.0, 13.75, 14080.0, 0),
            lowpass: Value::linear(1.0, 1.0, 0.0, 1.0, 1),
            highpass: Value::linear(0.0, 0.0, 0.0, 1.0, 1),
            bandpass: Value::linear(0.0, 0.0, 0.0, 1.0, 1),
            resonance: Value::linear(0.0, 0.0, 0.0, 1.0, 2),
            headroom: Value::logarithmic(1.0, 1.0, 2.0f32.powf(-4.0), 2.0f32.powf(4.0), 3),
            decay: Value::logarithmic(0.001, 0.001, 0.001, 1.0, 3),
            track: Value::linear(1.0, 1.0, 0.0, 1.0, 0),
        }
    }
}

impl FilterState {
    fn with_frequency(frequency: f32) -> Self {
        Self {
            frequency: Value::logarithmic(frequency, 440.0, 13.75, 14080.0, 0),
            lowpass: Value::linear(1.0, 1.0, 0.0, 1.0, 1),
            highpass: Value::linear(0.0, 0.0, 0.0, 1.0, 1),
            bandpass: Value::linear(0.0, 0.0, 0.0, 1.0, 1),
            resonance: Value::linear(0.0, 0.0, 0.0, 1.0, 2),
            headroom: Value::logarithmic(1.0, 1.0, 2.0f32.powf(-4.0), 2.0f32.powf(4.0), 3),
            decay: Value::logarithmic(0.001, 0.001, 0.001, 1.0, 3),
            track: Value::linear(1.0, 1.0, 0.0, 1.0, 0),
        }
    }

    #[inline(always)]
    pub fn get(&self, destination: FilterDestination) -> &Value {
        match destination {
            FilterDestination::Frequency => &self.frequency,
            FilterDestination::Lowpass => &self.lowpass,
            FilterDestination::Highpass => &self.highpass,
            FilterDestination::Bandpass => &self.bandpass,
            FilterDestination::Resonance => &self.resonance,
            FilterDestination::Headroom => &self.headroom,
            FilterDestination::Decay => &self.decay,
            FilterDestination::Track => &self.track,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: FilterDestination) -> &mut Value {
        match destination {
            FilterDestination::Frequency => &mut self.frequency,
            FilterDestination::Lowpass => &mut self.lowpass,
            FilterDestination::Highpass => &mut self.highpass,
            FilterDestination::Bandpass => &mut self.bandpass,
            FilterDestination::Resonance => &mut self.resonance,
            FilterDestination::Headroom => &mut self.headroom,
            FilterDestination::Decay => &mut self.decay,
            FilterDestination::Track => &mut self.track,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in FilterDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for WaveshaperState {
    fn default() -> Self {
        Self {
            pregain: Value::logarithmic(1.0, 1.0, 1.0, 2.0f32.powf(8.0), 0),
            asymmetry: Value::linear(0.0, 0.0, -8.0, 8.0, 0),
            gate: Value::linear(0.0, 0.0, 0.0, 1.0, 1),
            cushion: Value::linear(0.0, 0.0, 0.0, 1.0, 2),
            hardclip: Value::logarithmic(
                2.0f32.powf(3.0),
                1.0,
                2.0f32.powf(-5.0),
                2.0f32.powf(3.0),
                2,
            ),
            crossover: Value::linear(-1.0, 0.0, -1.0, 1.0, 2),
            crush: Value::linear(0.0, 0.0, 0.0, 1.0, 3),
            sampling: Value::logarithmic(1.0, 1.0, 2.0f32.powf(-8.0), 1.0, 3),
        }
    }
}

impl WaveshaperState {
    #[inline(always)]
    pub fn get(&self, destination: WaveshaperDestination) -> &Value {
        match destination {
            WaveshaperDestination::Pregain => &self.pregain,
            WaveshaperDestination::Asymmetry => &self.asymmetry,
            WaveshaperDestination::Gate => &self.gate,
            WaveshaperDestination::Cushion => &self.cushion,
            WaveshaperDestination::Hardclip => &self.hardclip,
            WaveshaperDestination::Crossover => &self.crossover,
            WaveshaperDestination::Crush => &self.crush,
            WaveshaperDestination::Sampling => &self.sampling,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: WaveshaperDestination) -> &mut Value {
        match destination {
            WaveshaperDestination::Pregain => &mut self.pregain,
            WaveshaperDestination::Asymmetry => &mut self.asymmetry,
            WaveshaperDestination::Gate => &mut self.gate,
            WaveshaperDestination::Cushion => &mut self.cushion,
            WaveshaperDestination::Hardclip => &mut self.hardclip,
            WaveshaperDestination::Crossover => &mut self.crossover,
            WaveshaperDestination::Crush => &mut self.crush,
            WaveshaperDestination::Sampling => &mut self.sampling,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in WaveshaperDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for EnvelopeState {
    fn default() -> Self {
        Self {
            amount: Value::linear(1.0, 0.0, 0.0, 1.0, 0),
            attack: Value::logarithmic(0.01, 1.0, 0.003, 30.0, 1),
            hold: Value::logarithmic(0.0005, 1.0, 0.0005, 5.0, 1),
            decay: Value::logarithmic(0.3, 1.0, 0.003, 30.0, 1),
            sustain: Value::linear(0.25, 1.0, 0.0, 1.0, 1),
            slope: Value::linear(0.0, 0.0, -1.0, 1.0, 1),
            echo: Value::linear(0.0, 0.0, 0.0, 1.0, 2),
            scatter: Value::linear(0.0, 0.0, 0.0, 1.0, 3),
            release: Value::logarithmic(0.05, 1.0, 0.003, 30.0, 1),
        }
    }
}

impl EnvelopeState {
    #[inline(always)]
    pub fn get(&self, destination: EnvelopeDestination) -> &Value {
        match destination {
            EnvelopeDestination::Amount => &self.amount,
            EnvelopeDestination::Attack => &self.attack,
            EnvelopeDestination::Hold => &self.hold,
            EnvelopeDestination::Decay => &self.decay,
            EnvelopeDestination::Sustain => &self.sustain,
            EnvelopeDestination::Slope => &self.slope,
            EnvelopeDestination::Scatter => &self.scatter,
            EnvelopeDestination::Echo => &self.echo,
            EnvelopeDestination::Release => &self.release,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: EnvelopeDestination) -> &mut Value {
        match destination {
            EnvelopeDestination::Amount => &mut self.amount,
            EnvelopeDestination::Attack => &mut self.attack,
            EnvelopeDestination::Hold => &mut self.hold,
            EnvelopeDestination::Decay => &mut self.decay,
            EnvelopeDestination::Sustain => &mut self.sustain,
            EnvelopeDestination::Slope => &mut self.slope,
            EnvelopeDestination::Scatter => &mut self.scatter,
            EnvelopeDestination::Echo => &mut self.echo,
            EnvelopeDestination::Release => &mut self.release,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in EnvelopeDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for LfoState {
    fn default() -> Self {
        Self {
            amount: Value::linear(1.0, 1.0, 0.0, 1.0, 0),
            frequency: Value::logarithmic(2.0, 2.0, 0.0625, 64.0, 1),
            phase: Value::linear(0.0, 1.0, 0.0, 1.0, 1),
            asymmetry: Value::linear(0.0, 1.0, 0.0, 1.0, 2),
            cushion: Value::linear(0.0, 1.0, 0.0, 1.0, 2),
            noise: Value::linear(0.0, 1.0, 0.0, 1.0, 3),
            sampling: Value::logarithmic(0.0625, 2.0, 0.0625, 64.0, 3),
            slew: Value::logarithmic(0.01, 1.0, 0.003, 30.0, 3),
            track: Value::linear(1.0, 1.0, 0.0, 1.0, 0),
        }
    }
}

impl LfoState {
    #[inline(always)]
    pub fn get(&self, destination: LfoDestination) -> &Value {
        match destination {
            LfoDestination::Amount => &self.amount,
            LfoDestination::Frequency => &self.frequency,
            LfoDestination::Phase => &self.phase,
            LfoDestination::Asymmetry => &self.asymmetry,
            LfoDestination::Cushion => &self.cushion,
            LfoDestination::Noise => &self.noise,
            LfoDestination::Sampling => &self.sampling,
            LfoDestination::Slew => &self.slew,
            LfoDestination::Track => &self.track,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: LfoDestination) -> &mut Value {
        match destination {
            LfoDestination::Amount => &mut self.amount,
            LfoDestination::Frequency => &mut self.frequency,
            LfoDestination::Phase => &mut self.phase,
            LfoDestination::Asymmetry => &mut self.asymmetry,
            LfoDestination::Cushion => &mut self.cushion,
            LfoDestination::Noise => &mut self.noise,
            LfoDestination::Sampling => &mut self.sampling,
            LfoDestination::Slew => &mut self.slew,
            LfoDestination::Track => &mut self.track,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in LfoDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for AccumulatorState {
    fn default() -> Self {
        Self {
            value: Value::linear(0.0, 0.0, 0.0, 1.0, 0),
            slew: Value::logarithmic(0.003, 1.0, 0.0003, 30.0, 1),
            depth: Value::linear(0.0, 0.0, 0.0, 1.0, 2),
            rate: Value::logarithmic(
                10.0f32.powf(5.0),
                10.0f32.powf(5.0),
                0.1,
                10.0f32.powf(5.0),
                2,
            ),
        }
    }
}

impl AccumulatorState {
    #[inline(always)]
    pub fn get(&self, destination: AccumulatorDestination) -> &Value {
        match destination {
            AccumulatorDestination::Value => &self.value,
            AccumulatorDestination::Slew => &self.slew,
            AccumulatorDestination::Depth => &self.depth,
            AccumulatorDestination::Rate => &self.rate,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: AccumulatorDestination) -> &mut Value {
        match destination {
            AccumulatorDestination::Value => &mut self.value,
            AccumulatorDestination::Slew => &mut self.slew,
            AccumulatorDestination::Depth => &mut self.depth,
            AccumulatorDestination::Rate => &mut self.rate,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in AccumulatorDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}

impl Default for ShaperState {
    fn default() -> Self {
        Self {
            trigger: Value::linear(0.0, 1.0, 0.0, 1.0, 0),
            delay: Value::logarithmic(0.0005, 1.0, 0.0005, 5.0, 1),
            attack: Value::logarithmic(0.01, 1.0, 0.003, 30.0, 2),
            hold: Value::logarithmic(0.0005, 1.0, 0.0005, 5.0, 2),
            decay: Value::logarithmic(0.3, 1.0, 0.003, 30.0, 2),
            sustain: Value::linear(1.0, 1.0, 0.0, 1.0, 2),
            release: Value::logarithmic(0.05, 1.0, 0.003, 30.0, 2),
            sense: Value::linear(0.0, 0.0, 0.0, 1.0, 0),
        }
    }
}

impl ShaperState {
    #[inline(always)]
    pub fn get(&self, destination: ShaperDestination) -> &Value {
        match destination {
            ShaperDestination::Trigger => &self.trigger,
            ShaperDestination::Delay => &self.delay,
            ShaperDestination::Attack => &self.attack,
            ShaperDestination::Hold => &self.hold,
            ShaperDestination::Decay => &self.decay,
            ShaperDestination::Sustain => &self.sustain,
            ShaperDestination::Release => &self.release,
            ShaperDestination::Sense => &self.sense,
        }
    }

    #[inline(always)]
    pub fn get_mut(&mut self, destination: ShaperDestination) -> &mut Value {
        match destination {
            ShaperDestination::Trigger => &mut self.trigger,
            ShaperDestination::Delay => &mut self.delay,
            ShaperDestination::Attack => &mut self.attack,
            ShaperDestination::Hold => &mut self.hold,
            ShaperDestination::Decay => &mut self.decay,
            ShaperDestination::Sustain => &mut self.sustain,
            ShaperDestination::Release => &mut self.release,
            ShaperDestination::Sense => &mut self.sense,
        }
    }

    pub fn set_from(&mut self, other: &Self) {
        for dest in ShaperDestination::iter() {
            self.get_mut(dest).set_from(other.get(dest));
        }
    }
}
