// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use std::fmt;

use crate::frontend::modes::Mode;

#[cfg(feature = "synth")]
use crossbeam::channel::Receiver;

#[derive(Debug, Clone)]
pub enum Action {
    Arm,
    AutoReturn(bool),
    Back(usize),
    Backspace,
    Cancel,
    #[cfg(feature = "master_effects")]
    CompressorState(Destination, f32),
    CopyProject(String),
    CycleInputSourceLeft,
    CycleInputSourceRight,
    Down,
    ExecuteScript(String),
    Export,
    ExportCompleted,
    FileSize(Destination, usize),
    Forward(usize),
    Letter(String),
    IsolateArm(usize),
    IsolateMute(usize),
    IsolateSolo(usize),
    JumpToEnd,
    JumpToStart,
    JumpToZero,
    Left,
    LoadStatus(Destination, Option<f32>),
    Log(String),
    Loop,
    LoopCycle,
    LoudnessRegion(Destination, usize, usize, f32), // dst, t, length, value
    MasterDown,
    MasterUp,
    Modes,
    Monitor,
    Mute,
    NewProject,
    NewProjectFromCurrent,
    NewProjectFromThis(String),
    NextMarker,
    NextMode,
    NextSubmix,
    #[cfg(feature = "master_effects")]
    NumPress(usize),
    OpenMode(Mode),
    OpenProject(String),
    Overrun(usize),
    PanLeft,
    PanRight,
    Peak(Destination, usize, f32, f32),
    Play,
    PlayPause,
    PreviousMarker,
    PreviousMode,
    PreviousSubmix,
    Projects,
    Quit,
    Record,
    RelabelMarker,
    Rename(String),
    RenameProject(String),
    RequestMackieState,
    RestoreProject(String),
    Right,
    Rms(Destination, usize, f32, f32),
    Scripts,
    ScrollLogger(isize),
    SelectProject(String),
    SetEnd,
    SetFader(Destination, f32),
    SetFaderRelative(Destination, f32),
    #[cfg(feature = "master_effects")]
    SetKnob(Destination, i8),
    #[cfg(feature = "master_effects")]
    SetKnobRelative(Destination, i8),
    SetLoopStart,
    SetLoopStop,
    SetPanRelative(usize, f32),
    SetRecordNotPlaying(bool),
    SetReverbSendRelative(usize, f32),
    SetStart,
    Solo,
    SpectrumAnalysis(Destination, usize, Vec<f32>),
    Stop,
    SubmixSelect(usize),
    Time(usize),
    ToggleArm(usize),
    ToggleHeight,
    ToggleLogger,
    ToggleMainMode,
    ToggleMarker,
    ToggleMonitor(usize),
    ToggleMute(usize),
    ToggleProjectMode,
    ToggleSolo(usize),
    ToggleTrackFocus,
    TrackSelect(usize),
    TrashProject(String),
    Trashbin,
    Underrun(usize),
    Up,
    VerticalZoom(i32),
    VolDown,
    VolUp,
    Workload {
        identifier: usize,
        percentage: i128,
        max: i128,
    },

    #[cfg(feature = "ballet")]
    BalletSetup(Receiver<BalletMessage>),

    #[cfg(feature = "opera")]
    OperaSetup(Receiver<OperaMessage>),
}

#[cfg(feature = "ballet")]
#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct BalletId {
    pub group_id: usize,
    pub voice_id: usize,
    pub note_id: usize,
}

#[cfg(feature = "ballet")]
#[derive(Debug, Clone, Copy)]
pub enum BalletConfig {
    OutputChannelLeft,
    OutputChannelRight,
    RateDivider,
    DisableOsc1,
    DisableOsc2,
    DisableTunedDelay,
    DisableFlt1,
    DisableFlt2,
    DisableWshp,
    DisableDelay,
    DisableEnv1,
    DisableEnv2,
    DisableLfo1,
    DisableLfo2,
}

#[cfg(feature = "ballet")]
#[derive(Debug, Clone, Copy)]
pub enum BalletMasterConfig {
    RootNote,
    Tuning,
    SidechainChannel,
    InputSource,
    RateDivider,
    DisableVocal,
    DisablePitchTracker,
    DisableDelay,
    DisableCompressor,
    DisableFilter,
}

#[cfg(feature = "ballet")]
#[derive(Debug, Clone)]
pub enum BalletMessage {
    Panic,
    LoadMasterPatch {
        patch: Box<crate::ballet::MasterPatch>,
        panic: bool,
        mute: bool,
    },
    LoadPatch {
        voice_idx: usize,
        patch: Box<crate::ballet::Patch>,
    },
    UnloadPatch {
        voice_idx: usize,
    },
    SetParam {
        param: crate::ballet::Parameter,
        value: f32,
    },
    SetNote {
        note: i16,
        value: bool,
    },
    SetModulation(crate::synth::Modulation<crate::ballet::Parameter>),
    SetConfig {
        channel: u8,
        dest: BalletConfig,
        value: usize,
    },
    SetMasterConfig {
        dest: BalletMasterConfig,
        value: usize,
    },
    SetTalk(bool),
    Morph {
        channel: u8,
        id: BalletId,
        axis: usize,
        amount: f32,
    },
    Sidechain {
        channel: usize,
        velocity: f32,
    },
    NoteOn {
        channel: u8,
        id: BalletId,
        frequency: f32,
        velocity: f32,
        mute: bool,
        latch: bool,
    },
    NoteOff {
        channel: u8,
        id: BalletId,
        velocity: f32,
        latch: bool,
    },
}

#[cfg(feature = "opera")]
#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct OperaId {
    pub group_id: usize,
    pub voice_id: usize,
    pub note_id: usize,
}

#[cfg(feature = "opera")]
#[derive(Debug, Clone, Copy)]
pub enum OperaConfig {
    OutputChannelLeft,
    OutputChannelRight,
    RateDivider,
}

#[cfg(feature = "opera")]
#[derive(Debug, Clone, Copy)]
pub enum OperaMasterConfig {
    InputSource,
    RateDivider,
}

#[cfg(feature = "opera")]
#[derive(Debug, Clone)]
pub enum OperaMessage {
    Panic,
    LoadMasterPatch {
        patch: Box<crate::opera::MasterPatch>,
        panic: bool,
        mute: bool,
    },
    LoadPatch {
        voice_idx: usize,
        patch: Box<crate::opera::Patch>,
    },
    UnloadPatch {
        voice_idx: usize,
    },
    SetParam {
        param: crate::opera::Parameter,
        value: f32,
    },
    SetModulation(crate::synth::Modulation<crate::opera::Parameter>),
    SetConfig {
        channel: u8,
        dest: OperaConfig,
        value: usize,
    },
    SetMasterConfig {
        dest: OperaMasterConfig,
        value: usize,
    },
    Morph {
        channel: u8,
        id: OperaId,
        axis: usize,
        amount: f32,
    },
    NoteOn {
        channel: u8,
        id: OperaId,
        frequency: f32,
        velocity: f32,
        mute: bool,
        latch: bool,
    },
    NoteOff {
        channel: u8,
        id: OperaId,
        velocity: f32,
        latch: bool,
    },
}

#[derive(Debug, Clone, Copy)]
pub enum Destination {
    Master,
    Track(usize),
}

pub enum Mackie {
    Fader(Destination, i16),
    Mute(Destination, bool),
    Play(bool),
    Record(bool),
    RecordArm(Destination, bool),
    Solo(Destination, bool),
    Stop(bool),
}

pub enum MidiMessage {
    Connect {
        addr: alsa::seq::Addr,
        name: String,
    },
    Disconnect {
        addr: alsa::seq::Addr,
        name: String,
    },
    NoteOn {
        addr: alsa::seq::Addr,
        channel: u8,
        note: u8,
        velocity: u8,
    },
    NoteOff {
        addr: alsa::seq::Addr,
        channel: u8,
        note: u8,
        velocity: u8,
    },
    CC {
        addr: alsa::seq::Addr,
        channel: u8,
        param: u8,
        value: u8,
    },
    Pitchbend {
        addr: alsa::seq::Addr,
        channel: u8,
        value: i16,
    },
    KeyPressure {
        addr: alsa::seq::Addr,
        channel: u8,
        note: u8,
        velocity: u8,
    },
    Sysex {
        addr: alsa::seq::Addr,
        data: Vec<u8>,
    },
}

pub enum Request {
    AbsoluteTime(usize),
    AutoReturn(bool),
    BackwardTimeShift(usize),
    Export(String, usize, usize),
    Fader(Destination, f32),
    ForwardTimeShift(usize),
    InputSourceLeft(Destination, usize),
    InputSourceRight(Destination, usize),
    Load(Destination, String),
    LoopState(bool),
    #[cfg(feature = "master_effects")]
    MasterEffect([f32; 8]),
    Monitor(Destination, bool),
    Mute(Destination, bool),
    Pan(Destination, f32),
    PlayState(bool),
    RecordArm(Destination, bool),
    RecordState(bool),
    ReverbSend(Destination, f32),
    SetAnalyzerState(bool),
    SetLoopStart(usize),
    SetLoopStop(usize),
    SetUpdateInterval(usize),
    Solo(Destination, bool),
    SubmixFader(usize, f32),
    SubmixGain(Destination, usize, f32),
    SubmixMute(Destination, usize, bool),
    SubmixPan(Destination, usize, f32),
    SubmixSolo(Destination, usize, bool),

    #[cfg(feature = "ballet")]
    BalletSetup(Receiver<BalletMessage>),

    #[cfg(feature = "opera")]
    OperaSetup(Receiver<OperaMessage>),
}

impl fmt::Display for Action {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
