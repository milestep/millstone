// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

mod app;
mod bus;
mod colors;
mod keyboard;
mod layout;
mod loudness_map;
mod markers;
mod menu;
mod midi;
mod mixer;
pub mod modes;
mod project;
mod submix;
mod track;

#[cfg(feature = "ballet")]
mod ballet;

#[cfg(feature = "opera")]
mod opera;

use crossbeam::channel;
use crossbeam::channel::{Receiver, RecvError, Sender};

use parking_lot::RwLock;

use std::{env, io, sync::Arc};

use termion::{raw::IntoRawMode, screen::AlternateScreen};

use tui::{backend::TermionBackend, Terminal};

use crate::depot::Depot;
use crate::frontend::app::{App, Workload};
use crate::frontend::menu::Menu;
use crate::frontend::modes::Mode;
use crate::global_settings::GlobalSettings;
use crate::logger::Logger;
use crate::messages::{Action, Destination, Mackie, MidiMessage, Request};
use crate::paths;
use crate::perf;
use crate::state::State;

//////////////////////////////////////////////////
// Frontend Main Run Function
//////////////////////////////////////////////////

#[allow(clippy::too_many_arguments)]
pub fn run(
    global_settings: Arc<RwLock<GlobalSettings>>,
    depot: Depot,
    request_tx: Sender<Request>,
    action_rx: Receiver<Action>,
    action_tx: Sender<Action>,
    midi_outbound_tx: Sender<MidiMessage>,
    midi_inbound_rx: Receiver<MidiMessage>,
    logger: Arc<RwLock<Logger>>,
) {
    perf::set_normal_thread_min_priority();

    // Create a new App instance for the frontend
    // Setup input handlers
    let (midi_tx, midi_rx) = channel::unbounded::<Mackie>();

    midi::spawn(
        Arc::clone(&global_settings),
        action_tx.clone(),
        midi_rx,
        midi_outbound_tx,
        midi_inbound_rx,
    );
    keyboard::spawn(action_tx.clone());

    let mut app = App::new(
        &global_settings,
        &logger,
        &request_tx,
        &midi_tx,
        depot,
        Menu::new(),
    );

    // Terminal initialization
    let stdout = io::stdout().into_raw_mode().unwrap();
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend).unwrap();

    // Main event loop
    let jump = 44100;
    let peak_decay_const = 60.0 / 44100.0; // dBps
    let peak_decay_const_crit = 20.0 / 44100.0; // dBps

    loop {
        // Listen to device actions (keyboard & midi)
        match action_rx.recv() {
            Ok(action) => {
                match action {
                    Action::RequestMackieState => {
                        app.send_mackie_state();
                    }
                    Action::Log(msg) => {
                        logger.write().log(&msg.to_string());
                    }
                    Action::Time(t) => {
                        app.state.t = t;
                    }
                    Action::Overrun(n) => {
                        logger
                            .write()
                            .log(&format!("Overrun! ({}) t = {}", n, app.state.t));
                        if app.play && app.record {
                            app.marker_state.add_marker(String::from("X"), app.state.t);
                        }
                    }
                    Action::Underrun(n) => {
                        logger
                            .write()
                            .log(&format!("Underrun! ({}) t = {}", n, app.state.t));
                    }
                    Action::JumpToZero => {
                        if !app.record {
                            app.state.t = 0;
                            request_tx.send(Request::AbsoluteTime(0)).unwrap();
                        }
                    }
                    #[cfg(feature = "master_effects")]
                    Action::NumPress(idx) => {
                        app.state.focused_effect = idx - 1;
                        app.mixer.master_bus.effects[idx - 1].cycle(0.01);
                        request_tx
                            .send(Request::MasterEffect(
                                app.mixer.master_bus.get_effect_state(),
                            ))
                            .unwrap();
                    }
                    Action::Up => match app.mode {
                        Mode::Tape | Mode::Performance => {
                            if !app.disable_track_focus && app.state.focused_track > 0 {
                                app.set_focused_track(app.state.focused_track - 1);
                            }
                        }
                        Mode::Menu => app.menu.previous(),
                        Mode::Logger => {
                            action_tx.send(Action::ScrollLogger(-1)).unwrap();
                        }
                    },
                    Action::Down => match app.mode {
                        Mode::Tape | Mode::Performance => {
                            if !app.disable_track_focus {
                                app.set_focused_track(app.state.focused_track + 1);
                            }
                        }
                        Mode::Menu => app.menu.next(),
                        Mode::Logger => {
                            action_tx.send(Action::ScrollLogger(1)).unwrap();
                        }
                    },
                    Action::Left => match app.mode {
                        Mode::Tape | Mode::Performance => {
                            action_tx.send(Action::Back(1)).unwrap();
                        }
                        Mode::Menu => {
                            action_tx.send(Action::Cancel).unwrap();
                        }
                        _ => (),
                    },
                    Action::Right => match app.mode {
                        Mode::Tape | Mode::Performance => {
                            action_tx.send(Action::Forward(1)).unwrap();
                        }
                        Mode::Menu => {
                            action_tx.send(app.menu.selected_action()).unwrap();
                        }
                        _ => (),
                    },
                    Action::ScrollLogger(value) => {
                        if app.mode == Mode::Logger {
                            logger.write().change_offset(value);
                        }
                    }
                    Action::ToggleTrackFocus => {
                        app.disable_track_focus = !app.disable_track_focus;
                    }
                    Action::TrackSelect(idx) => app.set_focused_track(idx),
                    Action::SubmixSelect(idx) => {
                        if idx == app.state.focused_submix {
                            app.set_focused_submix(0);
                        } else {
                            app.set_focused_submix(idx);
                        }
                    }
                    Action::PreviousSubmix => {
                        if app.mode == Mode::Tape && app.state.focused_submix > 0 {
                            app.set_focused_submix(app.state.focused_submix - 1);
                        }
                    }
                    Action::NextSubmix => {
                        if app.mode == Mode::Tape {
                            app.set_focused_submix(app.state.focused_submix + 1);
                        }
                    }
                    Action::PreviousMarker => match app.mode {
                        Mode::Menu => action_tx.send(Action::Cancel).unwrap(),
                        _ => {
                            if !app.record {
                                let t = app.marker_state.previous(app.state.t);
                                app.state.t = t;
                                request_tx.send(Request::AbsoluteTime(t)).unwrap();
                            }
                        }
                    },
                    Action::NextMarker => match app.mode {
                        Mode::Menu => action_tx.send(app.menu.selected_action()).unwrap(),
                        _ => {
                            if !app.record {
                                let t = app.marker_state.next(app.state.t);
                                app.state.t = t;
                                request_tx.send(Request::AbsoluteTime(t)).unwrap();
                            }
                        }
                    },
                    Action::Back(value) => match app.mode {
                        Mode::Tape | Mode::Performance => {
                            if !app.record {
                                let t = app.state.t;
                                let dt = jump * value;
                                if dt < t {
                                    app.state.t -= dt;
                                } else {
                                    app.state.t = 0;
                                }
                                request_tx.send(Request::BackwardTimeShift(dt)).unwrap();
                            }
                        }
                        Mode::Menu => app.menu.previous(),
                        Mode::Logger => {
                            action_tx
                                .send(Action::ScrollLogger(-(value as isize)))
                                .unwrap();
                        }
                    },
                    Action::Forward(value) => match app.mode {
                        Mode::Tape | Mode::Performance => {
                            if !app.record {
                                let dt = jump * value;
                                app.state.t += dt;
                                request_tx.send(Request::ForwardTimeShift(dt)).unwrap();
                            }
                        }
                        Mode::Menu => app.menu.next(),
                        Mode::Logger => {
                            action_tx
                                .send(Action::ScrollLogger(value as isize))
                                .unwrap();
                        }
                    },
                    Action::PlayPause => {
                        app.play = !app.play;
                        if !app.play {
                            app.record = false;
                            request_tx.send(Request::RecordState(app.record)).unwrap();
                        }
                        logger.write().log("Saving mixer and app states");
                        app.mixer.write("mixer.toml");
                        app.state.write("app.toml");
                        request_tx.send(Request::PlayState(app.play)).unwrap();
                        midi_tx.send(Mackie::Play(app.play)).unwrap();
                        midi_tx.send(Mackie::Stop(!app.play)).unwrap();
                        midi_tx.send(Mackie::Record(app.record)).unwrap();
                    }
                    Action::Play => {
                        app.play = true;
                        request_tx.send(Request::PlayState(app.play)).unwrap();
                        midi_tx.send(Mackie::Play(app.play)).unwrap();
                        midi_tx.send(Mackie::Stop(!app.play)).unwrap();
                    }
                    Action::Stop => {
                        app.play = false;
                        app.record = false;
                        app.autoreturn = false;
                        logger.write().log("Saving mixer and app states");
                        app.mixer.write("mixer.toml");
                        app.state.write("app.toml");
                        request_tx.send(Request::PlayState(app.play)).unwrap();
                        request_tx.send(Request::RecordState(app.record)).unwrap();
                        request_tx
                            .send(Request::AutoReturn(app.autoreturn))
                            .unwrap();
                        midi_tx.send(Mackie::Play(app.play)).unwrap();
                        midi_tx.send(Mackie::Stop(!app.play)).unwrap();
                        midi_tx.send(Mackie::Record(app.record)).unwrap();
                    }
                    Action::Record => {
                        app.record = !app.record;
                        request_tx.send(Request::RecordState(app.record)).unwrap();
                        midi_tx.send(Mackie::Record(app.record)).unwrap();
                    }
                    Action::SetRecordNotPlaying(value) => {
                        if !app.play {
                            app.record = value;
                            request_tx.send(Request::RecordState(app.record)).unwrap();
                            midi_tx.send(Mackie::Record(app.record)).unwrap();
                        }
                    }
                    Action::Monitor => {
                        if !app.disable_track_focus {
                            let focus = app.state.focused_track;
                            app.mixer.tracks[focus].monitor = !app.mixer.tracks[focus].monitor;

                            request_tx
                                .send(Request::Monitor(
                                    Destination::Track(focus),
                                    app.mixer.tracks[focus].monitor,
                                ))
                                .unwrap();
                        }
                    }
                    Action::Mute => match app.mode {
                        Mode::Tape => {
                            let idx = app.state.focused_track;
                            let submix = app.state.focused_submix;
                            let value = if submix == 0 {
                                app.mixer.tracks[idx].mute
                            } else {
                                app.mixer.submixes[submix - 1].track_mutes[idx]
                            };
                            app.set_mute(Destination::Track(idx), !value);
                        }
                        Mode::Performance => (),
                        _ => {}
                    },
                    Action::Solo => match app.mode {
                        Mode::Tape => {
                            let idx = app.state.focused_track;
                            let submix = app.state.focused_submix;
                            let value = if submix == 0 {
                                app.mixer.tracks[idx].solo
                            } else {
                                app.mixer.submixes[submix - 1].track_solos[idx]
                            };
                            app.set_solo(Destination::Track(idx), !value);
                        }
                        Mode::Performance => (),
                        _ => {}
                    },
                    Action::Arm => {
                        if !app.disable_track_focus {
                            let focus = app.state.focused_track;
                            app.mixer.tracks[focus].record_arm =
                                !app.mixer.tracks[focus].record_arm;

                            request_tx
                                .send(Request::RecordArm(
                                    Destination::Track(focus),
                                    app.mixer.tracks[focus].record_arm,
                                ))
                                .unwrap();

                            midi_tx
                                .send(Mackie::RecordArm(
                                    Destination::Track(focus),
                                    app.mixer.tracks[focus].record_arm,
                                ))
                                .unwrap();
                        }
                    }
                    Action::CycleInputSourceLeft => {
                        if !app.disable_track_focus {
                            let focus = app.state.focused_track;
                            let input = app.mixer.tracks[focus].physical_left;
                            let nchannels_in = global_settings.read().nchannels_in;

                            if cfg!(feature = "ballet") || cfg!(feature = "opera") {
                                app.mixer.tracks[focus].physical_left =
                                    if input + 1 >= nchannels_in && input + 1 < 256 {
                                        256
                                    } else if input + 1 == 264 {
                                        0
                                    } else {
                                        input + 1
                                    };
                            } else {
                                app.mixer.tracks[focus].physical_left = (input + 1) % nchannels_in;
                            }

                            request_tx
                                .send(Request::InputSourceLeft(
                                    Destination::Track(focus),
                                    app.mixer.tracks[focus].physical_left,
                                ))
                                .unwrap();
                        }
                    }
                    Action::CycleInputSourceRight => {
                        if !app.disable_track_focus {
                            let focus = app.state.focused_track;
                            let input = app.mixer.tracks[focus].physical_right;
                            let nchannels_in = global_settings.read().nchannels_in;

                            if cfg!(feature = "ballet") || cfg!(feature = "opera") {
                                app.mixer.tracks[focus].physical_right =
                                    if input + 1 >= nchannels_in && input + 1 < 256 {
                                        256
                                    } else if input + 1 == 264 {
                                        0
                                    } else {
                                        input + 1
                                    };
                            } else {
                                app.mixer.tracks[focus].physical_right = (input + 1) % nchannels_in;
                            }

                            request_tx
                                .send(Request::InputSourceRight(
                                    Destination::Track(focus),
                                    app.mixer.tracks[focus].physical_right,
                                ))
                                .unwrap();
                        }
                    }
                    Action::ToggleMainMode => app.toggle_main_mode(),
                    Action::ToggleProjectMode => app.toggle_menu_mode(),
                    Action::OpenMode(mode) => {
                        app.set_mode(mode);
                    }
                    Action::NextMode => match app.mode {
                        // Default modes
                        Mode::Tape => {
                            app.set_mode(Mode::Performance);
                        }
                        Mode::Performance => {
                            app.set_mode(Mode::Logger);
                        }
                        Mode::Logger => {
                            app.set_mode(Mode::Menu);
                        }
                        Mode::Menu => {
                            app.set_mode(Mode::Tape);
                        }
                    },
                    Action::PreviousMode => match app.mode {
                        // Default modes
                        Mode::Tape => {
                            app.set_mode(Mode::Menu);
                        }
                        Mode::Menu => {
                            app.set_mode(Mode::Logger);
                        }
                        Mode::Logger => {
                            app.set_mode(Mode::Performance);
                        }
                        Mode::Performance => {
                            app.set_mode(Mode::Tape);
                        }
                    },
                    Action::ToggleMute(idx) => {
                        let submix = app.state.focused_submix;
                        let value = if submix == 0 {
                            app.mixer.tracks[idx].mute
                        } else {
                            app.mixer.submixes[submix - 1].track_mutes[idx]
                        };
                        app.set_mute(Destination::Track(idx), !value);
                    }
                    Action::ToggleSolo(idx) => {
                        let submix = app.state.focused_submix;
                        let value = if submix == 0 {
                            app.mixer.tracks[idx].solo
                        } else {
                            app.mixer.submixes[submix - 1].track_solos[idx]
                        };
                        app.set_solo(Destination::Track(idx), !value);
                    }
                    Action::ToggleArm(idx) => {
                        app.mixer.tracks[idx].record_arm = !app.mixer.tracks[idx].record_arm;

                        request_tx
                            .send(Request::RecordArm(
                                Destination::Track(idx),
                                app.mixer.tracks[idx].record_arm,
                            ))
                            .unwrap();
                        midi_tx
                            .send(Mackie::RecordArm(
                                Destination::Track(idx),
                                app.mixer.tracks[idx].record_arm,
                            ))
                            .unwrap();
                    }
                    Action::ToggleMonitor(idx) => {
                        app.mixer.tracks[idx].monitor = !app.mixer.tracks[idx].monitor;

                        request_tx
                            .send(Request::Monitor(
                                Destination::Track(idx),
                                app.mixer.tracks[idx].monitor,
                            ))
                            .unwrap();
                    }
                    Action::IsolateMute(isolate_idx) => {
                        for (idx, track) in app.mixer.tracks.iter_mut().enumerate() {
                            track.mute = idx == isolate_idx;

                            request_tx
                                .send(Request::Mute(Destination::Track(idx), track.mute))
                                .unwrap();
                            midi_tx
                                .send(Mackie::Mute(Destination::Track(idx), track.mute))
                                .unwrap();
                        }
                    }
                    Action::IsolateSolo(isolate_idx) => {
                        for (idx, track) in app.mixer.tracks.iter_mut().enumerate() {
                            track.solo = idx == isolate_idx;

                            request_tx
                                .send(Request::Solo(Destination::Track(idx), track.solo))
                                .unwrap();
                            midi_tx
                                .send(Mackie::Solo(Destination::Track(idx), track.solo))
                                .unwrap();
                        }
                    }
                    Action::IsolateArm(isolate_idx) => {
                        for (idx, track) in app.mixer.tracks.iter_mut().enumerate() {
                            track.record_arm = idx == isolate_idx;

                            request_tx
                                .send(Request::RecordArm(
                                    Destination::Track(idx),
                                    track.record_arm,
                                ))
                                .unwrap();
                            midi_tx
                                .send(Mackie::RecordArm(Destination::Track(idx), track.record_arm))
                                .unwrap();
                        }
                    }
                    Action::VolUp => {
                        if !app.disable_track_focus {
                            let submix = app.state.focused_submix;
                            let idx = app.state.focused_track;
                            let gain = if submix == 0 {
                                app.mixer.tracks[idx].fader
                            } else {
                                app.mixer.submixes[submix - 1].track_gains[idx]
                            };
                            let value = if gain < -90.0 { -90.0 } else { gain + 1.0 };
                            app.set_gain(Destination::Track(app.state.focused_track), value);
                        }
                    }
                    Action::VolDown => {
                        if !app.disable_track_focus {
                            let submix = app.state.focused_submix;
                            let idx = app.state.focused_track;
                            let gain = if submix == 0 {
                                app.mixer.tracks[idx].fader
                            } else {
                                app.mixer.submixes[submix - 1].track_gains[idx]
                            };
                            let value = gain - 1.0;
                            app.set_gain(Destination::Track(app.state.focused_track), value);
                        }
                    }
                    Action::MasterUp => {
                        let submix = app.state.focused_submix;
                        let gain = if submix == 0 {
                            app.mixer.master_bus.fader
                        } else {
                            app.mixer.submixes[submix - 1].master_gain
                        };
                        let value = if gain < -90.0 { -90.0 } else { gain + 1.0 };
                        app.set_gain(Destination::Master, value);
                    }
                    Action::MasterDown => {
                        let submix = app.state.focused_submix;
                        let gain = if submix == 0 {
                            app.mixer.master_bus.fader
                        } else {
                            app.mixer.submixes[submix - 1].master_gain
                        };
                        let value = gain - 1.0;
                        app.set_gain(Destination::Master, value);
                    }
                    Action::SetFader(Destination::Track(idx), value) => {
                        app.set_focused_submix(idx + 1);
                        let value = if value < -90.0 { -f32::INFINITY } else { value };
                        app.set_gain(Destination::Master, value);
                    }
                    Action::SetFader(Destination::Master, _) => (),
                    Action::SetFaderRelative(Destination::Track(idx), value) => match app.mode {
                        Mode::Tape => {
                            let submix = app.state.focused_submix;
                            let gain = if submix == 0 {
                                app.mixer.tracks[idx].fader
                            } else {
                                app.mixer.submixes[submix - 1].track_gains[idx]
                            };
                            let value = if gain < -90.0 && value > 0.0 {
                                -90.0
                            } else {
                                gain + value
                            };
                            app.set_gain(Destination::Track(idx), value);
                        }
                        Mode::Performance => (),
                        _ => {}
                    },
                    Action::SetFaderRelative(Destination::Master, value) => {
                        let submix = app.state.focused_submix;
                        let gain = if submix == 0 {
                            app.mixer.master_bus.fader
                        } else {
                            app.mixer.submixes[submix - 1].master_gain
                        };
                        let value = if gain < -90.0 && value > 0.0 {
                            -90.0
                        } else {
                            gain + value
                        };
                        app.set_gain(Destination::Master, value);
                    }
                    #[cfg(feature = "master_effects")]
                    Action::SetKnob(Destination::Track(idx), value) => {
                        app.state.focused_effect = idx;
                        let normalised_value = if value < 60 {
                            value as f32 / 120.0
                        } else if value > 67 {
                            0.5 + (value as f32 - 67.0) / 120.0
                        } else {
                            0.5
                        };
                        app.mixer.master_bus.effects[idx].set(normalised_value);
                        request_tx
                            .send(Request::MasterEffect(
                                app.mixer.master_bus.get_effect_state(),
                            ))
                            .unwrap();
                    }
                    #[cfg(feature = "master_effects")]
                    Action::SetKnob(Destination::Master, _) => (),
                    #[cfg(feature = "master_effects")]
                    Action::SetKnobRelative(Destination::Track(idx), value) => {
                        app.state.focused_effect = idx;
                        app.mixer.master_bus.effects[idx].set_relative(value as f32 / 252.0);
                        request_tx
                            .send(Request::MasterEffect(
                                app.mixer.master_bus.get_effect_state(),
                            ))
                            .unwrap();
                    }
                    #[cfg(feature = "master_effects")]
                    Action::SetKnobRelative(Destination::Master, _) => (),
                    Action::PanLeft => {
                        if !app.disable_track_focus {
                            let submix = app.state.focused_submix;
                            let idx = app.state.focused_track;

                            let pan = if submix == 0 {
                                app.mixer.tracks[idx].pan
                            } else {
                                app.mixer.submixes[submix - 1].track_pans[idx]
                            };

                            app.set_pan(Destination::Track(idx), pan - 0.1);
                        }
                    }
                    Action::PanRight => {
                        if !app.disable_track_focus {
                            let submix = app.state.focused_submix;
                            let idx = app.state.focused_track;

                            let pan = if submix == 0 {
                                app.mixer.tracks[idx].pan
                            } else {
                                app.mixer.submixes[submix - 1].track_pans[idx]
                            };

                            app.set_pan(Destination::Track(idx), pan + 0.1);
                        }
                    }
                    Action::SetPanRelative(idx, value) => {
                        let submix = app.state.focused_submix;
                        let pan = if submix == 0 {
                            app.mixer.tracks[idx].pan
                        } else {
                            app.mixer.submixes[submix - 1].track_pans[idx]
                        };
                        app.set_pan(Destination::Track(idx), pan + value);
                    }
                    Action::SetReverbSendRelative(idx, value) => {
                        let reverb_send = app.mixer.tracks[idx].reverb_send;
                        app.set_reverb_send(Destination::Track(idx), reverb_send + value);
                    }
                    Action::ToggleHeight => {
                        let num_visible_tracks = if app.state.num_visible_tracks < 8 {
                            app.state.num_visible_tracks + 1
                        } else {
                            1
                        };
                        app.set_num_visible_tracks(num_visible_tracks);
                    }
                    Action::VerticalZoom(rel) => {
                        let num_visible_tracks = app.state.num_visible_tracks;
                        app.set_num_visible_tracks((num_visible_tracks as i32 - rel) as usize);
                    }
                    Action::Quit => {
                        app.save_states();
                        logger.write().log("Quiting Millstone");
                        break;
                    }
                    Action::NewProject => {
                        app.save_states();
                        app.depot.new_project();
                        action_tx
                            .send(Action::OpenProject(app.depot.get_current_project()))
                            .unwrap();
                    }
                    Action::NewProjectFromCurrent => {
                        action_tx
                            .send(Action::NewProjectFromThis(app.depot.get_current_project()))
                            .unwrap();
                    }
                    Action::NewProjectFromThis(project_name) => {
                        app.state.t = 0;
                        app.save_states();
                        app.depot.new_project_from(&project_name);
                        action_tx
                            .send(Action::OpenProject(app.depot.get_current_project()))
                            .unwrap();
                    }
                    Action::CopyProject(project_name) => {
                        app.save_states();
                        app.depot.copy_project(&project_name);
                        app.save_states();
                        app.menu.update();
                    }
                    Action::OpenProject(project_name) => {
                        request_tx.send(Request::PlayState(false)).unwrap();
                        app.save_states();
                        app.open_project(project_name);
                        app = App::new(
                            &global_settings,
                            &logger,
                            &request_tx,
                            &midi_tx,
                            app.depot,
                            app.menu,
                        );
                        request_tx.send(Request::AbsoluteTime(app.state.t)).unwrap();
                        app.menu.update();
                    }
                    Action::SelectProject(project_name) => {
                        app.save_states();
                        app.menu.attach_project(&project_name);
                    }
                    Action::Modes => {
                        app.menu.attach_modes();
                    }
                    Action::Cancel => {
                        app.menu.pop();
                    }
                    Action::Projects => {
                        app.menu.attach_projects();
                    }
                    Action::Scripts => {
                        app.menu.attach_scripts();
                    }
                    Action::Rename(project_name) => app.menu.attach_rename(&project_name),
                    Action::RenameProject(project_name) => {
                        app.save_states();
                        app.depot
                            .rename_project(&project_name, &app.menu.get_register());
                        if project_name == app.depot.get_current_project() {
                            app.depot.set_current_project(app.menu.get_register());
                            app.depot.write(".depot.toml");
                            env::set_var(
                                "_MILLSTONE_PROJECT_ROOT",
                                paths::depot_path(&app.depot.get_current_project()),
                            );
                            app = App::new(
                                &global_settings,
                                &logger,
                                &request_tx,
                                &midi_tx,
                                app.depot,
                                app.menu,
                            );
                        }
                        app.menu.update();
                        app.menu.pop();
                        action_tx.send(Action::OpenMode(Mode::Menu)).unwrap();
                    }
                    Action::Backspace => app.menu.backspace(),
                    Action::Letter(letter) => app.menu.type_letter(letter),
                    Action::Trashbin => app.menu.attach_trash(),
                    Action::TrashProject(project_name) => {
                        app.trash_project(&project_name);
                    }
                    Action::RestoreProject(project_name) => {
                        app.restore_project(&project_name);
                    }
                    Action::ExecuteScript(script_name) => {
                        app.execute_script(&script_name, action_tx.clone());
                        action_tx.send(Action::ToggleLogger).unwrap();
                    }
                    Action::ToggleMarker => match app.mode {
                        Mode::Tape | Mode::Performance => {
                            app.marker_state.toggle_marker(app.state.t)
                        }
                        Mode::Menu => {
                            action_tx.send(app.menu.selected_action()).unwrap();
                        }
                        Mode::Logger => (),
                    },
                    Action::RelabelMarker => match app.mode {
                        Mode::Tape => app.marker_state.relabel_marker(app.state.t),
                        Mode::Performance => app.toggle_analyzer_state(),
                        _ => (),
                    },
                    Action::AutoReturn(value) => {
                        app.autoreturn = value;
                        request_tx
                            .send(Request::AutoReturn(app.autoreturn))
                            .unwrap();
                    }
                    Action::JumpToStart => {
                        if !app.record {
                            let t = app.marker_state.start;
                            app.state.t = t;
                            request_tx.send(Request::AbsoluteTime(t)).unwrap();
                        }
                    }
                    Action::JumpToEnd => {
                        if !app.record {
                            let t = app.marker_state.stop;
                            app.state.t = t;
                            request_tx.send(Request::AbsoluteTime(t)).unwrap();
                        }
                    }
                    Action::SetStart => {
                        app.marker_state.start = app.state.t;
                        logger.write().log("Saving markers");
                        app.marker_state.write("markers.toml");
                    }
                    Action::SetEnd => {
                        app.marker_state.stop = app.state.t;
                        logger.write().log("Saving markers");
                        app.marker_state.write("markers.toml");
                    }
                    Action::SetLoopStart => {
                        app.marker_state.loop_start = app.state.t;
                        request_tx
                            .send(Request::SetLoopStart(app.marker_state.loop_start))
                            .unwrap();
                        logger.write().log("Saving markers");
                        app.marker_state.write("markers.toml");
                    }
                    Action::SetLoopStop => {
                        app.marker_state.loop_stop = app.state.t;
                        request_tx
                            .send(Request::SetLoopStop(app.marker_state.loop_stop))
                            .unwrap();
                        logger.write().log("Saving markers");
                        app.marker_state.write("markers.toml");
                    }
                    Action::Export => {
                        app.export = true;

                        app.play = false;
                        request_tx
                            .send(Request::SetUpdateInterval(10 * 44100))
                            .unwrap();
                        request_tx.send(Request::PlayState(false)).unwrap();
                        app.record = false;
                        request_tx.send(Request::RecordState(false)).unwrap();

                        let filename = paths::project_path(&format!(
                            "export/{}-r{}.wav",
                            app.project.title, app.project.revision,
                        ));
                        request_tx
                            .send(Request::Export(
                                filename.to_string(),
                                app.marker_state.start,
                                app.marker_state.stop,
                            ))
                            .unwrap();
                        logger.write().log("Export started");
                        logger.write().log(&filename[4..filename.len()]);
                    }
                    Action::ExportCompleted => {
                        app.export = false;
                        app.project.revision += 1;
                        logger.write().log("Writing project toml");
                        app.project.write("project.toml");
                        logger.write().log("Export completed");
                        request_tx
                            .send(Request::SetUpdateInterval(8 * 441))
                            .unwrap();
                    }
                    Action::Loop => {
                        // Send loop state
                        app.looping = !app.looping;
                        request_tx.send(Request::LoopState(app.looping)).unwrap();
                    }
                    Action::LoopCycle => {
                        match app.state.loop_cycle_state {
                            0 => {
                                app.state.loop_cycle_state = 1;
                                app.marker_state.loop_start = app.state.t;
                                request_tx
                                    .send(Request::SetLoopStart(app.marker_state.loop_start))
                                    .unwrap();
                                logger.write().log("Saving markers");
                                app.marker_state.write("markers.toml");
                            }
                            1 => {
                                app.state.loop_cycle_state = 2;
                                app.marker_state.loop_stop = app.state.t;
                                request_tx
                                    .send(Request::SetLoopStop(app.marker_state.loop_stop))
                                    .unwrap();
                                logger.write().log("Saving markers");
                                app.marker_state.write("markers.toml");
                                app.looping = true;
                                request_tx.send(Request::LoopState(app.looping)).unwrap();
                            }
                            _ => {
                                app.state.loop_cycle_state = 0;
                                app.looping = false;
                                request_tx.send(Request::LoopState(app.looping)).unwrap();
                            }
                        };
                    }
                    Action::LoadStatus(Destination::Master, _) => {}
                    Action::LoadStatus(Destination::Track(idx), value) => {
                        app.preload_status[idx] = value;
                    }
                    Action::Peak(Destination::Master, dt, left, right) => {
                        let c = match app.master_peak_left {
                            v if v > 0.0 => peak_decay_const_crit,
                            _ => peak_decay_const,
                        };
                        app.master_peak_left = (app.master_peak_left - c * dt as f32).max(left);

                        let c = match app.master_peak_right {
                            v if v > 0.0 => peak_decay_const_crit,
                            _ => peak_decay_const,
                        };
                        app.master_peak_right = (app.master_peak_right - c * dt as f32).max(right);
                    }
                    Action::Rms(Destination::Master, dt, left, right) => {
                        let c = match app.master_rms_left {
                            v if v > 0.0 => peak_decay_const_crit,
                            _ => peak_decay_const,
                        };
                        app.master_rms_left = (app.master_rms_left - c * dt as f32).max(left);

                        let c = match app.master_rms_right {
                            v if v > 0.0 => peak_decay_const_crit,
                            _ => peak_decay_const,
                        };
                        app.master_rms_right = (app.master_rms_right - c * dt as f32).max(right);
                    }
                    #[cfg(feature = "master_effects")]
                    Action::CompressorState(Destination::Track(_), _) => {}
                    #[cfg(feature = "master_effects")]
                    Action::CompressorState(Destination::Master, gain) => {
                        app.compressor_gain = gain;
                    }
                    Action::Peak(Destination::Track(idx), dt, left, right) => {
                        app.track_peak_left[idx] =
                            (app.track_peak_left[idx] - peak_decay_const * dt as f32).max(left);
                        app.track_peak_right[idx] =
                            (app.track_peak_right[idx] - peak_decay_const * dt as f32).max(right);
                    }
                    Action::Rms(Destination::Track(idx), dt, left, right) => {
                        app.track_rms_left[idx] =
                            (app.track_rms_left[idx] - peak_decay_const * dt as f32).max(left);
                        app.track_rms_right[idx] =
                            (app.track_rms_right[idx] - peak_decay_const * dt as f32).max(right);
                    }
                    Action::SpectrumAnalysis(Destination::Track(_), _dt, _buffer) => {}
                    Action::SpectrumAnalysis(Destination::Master, dt, buffer) => {
                        for idx in 0..app.master_spectrum.len() {
                            app.master_spectrum[idx] -= peak_decay_const * dt as f32;
                        }
                        app.master_spectrum_min_freq = 44100.0 / (2.0 * buffer.len() as f32);

                        let spectrum_length = app.master_spectrum.len() as f32;
                        let alpha = spectrum_length / (buffer.len() as f32).log10();

                        for (idx, buffer_value) in buffer.iter().enumerate() {
                            // Buffer contains data on a linear frequency scale.  We want to
                            // convert it to a logarithmic frequency scale when saving it to
                            // the spectrum.
                            let lower_idx = (alpha * ((idx + 1) as f32).log10()).round();
                            let lower_idx = (lower_idx - 1.0).max(0.0) as usize;

                            let upper_idx = (alpha * ((idx + 2) as f32).log10()).round();
                            let upper_idx = ((upper_idx - 1.0) as usize).max(lower_idx + 1);

                            for spec_idx in lower_idx..upper_idx {
                                app.master_spectrum[spec_idx] =
                                    app.master_spectrum[spec_idx].max(*buffer_value);
                            }
                        }
                    }
                    Action::ToggleLogger => {
                        app.toggle_logger_mode();
                    }
                    Action::LoudnessRegion(Destination::Track(idx), t, _length, value) => {
                        app.mixer.tracks[idx].loudness_map.insert(t, value);
                    }
                    Action::LoudnessRegion(_, _, _, _) => (), // Not implemented.
                    Action::FileSize(Destination::Track(idx), value) => {
                        app.mixer.tracks[idx].length = value;
                    }
                    Action::FileSize(_, _) => (), // Not implemented.

                    Action::Workload {
                        identifier,
                        percentage,
                        max,
                    } => {
                        app.workload[identifier] = Workload { percentage, max };
                    }

                    #[cfg(feature = "ballet")]
                    Action::BalletSetup(rx) => {
                        request_tx.send(Request::BalletSetup(rx)).unwrap();
                    }

                    #[cfg(feature = "opera")]
                    Action::OperaSetup(rx) => {
                        request_tx.send(Request::OperaSetup(rx)).unwrap();
                    }
                };
            }
            Err(RecvError) => panic!("Device channel disconnected."),
        }

        //// App updates /////
        app.frames += 1; // graphical frame counter increment
        if terminal.draw(|f| app.draw(f)).is_err() {
            continue;
        }
    }
}
