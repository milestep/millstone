// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use std::fs::{File, OpenOptions};
use std::io::Write;

use crate::paths;

pub struct Logger {
    file: File,
    pub entries: Vec<String>,
    pub offset: usize,
}

impl Logger {
    pub fn new(filename: &str) -> Self {
        let filename = &paths::depot_path(filename);
        Self {
            file: OpenOptions::new()
                .create(true)
                .append(true)
                .open(filename)
                .unwrap(),
            entries: vec![],
            offset: 0,
        }
    }

    pub fn change_offset(&mut self, delta: isize) {
        self.offset = ((self.offset as isize + delta).max(0) as usize).min(self.entries.len());
    }

    pub fn log(&mut self, msg: &str) {
        // Size of entry history, constraining memory consumption
        while self.entries.len() > 1000 {
            self.entries.remove(0);
        }
        // Write log to file.
        let time = chrono::Local::now().format("%H:%M:%S%.3f");
        if let Ok(()) = writeln!(self.file, "{}: {}", time, msg) {
            if self.offset == self.entries.len() {
                self.offset += 1;
            };
            self.entries.push(format!("{}: {}", time, msg));
        };
    }
}
