// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

mod analyzer;
mod bus;
#[cfg(feature = "master_effects")]
mod effects;
#[cfg(feature = "master_effects")]
mod filters;
mod frames;
mod io;
mod kernel;
mod midi;
mod mixer;
#[cfg(feature = "master_effects")]
mod reverb_bus;
mod submix;
mod tape;
mod track;
mod vinyl;
mod wheel;

#[cfg(feature = "ballet")]
mod ballet;

#[cfg(feature = "opera")]
mod opera;

use crossbeam::channel;
use crossbeam::channel::{Receiver, Sender};

use parking_lot::RwLock;

use std::sync::Arc;

use crate::global_settings::GlobalSettings;
use crate::logger::Logger;
use crate::messages::{Action, MidiMessage, Request};

pub fn run(
    global_settings: Arc<RwLock<GlobalSettings>>,
    request_rx: Receiver<Request>,
    action_tx: Sender<Action>,
    midi_inbound_tx: Sender<MidiMessage>,
    midi_outbound_rx: Receiver<MidiMessage>,
    logger: Arc<RwLock<Logger>>,
) {
    let (kernel_tx, kernel_rx) = channel::unbounded::<kernel::KernelMessage>();
    let (io_request_tx, io_request_rx) = channel::unbounded::<io::IOMessage<frames::StereoFrame>>();
    let (analyzer_tx, analyzer_rx) = channel::unbounded::<frames::StereoFrame>();

    kernel::spawn(
        Arc::clone(&logger),
        Arc::clone(&global_settings),
        action_tx.clone(),
        kernel_tx,
    );

    midi::spawn(
        &global_settings.read().midi_blacklist,
        action_tx.clone(),
        midi_inbound_tx,
        midi_outbound_rx,
    );

    io::spawn(action_tx.clone(), io_request_rx);

    analyzer::spawn(action_tx.clone(), analyzer_rx, 2048);

    mixer::spawn(action_tx, io_request_tx, analyzer_tx, request_rx, kernel_rx);
}
