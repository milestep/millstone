// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

use crate::state::State;

#[derive(Deserialize, Serialize)]
pub struct GlobalSettings {
    pub device_in: String,
    pub device_out: String,
    pub midi_device: String,
    pub rate: usize,
    pub nchannels_in: usize,
    pub nchannels_out: usize,
    pub period_size: usize,
    pub periods: usize,
    pub sample_format_in: String,
    pub sample_format_out: String,

    #[serde(default = "default_disable_track_focus")]
    pub disable_track_focus: bool,

    #[serde(default = "default_midi_blacklist")]
    pub midi_blacklist: Vec<String>,
}

fn default_disable_track_focus() -> bool {
    false
}

fn default_midi_blacklist() -> Vec<String> {
    vec![
        String::from("Client"),
        String::from("Midi Through"),
        String::from("System"),
    ]
}

impl State for GlobalSettings {
    fn new() -> Self {
        Self {
            device_in: String::from("default"),
            device_out: String::from("default"),
            midi_device: String::from("None"),
            rate: 44100,
            nchannels_in: 2,
            nchannels_out: 2,
            period_size: 1024,
            periods: 2,
            sample_format_in: String::from("S16_LE"),
            sample_format_out: String::from("S16_LE"),
            disable_track_focus: false,
            midi_blacklist: vec![],
        }
    }

    fn from_string(s: String) -> Self {
        match toml::from_str(&s) {
            Ok(state) => state,
            Err(_) => Self::new(),
        }
    }
}
