// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

#![cfg_attr(feature = "benchmark", feature(test))]

mod backend;
mod depot;
mod frontend;
mod global_settings;
mod logger;
mod messages;
mod paths;
mod perf;
mod state;

#[cfg(feature = "synth")]
mod synth;

#[cfg(feature = "ballet")]
mod ballet;

#[cfg(feature = "opera")]
mod opera;


use std::env;
use std::sync::Arc;

use crossbeam::channel;
use parking_lot::RwLock;

use crate::global_settings::GlobalSettings;
use crate::messages::{Action, MidiMessage, Request};
use crate::state::State;

//////////////////////////////////////////////////
// Main
//////////////////////////////////////////////////

fn main() {
    let logger = Arc::new(RwLock::new(logger::Logger::new("millstone.log")));
    logger.write().log("Millstone is starting");

    // Load depot settings.
    logger.write().log("Reading project depot");
    let depot = depot::Depot::read(".depot.toml");

    // Set active project.
    logger.write().log("Setting active project");
    let args: Vec<String> = env::args().collect();
    match args.get(1) {
        Some(path) => {
            if paths::is_dir(path) {
                env::set_var("_MILLSTONE_PROJECT_ROOT", path)
            } else {
                panic!("{} is not a directory", path);
            }
        }
        None => {
            match env::var("MILLSTONE_PROJECT_ROOT") {
                Ok(path) => env::set_var("_MILLSTONE_PROJECT_ROOT", path),
                Err(_) => {
                    let path = paths::depot_path(&depot.get_current_project());
                    env::set_var("_MILLSTONE_PROJECT_ROOT", path);
                }
            };
        }
    };

    // Load global settings and write to project directory.
    logger.write().log("Loading global settings");
    let global_settings: GlobalSettings = State::read_global("settings.toml");
    global_settings.write_global("settings.toml");
    let global_settings = Arc::new(RwLock::new(global_settings));

    // Channel for frontend to request an action in the backend.
    logger.write().log("Setting up backend frontend channels");
    let (request_tx, request_rx) = channel::unbounded::<Request>();

    // Channel for devices to send actions to the frontend.
    let (action_tx, action_rx) = channel::unbounded::<Action>();

    // Channels for MIDI communication.
    logger.write().log("Setting up midi channels");
    let (midi_inbound_tx, midi_inbound_rx) = channel::unbounded::<MidiMessage>();
    let (midi_outbound_tx, midi_outbound_rx) = channel::unbounded::<MidiMessage>();

    logger.write().log("Starting backend");
    backend::run(
        Arc::clone(&global_settings),
        request_rx,
        action_tx.clone(),
        midi_inbound_tx,
        midi_outbound_rx,
        Arc::clone(&logger),
    );

    logger.write().log("Starting frontend");
    frontend::run(
        Arc::clone(&global_settings),
        depot,
        request_tx,
        action_rx,
        action_tx,
        midi_outbound_tx,
        midi_inbound_rx,
        Arc::clone(&logger),
    );
}
