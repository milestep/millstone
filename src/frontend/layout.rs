// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use tui::layout::{Constraint, Direction, Layout, Rect};

pub const VU_WIDTH: u16 = 1;
pub const MAX_TRACKS: usize = 8;
pub const PLAYHEAD: usize = 50;

pub const LOGO_WIDTH: u16 = 15;
pub const SMALL_LOGO_WIDTH: u16 = 5;

pub const TRACK_VU_WIDTH: u16 = 1;
pub const TRACK_INFO_WIDTH: u16 = 14;
pub const TRACK_FADER_WIDTH: u16 = 1;
pub const MIXER_TRACK_WIDTH: u16 = 16;

pub const WORKLOAD_WIDTH: u16 = 10;
pub const EFFECTS_WIDTH: u16 = 24;
pub const TRANSPORT_WIDTH: u16 = 15;

pub const MIN_WIDTH: u16 = SMALL_LOGO_WIDTH + EFFECTS_WIDTH + TRANSPORT_WIDTH;
pub const MIN_HEIGHT: u16 = 5;

pub const MENU_SPACER: usize = 4;

#[allow(clippy::type_complexity)]
pub fn build(
    size: Rect,
) -> (
    Vec<Rect>,
    Vec<Rect>,
    Vec<Rect>,
    Vec<Rect>,
    Vec<Rect>,
    Vec<Rect>,
    Vec<Rect>,
) {
    // Main layout for user interface
    let layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Length(3),                   // for logo, transport bar and effects
            Constraint::Length(1),                   // for marker view
            Constraint::Length(size.height - 3 - 1), // for center content
        ])
        .split(size);

    let logo_width = if size.width >= EFFECTS_WIDTH + TRANSPORT_WIDTH + WORKLOAD_WIDTH {
        LOGO_WIDTH.min(size.width - EFFECTS_WIDTH - TRANSPORT_WIDTH - WORKLOAD_WIDTH)
    } else {
        0
    };

    // Top bar layout
    let top_bar = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Length(logo_width),     // for logo
            Constraint::Length(WORKLOAD_WIDTH), // workload indicator
            Constraint::Length(
                size.width - logo_width - WORKLOAD_WIDTH - EFFECTS_WIDTH - TRANSPORT_WIDTH,
            ), // spacer
            Constraint::Length(EFFECTS_WIDTH),  // transport or effects
            Constraint::Length(TRANSPORT_WIDTH), // transport or effects
        ])
        .split(layout[0]);

    let effects = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Length(top_bar[3].height - 1), // for faders
            Constraint::Length(1),                     // spacer
        ])
        .split(top_bar[3]);

    // Transport layout
    let top_three_lines = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Length(1); 3])
        .split(top_bar[4]);
    let right_just = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Length(top_three_lines[1].width - TRANSPORT_WIDTH),
            Constraint::Length(TRANSPORT_WIDTH),
        ])
        .split(top_three_lines[1]);
    let transport = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Length(5); 4]) // 4 icons of width 5
        .split(right_just[1]);

    // Marker layout
    let project_info_width = TRACK_INFO_WIDTH + 2 * TRACK_VU_WIDTH + TRACK_FADER_WIDTH;
    let marker_hsplit = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Length(project_info_width), // project name
            Constraint::Length(layout[1].width - project_info_width - 12), // Spacer
            Constraint::Length(12),                 // for time view
        ])
        .split(layout[1]);

    // Horizontal center split
    let hsplit = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Length(layout[1].width - 5 * VU_WIDTH),
            Constraint::Length(5 * VU_WIDTH),
        ])
        .split(layout[2]);

    // Horizontal center split
    let master_vu = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Length(VU_WIDTH); 5])
        .split(hsplit[1]);

    (
        layout,
        top_bar,
        transport,
        marker_hsplit,
        hsplit,
        master_vu,
        effects,
    )
}

pub fn build_tape_tracks(rect: Rect, num_visible_tracks: u16) -> Vec<Rect> {
    let mut constraints = vec![];
    for idx in 0..num_visible_tracks {
        let start = (idx * rect.height) / num_visible_tracks;
        let stop = ((idx + 1) * rect.height) / num_visible_tracks;
        constraints.push(Constraint::Length(stop - start));
    }

    // Build tracks layout rectangles
    Layout::default()
        .direction(Direction::Vertical)
        .constraints(constraints)
        .split(rect)
}

pub fn split_tape_track(rect: Rect) -> Vec<Rect> {
    Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Length(TRACK_VU_WIDTH),    // for track vu meter (L)
            Constraint::Length(TRACK_FADER_WIDTH), // for track fader
            Constraint::Length(TRACK_VU_WIDTH),    // for track vu meter (R)
            Constraint::Length(TRACK_INFO_WIDTH),  // for track information
            Constraint::Length(rect.width - (TRACK_VU_WIDTH + TRACK_INFO_WIDTH)), // for waveform view
        ])
        .split(rect)
}

pub fn build_popup(rect: Rect) -> Vec<Rect> {
    // Popup for selectors
    Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Length(3),               // for logo
            Constraint::Length(rect.height - 3), // for project selector
        ])
        .split(rect)
}
