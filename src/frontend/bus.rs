// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

#[cfg(feature = "master_effects")]
use math::round;

use serde::{Deserialize, Serialize};

//////////////////////////////////////////////////
// EffectSlot
//////////////////////////////////////////////////

#[cfg(feature = "master_effects")]
#[derive(Serialize, Deserialize)]
pub struct EffectSlot {
    pub value: f32,
    pub rounded: f32,
}

#[cfg(feature = "master_effects")]
impl EffectSlot {
    pub fn new(value: f32) -> Self {
        let mut slot = Self {
            value,
            rounded: 0.0,
        };
        slot.set_rounded();
        slot
    }

    pub fn set(&mut self, value: f32) {
        self.value = value.min(1.0).max(0.0);
        self.set_rounded();
    }

    pub fn set_relative(&mut self, value: f32) {
        self.value = (self.value + value).min(1.0).max(0.0);
        self.set_rounded();
    }

    pub fn cycle(&mut self, value: f32) {
        self.value += value;
        self.set_rounded();
        if self.rounded > 1.0 {
            self.value = 0.0;
            self.set_rounded();
        }
    }

    fn set_rounded(&mut self) {
        self.rounded = round::half_up(self.value as f64, 2) as f32;
    }
}

//////////////////////////////////////////////////
// BusState
//////////////////////////////////////////////////

#[derive(Serialize, Deserialize)]
pub struct BusState {
    pub name: String,
    pub mute: bool,
    pub solo: bool,
    pub fader: f32,
    pub pan: f32,
    #[cfg(feature = "master_effects")]
    pub effects: Vec<EffectSlot>,
}

impl BusState {
    pub fn new() -> Self {
        Self::from_string("Bus")
    }

    pub fn from_string(name: &str) -> Self {
        Self {
            name: String::from(name),
            mute: false,
            solo: false,
            fader: 0.0,
            pan: 0.0,
            #[cfg(feature = "master_effects")]
            effects: vec![
                EffectSlot::new(0.5),
                EffectSlot::new(0.5),
                EffectSlot::new(0.5),
                EffectSlot::new(0.5),
                EffectSlot::new(0.0),
                EffectSlot::new(0.0),
                EffectSlot::new(0.0),
                EffectSlot::new(0.0),
            ],
        }
    }

    #[cfg(feature = "master_effects")]
    pub fn get_effect_state(&self) -> [f32; 8] {
        let mut effects = [0.0f32; 8];
        for (i, x) in self.effects.iter().enumerate() {
            effects[i] = x.rounded;
        }
        effects
    }
}
