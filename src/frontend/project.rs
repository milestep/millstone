// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::state::State;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct ProjectState {
    pub title: String,
    pub revision: usize,
}

impl State for ProjectState {
    fn new() -> Self {
        Self {
            title: String::from("Untitled"),
            revision: 1,
        }
    }

    fn from_string(s: String) -> Self {
        match toml::from_str(&s) {
            Ok(state) => state,
            Err(_) => Self::new(),
        }
    }
}
