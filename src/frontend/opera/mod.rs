// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

mod boppad;

use alsa::seq;

use crossbeam::channel::Sender;

use crate::messages::{Action, OperaMessage};

pub struct Interface {
    boppad: boppad::BopPad,
    action_tx: Sender<Action>,
}

impl Interface {
    pub fn new(action_tx: Sender<Action>, opera_tx: Sender<OperaMessage>) -> Self {
        Self {
            boppad: boppad::BopPad::new(action_tx.clone(), opera_tx.clone()),
            action_tx,
        }
    }

    pub fn connect(&mut self, addr: seq::Addr, name: String) {
        if self.boppad.connect(&addr, &name) {
            self.action_tx
                .send(Action::Log(format!(
                    "MIDI connected: {} {}:{}",
                    name, addr.client, addr.port
                )))
                .unwrap();
        }
    }

    pub fn disconnect(&mut self, addr: seq::Addr, name: String) {
        if self.boppad.disconnect(&addr) {
            self.action_tx
                .send(Action::Log(format!(
                    "MIDI disconnected: {} {}:{}",
                    name, addr.client, addr.port
                )))
                .unwrap();
        }
    }

    pub fn cc(&mut self, addr: seq::Addr, channel: u8, param: u8, value: u8) {
        if self.boppad.has_addr(&addr) {
            self.action_tx
                .send(Action::Log(format!(
                    "Bobpad CC: {} {}:{}",
                    channel, param, value,
                )))
                .unwrap();
            self.boppad.cc(channel, param, value);
        }
    }

    pub fn note_on(&mut self, addr: seq::Addr, channel: u8, note: u8, velocity: u8) {
        if self.boppad.has_addr(&addr) {
            self.action_tx
                .send(Action::Log(format!(
                    "Bobpad On: {} {}:{}",
                    channel, note, velocity,
                )))
                .unwrap();
            self.boppad.note_on(channel, note, velocity);
        }
    }

    pub fn note_off(&mut self, addr: seq::Addr, channel: u8, note: u8, velocity: u8) {
        if self.boppad.has_addr(&addr) {
            self.action_tx
                .send(Action::Log(format!(
                    "Bobpad Off: {} {}:{}",
                    channel, note, velocity,
                )))
                .unwrap();
            self.boppad.note_off(channel, note, velocity);
        }
    }
}
