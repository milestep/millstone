// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use tui::style::Color::Rgb;
use tui::style::{Modifier, Style};

// Default theme
pub const NEUTRAL: tui::style::Color = Rgb(255, 255, 241); // cream white
pub const ACTIVE: tui::style::Color = Rgb(253, 251, 255); // silver
pub const ACTIVE_BOUND: tui::style::Color = Rgb(255, 215, 185); // pink gold
pub const INACTIVE: tui::style::Color = Rgb(127, 122, 129); // silver metallic
pub const SELECT: tui::style::Color = Rgb(20, 21, 29); // more grady
pub const RECORD: tui::style::Color = Rgb(159, 74, 84); // english red
pub const PLAY: tui::style::Color = Rgb(9, 188, 138); // mountain meadow
pub const STOP: tui::style::Color = Rgb(252, 186, 4); // selective yellow
pub const PLAYHEAD: tui::style::Color = NEUTRAL; // actively gray
pub const WAVE: tui::style::Color = Rgb(4, 139, 168); // blue munsell
pub const FORE: tui::style::Color = Rgb(241, 250, 238); // honneydew
pub const BACK: tui::style::Color = Rgb(4, 5, 9); // deep grey
pub const EXPORT: tui::style::Color = Rgb(159, 74, 84); // export red
pub const LOOP: tui::style::Color = Rgb(255, 20, 147); // loopdiloop pink
pub const CRIT: tui::style::Color = Rgb(192, 0, 42); // therewillbeblood
pub const FOG: tui::style::Color = Rgb(8, 6, 18); // bluish of dawn

pub fn nf() -> Style {
    Style::default()
        .fg(FORE)
        .bg(BACK)
        .add_modifier(Modifier::BOLD)
}

pub fn vu_pink(db: f32) -> tui::style::Color {
    let x = 10.0f32.powf(db / 40.0);
    Rgb(255, 255 - (235.0 * x) as u8, 255 - (108.0 * x) as u8)
}

pub fn vu_violet_fog(db: f32) -> tui::style::Color {
    let x = 10.0f32.powf(db / 40.0);
    Rgb(
        (6.0 * x + 133.0 * (1.0 - x)) as u8,
        (8.0 * x + 4.0 * (1.0 - x)) as u8,
        (18.0 * x + 150.0 * (1.0 - x)) as u8,
    )
}

#[cfg(feature = "master_effects")]
pub fn fader_electric(x: f32) -> tui::style::Color {
    if x == 0.5 {
        Rgb(255, 255, 255)
    } else {
        Rgb(
            (200.0 + 55.0 * x) as u8,
            (200.0 + 55.0 * x) as u8,
            (100.0 + 35.0 * x) as u8,
        )
    }
}

pub fn interpolate(c1: tui::style::Color, c2: tui::style::Color, x: f32) -> tui::style::Color {
    if let Rgb(r1, g1, b1) = c1 {
        if let Rgb(r2, g2, b2) = c2 {
            return Rgb(
                ((1.0 - x) * r1 as f32 + x * r2 as f32) as u8,
                ((1.0 - x) * g1 as f32 + x * g2 as f32) as u8,
                ((1.0 - x) * b1 as f32 + x * b2 as f32) as u8,
            );
        }
    }
    Rgb(255, 255, 255)
}
