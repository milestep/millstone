// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use alsa::seq;

use crossbeam::channel::{Receiver, RecvTimeoutError, Sender};

use parking_lot::RwLock;

use std::sync::Arc;
use std::thread;

use crate::global_settings::GlobalSettings;
use crate::messages::{Action, Destination, Mackie, MidiMessage};
use crate::perf;

#[cfg(feature = "ballet")]
use crate::messages::BalletMessage;

#[cfg(feature = "ballet")]
use super::ballet;

#[cfg(feature = "opera")]
use crate::messages::OperaMessage;

#[cfg(feature = "opera")]
use super::opera;

const FADER_X: [f32; 5] = [0.0, 0.001, 0.98, 1.02, 1.27];
const FADER_Y: [f32; 5] = [-120.0, -90.0, 0.0, 0.0, 12.0];

const LONGPRESS: std::time::Duration = std::time::Duration::from_millis(1000);
const DOUBLE_TAP_TIME: std::time::Duration = std::time::Duration::from_millis(200);

pub fn spawn(
    global_settings: Arc<RwLock<GlobalSettings>>,
    action_tx: Sender<Action>,
    midi_rx: Receiver<Mackie>,
    outbound_tx: Sender<MidiMessage>,
    inbound_rx: Receiver<MidiMessage>,
) {
    let midi_device_pattern = String::from(&global_settings.read().midi_device);

    thread::spawn(move || {
        perf::set_normal_thread_min_priority();
        let mut midi = MidiDevice::new(action_tx, &midi_device_pattern);
        midi.run(midi_rx, inbound_rx, outbound_tx);
    });
}

pub struct MidiDevice {
    action_tx: Sender<Action>,
    dest: Option<seq::Addr>,
    target: String,
    stop_modifier: bool,
    record_modifier: bool,
    solo_modifiers: Vec<bool>,
    mute_modifiers: Vec<bool>,
    stop_consumed: bool,
    record_consumed: bool,
    solo_consumed: Vec<bool>,
    mute_consumed: Vec<bool>,
    key_press_times: Vec<Option<std::time::SystemTime>>,
    key_release_times: Vec<Option<std::time::SystemTime>>,
}

pub fn fader_position_to_value(fader: f32) -> i16 {
    let mut x = FADER_X[0];

    for idx in 1..FADER_Y.len() {
        if fader <= FADER_Y[idx] {
            let a = if (FADER_Y[idx] - FADER_Y[idx - 1]).abs() < 1e-16 {
                0.5
            } else {
                (x - FADER_Y[idx - 1]) / (FADER_Y[idx] - FADER_Y[idx - 1])
            };
            x = FADER_X[idx - 1] + a * (FADER_X[idx] - FADER_X[idx - 1]);
            break;
        }
    }

    (12900.0 * x - 8192.0) as i16
}

pub fn fader_value_to_position(value: i16) -> f32 {
    let x = (8192.0 + value as f32) / 12900.0;
    let mut fader = FADER_Y[0];

    for idx in 1..FADER_X.len() {
        if x <= FADER_X[idx] {
            let a = if (FADER_X[idx] - FADER_X[idx - 1]).abs() < 1e-16 {
                0.5
            } else {
                (x - FADER_X[idx - 1]) / (FADER_X[idx] - FADER_X[idx - 1])
            };
            fader = FADER_Y[idx - 1] + a * (FADER_Y[idx] - FADER_Y[idx - 1]);
            break;
        }
    }

    fader
}

impl MidiDevice {
    pub fn new(action_tx: Sender<Action>, target: &str) -> Self {
        Self {
            action_tx,
            dest: None,
            target: target.to_string(),
            stop_modifier: false,
            record_modifier: false,
            solo_modifiers: vec![false; 8],
            mute_modifiers: vec![false; 8],
            stop_consumed: false,
            record_consumed: false,
            solo_consumed: vec![false; 8],
            mute_consumed: vec![false; 8],
            key_press_times: vec![None; 128],
            key_release_times: vec![None; 128],
        }
    }

    pub fn run(
        &mut self,
        midi_rx: Receiver<Mackie>,
        inbound_rx: Receiver<MidiMessage>,
        outbound_tx: Sender<MidiMessage>,
    ) {
        let mut midi_rx = Some(midi_rx);

        #[cfg(feature = "ballet")]
        let mut ballet_interface = {
            let (ballet_tx, ballet_rx) = crossbeam::channel::unbounded::<BalletMessage>();
            self.action_tx.send(Action::BalletSetup(ballet_rx)).unwrap();
            ballet::Interface::new(self.action_tx.clone(), outbound_tx.clone(), ballet_tx)
        };

        #[cfg(feature = "opera")]
        let mut opera_interface = {
            let (opera_tx, opera_rx) = crossbeam::channel::unbounded::<OperaMessage>();
            self.action_tx.send(Action::OperaSetup(opera_rx)).unwrap();
            opera::Interface::new(self.action_tx.clone(), opera_tx)
        };

        loop {
            if let Some(receiver) = &midi_rx {
                loop {
                    match receiver.recv_timeout(std::time::Duration::from_micros(100)) {
                        Ok(mackie) => {
                            if let Some(midi) = self.mackie_to_midi(mackie) {
                                outbound_tx.send(midi).unwrap();
                            }
                        }
                        Err(RecvTimeoutError::Timeout) => break,
                        Err(RecvTimeoutError::Disconnected) => {
                            midi_rx = None;
                            break;
                        }
                    }
                }
            }

            loop {
                match inbound_rx.recv_timeout(std::time::Duration::from_millis(1)) {
                    Ok(MidiMessage::Connect { addr, name }) => {
                        if name.contains(&self.target) {
                            self.dest = Some(addr);
                            self.action_tx
                                .send(Action::Log(format!("MIDI device connected: {}", name)))
                                .unwrap();
                            self.action_tx.send(Action::RequestMackieState).unwrap();
                        } else {
                            self.action_tx
                                .send(Action::Log(format!("MIDI device detected: {}", name)))
                                .unwrap();

                            #[cfg(feature = "ballet")]
                            ballet_interface.connect(addr, name);

                            #[cfg(feature = "opera")]
                            opera_interface.connect(addr, name);
                        }
                    }
                    Ok(MidiMessage::Disconnect { addr, name }) => {
                        self.action_tx
                            .send(Action::Log(format!("MIDI device disconnected: {}", name)))
                            .unwrap();

                        if let Some(dest) = self.dest {
                            if dest == addr {
                                self.dest = None;
                                continue;
                            }
                        }

                        #[cfg(feature = "ballet")]
                        ballet_interface.disconnect(addr, name);

                        #[cfg(feature = "opera")]
                        opera_interface.disconnect(addr, name);
                    }
                    Ok(MidiMessage::CC {
                        addr,
                        channel,
                        param,
                        value,
                    }) => {
                        #[cfg(feature = "opera")]
                        opera_interface.cc(addr, channel, param, value);

                        if let Some(dest) = self.dest {
                            if dest == addr {
                                self.cc(channel, param, value);
                                continue;
                            }
                        }

                        #[cfg(feature = "ballet")]
                        ballet_interface.cc(addr, channel, param, value);
                    }
                    Ok(MidiMessage::NoteOn {
                        addr,
                        channel,
                        note,
                        velocity,
                    }) => {
                        #[cfg(feature = "opera")]
                        opera_interface.note_on(addr, channel, note, velocity);

                        if let Some(dest) = self.dest {
                            if dest == addr {
                                if velocity > 0 {
                                    self.note_on(channel, note, velocity);
                                } else {
                                    self.note_off(channel, note, velocity);
                                }
                                continue;
                            }
                        }

                        #[cfg(feature = "ballet")]
                        ballet_interface.note_on(addr, channel, note, velocity);
                    }
                    Ok(MidiMessage::NoteOff {
                        addr,
                        channel,
                        note,
                        velocity,
                    }) => {
                        #[cfg(feature = "opera")]
                        opera_interface.note_off(addr, channel, note, velocity);

                        if let Some(dest) = self.dest {
                            if dest == addr {
                                self.note_off(channel, note, velocity);
                                continue;
                            }
                        }

                        #[cfg(feature = "ballet")]
                        ballet_interface.note_off(addr, channel, note, velocity);
                    }
                    Ok(MidiMessage::Pitchbend {
                        addr,
                        channel,
                        value,
                    }) => {
                        if let Some(dest) = self.dest {
                            if dest == addr {
                                self.pitchbend(channel, value);
                                continue;
                            }
                        }

                        #[cfg(feature = "ballet")]
                        ballet_interface.pitchbend(addr, channel, value);
                    }
                    #[allow(unused_variables)]
                    Ok(MidiMessage::KeyPressure {
                        addr,
                        channel,
                        note,
                        velocity,
                    }) => {
                        if let Some(dest) = self.dest {
                            if dest == addr {
                                continue;
                            }
                        }

                        #[cfg(feature = "ballet")]
                        ballet_interface.key_pressure(addr, channel, note, velocity);
                    }
                    #[allow(unused_variables)]
                    Ok(MidiMessage::Sysex { addr, data }) => {
                        if let Some(dest) = self.dest {
                            if dest == addr {
                                continue;
                            }
                        }

                        #[cfg(feature = "ballet")]
                        ballet_interface.sysex(addr, data);
                    }
                    Err(RecvTimeoutError::Timeout) => {
                        break;
                    }
                    Err(RecvTimeoutError::Disconnected) => {
                        self.action_tx
                            .send(Action::Log("Midi channel disconnected".to_string()))
                            .unwrap();
                        std::thread::sleep(std::time::Duration::from_secs(5));
                        break;
                    }
                }
            }

            for (note, key_press_time) in self.key_press_times.iter_mut().enumerate() {
                match *key_press_time {
                    None => continue,
                    Some(time) => {
                        match time.elapsed() {
                            Ok(elapsed) => {
                                if elapsed < LONGPRESS {
                                    continue;
                                }
                            }
                            Err(_) => continue,
                        }
                        match note {
                            note if note < 8 => {
                                self.action_tx.send(Action::ToggleMonitor(note)).unwrap();
                                *key_press_time = None;
                            }
                            40 | 46 => {
                                self.action_tx.send(Action::ToggleProjectMode).unwrap();
                                *key_press_time = None;
                            }
                            41 | 47 => {
                                self.action_tx.send(Action::ToggleLogger).unwrap();
                                *key_press_time = None;
                            }
                            _ => (),
                        }
                    }
                }
            }
        }
    }

    fn note_on(&mut self, _channel: u8, note: u8, _velocity: u8) {
        self.key_press_times[note as usize] = Some(std::time::SystemTime::now());

        match note {
            note if (8..16).contains(&note) => {
                self.solo_modifiers[note as usize - 8] = true;
                self.solo_consumed[note as usize - 8] = false;
            }
            note if (16..24).contains(&note) => {
                self.mute_modifiers[note as usize - 16] = true;
                self.mute_consumed[note as usize - 16] = false;
            }
            93 => {
                self.stop_modifier = true;
                self.stop_consumed = false;
            }
            94 => {
                if self.stop_modifier && self.record_modifier {
                    self.stop_consumed = true;
                    self.record_consumed = true;
                    self.action_tx.send(Action::Export).unwrap();
                } else if self.stop_modifier {
                    self.stop_consumed = true;
                    self.action_tx.send(Action::AutoReturn(true)).unwrap();
                    self.action_tx.send(Action::Play).unwrap();
                } else {
                    self.action_tx.send(Action::Play).unwrap();
                }
            }
            95 => {
                self.record_modifier = true;
                self.record_consumed = false;
                self.action_tx.send(Action::Record).unwrap();
            }
            _ => (),
        }
    }

    fn note_off(&mut self, _channel: u8, note: u8, _velocity: u8) {
        match note {
            note if (8..16).contains(&note) => self.solo_modifiers[note as usize - 8] = false,
            note if (16..24).contains(&note) => self.mute_modifiers[note as usize - 16] = false,
            93 => self.stop_modifier = false,
            95 => {
                self.record_modifier = false;
                self.action_tx
                    .send(Action::SetRecordNotPlaying(false))
                    .unwrap();
            }
            _ => (),
        }

        match self.key_press_times[note as usize] {
            None => return,
            Some(key_press_time) => match key_press_time.elapsed() {
                Ok(elapsed) => {
                    if elapsed > LONGPRESS {
                        self.key_press_times[note as usize] = None;
                        return;
                    }
                }
                Err(_) => {
                    self.key_press_times[note as usize] = None;
                    return;
                }
            },
        }

        let is_double_tap = match self.key_release_times[note as usize] {
            None => false,
            Some(key_release_time) => match key_release_time.elapsed() {
                Ok(elapsed) => {
                    if elapsed < DOUBLE_TAP_TIME {
                        self.key_release_times[note as usize] = None;
                        true
                    } else {
                        false
                    }
                }
                Err(_) => {
                    self.key_release_times[note as usize] = None;
                    false
                }
            },
        };

        match note {
            note if note < 8 => {
                if is_double_tap {
                    self.action_tx
                        .send(Action::IsolateArm(note as usize))
                        .unwrap();
                } else {
                    self.action_tx
                        .send(Action::ToggleArm(note as usize))
                        .unwrap();
                }
                self.action_tx
                    .send(Action::TrackSelect(note as usize))
                    .unwrap();
            }
            note if (8..16).contains(&note) => {
                if self.stop_modifier {
                    self.stop_consumed = true;
                    self.action_tx
                        .send(Action::SubmixSelect((note - 7) as usize))
                        .unwrap();
                } else if !self.solo_consumed[note as usize - 8] {
                    if is_double_tap {
                        self.action_tx
                            .send(Action::IsolateSolo((note - 8) as usize))
                            .unwrap();
                    } else {
                        self.action_tx
                            .send(Action::ToggleSolo((note - 8) as usize))
                            .unwrap();
                    }
                    self.action_tx
                        .send(Action::TrackSelect((note - 8) as usize))
                        .unwrap();
                }
            }
            note if (16..24).contains(&note) => {
                if !self.mute_consumed[note as usize - 16] {
                    if is_double_tap {
                        self.action_tx
                            .send(Action::IsolateMute((note - 16) as usize))
                            .unwrap();
                    } else {
                        self.action_tx
                            .send(Action::ToggleMute((note - 16) as usize))
                            .unwrap();
                    }
                    self.action_tx
                        .send(Action::TrackSelect((note - 16) as usize))
                        .unwrap();
                }
            }
            note if (24..32).contains(&note) => self
                .action_tx
                .send(Action::TrackSelect((note - 24) as usize))
                .unwrap(),

            40 | 46 => self.action_tx.send(Action::PreviousMode).unwrap(),
            41 | 47 => self.action_tx.send(Action::NextMode).unwrap(),

            51 => self.action_tx.send(Action::Monitor).unwrap(),
            55 => self.action_tx.send(Action::LoopCycle).unwrap(),
            56 | 84 => {
                if self.stop_modifier {
                    self.stop_consumed = true;
                    self.action_tx.send(Action::JumpToStart).unwrap();
                } else if self.record_modifier {
                    self.record_consumed = true;
                    self.action_tx.send(Action::SetStart).unwrap();
                } else {
                    self.action_tx.send(Action::PreviousMarker).unwrap();
                }
            }
            57 | 85 | 90 => {
                if self.stop_modifier {
                    self.stop_consumed = true;
                    self.action_tx.send(Action::JumpToEnd).unwrap();
                } else if self.record_modifier {
                    self.record_consumed = true;
                    self.action_tx.send(Action::SetEnd).unwrap();
                } else {
                    self.action_tx.send(Action::NextMarker).unwrap();
                }
            }
            58 | 83 | 88 | 101 => {
                if self.stop_modifier {
                    self.stop_consumed = true;
                    self.action_tx.send(Action::RelabelMarker).unwrap();
                } else {
                    self.action_tx.send(Action::ToggleMarker).unwrap();
                }
            }
            86 => self.action_tx.send(Action::LoopCycle).unwrap(),
            91 => {
                if self.stop_modifier {
                    self.stop_consumed = true;
                    self.action_tx.send(Action::CycleInputSourceLeft).unwrap();
                }
            }
            92 => {
                if self.stop_modifier {
                    self.stop_consumed = true;
                    self.action_tx.send(Action::CycleInputSourceRight).unwrap();
                }
            }
            93 => {
                if !self.stop_consumed {
                    self.action_tx.send(Action::Stop).unwrap();
                }
            }
            94 => (),
            96 => self.action_tx.send(Action::VerticalZoom(-1)).unwrap(),
            97 => self.action_tx.send(Action::VerticalZoom(1)).unwrap(),
            note => {
                // Logging
                self.action_tx
                    .send(Action::Log(format!("Unhandled MIDI note: {}", note)))
                    .unwrap();
            }
        }

        self.key_press_times[note as usize] = None;

        if !is_double_tap {
            self.key_release_times[note as usize] = Some(std::time::SystemTime::now());
        }
    }

    fn cc(&mut self, _channel: u8, param: u8, value: u8) {
        match param {
            // Scrub/Jog wheel
            60 => {
                let mut handled = false;

                for (idx, (solo_modifier, mute_modifier)) in self
                    .solo_modifiers
                    .iter()
                    .zip(self.mute_modifiers.iter())
                    .enumerate()
                {
                    if *solo_modifier && self.stop_modifier {
                        self.solo_consumed[idx] = true;
                        handled = true;

                        if value > 64 {
                            self.action_tx
                                .send(Action::SetReverbSendRelative(idx, -0.1))
                                .unwrap();
                        } else {
                            self.action_tx
                                .send(Action::SetReverbSendRelative(idx, 0.1))
                                .unwrap();
                        }

                        self.action_tx.send(Action::TrackSelect(idx)).unwrap();
                    } else if *solo_modifier {
                        self.solo_consumed[idx] = true;
                        handled = true;

                        if value > 64 {
                            self.action_tx
                                .send(Action::SetPanRelative(idx, -0.1))
                                .unwrap();
                        } else {
                            self.action_tx
                                .send(Action::SetPanRelative(idx, 0.1))
                                .unwrap();
                        }

                        self.action_tx.send(Action::TrackSelect(idx)).unwrap();
                    } else if *mute_modifier {
                        self.mute_consumed[idx] = true;
                        handled = true;

                        if value > 64 {
                            self.action_tx
                                .send(Action::SetFaderRelative(Destination::Track(idx), -1.0))
                                .unwrap();
                        } else {
                            self.action_tx
                                .send(Action::SetFaderRelative(Destination::Track(idx), 1.0))
                                .unwrap();
                        }

                        self.action_tx.send(Action::TrackSelect(idx)).unwrap();
                    }
                }

                // The event has already been handled by a modifier, so we
                // return early.
                if handled {
                    return;
                }

                if value > 64 {
                    if self.record_modifier {
                        self.record_consumed = true;
                        self.action_tx
                            .send(Action::SetFaderRelative(Destination::Master, -1.0))
                            .unwrap();
                    } else if self.stop_modifier {
                        self.stop_consumed = true;
                        self.action_tx.send(Action::VerticalZoom(-1)).unwrap();
                    } else {
                        self.action_tx
                            .send(Action::Back((value - 64) as usize))
                            .unwrap();
                    }
                } else if self.record_modifier {
                    self.record_consumed = true;
                    self.action_tx
                        .send(Action::SetFaderRelative(Destination::Master, 1.0))
                        .unwrap();
                } else if self.stop_modifier {
                    self.stop_consumed = true;
                    self.action_tx.send(Action::VerticalZoom(1)).unwrap();
                } else {
                    self.action_tx
                        .send(Action::Forward(value as usize))
                        .unwrap();
                }
            }
            // Pan
            #[cfg(feature = "master_effects")]
            ctrl if (16..24).contains(&ctrl) => {
                let idx = (ctrl - 16) as usize;
                if value == 127 {
                    // all left
                    self.action_tx
                        .send(Action::SetKnob(Destination::Track(idx), 0))
                        .unwrap();
                } else if value == 63 {
                    // all right
                    self.action_tx
                        .send(Action::SetKnob(Destination::Track(idx), 127))
                        .unwrap();
                } else if value > 64 {
                    // relative left
                    self.action_tx
                        .send(Action::SetKnobRelative(Destination::Track(idx), -1))
                        .unwrap();
                } else if value < 63 {
                    // relative right
                    self.action_tx
                        .send(Action::SetKnobRelative(Destination::Track(idx), 1))
                        .unwrap();
                }
            }
            ctrl => {
                self.action_tx
                    .send(Action::Log(format!("Unhandled MIDI controller: {}", ctrl)))
                    .unwrap();
            }
        }
    }

    fn pitchbend(&self, channel: u8, value: i16) {
        if channel < 9 {
            let destination = match channel {
                8 => Destination::Master,
                idx => Destination::Track(idx as usize),
            };

            self.action_tx
                .send(Action::SetFader(
                    destination,
                    fader_value_to_position(value),
                ))
                .unwrap();

            if channel < 8 {
                self.action_tx
                    .send(Action::TrackSelect(channel as usize))
                    .unwrap();
            }
        }
    }

    fn mackie_to_midi(&self, input: Mackie) -> Option<MidiMessage> {
        match self.dest {
            None => None,
            Some(addr) => match input {
                Mackie::Mute(Destination::Track(idx), value) => Some(MidiMessage::NoteOn {
                    addr,
                    channel: 0,
                    note: 16 + idx as u8,
                    velocity: 127 * value as u8,
                }),
                Mackie::Mute(Destination::Master, _) => None,
                Mackie::RecordArm(Destination::Track(idx), value) => Some(MidiMessage::NoteOn {
                    addr,
                    channel: 0,
                    note: idx as u8,
                    velocity: 127 * value as u8,
                }),
                Mackie::RecordArm(Destination::Master, _) => None,
                Mackie::Solo(Destination::Track(idx), value) => Some(MidiMessage::NoteOn {
                    addr,
                    channel: 0,
                    note: 8 + idx as u8,
                    velocity: 127 * value as u8,
                }),
                Mackie::Solo(Destination::Master, _) => None,
                Mackie::Stop(value) => Some(MidiMessage::NoteOn {
                    addr,
                    channel: 0,
                    note: 93,
                    velocity: 127 * value as u8,
                }),
                Mackie::Play(value) => Some(MidiMessage::NoteOn {
                    addr,
                    channel: 0,
                    note: 94,
                    velocity: 127 * value as u8,
                }),
                Mackie::Record(value) => Some(MidiMessage::NoteOn {
                    addr,
                    channel: 0,
                    note: 95,
                    velocity: 127 * value as u8,
                }),
                Mackie::Fader(Destination::Track(idx), value) => Some(MidiMessage::Pitchbend {
                    addr,
                    channel: idx as u8,
                    value,
                }),
                Mackie::Fader(Destination::Master, value) => Some(MidiMessage::Pitchbend {
                    addr,
                    channel: 8,
                    value,
                }),
            },
        }
    }
}
