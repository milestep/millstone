// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

use crate::state::State;

use super::bus::BusState;
use super::submix::Submix;
use super::track::Track;

#[derive(Deserialize, Serialize)]
pub struct Mixer {
    pub master_bus: BusState,

    #[serde(default = "default_submixes")]
    pub submixes: Vec<Submix>,

    #[serde(default = "default_tracks")]
    pub tracks: Vec<Track>,
    //pub selected_submix: isize, TODO broken
}

fn default_submixes() -> Vec<Submix> {
    let mut submixes: Vec<Submix> = Vec::with_capacity(8);
    for idx in 0..8 {
        let submix = Submix::from_idx(idx);
        submixes.push(submix);
    }
    submixes
}

fn default_tracks() -> Vec<Track> {
    let mut tracks: Vec<Track> = Vec::with_capacity(8);
    for idx in 0..8 {
        let track = Track::from_idx(idx);
        tracks.push(track);
    }
    tracks
}

impl State for Mixer {
    fn new() -> Self {
        Self {
            master_bus: BusState::new(),
            submixes: default_submixes(),
            tracks: default_tracks(),
            //selected_submix: 1, TODO broken
        }
    }

    fn from_string(s: String) -> Self {
        match toml::from_str(&s) {
            Ok(state) => {
                let mut state: Self = state;
                for track in state.tracks.iter_mut() {
                    track.filename = format!("audio/track-{:0>2}.wav", track.idx + 1);
                }
                state
            }
            Err(_) => Self::new(),
        }
    }
}
