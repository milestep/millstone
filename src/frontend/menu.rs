// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::depot::Depot;
use crate::frontend::modes::Mode;
use crate::messages::Action;

trait Column {
    fn actions(&self) -> &Vec<Action>;
    fn cursor(&self) -> usize;
    fn next(&mut self);
    fn previous(&mut self);
    fn update(&mut self) {}
    fn selected(&self) -> Action {
        self.actions()[self.cursor()].clone()
    }
}

////////////////////////
/// Base Menu Column ///
////////////////////////

pub struct Base {
    actions: Vec<Action>,
    cursor: usize,
}

impl Base {
    fn new() -> Self {
        Self {
            actions: vec![
                Action::Projects,
                Action::NewProject,
                Action::NewProjectFromCurrent,
                Action::Scripts,
                Action::Trashbin,
                Action::Modes,
                Action::Quit,
            ],
            cursor: 0,
        }
    }
}

impl Column for Base {
    fn actions(&self) -> &Vec<Action> {
        &self.actions
    }

    fn cursor(&self) -> usize {
        self.cursor
    }

    fn next(&mut self) {
        if self.cursor + 1 < self.actions.len() {
            self.cursor += 1;
        };
    }

    fn previous(&mut self) {
        if self.cursor > 0 {
            self.cursor -= 1;
        };
    }
}

////////////////////////////
/// Projects Menu Column ///
////////////////////////////

#[derive(Default)]
pub struct Projects {
    actions: Vec<Action>,
    cursor: usize,
}

impl Projects {
    fn new() -> Self {
        let mut projects = Projects::default();
        projects.update();
        projects.cursor = Depot::read(".depot.toml").get_current_project_index() + 3; // correct for menus

        projects
    }
}

impl Column for Projects {
    fn update(&mut self) {
        self.actions.clear();
        self.actions.push(Action::Cancel);
        self.actions.push(Action::NewProject);
        self.actions.push(Action::NewProjectFromCurrent);
        for name in Depot::read(".depot.toml").get_projects().iter() {
            self.actions.push(Action::SelectProject(name.to_string()));
        }
    }

    fn actions(&self) -> &Vec<Action> {
        &self.actions
    }

    fn cursor(&self) -> usize {
        self.cursor
    }

    fn next(&mut self) {
        if self.cursor + 1 < self.actions.len() {
            self.cursor += 1;
        };
    }

    fn previous(&mut self) {
        if self.cursor > 0 {
            self.cursor -= 1;
        };
    }
}

///////////////////////////
/// Scripts Menu Column ///
///////////////////////////

#[derive(Default)]
pub struct Scripts {
    actions: Vec<Action>,
    cursor: usize,
}

impl Scripts {
    fn new() -> Self {
        let mut scripts = Scripts::default();
        scripts.update();

        scripts
    }
}

impl Column for Scripts {
    fn update(&mut self) {
        self.actions.clear();
        self.actions.push(Action::Cancel);
        let depot = Depot::read(".depot.toml");
        for name in depot.get_scripts().iter() {
            self.actions.push(Action::ExecuteScript(name.to_string()));
        }
    }

    fn actions(&self) -> &Vec<Action> {
        &self.actions
    }

    fn cursor(&self) -> usize {
        self.cursor
    }

    fn next(&mut self) {
        if self.cursor + 1 < self.actions.len() {
            self.cursor += 1;
        };
    }

    fn previous(&mut self) {
        if self.cursor > 0 {
            self.cursor -= 1;
        };
    }
}

/////////////////////////
/// Modes Menu Column ///
/////////////////////////

#[derive(Default)]
pub struct Modes {
    actions: Vec<Action>,
    cursor: usize,
}

impl Modes {
    fn new() -> Self {
        Modes {
            actions: vec![
                Action::Cancel,
                Action::OpenMode(Mode::Tape),
                Action::OpenMode(Mode::Performance),
                Action::OpenMode(Mode::Menu),
                Action::OpenMode(Mode::Logger),
            ],
            ..Modes::default()
        }
    }
}

impl Column for Modes {
    fn actions(&self) -> &Vec<Action> {
        &self.actions
    }

    fn cursor(&self) -> usize {
        self.cursor
    }

    fn next(&mut self) {
        if self.cursor + 1 < self.actions.len() {
            self.cursor += 1;
        };
    }

    fn previous(&mut self) {
        if self.cursor > 0 {
            self.cursor -= 1;
        };
    }
}

/////////////////////////
/// Trash Menu Column ///
/////////////////////////

#[derive(Default)]
pub struct Trash {
    actions: Vec<Action>,
    cursor: usize,
}

impl Trash {
    fn new() -> Self {
        let mut trashed_projects = Trash::default();
        trashed_projects.update();

        trashed_projects
    }
}

impl Column for Trash {
    fn update(&mut self) {
        self.actions.clear();
        self.actions.push(Action::Cancel);
        let trashed = Depot::read(".depot.toml").get_trashed_projects();
        for name in trashed.iter() {
            self.actions.push(Action::RestoreProject(name.to_string()));
        }
        self.cursor = 0;
    }

    fn actions(&self) -> &Vec<Action> {
        &self.actions
    }

    fn cursor(&self) -> usize {
        self.cursor
    }

    fn next(&mut self) {
        if self.cursor + 1 < self.actions.len() {
            self.cursor += 1;
        };
    }

    fn previous(&mut self) {
        if self.cursor > 0 {
            self.cursor -= 1;
        };
    }
}

///////////////////////////
/// Project Menu Column ///
///////////////////////////

pub struct Project {
    actions: Vec<Action>,
    cursor: usize,
}

impl Project {
    fn new(project: &str) -> Self {
        let actions = vec![
            Action::Cancel,
            Action::OpenProject(project.to_string()),
            Action::NewProjectFromThis(project.to_string()),
            Action::CopyProject(project.to_string()),
            Action::Rename(project.to_string()),
            Action::TrashProject(project.to_string()),
        ];
        Self { actions, cursor: 0 }
    }
}

impl Column for Project {
    fn actions(&self) -> &Vec<Action> {
        &self.actions
    }

    fn cursor(&self) -> usize {
        self.cursor
    }

    fn next(&mut self) {
        if self.cursor + 1 < self.actions.len() {
            self.cursor += 1;
        };
    }

    fn previous(&mut self) {
        if self.cursor > 0 {
            self.cursor -= 1;
        };
    }
}

//////////////////////////
/// Rename Menu Column ///
//////////////////////////

pub struct Rename {
    actions: Vec<Action>,
    cursor: usize,
}

impl Rename {
    fn new(source: &str) -> Self {
        let mut actions = vec![
            Action::Cancel,
            Action::RenameProject(source.to_string()),
            Action::Backspace,
        ];
        let alphabet: Vec<char> = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789".chars().collect();
        for letter in alphabet.iter() {
            actions.push(Action::Letter(letter.to_string()))
        }
        Self { actions, cursor: 0 }
    }
}

impl Column for Rename {
    fn actions(&self) -> &Vec<Action> {
        &self.actions
    }

    fn cursor(&self) -> usize {
        self.cursor
    }

    fn next(&mut self) {
        if self.cursor + 1 < self.actions.len() {
            self.cursor += 1;
        };
    }

    fn previous(&mut self) {
        if self.cursor > 0 {
            self.cursor -= 1;
        };
    }
}

//////////////////////
/////// Menu /////////
//////////////////////

pub struct Menu {
    columns: Vec<Box<dyn Column>>,
    depth: usize,
    register: String,
}

impl Menu {
    pub fn new() -> Self {
        Self {
            columns: vec![Box::new(Base::new())],
            depth: 0,
            register: "".to_string(),
        }
    }

    pub fn len(&self) -> usize {
        self.columns.len()
    }

    pub fn next(&mut self) {
        self.columns[self.depth].next();
    }

    pub fn previous(&mut self) {
        self.columns[self.depth].previous();
    }

    pub fn selected_action(&self) -> Action {
        self.columns[self.depth].selected()
    }

    pub fn actions(&self, depth: usize) -> &Vec<Action> {
        self.columns[depth].actions()
    }

    pub fn pretty_actions(&self, depth: usize) -> Vec<String> {
        let mut strings = vec![];
        for item in self.actions(depth).iter() {
            match item {
                Action::NewProject => strings.push("New project".to_string()),
                Action::NewProjectFromCurrent => {
                    strings.push("New project from current".to_string())
                }
                Action::NewProjectFromThis(_name) => strings.push("New from this".to_string()),
                Action::CopyProject(_name) => strings.push("Copy".to_string()),
                Action::OpenProject(_name) => strings.push("Open".to_string()),
                Action::TrashProject(_name) => strings.push("Trash".to_string()),
                Action::RenameProject(_name) => strings.push("Save".to_string()),
                Action::Rename(_name) => strings.push("Rename".to_string()),
                Action::SelectProject(name) => strings.push(format!("Select {}", name)),
                Action::ExecuteScript(name) => strings.push(format!("Execute {}", name)),
                Action::RestoreProject(name) => strings.push(format!("Restore {}", name)),
                Action::Letter(name) => strings.push(name.to_string()),
                Action::OpenMode(mode) => strings.push(format!("Open mode {:?}", mode)),
                _ => strings.push(item.to_string()),
            }
        }
        strings
    }

    pub fn cursor(&self, depth: usize) -> usize {
        self.columns[depth].cursor()
    }

    pub fn pop(&mut self) {
        self.register = "".to_string();
        if self.len() > 1 {
            self.depth -= 1;
            self.columns.pop();
        }
    }

    pub fn update(&mut self) {
        for column in self.columns.iter_mut() {
            column.update()
        }
    }

    pub fn get_register(&mut self) -> String {
        self.register.to_string()
    }

    pub fn backspace(&mut self) {
        if !self.register.is_empty() {
            self.register = self.register[0..self.register.len() - 1].to_string();
        }
    }

    pub fn type_letter(&mut self, letter: String) {
        if self.register.len() < 28 {
            // max length
            self.register += &letter
        }
    }
}

// List attachments
impl Menu {
    pub fn attach_project(&mut self, project_name: &str) {
        self.depth += 1;
        self.columns.push(Box::new(Project::new(project_name)));
    }

    pub fn attach_projects(&mut self) {
        self.depth += 1;
        self.columns.push(Box::new(Projects::new()));
    }

    pub fn attach_scripts(&mut self) {
        self.depth += 1;
        self.columns.push(Box::new(Scripts::new()));
    }

    pub fn attach_trash(&mut self) {
        self.depth += 1;
        self.columns.push(Box::new(Trash::new()));
    }

    pub fn attach_rename(&mut self, register: &str) {
        self.depth += 1;
        self.register = register.to_string();
        self.columns.push(Box::new(Rename::new(register)));
    }

    pub fn attach_modes(&mut self) {
        self.depth += 1;
        self.columns.push(Box::new(Modes::new()));
    }
}
