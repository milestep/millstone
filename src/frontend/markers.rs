// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};
use std::cmp::Ordering;

use crate::state::State;

//////////////////////////////////////////////////
// Markers
//////////////////////////////////////////////////

#[derive(Debug, Eq, Serialize, Deserialize)]
pub struct Marker {
    pub label: String,
    pub pos: usize,
}

impl Ord for Marker {
    fn cmp(&self, other: &Self) -> Ordering {
        self.pos.cmp(&other.pos)
    }
}

impl PartialOrd for Marker {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Marker {
    fn eq(&self, other: &Self) -> bool {
        self.pos == other.pos
    }
}

impl Marker {
    pub fn new(label: String, pos: usize) -> Marker {
        Marker { label, pos }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Markers {
    pub start: usize,
    pub stop: usize,
    pub loop_start: usize,
    pub loop_stop: usize,
    pub markers: Vec<Marker>,
}

impl Markers {
    pub fn toggle_marker(&mut self, pos: usize) {
        match self.markers.iter().any(|x| x.pos == pos) {
            true => self.markers.retain(|x| x.pos != pos),
            false => self.markers.push(Marker::new(String::from("M"), pos)),
        }
        // TODO: Lazy here, we could implement sort only at insertion
        self.markers.sort();
        self.write("markers.toml");
    }

    pub fn add_marker(&mut self, label: String, pos: usize) {
        if self.markers.iter().any(|x| x.pos == pos) {
            self.markers.retain(|x| x.pos != pos);
        }
        self.markers.push(Marker::new(label, pos));
        // TODO: Lazy here, we could implement sort only at insertion
        self.markers.sort();
        self.write("markers.toml");
    }

    pub fn relabel_marker(&mut self, pos: usize) {
        // Relable the marker under the play head
        let labels = ["M", "I", "A", "B", "C", "D", "O"];
        // Find marker index (if any)
        let marker_idx = match self.markers.iter().position(|x| x.pos == pos) {
            Some(x) => x,
            None => return,
        };
        // Find current label and index (if any)
        let label = &self.markers[marker_idx].label;
        let label_idx = match labels.iter().position(|&x| x == label) {
            Some(x) => x,
            None => return,
        };
        // Set new label to next in line (if any, else first element)
        let new_label = match labels.get(label_idx + 1) {
            Some(x) => x,
            None => labels[0],
        };
        self.markers[marker_idx].label = String::from(new_label);
        self.write("markers.toml");
    }

    pub fn next(&self, pos: usize) -> usize {
        // Return the next marker position on time line
        let best_marker = self.markers.iter().find(|&x| x.pos > pos);
        let mut others = [self.stop, self.start, self.loop_start, self.loop_stop];
        others.sort_unstable();
        match best_marker {
            Some(marker) => match others.iter().find(|&x| x > &pos && x < &marker.pos) {
                Some(best_other) => *best_other,
                None => marker.pos,
            },
            None => match others.iter().find(|&x| x > &pos) {
                Some(best_other) => *best_other,
                None => pos,
            },
        }
    }

    pub fn previous(&self, pos: usize) -> usize {
        // Return the previous marker position on time line
        let best_marker = self.markers.iter().rev().find(|&x| x.pos < pos);
        let mut others = [self.stop, self.start, self.loop_start, self.loop_stop];
        others.sort_unstable();
        match best_marker {
            Some(marker) => match others.iter().rev().find(|&x| x < &pos && x > &marker.pos) {
                Some(best_other) => *best_other,
                None => marker.pos,
            },
            None => match others.iter().rev().find(|&x| x < &pos) {
                Some(best_other) => *best_other,
                None => pos,
            },
        }
    }
}

// Persistant read and write functions
impl State for Markers {
    fn new() -> Self {
        Self {
            start: 0,
            stop: 0,
            loop_start: 0,
            loop_stop: 0,
            markers: vec![Marker::new(String::from("0"), 0)],
        }
    }

    fn from_string(s: String) -> Self {
        match toml::from_str(&s) {
            Ok(state) => state,
            Err(_) => Self::new(),
        }
    }
}
