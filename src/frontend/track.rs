// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

use super::loudness_map::LoudnessMap;

#[derive(Serialize, Deserialize)]
pub struct Track {
    pub idx: usize,
    pub name: String,
    pub filename: String,
    pub monitor: bool,
    pub record_arm: bool,
    pub mute: bool,
    pub solo: bool,
    pub fader: f32,
    pub pan: f32,

    #[serde(default = "default_reverb_send")]
    pub reverb_send: f32,

    pub physical_left: usize,
    pub physical_right: usize,

    #[serde(skip)]
    pub length: usize,

    #[serde(skip)]
    pub loudness_map: LoudnessMap,
}

fn default_reverb_send() -> f32 {
    0.0
}

impl Track {
    pub fn from_idx(idx: usize) -> Self {
        let left = if cfg!(feature = "ballet") && idx > 3 {
            256 + 2 * idx % 8
        } else {
            2 * idx % 8
        };
        let right = if cfg!(feature = "ballet") && idx > 3 {
            256 + (2 * idx + 1) % 8
        } else {
            (2 * idx + 1) % 8
        };

        Self {
            idx,
            name: format!("{}", idx + 1),
            filename: format!("audio/track-{:0>2}.wav", idx + 1),
            monitor: false,
            record_arm: false,
            mute: false,
            solo: false,
            fader: 0.0,
            pan: 0.0,
            reverb_send: 0.0,
            physical_left: left,
            physical_right: right,
            length: 0,
            loudness_map: LoudnessMap::new(44100),
        }
    }
}
