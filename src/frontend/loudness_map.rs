// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

pub struct LoudnessMap {
    buffer: Vec<f32>,
    pub frames_per_window: usize,
}

impl LoudnessMap {
    pub fn new(frames_per_window: usize) -> Self {
        LoudnessMap {
            buffer: Vec::with_capacity(60 * 60),
            frames_per_window,
        }
    }

    pub fn insert(&mut self, t: usize, value: f32) {
        let idx = t / self.frames_per_window;

        while idx > self.buffer.len() {
            self.buffer.push(0.0);
        }

        if idx < self.buffer.len() {
            self.buffer[idx] = value;
        } else {
            self.buffer.push(value);
        }
    }

    pub fn as_levels(&self, levels: usize, start: isize, length: usize) -> Vec<u64> {
        let mut data = Vec::with_capacity(length);

        for i in 0..length {
            let val = match start + i as isize {
                idx if idx < 0 || idx >= self.buffer.len() as isize => 0,
                idx => (self.buffer[idx as usize] * levels as f32).ceil() as u64,
            };
            data.push(val);
        }

        data
    }
}

impl Default for LoudnessMap {
    fn default() -> Self {
        Self::new(44100)
    }
}
