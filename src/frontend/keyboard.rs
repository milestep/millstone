// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crossbeam::channel::Sender;

use std::collections::HashMap;
use std::{error, io, thread};

use termion::{event::Key, input::TermRead};

use crate::messages::Action;
use crate::perf;

pub struct KeyboardMap {
    map: Vec<(Action, String)>,
}

impl KeyboardMap {
    fn new() -> KeyboardMap {
        let map = vec![
            (Action::Arm, "r".to_string()),
            (Action::CycleInputSourceLeft, "c".to_string()),
            (Action::CycleInputSourceRight, "C".to_string()),
            (Action::Down, "Down".to_string()),
            (Action::Down, "j".to_string()),
            (Action::Export, "X".to_string()),
            (Action::JumpToEnd, "]".to_string()),
            (Action::JumpToStart, "[".to_string()),
            (Action::JumpToZero, "0".to_string()),
            (Action::Left, "Left".to_string()),
            (Action::Loop, "o".to_string()),
            (Action::MasterDown, "-".to_string()),
            (Action::MasterUp, "+".to_string()),
            (Action::Monitor, "i".to_string()),
            (Action::Mute, "m".to_string()),
            (Action::NextMarker, "n".to_string()),
            (Action::NextMode, "PageDown".to_string()),
            (Action::NextSubmix, "J".to_string()),
            #[cfg(feature = "master_effects")]
            (Action::NumPress(1), "1".to_string()),
            #[cfg(feature = "master_effects")]
            (Action::NumPress(2), "2".to_string()),
            #[cfg(feature = "master_effects")]
            (Action::NumPress(3), "3".to_string()),
            #[cfg(feature = "master_effects")]
            (Action::NumPress(4), "4".to_string()),
            #[cfg(feature = "master_effects")]
            (Action::NumPress(5), "5".to_string()),
            #[cfg(feature = "master_effects")]
            (Action::NumPress(6), "6".to_string()),
            #[cfg(feature = "master_effects")]
            (Action::NumPress(7), "7".to_string()),
            #[cfg(feature = "master_effects")]
            (Action::NumPress(8), "8".to_string()),
            (Action::PanLeft, "<".to_string()),
            (Action::PanRight, ">".to_string()),
            (Action::PlayPause, " ".to_string()),
            (Action::PreviousMarker, "p".to_string()),
            (Action::PreviousMode, "PageUp".to_string()),
            (Action::PreviousSubmix, "K".to_string()),
            (Action::Record, "R".to_string()),
            (Action::RelabelMarker, "T".to_string()),
            (Action::Right, "Right".to_string()),
            (Action::SetEnd, "}".to_string()),
            (Action::SetLoopStart, "(".to_string()),
            (Action::SetLoopStop, ")".to_string()),
            (Action::SetStart, "{".to_string()),
            (Action::Solo, "s".to_string()),
            (Action::ToggleHeight, "=".to_string()),
            (Action::ToggleLogger, "l".to_string()),
            (Action::ToggleMainMode, "Tab".to_string()),
            (Action::ToggleMarker, "Enter".to_string()),
            (Action::ToggleMarker, "t".to_string()),
            (Action::ToggleProjectMode, "Esc".to_string()),
            (Action::ToggleTrackFocus, "v".to_string()),
            (Action::Up, "Up".to_string()),
            (Action::Up, "k".to_string()),
            (Action::VolDown, ",".to_string()),
            (Action::VolUp, ".".to_string()),
            (Action::Quit, "q".to_string()),
        ];
        KeyboardMap { map }
    }
}

fn convert_keymap(rawmap: KeyboardMap) -> HashMap<Key, Action> {
    let mut map = HashMap::new();
    for (action, string) in rawmap.map.iter() {
        let key = match string.as_str() {
            "Up" => Key::Up,
            "Down" => Key::Down,
            "Left" => Key::Left,
            "Right" => Key::Right,
            "Enter" => Key::Char('\n'),
            "Tab" => Key::Char('\t'),
            "PageUp" => Key::PageUp,
            "PageDown" => Key::PageDown,
            "Esc" => Key::Esc,
            _ => Key::Char(string.chars().next().unwrap()),
        };
        map.insert(key, action.clone());
    }
    map
}

pub fn spawn(action_tx: Sender<Action>) {
    let rawmap = KeyboardMap::new();
    let map = convert_keymap(rawmap);
    thread::spawn(move || {
        perf::set_normal_thread_min_priority();
        let mut keyboard = Keyboard::new(action_tx, map);
        keyboard.run().unwrap();
    });
}

pub struct Keyboard {
    action_tx: Sender<Action>,
    map: HashMap<Key, Action>,
}

impl Keyboard {
    pub fn new(action_tx: Sender<Action>, map: HashMap<Key, Action>) -> Keyboard {
        Keyboard { action_tx, map }
    }
    pub fn run(&mut self) -> Result<(), Box<dyn error::Error>> {
        let stdin = io::stdin();
        for key in stdin.keys().flatten() {
            if let Some(action) = self.map.get(&key) {
                self.action_tx.send(action.clone()).unwrap();
            }
        }
        Ok(())
    }
}
