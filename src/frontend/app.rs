// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::paths;
use crossbeam::channel::Sender;

use serde::{Deserialize, Serialize};
use std::env;
use std::io::{BufRead, BufReader};
use std::process::{Command, Stdio};
use std::sync::Arc;

use parking_lot::RwLock;
use tui::{
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    terminal::Frame,
    text::{Span, Spans, Text},
    widgets::{Block, List, ListItem, ListState, Paragraph, Sparkline},
};

use crate::depot::Depot;
use crate::global_settings::GlobalSettings;
use crate::logger::Logger;
use crate::messages::{Action, Destination, Mackie, Request};
use crate::perf;
use crate::state::State;

use super::colors;
use super::colors::{
    nf, vu_pink, ACTIVE, ACTIVE_BOUND, BACK, CRIT, INACTIVE, LOOP, PLAY, RECORD, SELECT, STOP, WAVE,
};
use super::layout;
use super::markers::Markers;
use super::menu::Menu;
use super::midi;
use super::mixer::Mixer;
use super::modes::Mode;
use super::project::ProjectState;

#[derive(Debug, Serialize, Deserialize)]
pub struct AppState {
    pub t: usize,
    pub anchor_track: usize,
    pub focused_submix: usize,
    pub focused_track: usize,
    pub focused_effect: usize,
    pub num_visible_tracks: usize,

    #[serde(default = "default_loop_cycle_state")]
    pub loop_cycle_state: u8,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Workload {
    pub percentage: i128,
    pub max: i128,
}

impl Workload {
    pub fn new() -> Self {
        Self {
            percentage: 0,
            max: 0,
        }
    }
}

fn default_loop_cycle_state() -> u8 {
    0
}

impl State for AppState {
    fn new() -> AppState {
        AppState {
            t: 0,
            anchor_track: 0,
            focused_submix: 0,
            focused_track: 0,
            focused_effect: 0,
            num_visible_tracks: 4,
            loop_cycle_state: 0,
        }
    }

    fn from_string(s: String) -> Self {
        match toml::from_str(&s) {
            Ok(state) => state,
            Err(_) => Self::new(),
        }
    }
}

pub struct App {
    pub global_settings: Arc<RwLock<GlobalSettings>>,
    pub request_tx: Sender<Request>,
    pub midi_tx: Sender<Mackie>,
    pub depot: Depot,
    pub state: AppState,
    pub mixer: Mixer,
    pub project: ProjectState,
    pub marker_state: Markers,
    pub frames: usize,
    pub disable_track_focus: bool,
    pub analyzer_state: bool,
    pub play: bool,
    pub record: bool,
    pub export: bool,
    pub looping: bool,
    pub autoreturn: bool,
    pub compressor_gain: f32,
    pub master_peak_left: f32,
    pub master_peak_right: f32,
    pub master_rms_left: f32,
    pub master_rms_right: f32,
    pub master_spectrum: Vec<f32>,
    pub master_spectrum_min_freq: f32,
    pub track_peak_left: Vec<f32>,
    pub track_peak_right: Vec<f32>,
    pub track_rms_left: Vec<f32>,
    pub track_rms_right: Vec<f32>,
    pub preload_status: Vec<Option<f32>>,
    pub logger: Arc<RwLock<Logger>>,
    pub mode: Mode,
    pub previous_mode: Mode,
    pub project_name: String,
    pub workload: [Workload; 3],
    pub menu: Menu,
}

impl App {
    pub fn new(
        global_settings: &Arc<RwLock<GlobalSettings>>,
        logger: &Arc<RwLock<Logger>>,
        request_tx: &Sender<Request>,
        midi_tx: &Sender<Mackie>,
        depot: Depot,
        menu: Menu,
    ) -> Self {
        let project_name = depot.get_current_project();

        let app = Self {
            global_settings: Arc::clone(global_settings),
            request_tx: request_tx.clone(),
            midi_tx: midi_tx.clone(),
            depot,
            state: AppState::read("app.toml"),
            mixer: State::read("mixer.toml"),
            project: State::read("project.toml"),
            marker_state: Markers::read("markers.toml"),
            frames: 0,
            disable_track_focus: global_settings.read().disable_track_focus,
            analyzer_state: false,
            play: false,
            record: false,
            export: false,
            looping: false,
            autoreturn: false,
            compressor_gain: 0.0,
            master_peak_left: 0.0,
            master_peak_right: 0.0,
            master_rms_left: 0.0,
            master_rms_right: 0.0,
            master_spectrum: vec![0.0f32; 512],
            master_spectrum_min_freq: 0.0,
            track_peak_left: vec![0.0; layout::MAX_TRACKS],
            track_peak_right: vec![0.0; layout::MAX_TRACKS],
            track_rms_left: vec![0.0; layout::MAX_TRACKS],
            track_rms_right: vec![0.0; layout::MAX_TRACKS],
            preload_status: vec![None; layout::MAX_TRACKS],
            logger: Arc::clone(logger),
            mode: Mode::Tape,
            previous_mode: Mode::Tape,
            project_name,
            workload: [Workload::new(), Workload::new(), Workload::new()],
            menu,
        };

        app.send_mixer_state();
        app.send_mackie_state();

        app
    }

    pub fn send_mixer_state(&self) {
        self.request_tx
            .send(Request::SetUpdateInterval(8 * 441))
            .unwrap();

        self.request_tx
            .send(Request::AbsoluteTime(self.state.t))
            .unwrap();

        self.request_tx
            .send(Request::SetLoopStart(self.marker_state.loop_start))
            .unwrap();

        self.request_tx
            .send(Request::SetLoopStop(self.marker_state.loop_stop))
            .unwrap();

        self.request_tx
            .send(Request::Mute(
                Destination::Master,
                self.mixer.master_bus.mute,
            ))
            .unwrap();
        self.request_tx
            .send(Request::Solo(
                Destination::Master,
                self.mixer.master_bus.solo,
            ))
            .unwrap();
        self.request_tx
            .send(Request::Fader(
                Destination::Master,
                self.mixer.master_bus.fader,
            ))
            .unwrap();
        self.request_tx
            .send(Request::Pan(Destination::Master, self.mixer.master_bus.pan))
            .unwrap();

        #[cfg(feature = "master_effects")]
        self.request_tx
            .send(Request::MasterEffect(
                self.mixer.master_bus.get_effect_state(),
            ))
            .unwrap();

        for submix in self.mixer.submixes.iter() {
            self.request_tx
                .send(Request::SubmixFader(submix.idx, submix.fader))
                .unwrap();

            for idx in 0..8 {
                self.request_tx
                    .send(Request::SubmixGain(
                        Destination::Track(idx),
                        submix.idx,
                        submix.track_gains[idx],
                    ))
                    .unwrap();
                self.request_tx
                    .send(Request::SubmixPan(
                        Destination::Track(idx),
                        submix.idx,
                        submix.track_pans[idx],
                    ))
                    .unwrap();
                self.request_tx
                    .send(Request::SubmixMute(
                        Destination::Track(idx),
                        submix.idx,
                        submix.track_mutes[idx],
                    ))
                    .unwrap();
                self.request_tx
                    .send(Request::SubmixSolo(
                        Destination::Track(idx),
                        submix.idx,
                        submix.track_solos[idx],
                    ))
                    .unwrap();
            }

            self.request_tx
                .send(Request::SubmixGain(
                    Destination::Master,
                    submix.idx,
                    submix.master_gain,
                ))
                .unwrap();
            self.request_tx
                .send(Request::SubmixPan(
                    Destination::Master,
                    submix.idx,
                    submix.master_pan,
                ))
                .unwrap();
        }

        for track in self.mixer.tracks.iter() {
            self.request_tx
                .send(Request::Load(
                    Destination::Track(track.idx),
                    String::from(&track.filename),
                ))
                .unwrap();
            self.request_tx
                .send(Request::Monitor(
                    Destination::Track(track.idx),
                    track.monitor,
                ))
                .unwrap();
            self.request_tx
                .send(Request::Mute(Destination::Track(track.idx), track.mute))
                .unwrap();
            self.request_tx
                .send(Request::Solo(Destination::Track(track.idx), track.solo))
                .unwrap();
            self.request_tx
                .send(Request::RecordArm(
                    Destination::Track(track.idx),
                    track.record_arm,
                ))
                .unwrap();
            self.request_tx
                .send(Request::Fader(Destination::Track(track.idx), track.fader))
                .unwrap();
            self.request_tx
                .send(Request::Pan(Destination::Track(track.idx), track.pan))
                .unwrap();
            self.request_tx
                .send(Request::ReverbSend(
                    Destination::Track(track.idx),
                    track.reverb_send,
                ))
                .unwrap();
            self.request_tx
                .send(Request::InputSourceLeft(
                    Destination::Track(track.idx),
                    track.physical_left,
                ))
                .unwrap();
            self.request_tx
                .send(Request::InputSourceRight(
                    Destination::Track(track.idx),
                    track.physical_right,
                ))
                .unwrap();
        }
    }

    pub fn send_mackie_state(&self) {
        self.midi_tx.send(Mackie::Play(self.play)).unwrap();
        self.midi_tx.send(Mackie::Stop(!self.play)).unwrap();
        self.midi_tx.send(Mackie::Record(self.record)).unwrap();
        self.midi_tx
            .send(Mackie::Fader(
                Destination::Master,
                midi::fader_position_to_value(self.mixer.master_bus.fader),
            ))
            .unwrap();

        if self.state.focused_submix == 0 {
            for track in self.mixer.tracks.iter() {
                self.midi_tx
                    .send(Mackie::Mute(Destination::Track(track.idx), track.mute))
                    .unwrap();
                self.midi_tx
                    .send(Mackie::Solo(Destination::Track(track.idx), track.solo))
                    .unwrap();
            }
        } else {
            for (idx, mute) in self.mixer.submixes[self.state.focused_submix - 1]
                .track_mutes
                .iter()
                .enumerate()
            {
                self.midi_tx
                    .send(Mackie::Mute(Destination::Track(idx), *mute))
                    .unwrap();
            }

            for (idx, solo) in self.mixer.submixes[self.state.focused_submix - 1]
                .track_solos
                .iter()
                .enumerate()
            {
                self.midi_tx
                    .send(Mackie::Solo(Destination::Track(idx), *solo))
                    .unwrap();
            }
        }

        for track in self.mixer.tracks.iter() {
            self.midi_tx
                .send(Mackie::RecordArm(
                    Destination::Track(track.idx),
                    track.record_arm,
                ))
                .unwrap();
            self.midi_tx
                .send(Mackie::Fader(
                    Destination::Track(track.idx),
                    midi::fader_position_to_value(track.fader),
                ))
                .unwrap();
        }
    }

    pub fn set_mode(&mut self, mode: Mode) {
        // Remember previous main mode before swithing
        match self.mode {
            Mode::Menu | Mode::Logger => (),
            Mode::Performance => {
                self.request_tx
                    .send(Request::SetAnalyzerState(false))
                    .unwrap();

                self.request_tx
                    .send(Request::SetUpdateInterval(8 * 441))
                    .unwrap();

                self.previous_mode = self.mode;
            }
            _ => self.previous_mode = self.mode,
        }

        // Pre-events before switching mode
        match mode {
            Mode::Menu => self.menu.update(),
            Mode::Performance => {
                self.request_tx
                    .send(Request::SetAnalyzerState(self.analyzer_state))
                    .unwrap();

                if !self.analyzer_state {
                    self.request_tx
                        .send(Request::SetUpdateInterval(44100))
                        .unwrap();
                }
            }
            _ => (),
        }

        // Switch mode
        self.mode = mode;
    }

    pub fn toggle_main_mode(&mut self) {
        match self.mode {
            Mode::Tape => {
                self.set_mode(Mode::Performance);
            }
            _ => {
                self.set_mode(Mode::Tape);
            }
        }
    }

    pub fn toggle_menu_mode(&mut self) {
        match self.mode {
            Mode::Menu => self.set_mode(self.previous_mode),
            _ => {
                self.set_mode(Mode::Menu);
            }
        }
    }

    pub fn toggle_logger_mode(&mut self) {
        match self.mode {
            Mode::Logger => self.set_mode(self.previous_mode),
            _ => {
                self.set_mode(Mode::Logger);
            }
        }
    }

    pub fn toggle_analyzer_state(&mut self) {
        self.analyzer_state = !self.analyzer_state;
        self.request_tx
            .send(Request::SetAnalyzerState(self.analyzer_state))
            .unwrap();
        if self.analyzer_state {
            self.request_tx
                .send(Request::SetUpdateInterval(8 * 441))
                .unwrap();
        } else {
            self.request_tx
                .send(Request::SetUpdateInterval(44100))
                .unwrap();
        }
    }

    pub fn save_states(&mut self) {
        self.logger
            .write()
            .log("Saving mixer, app, project and markers");
        self.mixer.write("mixer.toml");
        self.state.write("app.toml");
        self.project.write("project.toml");
        self.marker_state.write("markers.toml");
    }

    pub fn open_project(&mut self, project_name: String) {
        self.logger
            .write()
            .log(&format!("Opening project {}", project_name));
        self.depot.set_current_project(project_name); // switch project
        self.menu.update();
        self.logger.write().log("Saving depot state");
        self.depot.write(".depot.toml");
        env::set_var(
            "_MILLSTONE_PROJECT_ROOT",
            paths::depot_path(&self.depot.get_current_project()),
        );
    }

    pub fn trash_project(&mut self, project_name: &str) {
        self.logger
            .write()
            .log(&format!("Trashing project {}", project_name));
        self.depot.trash_project(project_name);
        self.depot.write(".depot.toml");
        self.menu.update();
        self.menu.pop();
        self.menu.previous();
    }

    pub fn restore_project(&mut self, project_name: &str) {
        self.logger
            .write()
            .log(&format!("Restoring project {}", project_name));
        self.depot.restore_project(project_name);
        self.depot.write(".depot.toml");
        self.menu.update();
        self.menu.previous();
    }

    pub fn execute_script(&self, script_name: &str, action_tx: Sender<Action>) {
        let script_path = paths::join_paths(&paths::depot_path(".scripts"), script_name);

        action_tx
            .send(Action::Log(format!("[script] ./{}", &script_name)))
            .unwrap();

        let process = Command::new(script_path)
            .envs(std::env::vars())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn();

        match process {
            Err(e) => action_tx
                .send(Action::Log(format!("[script] {:?}", e)))
                .unwrap(),
            Ok(process) => {
                match process.stdout {
                    None => action_tx
                        .send(Action::Log("[script] Could not capture stdout".to_string()))
                        .unwrap(),
                    Some(stdout) => {
                        let action_tx = action_tx.clone();
                        std::thread::spawn(move || {
                            perf::set_normal_thread_min_priority();

                            BufReader::new(stdout)
                                .lines()
                                .map_while(Result::ok)
                                .for_each(|line| action_tx.send(Action::Log(line)).unwrap())
                        });
                    }
                }

                match process.stderr {
                    None => action_tx
                        .send(Action::Log("Could not capture stderr".to_string()))
                        .unwrap(),
                    Some(stderr) => {
                        std::thread::spawn(move || {
                            perf::set_normal_thread_min_priority();

                            BufReader::new(stderr)
                                .lines()
                                .map_while(Result::ok)
                                .for_each(|line| action_tx.send(Action::Log(line)).unwrap())
                        });
                    }
                }
            }
        }
    }

    pub fn set_focused_submix(&mut self, value: usize) {
        let new_value = value.max(0).min(self.mixer.submixes.len());
        let submix_changed = self.state.focused_submix == new_value;
        self.state.focused_submix = new_value;

        if submix_changed {
            if self.state.focused_submix == 0 {
                for track in self.mixer.tracks.iter() {
                    self.midi_tx
                        .send(Mackie::Mute(Destination::Track(track.idx), track.mute))
                        .unwrap();
                    self.midi_tx
                        .send(Mackie::Solo(Destination::Track(track.idx), track.solo))
                        .unwrap();
                }
            } else {
                for (idx, mute) in self.mixer.submixes[self.state.focused_submix - 1]
                    .track_mutes
                    .iter()
                    .enumerate()
                {
                    self.midi_tx
                        .send(Mackie::Mute(Destination::Track(idx), *mute))
                        .unwrap();
                }

                for (idx, solo) in self.mixer.submixes[self.state.focused_submix - 1]
                    .track_solos
                    .iter()
                    .enumerate()
                {
                    self.midi_tx
                        .send(Mackie::Solo(Destination::Track(idx), *solo))
                        .unwrap();
                }
            }
        }
    }

    pub fn set_focused_track(&mut self, value: usize) {
        self.state.focused_track = value.max(0).min(self.mixer.tracks.len() - 1);

        while self.state.focused_track >= self.state.anchor_track + self.state.num_visible_tracks {
            self.state.anchor_track += 1;
        }

        while self.state.focused_track < self.state.anchor_track {
            self.state.anchor_track -= 1;
        }
    }

    pub fn set_num_visible_tracks(&mut self, value: usize) {
        self.state.num_visible_tracks = value.max(1).min(self.mixer.tracks.len());

        while self.state.anchor_track + self.state.num_visible_tracks > self.mixer.tracks.len() {
            self.state.anchor_track -= 1;
        }

        // Ensure that the focused track is still visible.
        self.set_focused_track(self.state.focused_track);
    }

    pub fn set_gain(&mut self, dest: Destination, value: f32) {
        let submix = self.state.focused_submix;

        let gain = if value < -90.0 {
            -f32::INFINITY
        } else {
            value.min(12.0)
        };

        match dest {
            Destination::Track(idx) => {
                if submix == 0 {
                    self.mixer.tracks[idx].fader = gain;
                } else {
                    self.mixer.submixes[submix - 1].track_gains[idx] = gain;
                }
            }
            Destination::Master => {
                if submix == 0 {
                    self.mixer.master_bus.fader = gain;
                } else {
                    self.mixer.submixes[submix - 1].master_gain = gain;
                }
            }
        };

        if submix == 0 {
            self.request_tx.send(Request::Fader(dest, gain)).unwrap();
            self.midi_tx
                .send(Mackie::Fader(dest, midi::fader_position_to_value(gain)))
                .unwrap();
        } else {
            self.request_tx
                .send(Request::SubmixGain(dest, submix - 1, gain))
                .unwrap();
        }
    }

    pub fn set_pan(&mut self, dest: Destination, value: f32) {
        let submix = self.state.focused_submix;

        let pan = value.min(1.0).max(-1.0);

        match dest {
            Destination::Track(idx) => {
                if submix == 0 {
                    self.mixer.tracks[idx].pan = pan;
                } else {
                    self.mixer.submixes[submix - 1].track_pans[idx] = pan;
                }
            }
            Destination::Master => {
                if submix == 0 {
                    self.mixer.master_bus.pan = pan;
                } else {
                    self.mixer.submixes[submix - 1].master_pan = pan;
                }
            }
        };

        if submix == 0 {
            self.request_tx.send(Request::Pan(dest, pan)).unwrap();
        } else {
            self.request_tx
                .send(Request::SubmixPan(dest, submix - 1, pan))
                .unwrap();
        }
    }

    pub fn set_reverb_send(&mut self, dest: Destination, value: f32) {
        let reverb_send = value.min(1.0).max(0.0);

        match dest {
            Destination::Track(idx) => {
                self.mixer.tracks[idx].reverb_send = reverb_send;
            }
            Destination::Master => {}
        };

        self.request_tx
            .send(Request::ReverbSend(dest, reverb_send))
            .unwrap();
    }

    pub fn set_solo(&mut self, dest: Destination, value: bool) {
        let submix = self.state.focused_submix;

        if let Destination::Track(idx) = dest {
            if submix == 0 {
                self.mixer.tracks[idx].solo = value;
            } else {
                self.mixer.submixes[submix - 1].track_solos[idx] = value;
            }
        }

        if submix == 0 {
            self.request_tx.send(Request::Solo(dest, value)).unwrap();
        } else {
            self.request_tx
                .send(Request::SubmixSolo(dest, submix - 1, value))
                .unwrap();
        }

        self.midi_tx.send(Mackie::Solo(dest, value)).unwrap();
    }

    pub fn set_mute(&mut self, dest: Destination, value: bool) {
        let submix = self.state.focused_submix;

        if let Destination::Track(idx) = dest {
            if submix == 0 {
                self.mixer.tracks[idx].mute = value;
            } else {
                self.mixer.submixes[submix - 1].track_mutes[idx] = value;
            }
        }

        if submix == 0 {
            self.request_tx.send(Request::Mute(dest, value)).unwrap();
        } else {
            self.request_tx
                .send(Request::SubmixMute(dest, submix - 1, value))
                .unwrap();
        }

        self.midi_tx.send(Mackie::Mute(dest, value)).unwrap();
    }

    pub fn draw_size_warning<B: tui::backend::Backend>(&self, f: &mut Frame<'_, B>) {
        let text = Text::raw(format!(
            "Terminal size ({}, {}) wrong, please resize.",
            f.size().width,
            f.size().height
        ));
        f.render_widget(Paragraph::new(text).alignment(Alignment::Center), f.size());
    }

    fn draw_logo<B: tui::backend::Backend>(
        &self,
        f: &mut Frame<'_, B>,
        rect: Rect,
        full_rect: Rect,
    ) {
        let style = if self.looping { nf().fg(LOOP) } else { nf() };

        let text = if rect.width >= layout::LOGO_WIDTH {
            Text::styled(
                "\
                ┏┳┓╻╻╻┏┓╻┏┓┏┓┏┓\n\
                ┃╹┃╻┃┃┗┓╋┃┃┃┃┣┛\n\
                ╹ ╹┗┗┗┗┛┗┗┛╹╹┗┛\n\
                ",
                style,
            )
        } else {
            Text::styled(
                "\
                ┏┳┓┏┓\n\
                ┃╹┃┗┓\n\
                ╹ ╹┗┛\n\
                ",
                style,
            )
        };
        let rect = match self.frames <= 1000 {
            true => tui::layout::Rect::new(
                full_rect.width
                    - (full_rect.width as f32 * (self.frames as f32 / 1000.0)).max(15.0) as u16,
                rect.y,
                15,
                3,
            ),
            false => rect,
        };
        f.render_widget(Paragraph::new(text), rect);
    }

    fn draw_workload<B: tui::backend::Backend>(&self, f: &mut Frame<'_, B>, rect: Rect) {
        let text = Text::raw(format!(
            " {:>2}%|{}\n {:>2}%|{}",
            self.workload[0].percentage,
            self.workload[0].max,
            self.workload[1].percentage,
            self.workload[1].max,
        ));
        f.render_widget(Paragraph::new(text), rect);
    }

    fn draw_project_name<B: tui::backend::Backend>(&self, f: &mut Frame<'_, B>, rect: Rect) {
        let text = Text::raw((self.project_name).to_string());
        f.render_widget(Paragraph::new(text), rect);
    }

    fn draw_transport<B: tui::backend::Backend>(&self, f: &mut Frame<'_, B>, rects: Vec<Rect>) {
        let icons = ["■", "▶", "●"];

        // Draw transport icons
        for (i, &icon) in icons.iter().enumerate() {
            // Icon style depending on transport state
            let style = match icon {
                "●" => {
                    if self.record && self.export {
                        nf().fg(colors::EXPORT)
                    } else if self.record {
                        nf().fg(RECORD)
                    } else {
                        nf()
                    }
                }
                "▶" => {
                    if self.play && self.export {
                        nf().fg(colors::EXPORT)
                    } else if self.play && self.looping {
                        nf().fg(LOOP)
                    } else if self.play {
                        nf().fg(PLAY)
                    } else {
                        nf()
                    }
                }
                "■" => {
                    if self.play {
                        nf()
                    } else if self.looping {
                        nf().fg(LOOP)
                    } else {
                        nf().fg(STOP)
                    }
                }
                _ => nf(),
            };
            let text = Span::styled(icon, style);
            let paragraph = Paragraph::new(text).alignment(Alignment::Center);
            f.render_widget(paragraph, rects[i]);
        }
    }

    fn draw_time_indicator<B: tui::backend::Backend>(&self, f: &mut Frame<'_, B>, rect: Rect) {
        // Calculate clock time from transport position
        let t = self.state.t as f32 / self.global_settings.read().rate as f32;
        let time = format!(
            "{:0>2}:{:0>2}:{:0>2}:{:0>3}",
            (t / 3600.0) as u32 % 60,
            (t / 60.0) as u32 % 60,
            t as u32 % 60,
            (1000.0 * t) as u32 % 1000,
        );

        // Draw time information in top bar
        let paragraph = Paragraph::new(Text::raw(time));
        f.render_widget(paragraph, rect);
    }

    fn draw_master_vu<B: tui::backend::Backend>(
        &self,
        f: &mut Frame<'_, B>,
        rect: Rect,
        peak: f32,
        comp: f32,
    ) {
        let style = match peak {
            db if db >= 0.0 => nf().fg(CRIT).bg(colors::vu_violet_fog(comp)),
            db => nf().fg(vu_pink(db)).bg(colors::vu_violet_fog(comp)),
        };
        let data = [(peak + 90.0) as u64; 100];
        let fader = Sparkline::default().data(&data).max(102).style(style);
        f.render_widget(fader, rect);
    }

    fn draw_vu_mid_bar<B: tui::backend::Backend>(
        &self,
        f: &mut Frame<'_, B>,
        rect: Rect,
        gain: f32,
        comp: f32,
        show_submix: bool,
    ) {
        // The faders run from -90 dB to +12 dB.
        // To put a line near x dB,
        //
        let db_per_line = 102.0 / (rect.height as f32);
        let bar_spacing = 10.0 * (db_per_line / 10.0).ceil();
        let bar_characters = ["▔\n", "━\n", "▁\n"];
        let submix_alphabet = [
            "M\n", "A\n", "B\n", "C\n", "D\n", "E\n", "F\n", "G\n", "H\n",
        ];
        let db_per_bar = db_per_line / (bar_characters.len() as f32);

        let mut next_db = 0.0;

        let mut lines = Vec::with_capacity(rect.height as usize);

        for line_idx in 0..(rect.height as usize) {
            let mut symbol = " \n".to_string();

            for (bar_idx, bar) in bar_characters.iter().enumerate() {
                let db = (line_idx as f32) * db_per_line + (bar_idx as f32) * db_per_bar;

                if 12.0 - db <= next_db {
                    symbol = bar.to_string();
                    next_db -= bar_spacing;
                    break;
                }
            }

            if show_submix && line_idx == rect.height as usize - 1 {
                symbol = submix_alphabet[self.state.focused_submix].to_string();
            }

            let line_db = 12.0 - (line_idx as f32) * db_per_line;
            lines.push(Spans::from(Span::styled(
                symbol,
                nf().fg(vu_pink(next_db)).bg(colors::interpolate(
                    colors::vu_violet_fog(comp),
                    if gain > 0.0 {
                        colors::CRIT
                    } else if gain == 0.0 {
                        colors::INACTIVE
                    } else {
                        colors::WAVE
                    },
                    0.6 * ((gain - line_db) / db_per_line).min(1.0).max(0.0),
                )),
            )));
        }

        f.render_widget(Paragraph::new(Text::from(lines)), rect);
    }

    fn draw_track_vu<B: tui::backend::Backend>(
        &self,
        f: &mut Frame<'_, B>,
        rect: Rect,
        db: f32,
        i: usize,
    ) {
        let track = &self.mixer.tracks[i];
        f.render_widget(
            Sparkline::default()
                .data(&[(db + 90.0) as u64; (layout::MIXER_TRACK_WIDTH / 2) as usize])
                .max(102)
                .style(
                    match track.monitor
                        || (track.record_arm && !track.mute && (!self.play || self.record))
                    {
                        false => nf().fg(INACTIVE).bg(colors::FOG),
                        true => match db {
                            db if db >= -0.3 => nf().fg(CRIT).bg(colors::FOG),
                            db => nf().fg(vu_pink(db)).bg(colors::FOG),
                        },
                    },
                ),
            rect,
        );
    }

    fn draw_logger<B: tui::backend::Backend>(&self, f: &mut Frame<'_, B>, rect: Rect) {
        let entries = &self.logger.read().entries;
        let height = rect.height as usize - 1;
        let mut stop = self.logger.read().offset;
        let mut start = (stop as isize - height as isize).max(0) as usize;
        if start == 0 && stop < height {
            // TODO, overturning first page is possible
            // TODO, logger does not know the window draw height
            stop = height;
        };
        if height > entries.len() {
            stop = entries.len();
            start = 0;
        };
        let paragraph = Paragraph::new(Text::from(entries[start..stop].join("\n")))
            .alignment(Alignment::Left)
            .block(Block::default().title("Logger"));
        f.render_widget(paragraph, rect);
    }

    fn draw_track<B: tui::backend::Backend>(&self, f: &mut Frame<'_, B>, rect: Rect, idx: usize) {
        // Tracks split zone for status / waveform
        let split = layout::split_tape_track(rect);

        // Draw track input vu
        self.draw_track_vu(f, split[0], self.track_peak_left[idx], idx);

        let submix = self.state.focused_submix;
        let gain = if submix == 0 {
            self.mixer.tracks[idx].fader
        } else {
            self.mixer.submixes[submix - 1].track_gains[idx]
        };

        self.draw_vu_mid_bar(f, split[1], gain, 0.0, false);
        self.draw_track_vu(f, split[2], self.track_peak_right[idx], idx);

        // Draw current track state
        self.draw_track_state(f, split[3], idx);

        // Draw current track waveform
        self.draw_track_wave(f, split[4], idx);
    }

    fn draw_tracks<B: tui::backend::Backend>(&self, f: &mut Frame<'_, B>, rect: Rect) {
        // Build track slots
        let tracks = layout::build_tape_tracks(rect, self.state.num_visible_tracks as u16);

        // Draw each track
        for (idx, track) in tracks.iter().enumerate() {
            self.draw_track(f, *track, self.state.anchor_track + idx);
        }
    }

    fn draw_track_state<B: tui::backend::Backend>(
        &self,
        f: &mut Frame<'_, B>,
        rect: Rect,
        i: usize,
    ) {
        let track = &self.mixer.tracks[i];

        let submix = self.state.focused_submix;
        let mute = if submix == 0 {
            track.mute
        } else {
            self.mixer.submixes[submix - 1].track_mutes[i]
        };

        let solo = if submix == 0 {
            track.solo
        } else {
            self.mixer.submixes[submix - 1].track_solos[i]
        };

        let monitor_style = match track.monitor {
            true => nf().fg(ACTIVE),
            false => match track.record_arm && !track.mute && (!self.play || self.record) {
                true => nf().fg(ACTIVE_BOUND),
                false => nf().fg(INACTIVE),
            },
        };
        let rec_style = match track.record_arm {
            true => nf().fg(RECORD),
            false => nf().fg(INACTIVE),
        };
        let mute_style = match mute {
            true => nf().fg(STOP),
            false => nf().fg(INACTIVE),
        };
        let solo_style = match solo {
            true => nf().fg(PLAY),
            false => nf().fg(INACTIVE),
        };
        let bg = match self.export {
            true => colors::EXPORT,
            false => match i == self.state.focused_track && !self.disable_track_focus {
                true => SELECT,
                false => BACK,
            },
        };

        let submix = self.state.focused_submix;
        let track_pan = if submix == 0 {
            self.mixer.tracks[i].pan
        } else {
            self.mixer.submixes[submix - 1].track_pans[i]
        };

        let pan_text = format!(
            " {}{}",
            match track_pan {
                pan if pan > 0.001 => "R",
                pan if pan < -0.001 => "L",
                _ => "C",
            },
            match track_pan {
                pan if !(-0.001..=0.001).contains(&pan) =>
                    format!("{:>3}", (track_pan.abs() * 100.0).round()),
                _ => "   ".to_string(),
            },
        );
        // TODO fix so Ballet and Opera can live together
        let input_text = if cfg!(feature = "ballet") || cfg!(feature = "opera") {
            format!(
                "{}:{} ",
                if track.physical_left >= 256 {
                    format!("s{}", track.physical_left - 255)
                } else {
                    format!("{}", track.physical_left + 1)
                },
                if track.physical_right >= 256 {
                    format!("s{}", track.physical_right - 255)
                } else {
                    format!("{}", track.physical_right + 1)
                }
            )
        } else {
            format!("{}:{} ", track.physical_left + 1, track.physical_right + 1)
        };
        let preload = match self.preload_status[i] {
            Some(v) if v >= 1.0 => Span::styled("●", nf().fg(bg).bg(bg)),
            Some(v) if v >= 0.75 => Span::styled("◕", nf().fg(colors::CRIT).bg(bg)),
            Some(v) if v >= 0.5 => Span::styled("◑", nf().fg(colors::CRIT).bg(bg)),
            Some(v) if v >= 0.25 => Span::styled("◔", nf().fg(colors::CRIT).bg(bg)),
            Some(_) => Span::styled("○", nf().fg(colors::CRIT).bg(bg)),
            None => Span::styled("◌", nf().bg(bg)),
        };
        let reverb_send_color = nf()
            .fg(colors::interpolate(
                colors::INACTIVE,
                colors::WAVE,
                track.reverb_send,
            ))
            .bg(bg);
        let reverb_send = match track.reverb_send {
            v if v >= 1.0 => Span::styled("●", reverb_send_color),
            v if v >= 0.75 => Span::styled("◕", reverb_send_color),
            v if v >= 0.5 => Span::styled("◑", reverb_send_color),
            v if v >= 0.25 => Span::styled("◔", reverb_send_color),
            _ => Span::styled("○", reverb_send_color),
        };
        let info_text = Text::from(vec![
            Spans::from(vec![
                Span::styled(
                    format!("{} ", track.name),
                    if self.record {
                        rec_style.bg(bg)
                    } else {
                        monitor_style.bg(bg)
                    },
                ),
                Span::raw(input_text),
                preload,
            ]),
            Spans::from(vec![
                Span::styled("🅸 ", monitor_style.bg(bg)),
                Span::styled("🅼 ", mute_style.bg(bg)),
                Span::styled("🆂 ", solo_style.bg(bg)),
                Span::styled("🆁", rec_style.bg(bg)),
                Span::raw(pan_text),
            ]),
            Spans::from(vec![reverb_send]),
        ]);
        f.render_widget(
            Paragraph::new(info_text).style(match self.export {
                true => nf().bg(colors::EXPORT),
                false => match i == self.state.focused_track && !self.disable_track_focus {
                    true => nf().bg(SELECT),
                    false => nf(),
                },
            }),
            rect,
        );
    }

    fn draw_track_wave<B: tui::backend::Backend>(
        &self,
        f: &mut Frame<'_, B>,
        rect: Rect,
        i: usize,
    ) {
        let track = &self.mixer.tracks[i];
        let max_levels = 8 * rect.height as usize;

        let playhead = (rect.width as usize * layout::PLAYHEAD) / 100;

        // First index into loudness map levels.
        let idx0 = (self.state.t / 44100) as isize - playhead as isize;

        let margin_left = (-idx0).max(0).min(rect.width as isize);
        let margin_right = (idx0 + rect.width as isize - (track.length / 44100) as isize)
            .max(0)
            .min(rect.width as isize);
        let wave_length = rect.width as isize - margin_left - margin_right;

        let data = track
            .loudness_map
            .as_levels(max_levels, idx0.max(0), wave_length as usize);

        let regions = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([
                Constraint::Length(margin_left as u16),  // fog on the left
                Constraint::Length(wave_length as u16),  // track wave
                Constraint::Length(margin_right as u16), // fog on the right
            ])
            .split(rect);

        let fog_style = match self.record && track.record_arm {
            false => nf(),
            true => nf().bg(RECORD),
        };

        f.render_widget(Block::default().style(fog_style), regions[0]);
        f.render_widget(Block::default().style(fog_style), regions[2]);

        f.render_widget(
            Sparkline::default()
                .data(&data) //  max terminal allowed width
                .max(max_levels as u64) // 8 steps/char
                .style(match self.record && track.record_arm {
                    false => nf().fg(WAVE).bg(colors::FOG),
                    true => nf().fg(WAVE).bg(RECORD),
                }),
            regions[1],
        );
    }

    #[cfg(feature = "master_effects")]
    fn draw_effects<B: tui::backend::Backend>(
        &self,
        f: &mut Frame<'_, B>,
        rect: Rect,
        labels: Rect,
    ) {
        let faders = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Length(3); 8])
            .split(rect);

        let labels = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Length(3); 8])
            .split(labels);

        let symbols = ["lo", "mi", "fq", "hi", "ns", "cp", "in", "sa"];

        for (idx, (&fader, &label)) in faders.iter().zip(labels.iter()).enumerate() {
            let value = self.mixer.master_bus.effects[idx].rounded;
            let fg = if idx == self.state.focused_effect {
                colors::ACTIVE
            } else {
                colors::INACTIVE
            };
            self.draw_effect_fader(f, fader, label, value, symbols[idx], fg);
        }
    }

    #[cfg(feature = "master_effects")]
    fn draw_effect_fader<B: tui::backend::Backend>(
        &self,
        f: &mut Frame<'_, B>,
        fader: Rect,
        label: Rect,
        value: f32,
        text: &str,
        fg: tui::style::Color,
    ) {
        let fader = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([
                Constraint::Length(1),
                Constraint::Length(1),
                Constraint::Length(1),
            ])
            .split(fader);

        f.render_widget(
            Paragraph::new(Text::styled("├\n┝\n├", nf().fg(fg))),
            fader[0],
        );

        f.render_widget(
            Sparkline::default()
                .data(&[(value * 100.0) as u64])
                .max(100)
                .style(nf().fg(colors::fader_electric(value)).bg(colors::BACK)),
            fader[1],
        );

        f.render_widget(Paragraph::new(Span::styled(text, nf().fg(fg))), label);
    }

    fn draw_analyzer<B: tui::backend::Backend>(
        &self,
        f: &mut Frame<'_, B>,
        rect: Rect,
        spectrum: &[f32],
        comp: f32,
    ) {
        let peak = self.master_peak_left.max(self.master_peak_right);
        let style = match peak {
            db if db >= 0.0 => nf().fg(CRIT).bg(colors::vu_violet_fog(comp)),
            db => nf().fg(vu_pink(db)).bg(colors::vu_violet_fog(comp)),
        };
        let mut data = Vec::with_capacity(rect.width as usize);

        let rect = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Length(rect.height - 1), Constraint::Length(1)])
            .split(rect);

        let window_size = spectrum.len() as f32 / (rect[0].width as f32);

        for idx in 0..(rect[0].width as usize) {
            let mut value = 0.0;

            for offset in 0..(window_size.round() as usize) {
                value += spectrum[(window_size * idx as f32 + offset as f32).round() as usize]
                    / window_size;
            }

            data.push((60.0 + value) as u64);
        }

        let fader = Sparkline::default().data(&data).max(112 - 10).style(style);
        f.render_widget(fader, rect[0]);

        // Draw frequency indication ticks.
        let max_freq: f32 = 44100.0 / 2.0;
        let min_freq = self.master_spectrum_min_freq;
        let range = max_freq.log10() - min_freq.log10();

        let width = rect[1].width as usize;
        let mut ticks = vec![" "; width];

        let mut pow_base = 0.0f32;
        let mut next_pow = 1.0f32;

        // Skip ticks that are not needed.  The compiler should
        // optimize this loop away, hopefully.
        while min_freq.log10() >= pow_base + next_pow.log10() {
            next_pow += 1.0;
            if next_pow > 10.0 {
                next_pow = 0.0;
                pow_base += 1.0;
            }
        }

        for (idx, tick) in ticks.iter_mut().enumerate() {
            let pow = min_freq.log10() + (idx as f32) * range / (width as f32);

            while pow >= pow_base + next_pow.log10() {
                if next_pow == 0.0 {
                    *tick = "┃";
                } else if next_pow == 5.0 {
                    *tick = "╹";
                }

                if *tick == " " {
                    *tick = "╵";
                }

                next_pow += 1.0;
                if next_pow > 10.0 {
                    next_pow = 0.0;
                    pow_base += 1.0;
                }
            }
        }

        let ticks = Paragraph::new(Text::from(ticks.join("")));
        f.render_widget(ticks, rect[1]);
    }
}

impl App {
    fn marker_pos_to_index(&self, pos: usize, playhead: usize) -> isize {
        let frames_per_window = 44100; // TODO hardcoded, please update number from backend
        let mut x = (pos / frames_per_window) as isize;
        x -= (self.state.t / frames_per_window) as isize;
        x + playhead as isize
    }

    fn draw_markers<B: tui::backend::Backend>(
        &self,
        f: &mut Frame<'_, B>,
        rect: Rect,
        margin_right: i16,
    ) {
        // Draw marker view
        let mut marker_labels = vec![Span::raw(" "); rect.width as usize];

        let playhead = ((rect.width as i16 - margin_right) as usize * layout::PLAYHEAD) / 100;

        // Draw latest applicable marker on marker grid
        marker_labels[playhead] = Span::styled(" ", nf().bg(colors::PLAYHEAD));
        for marker in self.marker_state.markers.iter() {
            let x = self.marker_pos_to_index(marker.pos, playhead) as usize;
            let style = match x == playhead {
                true => nf().fg(colors::BACK).bg(colors::PLAYHEAD),
                false => nf(),
            };
            if x < rect.width as usize {
                marker_labels[x] = Span::styled(&marker.label, style);
            }
        }

        // Draw start position
        let x = self.marker_pos_to_index(self.marker_state.start, playhead) as usize;
        if x < rect.width as usize {
            marker_labels[x] = Span::styled(
                "[",
                match x == playhead {
                    true => nf().fg(colors::BACK).bg(colors::PLAYHEAD),
                    false => nf(),
                },
            );
        };

        // Draw end position
        let x = self.marker_pos_to_index(self.marker_state.stop, playhead) as usize;
        if x < rect.width as usize {
            marker_labels[x] = Span::styled(
                "]",
                match x == playhead {
                    true => nf().fg(colors::BACK).bg(colors::PLAYHEAD),
                    false => nf(),
                },
            );
        };

        // Draw loop start position
        let x = self.marker_pos_to_index(self.marker_state.loop_start, playhead) as usize;
        if x < rect.width as usize {
            marker_labels[x] = Span::styled(
                "(",
                match self.looping {
                    true => nf().bg(colors::LOOP),
                    false => match x == playhead {
                        true => nf().fg(colors::BACK).bg(colors::PLAYHEAD),
                        false => nf(),
                    },
                },
            );
        };

        // Draw loop stop position
        let x = self.marker_pos_to_index(self.marker_state.loop_stop, playhead) as usize;
        if x < rect.width as usize {
            marker_labels[x] = Span::styled(
                ")",
                match self.looping {
                    true => nf().bg(colors::LOOP),
                    false => match x == playhead {
                        true => nf().fg(colors::BACK).bg(colors::PLAYHEAD),
                        false => nf(),
                    },
                },
            );
        };

        // Render colorstripe on active region
        // TODO add different colors per marker name
        let t = self.state.t;

        // Find the left region x point x1
        let mut x1 = self.marker_pos_to_index(self.marker_state.previous(t + 1), playhead) + 1;

        // Make sure x1 is on screen
        if x1 < 0 {
            x1 = 0;
        }

        // Find the right region x point x2
        let next = self.marker_state.next(t);

        // Draw if playhead is on a region
        if next > t {
            let mut x2 = self.marker_pos_to_index(next, playhead) as u16;
            // Make sure x2 is on screen
            if x2 > rect.width {
                x2 = rect.width;
            }
            let stripe = Rect::new(rect.x + x1 as u16, rect.y, x2 - x1 as u16, 1);
            f.render_widget(
                Block::default().style(
                    nf().fg(colors::PLAYHEAD)
                        .bg(colors::vu_violet_fog(self.compressor_gain)),
                ),
                stripe,
            );
        }

        // Render markers and symbols
        f.render_widget(Paragraph::new(vec![Spans::from(marker_labels)]), rect);
    }

    fn draw_menu<B: tui::backend::Backend>(&self, f: &mut Frame<'_, B>, rect: Rect) {
        for offset in 0..self.menu.len() {
            if self.test_menu_offset(offset, rect.width as usize) {
                self._draw_menu(f, rect, offset);
                break;
            }
        }
    }

    fn test_menu_offset(&self, offset: usize, available_width: usize) -> bool {
        let mut width = 0;
        for i in offset..self.menu.len() {
            let mut required_width = 0;
            for item in self.menu.pretty_actions(i).iter() {
                required_width = required_width.max(item.len() + layout::MENU_SPACER);
            }
            width += required_width;
        }
        width < available_width + layout::MENU_SPACER / 2
    }

    fn _draw_menu<B: tui::backend::Backend>(
        &self,
        f: &mut Frame<'_, B>,
        rect: Rect,
        offset: usize,
    ) {
        let mut used_width = 0;
        for i in offset..self.menu.len() {
            let is_active = i + 1 == self.menu.len();

            // Convert action in menu column to displayble strings
            let mut required_width = 0;
            let mut list_items = vec![];
            for item in self.menu.pretty_actions(i).iter() {
                required_width = required_width.max(item.len() + layout::MENU_SPACER);
                list_items.push(ListItem::new(item.to_string()));
            }

            // Build list
            let list = List::new(list_items)
                .highlight_style(match is_active {
                    true => colors::nf().fg(colors::LOOP),
                    false => colors::nf().fg(colors::INACTIVE),
                })
                .highlight_symbol(match is_active {
                    true => ">> ",
                    false => "   ",
                });
            let mut state = ListState::default();
            state.select(Some(self.menu.cursor(i)));

            // Build rect and render
            let mut column = rect;
            column.width = required_width as u16;
            if is_active {
                column.width -= 1
            }
            column.x = used_width as u16;
            f.render_stateful_widget(list, column, &mut state);

            // Update the used width (x-location) for next column
            used_width += required_width;
        }
    }
}

impl App {
    pub fn draw<B: tui::backend::Backend>(&mut self, f: &mut Frame<'_, B>) {
        // Set default styling for entire app
        f.render_widget(Block::default().style(nf()), f.size());

        // Issue warning to resize if terminal too big or small
        if f.size().width < layout::MIN_WIDTH || f.size().height < layout::MIN_HEIGHT {
            self.draw_size_warning(f);
            return;
        }

        match self.mode {
            Mode::Logger => {
                self.draw_logger(f, f.size());
            }
            Mode::Menu => {
                // Draw logo in tape mode
                let popup = layout::build_popup(f.size());
                self.draw_logo(f, popup[0], popup[0]);
                f.render_widget(
                    Paragraph::new("\n\n".to_owned() + &self.menu.get_register())
                        .alignment(Alignment::Right),
                    popup[0],
                );

                // Create and render the project list with current state
                self.draw_menu(f, popup[1]);
            }
            Mode::Tape | Mode::Performance => {
                // Create app layout
                #[allow(unused_variables)]
                let (layout, top_bar, transport, marker_hsplit, hsplit, master_vu, effects) =
                    layout::build(f.size());

                // Draw project name
                self.draw_project_name(f, marker_hsplit[0]);

                // Draw time indicator
                self.draw_time_indicator(f, marker_hsplit[2]);

                // Draw logo
                self.draw_logo(f, top_bar[0], layout[0]);

                // Draw workload
                self.draw_workload(f, top_bar[1]);

                // Draw markers
                self.draw_markers(
                    f,
                    marker_hsplit[1],
                    hsplit[1].width as i16 - marker_hsplit[2].width as i16,
                );

                // Draw master VU meter
                if self.mode == Mode::Tape || self.analyzer_state {
                    let submix = self.state.focused_submix;
                    let gain = if submix == 0 {
                        self.mixer.master_bus.fader
                    } else {
                        self.mixer.submixes[submix - 1].master_gain
                    };

                    self.draw_master_vu(
                        f,
                        master_vu[0],
                        self.master_peak_left,
                        self.compressor_gain,
                    );
                    self.draw_master_vu(
                        f,
                        master_vu[1],
                        self.master_rms_left,
                        self.compressor_gain,
                    );
                    self.draw_vu_mid_bar(f, master_vu[2], gain, self.compressor_gain, true);
                    self.draw_master_vu(
                        f,
                        master_vu[3],
                        self.master_rms_right,
                        self.compressor_gain,
                    );
                    self.draw_master_vu(
                        f,
                        master_vu[4],
                        self.master_peak_right,
                        self.compressor_gain,
                    );
                }

                // Draw transport block
                self.draw_transport(f, transport);

                // Dependent on main mode
                if self.mode == Mode::Tape {
                    // Draw tracks
                    self.draw_tracks(f, hsplit[0]);
                };
                if self.mode == Mode::Performance {
                    // Draw effect values
                    #[cfg(feature = "master_effects")]
                    self.draw_effects(f, effects[0], effects[1]);

                    // Draw analyzer
                    if self.analyzer_state {
                        self.draw_analyzer(
                            f,
                            hsplit[0],
                            &self.master_spectrum,
                            self.compressor_gain,
                        );
                    }
                };
            }
        }
    }
}
