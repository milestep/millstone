// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use alsa::seq;

use crossbeam::channel::Sender;

use crate::messages::{BalletId, BalletMessage};

pub struct ToucheSE {
    addr: Option<seq::Addr>,
    ballet_tx: Sender<BalletMessage>,
}

impl ToucheSE {
    pub fn new(ballet_tx: Sender<BalletMessage>) -> Self {
        Self {
            addr: None,
            ballet_tx,
        }
    }

    pub fn connect(&mut self, addr: &seq::Addr, name: &String) -> bool {
        if self.addr.is_none() && name == "TOUCHE_SE" {
            self.addr = Some(*addr);
            return true;
        }

        false
    }

    pub fn disconnect(&mut self, addr: &seq::Addr) -> bool {
        let addr_ = self.addr.take();

        if addr_.is_some() && addr_.unwrap() == *addr {
            return true;
        }

        self.addr = addr_;

        false
    }

    pub fn has_addr(&self, addr: &seq::Addr) -> bool {
        self.addr.is_some() && self.addr.unwrap() == *addr
    }

    pub fn cc(&self, _channel: u8, param: u8, value: u8) {
        if (16..20).contains(&param) {
            self.ballet_tx
                .send(BalletMessage::Morph {
                    channel: 0,
                    id: BalletId {
                        group_id: 0,
                        voice_id: 0,
                        note_id: 0,
                    },
                    axis: (param - 14) as usize,
                    amount: value as f32 / 126.0,
                })
                .unwrap();
        }
    }
}
