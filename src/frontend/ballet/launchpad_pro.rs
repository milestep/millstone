// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use alsa::seq;

use crossbeam::channel::Sender;

use std::collections::HashMap;
use std::path::Path;

use crate::ballet::Patch;
use crate::messages::{
    Action, BalletConfig, BalletId, BalletMasterConfig, BalletMessage, MidiMessage,
};
use crate::paths;
use crate::state::State;
use crate::synth::Bounds;

use super::menu;
use super::menu::{MenuMode, ParamMenu};
use super::session::{GlobalState, Pad, Param, Session, SessionMetadata};

pub struct LaunchpadPro {
    addr: Option<seq::Addr>,
    action_tx: Sender<Action>,
    midi_tx: Sender<MidiMessage>,
    ballet_tx: Sender<BalletMessage>,
    // Session
    is_initialized: bool,
    global_state: GlobalState,
    session: Session,
    session_page: u8,
    patch_page: u8,
    // Modes
    menu_height: u8,
    menu_mode: Option<MenuMode>,
    param_menu_page: u8,
    param_menu_idx: u8,
    show_modulation_sources: bool,
    show_palette: bool,
    bounds_mode: Option<Bounds>,
    note_mode: bool,
    is_note_mode_consumed: bool,
    save_mode: bool,
    delete: bool,
    duplicate: bool,
    record: bool,
    shift: bool,
    click: bool,
    undo: bool,
    mute: bool,
    solo: bool,
    // Tap time
    taps: [u128; 4],
    tap_idx: usize,
    tap_time: std::time::Instant,
    // Duplicate
    held_value: Option<f32>,
    duplicate_cycle: usize,
    // State
    active_color: [u8; 3],
    base_state: [[u8; 3]; 100],
    selected: Vec<[isize; 2]>,
    state: [[u8; 3]; 100],
    state_mode: [u8; 100],
    prev_state: [[u8; 4]; 100],
    peak_key_pressure: [u8; 100],
    pad_on_grid: [bool; 100],
    // Voice
    modulation_source: Option<[usize; 4]>,
    group_id: usize,
    piano_group_ids: HashMap<u8, usize>,
    // Queues
    param_queue: Vec<(Param, f32)>,
    scale_queue: Vec<i16>,
}

impl LaunchpadPro {
    pub fn new(
        action_tx: Sender<Action>,
        midi_tx: Sender<MidiMessage>,
        ballet_tx: Sender<BalletMessage>,
    ) -> Self {
        Self {
            addr: None,
            action_tx,
            midi_tx,
            ballet_tx,
            // Session
            is_initialized: false,
            global_state: GlobalState::read_ballet("global.ron"),
            session: Session::new(),
            session_page: 0,
            patch_page: 0,
            // Modes
            menu_height: 0,
            menu_mode: None,
            param_menu_page: 0,
            param_menu_idx: 255,
            show_modulation_sources: false,
            show_palette: false,
            bounds_mode: None,
            note_mode: false,
            is_note_mode_consumed: false,
            save_mode: false,
            delete: false,
            duplicate: false,
            record: false,
            shift: false,
            click: false,
            undo: false,
            mute: false,
            solo: false,
            // Tap time
            taps: [0; 4],
            tap_idx: 0,
            tap_time: std::time::Instant::now(),
            // Duplicate
            held_value: None,
            duplicate_cycle: 0,
            // State
            active_color: [63, 63, 63],
            base_state: [[0, 0, 0]; 100],
            selected: vec![],
            state: [[255, 255, 255]; 100],
            state_mode: [0; 100],
            prev_state: [[255, 255, 255, 0]; 100],
            peak_key_pressure: [0; 100],
            pad_on_grid: [false; 100],
            // Voice
            modulation_source: None,
            group_id: 0,
            piano_group_ids: HashMap::new(),
            // Queues
            param_queue: vec![],
            scale_queue: vec![],
        }
    }

    pub fn connect(&mut self, addr: &seq::Addr, name: &String) -> bool {
        if self.addr.is_none() && name == "Launchpad Pro" && addr.port == 1 {
            self.addr = Some(*addr);
            self.initialize();
            return true;
        }

        false
    }

    pub fn disconnect(&mut self, addr: &seq::Addr) -> bool {
        let addr_ = self.addr.take();

        if addr_.is_some() && addr_.unwrap() == *addr {
            return true;
        }

        self.addr = addr_;

        false
    }

    pub fn has_addr(&self, addr: &seq::Addr) -> bool {
        self.addr.is_some() && self.addr.unwrap() == *addr
    }

    pub fn cc(&mut self, channel: u8, param: u8, value: u8) {
        if value > 0 {
            if self.global_state.locked && !self.shift && param != 80 {
                self.set_state(param, 1, 1, 1);
            } else {
                self.set_state(param, 63, 63, 63);
            }

            if param == 80 {
                self.shift = true;
            }

            if !self.global_state.locked || self.shift {
                match param {
                    // Mute
                    3 => {
                        self.mute = true;
                    }
                    // Double
                    20 => {}
                    // Duplicate
                    30 => {
                        self.duplicate = true;
                        self.duplicate_cycle = 0;
                    }
                    // Quantise
                    40 => {}
                    // Delete
                    50 => {
                        self.delete = true;
                    }
                    // Undo
                    60 => {
                        self.undo = true;
                    }
                    // Click
                    70 => {
                        self.click = true;

                        if !self.record {
                            self.ballet_tx
                                .send(BalletMessage::Sidechain {
                                    channel: 9,
                                    velocity: 1.0,
                                })
                                .unwrap();

                            self.taps.fill(0);
                            self.tap_time = std::time::Instant::now();
                        }
                    }

                    // Note
                    96 => {
                        self.note_mode = true;
                        self.is_note_mode_consumed = false;
                    }

                    _ => (),
                }
            }
        } else {
            self.unset_state(param);

            if param == 80 {
                self.shift = false;
            }

            if !self.global_state.locked || self.shift {
                match param {
                    // Track select
                    1 => {
                        if self.shift {
                            self.global_state.locked = !self.global_state.locked;
                            self.global_state.write_ballet("global.ron");
                        }
                    }

                    // Track select
                    2 => {
                        self.show_palette = !self.show_palette;
                    }

                    // Mute
                    3 => {
                        self.mute = false;

                        if self.record {
                            for coord in self.selected.iter() {
                                if let Some(pad) = self.session.grid.get_mut(coord) {
                                    pad.toggle_mute();
                                }
                            }
                        }
                    }

                    // Solo
                    4 => {
                        self.solo = !self.solo;
                        self.ballet_tx
                            .send(BalletMessage::SetTalk(self.solo))
                            .unwrap()
                    }

                    // Pan
                    6 => {
                        match self.param_menu_page {
                            0 => {
                                self.param_menu_page = 1;
                            }
                            _ => {
                                self.param_menu_page = 0;
                            }
                        }

                        if self.param_menu_idx != 255 {
                            self.enter_param_menu(self.param_menu_idx);
                        }
                    }

                    // Sends
                    7 => {
                        if let Some(MenuMode::Param(_)) = self.menu_mode {
                            self.show_modulation_sources = !self.show_modulation_sources;
                            self.enter_menu(self.menu_mode);
                        }
                    }

                    // Panic
                    8 => self.ballet_tx.send(BalletMessage::Panic).unwrap(),

                    // Record
                    10 => {
                        if self.record {
                            self.record = false;
                            self.set_base_state(param, 0, 0, 0);
                        } else {
                            self.record = true;
                            self.set_base_state(param, 63, 0, 0);
                        }
                    }

                    // Double
                    20 => {
                        if self.record {
                            for coord in self.selected.iter() {
                                if let Some(pad) = self.session.grid.get_mut(coord) {
                                    pad.toggle_latch();
                                }
                            }
                        }
                    }
                    // Duplicate
                    30 => {
                        self.duplicate = false;
                        self.held_value = None;
                    }
                    // Quantise
                    40 => {}
                    // Delete
                    50 => {
                        self.delete = false;
                    }
                    // Undo
                    60 => {
                        self.undo = false;
                    }
                    // Click
                    70 => {
                        self.click = false;

                        if self.record {
                            for coord in self.selected.iter() {
                                if let Some(pad) = self.session.grid.get_mut(coord) {
                                    pad.toggle_sidechain();
                                }
                            }
                        }
                    }
                    // Shift
                    80 => (),

                    // Menus
                    89 => self.enter_param_menu(0),
                    79 => self.enter_param_menu(1),
                    69 => self.enter_param_menu(2),
                    59 => self.enter_param_menu(3),
                    49 => self.enter_param_menu(4),
                    39 => self.enter_param_menu(5),
                    29 => self.enter_param_menu(6),
                    19 => self.enter_param_menu(7),

                    // Up
                    91 => {
                        if let Some(MenuMode::Session) = self.menu_mode {
                            self.save_mode = !self.save_mode;
                        } else if let Some(MenuMode::ChannelSelector) = self.menu_mode {
                            self.save_mode = !self.save_mode;
                        } else if self.note_mode {
                            self.is_note_mode_consumed = true;

                            if self.session.base_octave < 12 {
                                self.session.base_octave += 1;
                            }
                        } else if self.record && self.shift {
                            for coord in self.selected.iter() {
                                if let Some(pad) = self.session.grid.get_mut(coord) {
                                    pad.transpose(12);
                                }
                            }
                        } else if self.show_modulation_sources {
                            if self.modulation_source.is_some() {
                                self.bounds_mode = if self.shift {
                                    match self.bounds_mode {
                                        Some(Bounds::Offset) => None,
                                        _ => Some(Bounds::Offset),
                                    }
                                } else {
                                    match self.bounds_mode {
                                        Some(Bounds::Upper) => None,
                                        _ => Some(Bounds::Upper),
                                    }
                                };
                            }
                        } else {
                            self.session.offset[1] -= 1;
                        }
                    }
                    // Down
                    92 => {
                        if let Some(MenuMode::Session) = self.menu_mode {
                            self.save_mode = false;
                        } else if let Some(MenuMode::ChannelSelector) = self.menu_mode {
                            self.save_mode = false;
                        } else if self.note_mode {
                            self.is_note_mode_consumed = true;

                            if self.session.base_octave > 0 {
                                self.session.base_octave -= 1;
                            }
                        } else if self.record && self.shift {
                            for coord in self.selected.iter() {
                                if let Some(pad) = self.session.grid.get_mut(coord) {
                                    pad.transpose(-12);
                                }
                            }
                        } else if self.show_modulation_sources {
                            if self.modulation_source.is_some() {
                                self.bounds_mode = if self.shift {
                                    match self.bounds_mode {
                                        Some(Bounds::Offset) => None,
                                        _ => Some(Bounds::Offset),
                                    }
                                } else {
                                    match self.bounds_mode {
                                        Some(Bounds::Lower) => None,
                                        _ => Some(Bounds::Lower),
                                    }
                                };
                            }
                        } else {
                            self.session.offset[1] += 1;
                        }
                    }

                    // Left
                    93 => {
                        if self.record && self.shift {
                            for coord in self.selected.iter() {
                                if let Some(pad) = self.session.grid.get_mut(coord) {
                                    pad.transpose(-1);
                                }
                            }
                        } else if self.note_mode {
                            self.is_note_mode_consumed = true;

                            if self.session.base_octave > 0 {
                                self.session.shift_piano -= 1;
                            }

                            if self.session.shift_piano < -3 {
                                self.session.shift_piano += 7;
                                self.session.base_octave -= 1;
                            }
                        } else {
                            match self.menu_mode {
                                Some(menu_mode) => {
                                    let page = if menu_mode == MenuMode::Session {
                                        self.session_page
                                    } else if menu_mode == MenuMode::ChannelSelector {
                                        self.patch_page
                                    } else {
                                        self.session.get_page(menu_mode)
                                    };
                                    if page > 0 {
                                        self.set_page(page - 1);
                                    }
                                }
                                None => {
                                    self.session.offset[0] += 1;
                                }
                            }
                        }
                    }
                    // Right
                    94 => {
                        if self.record && self.shift {
                            for coord in self.selected.iter() {
                                if let Some(pad) = self.session.grid.get_mut(coord) {
                                    pad.transpose(1);
                                }
                            }
                        } else if self.note_mode {
                            self.is_note_mode_consumed = true;

                            if self.session.base_octave < 12 {
                                self.session.shift_piano += 1;
                            }

                            if self.session.shift_piano > 3 {
                                self.session.shift_piano -= 7;
                                self.session.base_octave += 1;
                            }
                        } else {
                            match self.menu_mode {
                                Some(menu_mode) => {
                                    let page = if menu_mode == MenuMode::Session {
                                        self.session_page
                                    } else if menu_mode == MenuMode::ChannelSelector {
                                        self.patch_page
                                    } else {
                                        self.session.get_page(menu_mode)
                                    };
                                    let page_count = menu::get_page_count(menu_mode);

                                    if page < page_count - 1 {
                                        self.set_page(page + 1);
                                    }
                                }
                                None => {
                                    self.session.offset[0] -= 1;
                                }
                            }
                        }
                    }

                    // Session
                    95 => match self.menu_mode {
                        Some(MenuMode::Session) => self.enter_menu(None),
                        _ => self.enter_menu(Some(MenuMode::Session)),
                    },

                    // Note
                    96 => {
                        self.note_mode = false;

                        if !self.is_note_mode_consumed {
                            if self.shift {
                                self.set_piano_octaves(4);
                            } else if self.session.piano_octaves > 0 {
                                self.set_piano_octaves(0);
                            } else {
                                self.set_piano_octaves(2);
                            }
                        }
                    }

                    // Device
                    97 => match self.menu_mode {
                        Some(MenuMode::ChannelSelector) => self.enter_menu(None),
                        _ => self.enter_menu(Some(MenuMode::ChannelSelector)),
                    },

                    // User
                    98 => match self.menu_mode {
                        Some(MenuMode::VoiceConfiguration) => self.enter_menu(None),
                        _ => self.enter_menu(Some(MenuMode::VoiceConfiguration)),
                    },

                    _ => self
                        .action_tx
                        .send(Action::Log(format!(
                            "Unhandled CC: {} {} {}",
                            channel, param, value
                        )))
                        .unwrap(),
                }
            }
        }

        self.redraw();
    }

    pub fn note_on(&mut self, _channel: u8, idx: u8, velocity: u8) {
        if velocity == 0 {
            self.unset_state(idx);
            self.peak_key_pressure[idx as usize] = 0;
        }

        let on_grid = self.pad_on_grid[idx as usize];

        if !on_grid && idx < (30 * self.show_palette as u8) {
            self.note_on_palette(idx, velocity, 1);
        } else if !on_grid && self.menu_mode.is_some() && idx > (90 - 10 * self.menu_height) {
            self.note_on_menu(idx, velocity, 1, false);
        } else if idx > (90 - 20 * self.session.piano_octaves) {
            self.pad_on_grid[idx as usize] = velocity > 0;
            self.note_on_piano(idx, velocity);
        } else {
            self.pad_on_grid[idx as usize] = velocity > 0;
            self.note_on_grid(idx, velocity);
        }

        self.redraw();
    }

    fn note_on_menu(&mut self, idx: u8, velocity: u8, steps: u8, is_pressure: bool) {
        let (row, col) = self.note_to_row_col(idx);

        match self.menu_mode {
            Some(MenuMode::Session) => {
                if velocity > 0 {
                    let session_idx = (col + 8 * row + 32 * self.session_page) as usize;
                    if self.save_mode {
                        self.save_session(session_idx);
                    } else if self.delete {
                        self.remove_session(session_idx);
                    } else {
                        self.load_session(session_idx);
                    }

                    self.enter_menu(None);
                }
            }

            Some(MenuMode::ChannelSelector) => {
                if velocity > 0 {
                    if row == 0 || row == 1 {
                        self.session.channel = 1 + col + 8 * row;

                        if self.session.channel > 0 && self.session.channel <= 16 {
                            let patch_idx =
                                self.session.channel_patch_idx[self.session.channel as usize - 1];
                            self.patch_page = (patch_idx / 32) as u8;
                        }
                    } else if row == 2 || row == 3 {
                        let voice_idx = (col + 8 * (row - 2)) as usize;

                        if self.session.voice_channels[voice_idx] == self.session.channel {
                            self.session.voice_channels[voice_idx] = 255;
                            self.ballet_tx
                                .send(BalletMessage::UnloadPatch { voice_idx })
                                .unwrap();
                        } else if self.shift || self.session.voice_channels[voice_idx] == 255 {
                            self.session.voice_channels[voice_idx] = self.session.channel;
                            self.ballet_tx
                                .send(BalletMessage::LoadPatch {
                                    voice_idx,
                                    patch: Box::new(
                                        self.session.get_channel_patch(self.session.channel),
                                    ),
                                })
                                .unwrap();
                        }
                    } else if row < 8 {
                        let patch_idx = (col + 8 * (row - 4) + 32 * self.patch_page) as usize;

                        if self.save_mode {
                            self.save_patch(
                                patch_idx,
                                self.session.get_channel_patch(self.session.channel),
                            );

                            self.save_mode = false;
                        } else if self.delete {
                            self.remove_patch(patch_idx);
                        } else if self.shift {
                            let patch = self.load_patch(patch_idx);
                            self.session.set_channel_patch(self.session.channel, patch);

                            for (voice_idx, &channel) in
                                self.session.voice_channels.iter().enumerate()
                            {
                                if self.session.channel != channel {
                                    continue;
                                }

                                self.ballet_tx
                                    .send(BalletMessage::LoadPatch {
                                        voice_idx,
                                        patch: Box::new(
                                            self.session.get_channel_patch(self.session.channel),
                                        ),
                                    })
                                    .unwrap();
                            }
                        }
                    }
                }
            }

            Some(MenuMode::VoiceConfiguration) => {
                let page = self.session.get_page(MenuMode::VoiceConfiguration);

                if velocity > 0 {
                    if page == 0 {
                        if row == 0 || row == 1 {
                            let dest = match row {
                                0 => match col {
                                    0 => Some(BalletConfig::DisableOsc1),
                                    1 => Some(BalletConfig::DisableOsc2),
                                    2 => Some(BalletConfig::DisableTunedDelay),
                                    3 => Some(BalletConfig::DisableFlt1),
                                    4 => Some(BalletConfig::DisableFlt2),
                                    5 => Some(BalletConfig::DisableWshp),
                                    6 => Some(BalletConfig::DisableDelay),
                                    _ => None,
                                },
                                1 => match col {
                                    0 => Some(BalletConfig::DisableEnv1),
                                    1 => Some(BalletConfig::DisableEnv2),
                                    2 => Some(BalletConfig::DisableLfo1),
                                    3 => Some(BalletConfig::DisableLfo2),
                                    _ => None,
                                },
                                _ => None,
                            };

                            if let Some(dest) = dest {
                                let value = self.session.get_config(self.session.channel, dest);
                                let new_value = if value > 0 { 0 } else { 1 };

                                self.session
                                    .set_config(self.session.channel, dest, new_value);

                                self.ballet_tx
                                    .send(BalletMessage::SetConfig {
                                        channel: self.session.channel,
                                        dest,
                                        value: self.session.get_config(self.session.channel, dest),
                                    })
                                    .unwrap();
                            }

                            // Master config.

                            let dest = match row {
                                0 => match col {
                                    7 => Some(BalletMasterConfig::DisablePitchTracker),
                                    _ => None,
                                },
                                1 => match col {
                                    4 => Some(BalletMasterConfig::DisableVocal),
                                    5 => Some(BalletMasterConfig::DisableDelay),
                                    6 => Some(BalletMasterConfig::DisableCompressor),
                                    7 => Some(BalletMasterConfig::DisableFilter),
                                    _ => None,
                                },
                                _ => None,
                            };

                            if let Some(dest) = dest {
                                let value = self.session.get_master_config(dest);
                                let new_value = if value > 0 { 0 } else { 1 };

                                self.session.set_master_config(dest, new_value);

                                self.ballet_tx
                                    .send(BalletMessage::SetMasterConfig {
                                        dest,
                                        value: self.session.get_master_config(dest),
                                    })
                                    .unwrap();
                            }
                        } else if row == 2 {
                            let dest = BalletMasterConfig::RateDivider;

                            self.session.set_master_config(dest, col as usize);

                            self.ballet_tx
                                .send(BalletMessage::SetMasterConfig {
                                    dest,
                                    value: self.session.get_master_config(dest),
                                })
                                .unwrap();
                        } else if row == 3 {
                            let dest = BalletConfig::RateDivider;

                            self.session
                                .set_config(self.session.channel, dest, col as usize);

                            self.ballet_tx
                                .send(BalletMessage::SetConfig {
                                    channel: self.session.channel,
                                    dest,
                                    value: self.session.get_config(self.session.channel, dest),
                                })
                                .unwrap();
                        } else if row == 4 || row == 5 {
                            let dest = BalletMasterConfig::SidechainChannel;

                            self.session
                                .set_master_config(dest, (col + 8 * (row - 4)) as usize);

                            self.ballet_tx
                                .send(BalletMessage::SetMasterConfig {
                                    dest,
                                    value: self.session.get_master_config(dest),
                                })
                                .unwrap();
                        } else if row == 6 {
                            let dest = if self.shift {
                                BalletConfig::OutputChannelRight
                            } else {
                                BalletConfig::OutputChannelLeft
                            };

                            self.session
                                .set_config(self.session.channel, dest, col as usize);

                            self.ballet_tx
                                .send(BalletMessage::SetConfig {
                                    channel: self.session.channel,
                                    dest,
                                    value: self.session.get_config(self.session.channel, dest),
                                })
                                .unwrap();
                        } else if row == 7 {
                            let dest = BalletMasterConfig::InputSource;

                            self.session.set_master_config(dest, col as usize);

                            self.ballet_tx
                                .send(BalletMessage::SetMasterConfig {
                                    dest,
                                    value: self.session.get_master_config(dest),
                                })
                                .unwrap();
                        }
                    } else if page == 1 {
                        if row == 0 || row == 1 {
                            let note = self.note_to_piano_note(idx);

                            if note != -128 {
                                if self.delete {
                                    if self.record {
                                        for coord in self.selected.iter() {
                                            if let Some(pad) = self.session.grid.get_mut(coord) {
                                                pad.remove_scale_note(note)
                                            }
                                        }
                                    }
                                } else if self.shift {
                                    let dest = BalletMasterConfig::RootNote;

                                    self.session
                                        .set_master_config(dest, note.rem_euclid(12) as usize);

                                    self.ballet_tx
                                        .send(BalletMessage::SetMasterConfig {
                                            dest,
                                            value: self.session.get_master_config(dest),
                                        })
                                        .unwrap();
                                } else {
                                    let enabled = self.session.toggle_note(note);

                                    if self.record {
                                        for coord in self.selected.iter() {
                                            if let Some(pad) = self.session.grid.get_mut(coord) {
                                                pad.toggle_scale_note(note, enabled);
                                            }
                                        }
                                    }

                                    self.ballet_tx
                                        .send(BalletMessage::SetNote {
                                            note,
                                            value: enabled,
                                        })
                                        .unwrap();
                                }
                            }
                        } else if row == 2 {
                            let dest = BalletMasterConfig::Tuning;

                            self.session.set_master_config(dest, col as usize);

                            self.ballet_tx
                                .send(BalletMessage::SetMasterConfig {
                                    dest,
                                    value: self.session.get_master_config(dest),
                                })
                                .unwrap();
                        }
                    }
                }
            }

            Some(MenuMode::Param(dest)) => {
                if velocity > 0 {
                    let row_offset = 3 * self.show_modulation_sources as u8;

                    if self.show_modulation_sources && row < row_offset {
                        let idx = (col + 8 * row) as usize;

                        if !is_pressure {
                            self.toggle_modulation(idx);
                            self.bounds_mode = None;
                        }
                    } else {
                        let page = self.session.get_page(MenuMode::Param(dest));

                        let param = self.get_parameter_type(page, col);

                        if self.undo {
                            let value = self.session.get_param(param);
                            self.set_param(param, value.map_or(0.0, |x| x.initial));
                        } else if self.duplicate {
                            let value = self.session.get_param(param);

                            match self.held_value {
                                Some(held_value) => {
                                    self.set_param(
                                        param,
                                        held_value
                                            .min(value.map_or(1.0, |x| x.high))
                                            .max(value.map_or(0.0, |x| x.low)),
                                    );
                                }
                                None => {
                                    self.held_value = Some(value.map_or(0.0, |x| x.x));
                                }
                            }
                        } else if self.click {
                            let tap_value = self.tap();
                            let value = self.session.get_param(param);

                            if tap_value > 0.0 {
                                self.set_param(
                                    param,
                                    tap_value
                                        .min(value.map_or(1.0, |x| x.high))
                                        .max(value.map_or(0.0, |x| x.low)),
                                );
                            }
                        } else {
                            let amount = match row - row_offset {
                                0 => {
                                    if self.shift {
                                        -1.0 / 192.0
                                    } else {
                                        -1.0 / 8.0
                                    }
                                }
                                1 => {
                                    if self.shift {
                                        -1.0 / 9600.0
                                    } else {
                                        -1.0 / 96.0
                                    }
                                }
                                2 => {
                                    if self.shift {
                                        1.0 / 9600.0
                                    } else {
                                        1.0 / 96.0
                                    }
                                }
                                3 => {
                                    if self.shift {
                                        1.0 / 192.0
                                    } else {
                                        1.0 / 8.0
                                    }
                                }
                                _ => 0.0,
                            };

                            self.shift_param(param, steps as f32 * amount);
                        }

                        let value = self.session.get_param(param).map_or(0.0, |x| x.x);

                        if let Some(x) = param {
                            match x.modulation_source {
                                None => self
                                    .ballet_tx
                                    .send(BalletMessage::SetParam {
                                        param: x.parameter,
                                        value,
                                    })
                                    .unwrap(),
                                Some(_) => {
                                    if let Some(modulation) = self.session.get_modulation(x) {
                                        self.ballet_tx
                                            .send(BalletMessage::SetModulation(modulation))
                                            .unwrap();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            None => {}
        }
    }

    fn note_on_grid(&mut self, idx: u8, velocity: u8) {
        let (x, y) = self.note_to_grid_xy(idx);
        let coord = [x, y];

        if velocity > 0 {
            if self.show_palette && self.shift {
                if let Some(pad) = self.session.grid.get(&coord) {
                    self.session.base_color = pad.color;
                }
            }

            if self.delete {
                self.session.grid.remove(&[x, y]);
            } else if self.duplicate {
                let coord_ = self.selected[self.duplicate_cycle % self.selected.len()];

                if let Some(pad) = self.session.grid.get(&coord_) {
                    let mut pad = pad.clone();
                    pad.active = false;
                    self.session.grid.insert(coord, pad);
                }

                self.duplicate_cycle += 1;
            } else if self.record {
                self.set_state(idx, 1 + velocity / 2, 0, 0);

                if !self.shift {
                    self.selected.clear();
                }

                if !self.selected.contains(&coord) {
                    self.selected.push(coord);
                }

                self.session
                    .grid
                    .entry(coord)
                    .and_modify(|pad| {
                        if self.show_palette {
                            pad.color = self.session.base_color;
                        }
                    })
                    .or_insert(Pad::new(
                        self.session.base_color[0],
                        self.session.base_color[1],
                        self.session.base_color[2],
                    ));
            } else {
                self.set_state(idx, 1 + velocity / 2, 1 + velocity / 2, 1 + velocity / 2);
            }
        }

        if self.delete || self.duplicate {
            return;
        }

        if let Some(pad) = self.session.grid.get_mut(&[x, y]) {
            if velocity > 0 {
                self.group_id += 1;

                pad.group_id = self.group_id;
                pad.active = true;
            } else {
                pad.active = false;
            }
        }

        if let Some(pad) = self.session.grid.get(&[x, y]) {
            if velocity > 0 {
                if pad.sidechain {
                    self.ballet_tx
                        .send(BalletMessage::Sidechain {
                            channel: 9,
                            velocity: velocity as f32 / 127.0,
                        })
                        .unwrap();
                }

                for (param, value) in pad.params.iter() {
                    self.param_queue.push((*param, *value));
                }

                for (note, enabled) in pad.scale_notes.iter() {
                    if self.session.get_note(*note) != *enabled {
                        self.scale_queue.push(*note);
                    }
                }

                for (note_id, (channel, note)) in pad.notes.iter().enumerate() {
                    self.ballet_tx
                        .send(BalletMessage::NoteOn {
                            channel: *channel,
                            id: BalletId {
                                group_id: pad.group_id,
                                voice_id: *note as usize,
                                note_id,
                            },
                            frequency: 440.0 * 2.0f32.powf((*note as f32 - 69.0) / 12.0),
                            velocity: velocity as f32 / 127.0,
                            mute: pad.mute,
                            latch: pad.latch,
                        })
                        .unwrap();
                }
            } else {
                for (note_id, (channel, note)) in pad.notes.iter().enumerate() {
                    self.ballet_tx
                        .send(BalletMessage::NoteOff {
                            channel: *channel,
                            id: BalletId {
                                group_id: pad.group_id,
                                voice_id: *note as usize,
                                note_id,
                            },
                            velocity: velocity as f32 / 127.0,
                            latch: pad.latch,
                        })
                        .unwrap();
                }
            }
        }

        for note in self.scale_queue.iter() {
            self.ballet_tx
                .send(BalletMessage::SetNote {
                    note: *note,
                    value: self.session.toggle_note(*note),
                })
                .unwrap();
        }

        self.scale_queue.clear();

        for (param, value) in self.param_queue.iter() {
            self.session.set_param(Some(*param), *value);

            match param.modulation_source {
                None => {
                    self.ballet_tx
                        .send(BalletMessage::SetParam {
                            param: param.parameter,
                            value: *value,
                        })
                        .unwrap();
                }
                Some(_) => {
                    if let Some(modulation) = self.session.get_modulation(*param) {
                        self.ballet_tx
                            .send(BalletMessage::SetModulation(modulation))
                            .unwrap();
                    }
                }
            }
        }

        self.param_queue.clear();
    }

    fn note_on_piano(&mut self, idx: u8, velocity: u8) {
        let note = self.note_to_piano_note(idx);

        if note != -128 {
            if velocity > 0 {
                if self.record {
                    self.bind_note(note);
                    return;
                }

                self.set_state(idx, 1 + velocity / 4, 1 + velocity / 16, 1 + velocity / 2);
                self.group_id += 1;

                if let Some(group_id) = self.piano_group_ids.insert(idx, self.group_id) {
                    self.ballet_tx
                        .send(BalletMessage::NoteOff {
                            channel: self.session.channel,
                            id: BalletId {
                                group_id,
                                voice_id: 0,
                                note_id: 0,
                            },
                            velocity: 0.0,
                            latch: self.shift,
                        })
                        .unwrap();
                }

                self.ballet_tx
                    .send(BalletMessage::NoteOn {
                        channel: self.session.channel,
                        id: BalletId {
                            group_id: self.group_id,
                            voice_id: 0,
                            note_id: 0,
                        },
                        frequency: 440.0 * 2.0f32.powf((note as f32 - 69.0) / 12.0),
                        velocity: velocity as f32 / 127.0,
                        mute: self.mute,
                        latch: self.shift,
                    })
                    .unwrap();
            } else if let Some(group_id) = self.piano_group_ids.remove(&idx) {
                self.ballet_tx
                    .send(BalletMessage::NoteOff {
                        channel: self.session.channel,
                        id: BalletId {
                            group_id,
                            voice_id: 0,
                            note_id: 0,
                        },
                        velocity: velocity as f32 / 127.0,
                        latch: self.shift,
                    })
                    .unwrap();
            }
        }
    }

    fn note_on_palette(&mut self, idx: u8, velocity: u8, steps: u8) {
        if self.show_palette && velocity > 0 {
            let r = self.session.base_color[0];
            let g = self.session.base_color[1];
            let b = self.session.base_color[2];

            let (h, s, l) = self.rgb_to_hsl(
                self.session.base_color[0],
                self.session.base_color[1],
                self.session.base_color[2],
            );

            match idx {
                11 => {
                    self.session.base_color[0] = (r as i8 - steps as i8).max(0) as u8;
                }
                12 => {
                    self.session.base_color[1] = (g as i8 - steps as i8).max(0) as u8;
                }
                13 => {
                    self.session.base_color[2] = (b as i8 - steps as i8).max(0) as u8;
                }
                14 => {
                    let (r, g, b) = self.hsl_to_rgb((h - steps as f32).rem_euclid(360.0), s, l);
                    self.session.base_color = [r, g, b];
                }
                15 => {
                    let (r, g, b) = self.hsl_to_rgb(h, (s - 0.01 * steps as f32).max(0.0), l);
                    self.session.base_color = [r, g, b];
                }
                16 => {
                    let (r, g, b) = self.hsl_to_rgb(h, s, (l - 0.01 * steps as f32).max(0.0));
                    self.session.base_color = [r, g, b];
                }
                21 => {
                    self.session.base_color[0] = (r as i8 + steps as i8).min(63) as u8;
                }
                22 => {
                    self.session.base_color[1] = (g as i8 + steps as i8).min(63) as u8;
                }
                23 => {
                    self.session.base_color[2] = (b as i8 + steps as i8).min(63) as u8;
                }
                24 => {
                    let (r, g, b) = self.hsl_to_rgb((h + steps as f32).rem_euclid(360.0), s, l);
                    self.session.base_color = [r, g, b];
                }
                25 => {
                    let (r, g, b) = self.hsl_to_rgb(h, (s + 0.01 * steps as f32).min(1.0), l);
                    self.session.base_color = [r, g, b];
                }
                26 => {
                    let (r, g, b) = self.hsl_to_rgb(h, s, (l + 0.01 * steps as f32).min(1.0));
                    self.session.base_color = [r, g, b];
                }
                _ => (),
            }
        }
    }

    fn toggle_modulation(&mut self, idx: usize) {
        self.modulation_source = match self.modulation_source {
            None => Some([idx, usize::MAX, usize::MAX, usize::MAX]),
            Some(mut modulation_source) => {
                if self.shift {
                    for modulation_idx in modulation_source.iter_mut() {
                        if *modulation_idx == idx {
                            *modulation_idx = usize::MAX;
                            break;
                        } else if *modulation_idx == usize::MAX {
                            *modulation_idx = idx;
                            break;
                        }
                    }
                } else {
                    if modulation_source[0] == idx {
                        modulation_source[0] = usize::MAX;
                    } else {
                        modulation_source[0] = idx;
                    }

                    if modulation_source.iter().any(|&x| x != usize::MAX) {
                        modulation_source[0] = idx;
                    }

                    for modulation_idx in modulation_source.iter_mut().skip(1) {
                        *modulation_idx = usize::MAX;
                    }
                }

                modulation_source.sort();

                if modulation_source.iter().all(|&x| x == usize::MAX) {
                    None
                } else {
                    Some(modulation_source)
                }
            }
        };
    }

    fn bind_note(&mut self, note: i16) {
        for coord in self.selected.iter() {
            if let Some(pad) = self.session.grid.get_mut(coord) {
                let has_added_note = if self.shift {
                    pad.add_note(self.session.channel, note);
                    true
                } else {
                    pad.toggle_note(self.session.channel, note)
                };

                if has_added_note {
                    if pad.active {
                        self.ballet_tx
                            .send(BalletMessage::NoteOn {
                                channel: self.session.channel,
                                id: BalletId {
                                    group_id: pad.group_id,
                                    voice_id: note as usize,
                                    note_id: pad.notes.len(),
                                },
                                frequency: 440.0 * 2.0f32.powf((note as f32 - 69.0) / 12.0),
                                velocity: 1.0,
                                mute: pad.latch,
                                latch: pad.mute,
                            })
                            .unwrap();
                    }
                } else if pad.active {
                    self.ballet_tx
                        .send(BalletMessage::NoteOff {
                            channel: self.session.channel,
                            id: BalletId {
                                group_id: pad.group_id,
                                voice_id: note as usize,
                                note_id: pad.notes.len() + 1,
                            },
                            velocity: 0.0,
                            latch: pad.latch,
                        })
                        .unwrap();
                }
            }
        }
    }

    pub fn note_off(&mut self, channel: u8, idx: u8, _: u8) {
        self.note_on(channel, idx, 0);
    }

    fn tap(&mut self) -> f32 {
        self.taps[self.tap_idx] = self.tap_time.elapsed().as_millis();
        self.tap_idx = (self.tap_idx + 1) % 4;
        self.tap_time = std::time::Instant::now();

        if self.taps.iter().any(|x| *x == 0) {
            -1.0
        } else {
            self.taps.iter().sum::<u128>() as f32 / 4000.0
        }
    }

    pub fn key_pressure(&mut self, _channel: u8, idx: u8, velocity: u8) {
        let on_grid = self.pad_on_grid[idx as usize];

        if !on_grid && self.menu_mode.is_some() && idx > (90 - 10 * self.menu_height) {
            if !self.click
                && velocity > 0
                && velocity / 8 > self.peak_key_pressure[idx as usize] / 8
            {
                let steps = velocity / 8 - self.peak_key_pressure[idx as usize] / 8;
                self.peak_key_pressure[idx as usize] = velocity;
                self.note_on_menu(idx, velocity, steps, true);
            }
        } else if !on_grid && idx < (30 * self.show_palette as u8) {
            if velocity > 0 && velocity / 8 > self.peak_key_pressure[idx as usize] / 8 {
                let steps = velocity / 8 - self.peak_key_pressure[idx as usize] / 8;
                self.peak_key_pressure[idx as usize] = velocity;
                self.note_on_palette(idx, velocity, steps);
            }
        } else if idx > (90 - 20 * self.session.piano_octaves) {
            let note = self.note_to_piano_note(idx);

            if note != -128 {
                if let Some(group_id) = self.piano_group_ids.get(&idx) {
                    self.ballet_tx
                        .send(BalletMessage::Morph {
                            channel: 0,
                            id: BalletId {
                                group_id: *group_id,
                                voice_id: 0,
                                note_id: 0,
                            },
                            axis: 1,
                            amount: velocity as f32 / 127.0,
                        })
                        .unwrap();
                }
            }
        } else {
            let (x, y) = self.note_to_grid_xy(idx);

            if let Some(pad) = self.session.grid.get_mut(&[x, y]) {
                self.ballet_tx
                    .send(BalletMessage::Morph {
                        channel: 0,
                        id: BalletId {
                            group_id: pad.group_id,
                            voice_id: 0,
                            note_id: 0,
                        },
                        axis: 1,
                        amount: velocity as f32 / 127.0,
                    })
                    .unwrap();
            }
        }

        self.redraw();
    }

    pub fn sysex(&mut self, data: Vec<u8>) {
        // This SysEx message is sent when the Launchpad Pro enters its default mode, which
        // it does on start-up.  If the Launchpad Pro is hotplugged, this code ensures that
        // the device is automatically changed to programmer mode.
        if data
            .iter()
            .zip(vec![240, 0, 32, 41, 2, 16, 47, 0, 247])
            .all(|(&x, y)| x == y)
        {
            self.initialize();
        }
    }

    fn initialize(&mut self) {
        // Select programmer mode.
        self.midi_tx
            .send(MidiMessage::Sysex {
                addr: self.addr.unwrap(),
                data: vec![240, 0, 32, 41, 2, 16, 44, 3, 247],
            })
            .unwrap();

        self.clear_display();

        if !self.is_initialized {
            self.load_session(self.global_state.session_idx);
            self.session_page = (self.global_state.session_idx / 32) as u8;
            self.patch_page =
                (self.session.channel_patch_idx[self.session.channel as usize - 1] / 32) as u8;
            self.is_initialized = true;
        }

        self.redraw();
    }

    fn load_session(&mut self, session_idx: usize) {
        for pad in self.session.grid.values() {
            if pad.active {
                for (note_id, (channel, note)) in pad.notes.iter().enumerate() {
                    self.ballet_tx
                        .send(BalletMessage::NoteOff {
                            channel: *channel,
                            id: BalletId {
                                group_id: pad.group_id,
                                voice_id: *note as usize,
                                note_id,
                            },
                            velocity: 0.0,
                            latch: pad.latch,
                        })
                        .unwrap();
                }
            }
        }

        self.global_state.session_idx = session_idx;
        self.global_state.write_ballet("global.ron");
        self.session =
            Session::read_ballet_with_logger(&format!("{}.ron", session_idx), &self.action_tx);

        self.set_piano_octaves(self.session.piano_octaves);

        self.ballet_tx
            .send(BalletMessage::LoadMasterPatch {
                patch: Box::new(self.session.master_patch.clone()),
                panic: !self.shift,
                mute: self.shift,
            })
            .unwrap();

        for (voice_idx, &channel) in self.session.voice_channels.iter().enumerate() {
            self.ballet_tx
                .send(BalletMessage::LoadPatch {
                    voice_idx,
                    patch: Box::new(self.session.get_channel_patch(channel)),
                })
                .unwrap();
        }
    }

    fn save_session(&mut self, session_idx: usize) {
        self.global_state.session_idx = session_idx;
        self.global_state.write_ballet("global.ron");
        self.session.write_ballet(&format!("{}.ron", session_idx));
        SessionMetadata::from_color(self.session.base_color)
            .write_ballet(&format!("{}.meta.ron", session_idx));
    }

    fn remove_session(&mut self, session_idx: usize) {
        let from_path = paths::ballet_path(&format!("{}.ron", session_idx));
        let to_path = paths::ballet_path(&format!("removed_{}.ron", session_idx));
        let dir = Path::new(&to_path).parent().unwrap();
        std::fs::create_dir_all(dir).unwrap();
        std::fs::rename(from_path, &to_path).unwrap_or_default();

        let from_path = paths::ballet_path(&format!("{}.meta.ron", session_idx));
        let to_path = paths::ballet_path(&format!("removed_{}.meta.ron", session_idx));
        let dir = Path::new(&to_path).parent().unwrap();
        std::fs::create_dir_all(dir).unwrap();
        std::fs::rename(from_path, &to_path).unwrap_or_default();
    }

    fn load_patch(&mut self, patch_idx: usize) -> Patch {
        self.session.channel_patch_idx[self.session.channel as usize - 1] = patch_idx;
        Patch::read_ballet(&format!("patch_{}.ron", patch_idx))
    }

    fn save_patch(&mut self, patch_idx: usize, patch: Patch) {
        self.session.channel_patch_idx[self.session.channel as usize - 1] = patch_idx;
        self.global_state.write_ballet("global.ron");
        SessionMetadata::from_color(self.session.base_color)
            .write_ballet(&format!("patch_{}.meta.ron", patch_idx));
        patch.write_ballet(&format!("patch_{}.ron", patch_idx));
    }

    fn remove_patch(&mut self, patch_idx: usize) {
        let from_path = paths::ballet_path(&format!("patch_{}.ron", patch_idx));
        let to_path = paths::ballet_path(&format!("removed_patch_{}.ron", patch_idx));
        let dir = Path::new(&to_path).parent().unwrap();
        std::fs::create_dir_all(dir).unwrap();
        std::fs::rename(from_path, &to_path).unwrap_or_default();

        let from_path = paths::ballet_path(&format!("patch_{}.meta.ron", patch_idx));
        let to_path = paths::ballet_path(&format!("removed_patch_{}.meta.ron", patch_idx));
        let dir = Path::new(&to_path).parent().unwrap();
        std::fs::create_dir_all(dir).unwrap();
        std::fs::rename(from_path, &to_path).unwrap_or_default();
    }

    fn enter_param_menu(&mut self, idx: u8) {
        let menu = match self.param_menu_page {
            0 => match idx {
                0 => Some(MenuMode::Param(ParamMenu::Voice)),
                1 => Some(MenuMode::Param(ParamMenu::Oscillator)),
                2 => Some(MenuMode::Param(ParamMenu::TunedDelay)),
                3 => Some(MenuMode::Param(ParamMenu::Filter)),
                4 => Some(MenuMode::Param(ParamMenu::Waveshaper)),
                5 => Some(MenuMode::Param(ParamMenu::Envelope)),
                6 => Some(MenuMode::Param(ParamMenu::Lfo)),
                7 => Some(MenuMode::Param(ParamMenu::Accumulator)),
                _ => None,
            },
            1 => match idx {
                0 => Some(MenuMode::Param(ParamMenu::Mixer)),
                1 => Some(MenuMode::Param(ParamMenu::Vocal)),
                2 => Some(MenuMode::Param(ParamMenu::Delay)),
                3 => Some(MenuMode::Param(ParamMenu::Compressor)),
                5 => Some(MenuMode::Param(ParamMenu::Sidechain)),
                6 => Some(MenuMode::Param(ParamMenu::GlobalLfo)),
                7 => Some(MenuMode::Param(ParamMenu::GlobalAccumulator)),
                _ => None,
            },
            _ => None,
        };

        if self.menu_mode == menu {
            self.enter_menu(None);
        } else {
            self.param_menu_idx = idx;
            self.enter_menu(menu);
        }
    }

    fn enter_menu(&mut self, menu: Option<MenuMode>) {
        self.menu_mode = menu;

        // Enter new menu, if any.
        match self.menu_mode {
            Some(MenuMode::Session) => {
                self.menu_height = 4;
                self.save_mode = false;
                self.global_state = GlobalState::read_ballet("global.ron");
                self.param_menu_idx = 255;
            }
            Some(MenuMode::ChannelSelector) => {
                self.menu_height = 8;
                self.save_mode = false;
                self.global_state = GlobalState::read_ballet("global.ron");
                self.param_menu_idx = 255;
            }
            Some(MenuMode::VoiceConfiguration) => {
                self.menu_height = 8;
                self.param_menu_idx = 255;
            }
            Some(MenuMode::Param(_)) => {
                self.menu_height = 4 + 3 * self.show_modulation_sources as u8;

                if !self.show_modulation_sources {
                    self.modulation_source = None;
                    self.bounds_mode = None;
                }
            }
            None => {
                self.show_modulation_sources = false;

                if !self.show_modulation_sources {
                    self.modulation_source = None;
                    self.bounds_mode = None;
                }
                self.menu_height = 0;
                self.param_menu_idx = 255;
            }
        }
    }

    fn set_page(&mut self, value: u8) {
        if let Some(menu_mode) = self.menu_mode {
            if menu_mode == MenuMode::Session {
                self.session_page = value;
            } else if menu_mode == MenuMode::ChannelSelector {
                self.patch_page = value;
            } else {
                self.session.set_page(menu_mode, value);
            }
        }
    }

    fn get_parameter_type(&self, page: u8, idx: u8) -> Option<Param> {
        let parameter_type =
            menu::get_parameter_type(self.menu_mode, self.session.channel, page, idx);

        parameter_type.map(|parameter_type| Param {
            parameter: parameter_type,
            modulation_source: self.modulation_source,
            bounds_mode: self.bounds_mode,
        })
    }

    fn set_param(&mut self, param: Option<Param>, value: f32) {
        if self.delete && self.record {
            for coord in self.selected.iter() {
                if let Some(pad) = self.session.grid.get_mut(coord) {
                    if let Some(x) = param {
                        pad.remove_param(x);
                    }
                }
            }
        } else {
            self.session.set_param(param, value);

            if self.record {
                for coord in self.selected.iter() {
                    if let Some(pad) = self.session.grid.get_mut(coord) {
                        if let Some(x) = param {
                            pad.add_param(x, value);
                        }
                    }
                }
            }
        }
    }

    fn shift_param(&mut self, param: Option<Param>, value: f32) {
        if self.delete && self.record {
            for coord in self.selected.iter() {
                if let Some(pad) = self.session.grid.get_mut(coord) {
                    if let Some(x) = param {
                        pad.remove_param(x);
                    }
                }
            }
        } else {
            self.session.shift_param(param, value);

            if self.record {
                let value = self.session.get_param(param).map_or(0.0, |x| x.x);

                for coord in self.selected.iter() {
                    if let Some(pad) = self.session.grid.get_mut(coord) {
                        if let Some(x) = param {
                            pad.add_param(x, value);
                        }
                    }
                }
            }
        }
    }

    fn redraw(&mut self) {
        self.draw_grid();
        self.draw_piano();
        self.draw_menu();
        self.draw_palette();
        self.draw_buttons();
        self.flip_display();
    }

    fn draw_coord(&mut self, x: isize, y: isize) {
        // Draw the grid inside
        let color = match self.session.grid.get(&[x, y]) {
            Some(pad) => pad.color,
            None => [0, 0, 0],
        };

        let x0 = x + self.session.offset[0];
        let y0 = y + self.session.offset[1];

        if (0..8).contains(&x0) && (0..8).contains(&y0) {
            let idx = 10 * (y0 as u8 + 1) + x0 as u8 + 1;

            self.set_base_state(idx, color[0], color[1], color[2]);

            if self.record && self.selected.contains(&[x, y]) {
                self.set_flash(idx);
            } else {
                self.unset_flash(idx);
            }
        }
    }

    fn draw_grid(&mut self) {
        for x in 0..8 {
            for y in 0..8 {
                self.draw_coord(
                    x as isize - self.session.offset[0],
                    y as isize - self.session.offset[1],
                );
            }
        }
    }

    fn set_piano_octaves(&mut self, octaves: u8) {
        self.session.piano_octaves = octaves;
    }

    fn draw_arrow_buttons(&mut self) {
        if let Some(MenuMode::Session) = self.menu_mode {
            if self.save_mode {
                self.set_base_state(91, 0, 32, 0);
            }
        } else if let Some(MenuMode::ChannelSelector) = self.menu_mode {
            if self.save_mode {
                self.set_base_state(91, 0, 32, 0);
            }
        } else if self.show_modulation_sources {
            if self.modulation_source.is_some() {
                match self.bounds_mode {
                    Some(Bounds::Lower) => {
                        self.set_base_state(91, 3, 0, 2);
                        self.set_base_state(92, 63, 48, 48);
                    }
                    Some(Bounds::Upper) => {
                        self.set_base_state(91, 63, 48, 48);
                        self.set_base_state(92, 3, 0, 2);
                    }
                    Some(Bounds::Offset) => {
                        self.set_base_state(91, 63, 48, 48);
                        self.set_base_state(92, 63, 48, 48);
                    }
                    None => {
                        self.set_base_state(91, 3, 0, 2);
                        self.set_base_state(92, 3, 0, 2);
                    }
                }
            } else {
                match self.bounds_mode {
                    Some(Bounds::Lower) => {
                        self.set_base_state(91, 63, 16, 63);
                        self.set_base_state(92, 63, 16, 63);
                    }
                    _ => {
                        self.set_base_state(91, 2, 2, 2);
                        self.set_base_state(92, 2, 2, 2);
                    }
                }
            }
        } else if self.menu_mode.is_none() && self.session.piano_octaves > 0 {
            match self.session.base_octave.cmp(&4) {
                std::cmp::Ordering::Greater => {
                    let color = (16 * (self.session.base_octave - 4)).min(63);
                    self.set_base_state(91, color as u8, color as u8, color as u8);
                }
                std::cmp::Ordering::Less => {
                    let color = (16 * (4 - self.session.base_octave)).min(63);
                    self.set_base_state(92, color as u8, color as u8, color as u8);
                }
                _ => (),
            }

            match self.session.shift_piano.cmp(&0) {
                std::cmp::Ordering::Less => {
                    let color = (16 * (-self.session.shift_piano)).min(63);
                    self.set_base_state(93, color as u8, color as u8, color as u8);
                }
                std::cmp::Ordering::Greater => {
                    let color = (16 * (self.session.shift_piano)).min(63);
                    self.set_base_state(94, color as u8, color as u8, color as u8);
                }
                _ => (),
            }
        }
    }

    fn draw_buttons(&mut self) {
        for idx in 1..9 {
            self.set_base_state(idx, 0, 0, 0);
            self.set_base_state(10 * idx, 0, 0, 0);
            self.set_base_state(9 + 10 * idx, 0, 0, 0);
            self.set_base_state(90 + idx, 0, 0, 0);
        }

        if !self.global_state.locked {
            self.set_base_state(1, 2, 2, 2);
            self.set_base_state(8, 2, 2, 2);
            self.set_base_state(10, 2, 2, 2);
            self.set_base_state(80, 2, 2, 2);
        }

        if self.show_palette {
            self.set_base_state(
                2,
                self.active_color[0],
                self.active_color[1],
                self.active_color[2],
            );
        }

        if self.solo {
            self.set_base_state(
                4,
                self.active_color[0],
                self.active_color[1],
                self.active_color[2],
            );
        }

        if self.record {
            let mut has_sidechain = false;
            let mut has_mute = false;
            let mut has_latch = false;

            for coord in self.selected.iter() {
                if let Some(pad) = self.session.grid.get(coord) {
                    if pad.latch {
                        has_latch = true;
                    }

                    if pad.mute {
                        has_mute = true;
                    }

                    if pad.sidechain {
                        has_sidechain = true;
                    }
                }
            }

            self.set_base_state(10, 63, 0, 0);

            if has_latch {
                self.set_base_state(20, 18, 54, 63);
            } else if !self.selected.is_empty() {
                self.set_base_state(20, 2, 2, 2);
            }

            if has_mute {
                self.set_base_state(3, 18, 0, 0);
            } else if !self.selected.is_empty() {
                self.set_base_state(3, 2, 2, 2);
            }

            if has_sidechain {
                self.set_base_state(70, 4, 0, 63);
            } else if !self.selected.is_empty() {
                self.set_base_state(70, 2, 2, 2);
            }
        }

        self.draw_arrow_buttons();

        if let Some(menu_mode) = self.menu_mode {
            let page = if menu_mode == MenuMode::Session {
                self.session_page
            } else if menu_mode == MenuMode::ChannelSelector {
                self.patch_page
            } else {
                self.session.get_page(menu_mode)
            };
            let page_count = menu::get_page_count(menu_mode);

            if page > 0 {
                self.set_base_state(93, 32, 32, 32);
            }

            if page < page_count - 1 {
                self.set_base_state(94, 32, 32, 32);
            }
        }

        if self.session.piano_octaves > 0 {
            self.set_base_state(
                96,
                self.active_color[0],
                self.active_color[1],
                self.active_color[2],
            );
        }

        if self.show_modulation_sources {
            self.set_base_state(
                7,
                self.active_color[0],
                self.active_color[1],
                self.active_color[2],
            );
        }

        if self.param_menu_page > 0 {
            self.set_base_state(
                6,
                self.active_color[0],
                self.active_color[1],
                self.active_color[2],
            );
        }

        match self.param_menu_page {
            0 => {
                self.set_base_state(89, 4, 1, 2);
                self.set_base_state(79, 1, 2, 4);
                self.set_base_state(69, 1, 4, 4);
                self.set_base_state(59, 4, 3, 1);
                self.set_base_state(49, 4, 3, 1);
                self.set_base_state(39, 2, 4, 1);
                self.set_base_state(29, 4, 2, 0);
                self.set_base_state(19, 0, 0, 4);
            }
            1 => {
                self.set_base_state(89, 4, 1, 4);
                self.set_base_state(79, 2, 2, 2);
                self.set_base_state(69, 0, 2, 4);
                self.set_base_state(59, 4, 2, 1);
                self.set_base_state(49, 255, 255, 255);
                self.set_base_state(39, 0, 3, 1);
                self.set_base_state(29, 4, 2, 0);
                self.set_base_state(19, 0, 0, 4);
            }
            _ => (),
        }

        let idx = match self.menu_mode {
            Some(MenuMode::Session) => 95,
            Some(MenuMode::ChannelSelector) => 97,
            Some(MenuMode::VoiceConfiguration) => 98,
            Some(MenuMode::Param(ParamMenu::Voice)) => 89,
            Some(MenuMode::Param(ParamMenu::Oscillator)) => 79,
            Some(MenuMode::Param(ParamMenu::TunedDelay)) => 69,
            Some(MenuMode::Param(ParamMenu::Filter)) => 59,
            Some(MenuMode::Param(ParamMenu::Waveshaper)) => 49,
            Some(MenuMode::Param(ParamMenu::Envelope)) => 39,
            Some(MenuMode::Param(ParamMenu::Lfo)) => 29,
            Some(MenuMode::Param(ParamMenu::Accumulator)) => 19,
            Some(MenuMode::Param(ParamMenu::Mixer)) => 89,
            Some(MenuMode::Param(ParamMenu::Vocal)) => 79,
            Some(MenuMode::Param(ParamMenu::Delay)) => 69,
            Some(MenuMode::Param(ParamMenu::Compressor)) => 59,
            Some(MenuMode::Param(ParamMenu::Sidechain)) => 39,
            Some(MenuMode::Param(ParamMenu::GlobalLfo)) => 29,
            Some(MenuMode::Param(ParamMenu::GlobalAccumulator)) => 19,
            None => 0,
        };

        if idx > 0 {
            self.set_base_state(idx, 63, 63, 63);
        }
    }

    fn draw_menu(&mut self) {
        match self.menu_mode {
            Some(MenuMode::Session) => {
                for row in 0..4 {
                    for col in 0..8 {
                        let idx = ((80 - 10 * row) + col + 1) as u8;
                        let session_idx = col + 8 * row + 32 * self.session_page as usize;

                        if self.global_state.session_idx == session_idx {
                            self.set_base_state(idx, 63, 63, 63);
                        } else {
                            let color = if std::path::Path::new(&paths::ballet_path(&format!(
                                "{}.ron",
                                session_idx
                            )))
                            .exists()
                            {
                                let session_metadata = SessionMetadata::read_ballet(&format!(
                                    "{}.meta.ron",
                                    &session_idx
                                ));

                                if session_metadata.color[0] == 255 {
                                    [32, 8, 32]
                                } else {
                                    session_metadata.color
                                }
                            } else {
                                let (r, g, b) = self.hsl_to_rgb(
                                    (-60.0 - 60.0 * self.session_page as f32).rem_euclid(360.0),
                                    1.0,
                                    0.01,
                                );
                                [r, g, b]
                            };

                            self.set_base_state(idx, color[0], color[1], color[2]);
                        }
                    }
                }
            }

            Some(MenuMode::ChannelSelector) => {
                for row in 0..2 {
                    for x in 0..8 {
                        let idx = (80 - 10 * row) + x + 1;
                        let channel = 1 + x + 8 * row;

                        if self.session.channel == channel {
                            self.set_base_state(idx, 32, 63, 32);
                        } else if self
                            .session
                            .voice_channels
                            .iter()
                            .any(|voice_channel| *voice_channel == channel)
                        {
                            self.set_base_state(idx, 4, 16, 4);
                        } else {
                            self.set_base_state(idx, 0, 2, 0);
                        }
                    }
                }

                for row in 2..4 {
                    for x in 0..8 {
                        let idx = (80 - 10 * row) + x + 1;
                        let voice_idx = x + 8 * (row - 2);
                        let voice_channel = self.session.voice_channels[voice_idx];

                        if self.session.channel == voice_channel {
                            self.set_base_state(idx as u8, 32, 32, 63);
                        } else if voice_channel == 255 {
                            self.set_base_state(idx as u8, 2, 2, 8);
                        } else {
                            self.set_base_state(idx as u8, 2, 0, 0);
                        }
                    }
                }

                for row in 4..8 {
                    for col in 0..8 {
                        let idx = ((80 - 10 * row) + col + 1) as u8;
                        let patch_idx = col + 8 * (row - 4) + 32 * self.patch_page as usize;

                        if self.session.channel > 0
                            && self.session.channel <= 16
                            && self.session.channel_patch_idx[self.session.channel as usize - 1]
                                == patch_idx
                        {
                            self.set_base_state(idx, 63, 63, 63);
                        } else {
                            let color = if std::path::Path::new(&paths::ballet_path(&format!(
                                "patch_{}.ron",
                                patch_idx
                            )))
                            .exists()
                            {
                                let patch_metadata = SessionMetadata::read_ballet(&format!(
                                    "patch_{}.meta.ron",
                                    &patch_idx
                                ));

                                if patch_metadata.color[0] == 255 {
                                    [32, 8, 32]
                                } else {
                                    patch_metadata.color
                                }
                            } else {
                                let (r, g, b) = self.hsl_to_rgb(
                                    (-60.0 - 60.0 * self.patch_page as f32).rem_euclid(360.0),
                                    1.0,
                                    0.01,
                                );
                                [r, g, b]
                            };

                            self.set_base_state(idx, color[0], color[1], color[2]);
                        }
                    }
                }
            }

            Some(MenuMode::VoiceConfiguration) => {
                let page = self.session.get_page(MenuMode::VoiceConfiguration);

                if page == 0 {
                    for row in 0..2 {
                        for col in 0..8 {
                            let disabled = match row {
                                0 => match col {
                                    0 => self.session.get_config(
                                        self.session.channel,
                                        BalletConfig::DisableOsc1,
                                    ),
                                    1 => self.session.get_config(
                                        self.session.channel,
                                        BalletConfig::DisableOsc2,
                                    ),
                                    2 => self.session.get_config(
                                        self.session.channel,
                                        BalletConfig::DisableTunedDelay,
                                    ),
                                    3 => self.session.get_config(
                                        self.session.channel,
                                        BalletConfig::DisableFlt1,
                                    ),
                                    4 => self.session.get_config(
                                        self.session.channel,
                                        BalletConfig::DisableFlt2,
                                    ),
                                    5 => self.session.get_config(
                                        self.session.channel,
                                        BalletConfig::DisableWshp,
                                    ),
                                    6 => self.session.get_config(
                                        self.session.channel,
                                        BalletConfig::DisableDelay,
                                    ),
                                    7 => self
                                        .session
                                        .get_master_config(BalletMasterConfig::DisablePitchTracker),
                                    _ => 0,
                                },
                                1 => match col {
                                    0 => self.session.get_config(
                                        self.session.channel,
                                        BalletConfig::DisableEnv1,
                                    ),
                                    1 => self.session.get_config(
                                        self.session.channel,
                                        BalletConfig::DisableEnv2,
                                    ),
                                    2 => self.session.get_config(
                                        self.session.channel,
                                        BalletConfig::DisableLfo1,
                                    ),
                                    3 => self.session.get_config(
                                        self.session.channel,
                                        BalletConfig::DisableLfo2,
                                    ),
                                    4 => self
                                        .session
                                        .get_master_config(BalletMasterConfig::DisableVocal),
                                    5 => self
                                        .session
                                        .get_master_config(BalletMasterConfig::DisableDelay),
                                    6 => self
                                        .session
                                        .get_master_config(BalletMasterConfig::DisableCompressor),
                                    7 => self
                                        .session
                                        .get_master_config(BalletMasterConfig::DisableFilter),
                                    _ => 0,
                                },
                                _ => 0,
                            };

                            let idx = (80 - row * 10 + col + 1) as u8;

                            if disabled == 0 {
                                match row {
                                    0 => match col {
                                        0..=2 => self.set_base_state(idx, 16, 32, 63),
                                        3..=5 => self.set_base_state(idx, 63, 40, 16),
                                        6 => self.set_base_state(idx, 63, 16, 32),
                                        7 => self.set_base_state(idx, 48, 48, 63),
                                        _ => self.set_base_state(idx, 255, 255, 255),
                                    },
                                    1 => match col {
                                        0..=1 => self.set_base_state(idx, 63, 63, 8),
                                        2..=3 => self.set_base_state(idx, 63, 32, 0),
                                        4 => self.set_base_state(idx, 63, 48, 48),
                                        5 => self.set_base_state(idx, 8, 48, 32),
                                        6 => self.set_base_state(idx, 32, 8, 0),
                                        7 => self.set_base_state(idx, 63, 40, 16),
                                        _ => self.set_base_state(idx, 255, 255, 255),
                                    },
                                    _ => self.set_base_state(idx, 255, 255, 255),
                                }
                            } else {
                                match row {
                                    0 => match col {
                                        0..=2 => self.set_base_state(idx, 1, 2, 4),
                                        3..=5 => self.set_base_state(idx, 4, 2, 1),
                                        6 => self.set_base_state(idx, 4, 0, 2),
                                        7 => self.set_base_state(idx, 2, 2, 4),
                                        _ => self.set_base_state(idx, 255, 255, 255),
                                    },
                                    1 => match col {
                                        0..=1 => self.set_base_state(idx, 4, 4, 0),
                                        2..=3 => self.set_base_state(idx, 4, 2, 0),
                                        4 => self.set_base_state(idx, 4, 2, 2),
                                        5 => self.set_base_state(idx, 1, 3, 2),
                                        6 => self.set_base_state(idx, 3, 1, 0),
                                        7 => self.set_base_state(idx, 4, 4, 1),
                                        _ => self.set_base_state(idx, 255, 255, 255),
                                    },
                                    _ => self.set_base_state(idx, 255, 255, 255),
                                }
                            }
                        }
                    }

                    for x in 0..8 {
                        let idx = (60 + x + 1) as u8;

                        let master_rate_divider = self
                            .session
                            .get_master_config(BalletMasterConfig::RateDivider);

                        if x == master_rate_divider {
                            self.set_base_state(idx, 48, 32, 63);
                        } else {
                            self.set_base_state(idx, 2, 2, 3);
                        }

                        let idx = (50 + x + 1) as u8;

                        let patch_rate_divider = self
                            .session
                            .get_config(self.session.channel, BalletConfig::RateDivider);

                        if x == patch_rate_divider {
                            self.set_base_state(idx, 32, 32, 63);
                        } else {
                            self.set_base_state(idx, 1, 1, 3);
                        }
                    }

                    for row in 4..6 {
                        for x in 0..8 {
                            let idx = ((80 - 10 * row) + x + 1) as u8;
                            let channel = x + 8 * (row - 4);

                            let sidechain_channel = self
                                .session
                                .get_master_config(BalletMasterConfig::SidechainChannel);

                            if channel == sidechain_channel {
                                self.set_base_state(idx, 63, 32, 48);
                            } else {
                                self.set_base_state(idx, 2, 0, 1);
                            }
                        }
                    }

                    for x in 0..8 {
                        let idx = (20 + x + 1) as u8;

                        let output_channel_left = self
                            .session
                            .get_config(self.session.channel, BalletConfig::OutputChannelLeft);

                        let output_channel_right = self
                            .session
                            .get_config(self.session.channel, BalletConfig::OutputChannelRight);

                        if x == output_channel_left && x == output_channel_right {
                            self.set_base_state(idx, 63, 63, 63);
                        } else if x == output_channel_left {
                            self.set_base_state(idx, 63, 48, 32);
                        } else if x == output_channel_right {
                            self.set_base_state(idx, 32, 48, 63);
                        } else {
                            self.set_base_state(idx, 2, 2, 2);
                        }

                        let input_source = self
                            .session
                            .get_master_config(BalletMasterConfig::InputSource);

                        let idx = (10 + x + 1) as u8;

                        if x == input_source {
                            self.set_base_state(idx, 63, 32, 32);
                        } else {
                            self.set_base_state(idx, 3, 1, 1);
                        }
                    }
                } else if page == 1 {
                    let root_note =
                        self.session.get_master_config(BalletMasterConfig::RootNote) as i16;

                    self.draw_enabled_notes(0, root_note);

                    for x in 0..8 {
                        let tuning = self.session.get_master_config(BalletMasterConfig::Tuning);

                        let idx = (60 + x + 1) as u8;

                        if x == tuning {
                            self.set_base_state(idx, 32, 48, 63);
                        } else {
                            self.set_base_state(idx, 0, 1, 2);
                        }

                        for col in 0..5 {
                            let idx = ((col + 1) * 10 + x + 1) as u8;
                            self.set_base_state(idx, 255, 255, 255);
                        }
                    }
                }
            }

            Some(MenuMode::Param(dest)) => {
                if self.show_modulation_sources {
                    self.draw_modulation_sources();
                }

                let page = self.session.get_page(MenuMode::Param(dest));

                for col in 0..8 {
                    let param = self.get_parameter_type(page, col);

                    let (value, default, group) = match self.session.get_param(param) {
                        None => (-1.0, 0.0, 0),
                        Some(value) => (value.relative, value.relative_default, value.group),
                    };

                    let mut is_bound = false;

                    if self.record {
                        for coord in self.selected.iter() {
                            if let Some(pad) = self.session.grid.get_mut(coord) {
                                if param.is_some_and(|x| pad.has_param(x)) {
                                    is_bound = true;
                                    break;
                                }
                            }
                        }
                    }

                    self.draw_param(col, value, default, group, is_bound);
                }
            }

            None => (),
        }
    }

    fn draw_param(&mut self, col: u8, value: f32, default: f32, group: u8, is_bound: bool) {
        let is_default = (value - default).abs() < 1.0 / 9600.0;

        let color0 = if is_bound {
            [4.0, 0.0, 0.0]
        } else if group % 3 == 0 {
            [4.0, 0.0, 2.0]
        } else if group % 3 == 1 {
            [3.0, 0.0, 3.0]
        } else {
            [5.0, 0.0, 1.0]
        };
        let color1 = if is_bound {
            [63.0, 0.0, 0.0]
        } else if group % 3 == 0 {
            [63.0, 8.0, 32.0]
        } else if group % 3 == 1 {
            [63.0, 0.0, 63.0]
        } else {
            [63.0, 16.0, 24.0]
        };
        let color2 = if is_bound {
            [63.0, 24.0, 24.0]
        } else if group % 3 == 0 {
            [63.0, 24.0, 48.0]
        } else if group % 3 == 1 {
            [63.0, 24.0, 63.0]
        } else {
            [63.0, 32.0, 48.0]
        };

        let row_offset = 3 * self.show_modulation_sources as u8;

        for row in 0..4 {
            let idx = (80 - 10 * (row + row_offset)) + col + 1;

            let color = if value < 0.0 {
                [4.0, 4.0, 4.0]
            } else {
                let x = (4.0 * value - row as f32).min(1.0).max(0.0);

                if is_default {
                    [
                        color0[0] + x * (color2[0] - color0[0]),
                        color0[1] + x * (color2[1] - color0[1]),
                        color0[2] + x * (color2[2] - color0[2]),
                    ]
                } else {
                    [
                        color0[0] + x * (color1[0] - color0[0]),
                        color0[1] + x * (color1[1] - color0[1]),
                        color0[2] + x * (color1[2] - color0[2]),
                    ]
                }
            };

            self.set_base_state(idx, color[0] as u8, color[1] as u8, color[2] as u8);
        }
    }

    fn draw_enabled_notes(&mut self, row: u8, root_note: i16) {
        for y in 0..2 {
            for x in 0..8 {
                self.draw_enabled_note(80 - 10 * (row + y) + 1 + x, root_note);
            }
        }
    }

    fn draw_enabled_note(&mut self, idx: u8, root_note: i16) {
        let note = self.note_to_piano_note(idx);
        let is_root = note.rem_euclid(12) == root_note;

        let mut is_bound = false;
        let mut is_enabled = false;

        if self.record {
            for coord in self.selected.iter() {
                if let Some(pad) = self.session.grid.get(coord) {
                    for (scale_note, enabled) in pad.scale_notes.iter() {
                        if *scale_note == note.rem_euclid(12) {
                            is_bound = true;
                            is_enabled = *enabled;
                            break;
                        }
                    }
                }
            }
        }

        if note == -128 {
            self.set_base_state(idx, 255, 255, 255);
        } else if is_bound && is_enabled {
            if is_root {
                self.set_base_state(idx, 63, 48, 18);
            } else {
                self.set_base_state(idx, 63, 18, 18);
            }
        } else if is_bound && !is_enabled {
            if is_root {
                self.set_base_state(idx, 18, 8, 0);
            } else {
                self.set_base_state(idx, 18, 0, 0);
            }
        } else if self.session.get_note(note) {
            if is_root {
                self.set_base_state(idx, 63, 63, 0);
            } else {
                self.set_base_state(idx, 18, 63, 54);
            }
        } else if is_root {
            self.set_base_state(idx, 63, 63, 32);
        } else if idx % 20 > 10 {
            self.set_base_state(idx, 63, 48, 48);
        } else {
            self.set_base_state(idx, 18, 16, 18);
        }
    }

    fn draw_modulation_sources(&mut self) {
        for row in 0..3 {
            for x in 0..8 {
                let idx = ((80 - 10 * row) + x + 1) as u8;
                let source_idx = x + 8 * row;

                match source_idx {
                    source_idx
                        if self
                            .modulation_source
                            .map_or(false, |x| x.iter().any(|&x| x == source_idx)) =>
                    {
                        self.set_base_state(idx, 48, 32, 63)
                    }
                    0..=1 => self.set_base_state(idx, 4, 0, 2),
                    2..=5 => self.set_base_state(idx, 2, 0, 4),
                    6..=7 => self.set_base_state(idx, 0, 0, 4),
                    8..=11 => self.set_base_state(idx, 4, 0, 4),
                    12..=13 => self.set_base_state(idx, 2, 2, 0),
                    14..=15 => self.set_base_state(idx, 4, 2, 0),
                    16 => self.set_base_state(idx, 0, 4, 4),
                    17 => self.set_base_state(idx, 0, 4, 4),
                    18 => self.set_base_state(idx, 4, 2, 2),
                    19 => self.set_base_state(idx, 4, 2, 4),
                    20 => self.set_base_state(idx, 2, 4, 2),
                    21 => self.set_base_state(idx, 2, 2, 2),
                    22 => self.set_base_state(idx, 4, 2, 2),
                    23 => self.set_base_state(idx, 2, 0, 4),
                    _ => (),
                }
            }
        }
    }

    fn draw_piano(&mut self) {
        for idx in 0..self.session.piano_octaves {
            self.draw_piano_row(7 - 2 * idx);
        }
    }

    fn draw_piano_row(&mut self, row: u8) {
        for y in 0..2 {
            for x in 0..8 {
                self.draw_piano_note(10 * (row + y) + 1 + x);
            }
        }
    }

    fn draw_piano_note(&mut self, idx: u8) {
        let note = self.note_to_piano_note(idx);

        if note == -128 {
            self.set_base_state(idx, 255, 255, 255);
        } else if self.record {
            let mut has_ref = false;
            let mut mute = false;
            let mut latch = false;

            for coord in self.selected.iter() {
                if let Some(pad) = self.session.grid.get(coord) {
                    if !has_ref {
                        has_ref = pad.notes.contains(&(self.session.channel, note));
                    }
                    if !mute {
                        mute = pad.mute;
                    }
                    if !latch {
                        latch = pad.latch;
                    }
                }
            }

            if has_ref {
                if mute {
                    self.set_base_state(idx, 18, 0, 0);
                } else if latch {
                    self.set_base_state(idx, 18, 54, 63);
                } else {
                    self.set_base_state(idx, 63, 18, 54);
                }
            } else if idx % 20 > 10 {
                self.set_base_state(idx, 63, 48, 48);
            } else {
                self.set_base_state(idx, 18, 16, 18);
            }
        } else if idx % 20 > 10 {
            self.set_base_state(idx, 63, 48, 48);
        } else {
            self.set_base_state(idx, 18, 16, 18);
        }
    }

    fn draw_palette(&mut self) {
        if self.show_palette {
            let r = self.session.base_color[0];
            let g = self.session.base_color[1];
            let b = self.session.base_color[2];

            let (h, s, l) = self.rgb_to_hsl(r, g, b);

            self.set_base_state(11, r.max(1) - 1, 0, 0);
            self.set_base_state(21, r.min(62) + 1, 0, 0);

            self.set_base_state(12, 0, g.max(1) - 1, 0);
            self.set_base_state(22, 0, g.min(62) + 1, 0);

            self.set_base_state(13, 0, 0, b.max(1) - 1);
            self.set_base_state(23, 0, 0, b.min(62) + 1);

            let (r_, g_, b_) = self.hsl_to_rgb(h, 1.0, 0.5);
            self.set_base_state(14, r_, g_, b_);
            self.set_base_state(24, r_, g_, b_);

            let (r_, g_, b_) = self.hsl_to_rgb(h, s, 0.5);
            self.set_base_state(15, r_, g_, b_);
            self.set_base_state(25, r_, g_, b_);

            let (r_, g_, b_) = self.hsl_to_rgb(h, 1.0, l);
            self.set_base_state(16, r_, g_, b_);
            self.set_base_state(26, r_, g_, b_);

            self.set_base_state(17, r, g, b);
            self.set_base_state(27, r, g, b);
            self.set_base_state(18, r, g, b);
            self.set_base_state(28, r, g, b);
        }

        self.set_base_state(
            99,
            self.session.base_color[0],
            self.session.base_color[1],
            self.session.base_color[2],
        );
    }

    fn rgb_to_hsl(&self, r: u8, g: u8, b: u8) -> (f32, f32, f32) {
        let r_ = r as f32 / 63.0;
        let g_ = g as f32 / 63.0;
        let b_ = b as f32 / 63.0;
        let c_max = r_.max(g_).max(b_);
        let c_min = r_.min(g_).min(b_);
        let delta = c_max - c_min;

        let h = if delta == 0.0 {
            0.0
        } else if r_ == c_max {
            60.0 * ((g_ - b_) / delta).rem_euclid(6.0)
        } else if g_ == c_max {
            60.0 * ((b_ - r_) / delta + 2.0)
        } else {
            60.0 * ((r_ - g_) / delta + 4.0)
        };

        let l = (c_max + c_min) / 2.0;

        let s = if delta == 0.0 {
            0.0
        } else {
            delta / (1.0 - (c_max + c_min - 1.0).abs())
        };

        (h, s, l)
    }

    fn hsl_to_rgb(&self, h: f32, s: f32, l: f32) -> (u8, u8, u8) {
        let c = (1.0 - (2.0 * l - 1.0).abs()) * s;
        let x = c * (1.0 - ((h / 60.0).rem_euclid(2.0) - 1.0).abs());
        let m = l - c / 2.0;

        let (r_, g_, b_) = if h < 60.0 {
            (c, x, 0.0)
        } else if h < 120.0 {
            (x, c, 0.0)
        } else if h < 180.0 {
            (0.0, c, x)
        } else if h < 240.0 {
            (0.0, x, c)
        } else if h < 300.0 {
            (x, 0.0, c)
        } else {
            (c, 0.0, x)
        };

        (
            ((r_ + m) * 63.0) as u8,
            ((g_ + m) * 63.0) as u8,
            ((b_ + m) * 63.0) as u8,
        )
    }

    fn set_base_state(&mut self, idx: u8, r: u8, g: u8, b: u8) {
        self.base_state[idx as usize] = [r, g, b];
    }

    fn set_flash(&mut self, idx: u8) {
        self.state_mode[idx as usize] = 1;
    }

    fn unset_flash(&mut self, idx: u8) {
        self.state_mode[idx as usize] = 0;
    }

    fn set_state(&mut self, idx: u8, r: u8, g: u8, b: u8) {
        let base_state = self.base_state[idx as usize];

        if base_state[0] != 255 {
            self.state[idx as usize] = [r, g, b];
        }
    }

    fn unset_state(&mut self, idx: u8) {
        // Because LED values range from 0-63, We use `255` set on `r` as a sentinel value
        // for a "transparent" state, i.e., fallback to the base state.
        self.state[idx as usize] = [255, 255, 255];
    }

    fn get_state(&self, idx: u8) -> [u8; 3] {
        let state = self.state[idx as usize];

        if state[0] == 255 {
            self.base_state[idx as usize]
        } else {
            state
        }
    }

    fn flip_display(&mut self) {
        for idx in 0..100 {
            let state = self.get_state(idx);
            let state_mode = self.state_mode[idx as usize];
            let prev_state = self.prev_state[idx as usize];

            if state[0] == prev_state[0]
                && state[1] == prev_state[1]
                && state[2] == prev_state[2]
                && state_mode == prev_state[3]
            {
                continue;
            }

            if state[0] == 255 {
                self.set_led(idx, 0, 0, 0, state_mode);
            } else {
                self.set_led(idx, state[0], state[1], state[2], state_mode);
            }
        }
    }

    fn set_led(&mut self, idx: u8, r: u8, g: u8, b: u8, mode: u8) {
        self.midi_tx
            .send(MidiMessage::Sysex {
                addr: self.addr.unwrap(),
                data: vec![240, 0, 32, 41, 2, 16, 11, idx, r, g, b, 247],
            })
            .unwrap();

        if mode == 1 {
            self.midi_tx
                .send(MidiMessage::Sysex {
                    addr: self.addr.unwrap(),
                    data: vec![240, 0, 32, 41, 2, 16, 35, idx, 0, 247],
                })
                .unwrap();
        }

        self.prev_state[idx as usize] = [r, g, b, mode];
    }

    fn clear_display(&mut self) {
        self.prev_state.fill([255, 255, 255, 0]);

        self.midi_tx
            .send(MidiMessage::Sysex {
                addr: self.addr.unwrap(),
                data: vec![240, 0, 32, 41, 2, 16, 14, 0, 247],
            })
            .unwrap();
    }

    fn note_to_grid_xy(&self, note: u8) -> (isize, isize) {
        let x = ((note % 10) - 1) as isize - self.session.offset[0];
        let y = ((note / 10) - 1) as isize - self.session.offset[1];
        (x, y)
    }

    fn note_to_row_col(&self, note: u8) -> (u8, u8) {
        (8 - note / 10, note % 10 - 1)
    }

    fn note_to_piano_note(&self, note: u8) -> i16 {
        self.note_to_piano_note_from_octave(
            note,
            ((10 + note) / 20 - 1) as i16 + self.session.base_octave - 2,
            self.session.shift_piano,
        )
    }

    fn note_to_piano_note_from_octave(&self, note: u8, octave: i16, shift_piano: i16) -> i16 {
        let x = ((note % 10) - 1) as i16 + shift_piano;
        let normalised_note = x.rem_euclid(7);
        let shift_piano_octave = if x >= 7 {
            1
        } else if x < 0 {
            -1
        } else {
            0
        };

        // White keys
        if (note % 20) > 10 {
            if normalised_note < 3 {
                12 * (octave + shift_piano_octave) + 2 * normalised_note
            } else {
                12 * (octave + shift_piano_octave) + 2 * normalised_note - 1
            }
        } else if normalised_note == 0 || normalised_note == 3 {
            -128
        } else if normalised_note < 3 {
            12 * (octave + shift_piano_octave) + 2 * normalised_note - 1
        } else {
            12 * (octave + shift_piano_octave) + 2 * normalised_note - 2
        }
    }
}

impl Pad {
    pub fn new(r: u8, g: u8, b: u8) -> Self {
        Self {
            color: [r, g, b],
            scale_notes: Vec::with_capacity(8),
            notes: Vec::with_capacity(8),
            params: Vec::with_capacity(8),
            sidechain: false,
            latch: false,
            mute: false,
            group_id: 0,
            active: false,
        }
    }

    pub fn transpose(&mut self, amount: i16) {
        for (_, note) in self.notes.iter_mut() {
            *note += amount;
        }
    }

    fn add_note(&mut self, channel: u8, note: i16) {
        self.notes.push((channel, note));
    }

    fn remove_note(&mut self, channel: u8, note: i16) {
        self.notes
            .retain(|(channel_, note_)| *channel_ != channel || *note_ != note);
    }

    pub fn toggle_note(&mut self, channel: u8, note: i16) -> bool {
        if self.notes.contains(&(channel, note)) {
            self.remove_note(channel, note);
            false
        } else {
            self.add_note(channel, note);
            true
        }
    }

    fn remove_scale_note(&mut self, note: i16) {
        self.scale_notes
            .retain(|(note_, _enabled)| *note_ != note.rem_euclid(12));
    }

    pub fn toggle_scale_note(&mut self, note: i16, enabled: bool) {
        let mut has_scale_note = false;

        for (scale_note, scale_enabled) in self.scale_notes.iter_mut() {
            if *scale_note == note.rem_euclid(12) {
                *scale_enabled = enabled;
                has_scale_note = true;
            }
        }

        if !has_scale_note {
            self.scale_notes.push((note.rem_euclid(12), enabled));
        }
    }

    fn add_param(&mut self, dest: Param, value: f32) {
        let mut has_param = false;

        for (dest_, value_) in self.params.iter_mut() {
            if dest == *dest_ {
                *value_ = value;
                has_param = true;
            }
        }

        if !has_param {
            self.params.push((dest, value));
        }
    }

    fn remove_param(&mut self, dest: Param) {
        self.params.retain(|(dest_, _)| *dest_ != dest);
    }

    fn has_param(&mut self, dest: Param) -> bool {
        self.params.iter().any(|(dest_, _)| *dest_ == dest)
    }

    pub fn toggle_mute(&mut self) -> bool {
        self.mute = !self.mute;
        self.mute
    }

    pub fn toggle_latch(&mut self) -> bool {
        self.latch = !self.latch;
        self.latch
    }

    pub fn toggle_sidechain(&mut self) -> bool {
        self.sidechain = !self.sidechain;
        self.sidechain
    }
}
