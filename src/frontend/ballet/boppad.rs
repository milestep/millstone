// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use crate::messages::{Action, BalletId, BalletMessage};
use alsa::seq;
use crossbeam::channel::Sender;

pub struct BopPad {
    addr: Option<seq::Addr>,
    action_tx: Sender<Action>,
    ballet_tx: Sender<BalletMessage>,
    velocity: u8,
    drum_id: usize,
}

impl BopPad {
    pub fn new(action_tx: Sender<Action>, ballet_tx: Sender<BalletMessage>) -> Self {
        Self {
            addr: None,
            action_tx,
            ballet_tx,
            velocity: 0,
            drum_id: 0,
        }
    }

    pub fn connect(&mut self, addr: &seq::Addr, name: &String) -> bool {
        if self.addr.is_none() && name == "BopPad" {
            self.addr = Some(*addr);
            self.action_tx
                .send(Action::Log(
                    "[boppad] BodPad connected with succes".to_string(),
                ))
                .unwrap();
            return true;
        }
        false
    }

    pub fn disconnect(&mut self, addr: &seq::Addr) -> bool {
        let addr_ = self.addr.take();
        if addr_.is_some() && addr_.unwrap() == *addr {
            return true;
        }
        self.addr = addr_;
        false
    }

    pub fn has_addr(&self, addr: &seq::Addr) -> bool {
        self.addr.is_some() && self.addr.unwrap() == *addr
    }

    pub fn cc(&mut self, channel: u8, param: u8, value: u8) {
        if param == 0 {
            let step = match value {
                0..=45 => 0,
                46..=95 => 1,
                _ => 2,
            };
            self.drum_id = ((3 * channel) + step) as usize;
            self.ballet_tx
                .send(BalletMessage::NoteOn {
                    channel,
                    id: BalletId {
                        group_id: 8,
                        voice_id: self.drum_id,
                        note_id: 0,
                    },
                    frequency: 440.0 * 2.0f32.powf((self.drum_id as f32 - 69.0) / 12.0),
                    velocity: self.velocity as f32 / 127.0,
                    mute: false,
                    latch: false,
                })
                .unwrap();
        };
    }

    pub fn note_on(&mut self, _channel: u8, _note: u8, velocity: u8) {
        self.velocity = velocity;
    }

    pub fn note_off(&mut self, _channel: u8, _note: u8, _velocity: u8) {
        // pass
    }
}
