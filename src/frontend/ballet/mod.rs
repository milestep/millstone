// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

mod boppad;
mod launchpad_pro;
mod menu;
mod session;
mod touche_se;

use alsa::seq;

use crossbeam::channel::Sender;

use crate::messages::{Action, BalletId, BalletMessage, MidiMessage};

pub struct Interface {
    launchpad_pro: launchpad_pro::LaunchpadPro,
    touche_se: touche_se::ToucheSE,
    boppad: boppad::BopPad,
    action_tx: Sender<Action>,
    ballet_tx: Sender<BalletMessage>,
}

impl Interface {
    pub fn new(
        action_tx: Sender<Action>,
        midi_tx: Sender<MidiMessage>,
        ballet_tx: Sender<BalletMessage>,
    ) -> Self {
        Self {
            launchpad_pro: launchpad_pro::LaunchpadPro::new(
                action_tx.clone(),
                midi_tx,
                ballet_tx.clone(),
            ),
            touche_se: touche_se::ToucheSE::new(ballet_tx.clone()),
            boppad: boppad::BopPad::new(action_tx.clone(), ballet_tx.clone()),
            action_tx,
            ballet_tx,
        }
    }

    pub fn connect(&mut self, addr: seq::Addr, name: String) {
        if self.launchpad_pro.connect(&addr, &name)
            || self.touche_se.connect(&addr, &name)
            || self.boppad.connect(&addr, &name)
        {
            self.action_tx
                .send(Action::Log(format!(
                    "MIDI connected: {} {}:{}",
                    name, addr.client, addr.port
                )))
                .unwrap();
        }
    }

    pub fn disconnect(&mut self, addr: seq::Addr, name: String) {
        if self.launchpad_pro.disconnect(&addr)
            || self.touche_se.disconnect(&addr)
            || self.boppad.disconnect(&addr)
        {
            self.action_tx
                .send(Action::Log(format!(
                    "MIDI disconnected: {} {}:{}",
                    name, addr.client, addr.port
                )))
                .unwrap();
        }
    }

    pub fn cc(&mut self, addr: seq::Addr, channel: u8, param: u8, value: u8) {
        if self.launchpad_pro.has_addr(&addr) {
            self.launchpad_pro.cc(channel, param, value);
        } else if self.touche_se.has_addr(&addr) {
            self.touche_se.cc(channel, param, value);
        } else if self.boppad.has_addr(&addr) {
            self.boppad.cc(channel, param, value);
        }
    }

    pub fn note_on(&mut self, addr: seq::Addr, channel: u8, note: u8, velocity: u8) {
        if self.launchpad_pro.has_addr(&addr) {
            self.launchpad_pro.note_on(channel, note, velocity);
        } else if self.boppad.has_addr(&addr) {
            self.boppad.note_on(channel, note, velocity);
        } else {
            if note == 60 {
                self.ballet_tx
                    .send(BalletMessage::Sidechain {
                        channel: channel as usize,
                        velocity: (velocity as f32) / 127.0,
                    })
                    .unwrap();
            }

            if channel == 0 {
                self.ballet_tx
                    .send(BalletMessage::NoteOn {
                        channel: 1 + channel,
                        id: BalletId {
                            group_id: note as usize,
                            voice_id: 0,
                            note_id: 0,
                        },
                        frequency: 440.0 * 2.0f32.powf((note as f32 - 69.0) / 12.0),
                        velocity: velocity as f32 / 127.0,
                        mute: false,
                        latch: false,
                    })
                    .unwrap();
            }
        }
    }

    pub fn note_off(&mut self, addr: seq::Addr, channel: u8, note: u8, velocity: u8) {
        if self.launchpad_pro.has_addr(&addr) {
            self.launchpad_pro.note_off(channel, note, velocity);
        } else if self.boppad.has_addr(&addr) {
            self.boppad.note_off(channel, note, velocity);
        } else {
            self.ballet_tx
                .send(BalletMessage::NoteOff {
                    channel: 1 + channel,
                    id: BalletId {
                        group_id: note as usize,
                        voice_id: 0,
                        note_id: 0,
                    },
                    velocity: velocity as f32 / 127.0,
                    latch: false,
                })
                .unwrap();
        }
    }

    pub fn pitchbend(&mut self, _addr: seq::Addr, _channel: u8, _value: i16) {}

    pub fn key_pressure(&mut self, addr: seq::Addr, channel: u8, note: u8, velocity: u8) {
        if self.launchpad_pro.has_addr(&addr) {
            self.launchpad_pro.key_pressure(channel, note, velocity);
        }
    }

    pub fn sysex(&mut self, addr: seq::Addr, data: Vec<u8>) {
        if self.launchpad_pro.has_addr(&addr) {
            self.launchpad_pro.sysex(data);
        }
    }
}
