use serde::{Deserialize, Serialize};

use crate::ballet::{
    AccumulatorDestination, CompressorDestination, DelayDestination, EnvelopeDestination,
    FilterDestination, LfoDestination, MasterDestination, MasterParameter, NoiseDestination,
    OscillatorDestination, Parameter, PitchTrackerDestination, ShaperDestination,
    SidechainDestination, TunedDelayDestination, VocalProcessorDestination, VoiceDestination,
    VoiceParameter, WaveshaperDestination,
};

#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub enum MenuMode {
    Session,
    ChannelSelector,
    VoiceConfiguration,
    Param(ParamMenu),
}

#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub enum ParamMenu {
    Voice,
    Oscillator,
    TunedDelay,
    Filter,
    Waveshaper,
    Envelope,
    Lfo,
    Accumulator,

    Mixer,
    Vocal,
    Delay,
    Compressor,
    Sidechain,
    GlobalLfo,
    GlobalAccumulator,
}

pub fn get_page_count(menu_mode: MenuMode) -> u8 {
    match menu_mode {
        MenuMode::Session => 6,
        MenuMode::ChannelSelector => 6,
        MenuMode::VoiceConfiguration => 2,
        MenuMode::Param(ParamMenu::Voice) => 3,
        MenuMode::Param(ParamMenu::Oscillator) => 3,
        MenuMode::Param(ParamMenu::TunedDelay) => 2,
        MenuMode::Param(ParamMenu::Filter) => 2,
        MenuMode::Param(ParamMenu::Waveshaper) => 1,
        MenuMode::Param(ParamMenu::Envelope) => 2,
        MenuMode::Param(ParamMenu::Lfo) => 2,
        MenuMode::Param(ParamMenu::Accumulator) => 1,

        MenuMode::Param(ParamMenu::Mixer) => 1,
        MenuMode::Param(ParamMenu::Vocal) => 2,
        MenuMode::Param(ParamMenu::Delay) => 2,
        MenuMode::Param(ParamMenu::Compressor) => 1,
        MenuMode::Param(ParamMenu::Sidechain) => 1,
        MenuMode::Param(ParamMenu::GlobalLfo) => 2,
        MenuMode::Param(ParamMenu::GlobalAccumulator) => 1,
    }
}

pub fn get_parameter_type(
    menu_mode: Option<MenuMode>,
    channel: u8,
    page: u8,
    idx: u8,
) -> Option<Parameter> {
    match menu_mode {
        Some(MenuMode::Param(ParamMenu::Voice)) => match page {
            0 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Osc1),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Osc2),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Osc3),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Noise),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Noise(NoiseDestination::Balance),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::TunedDelay),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Flt2),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Gain),
                )),
                _ => None,
            },
            1 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Shaper(ShaperDestination::Trigger),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Shaper(ShaperDestination::Delay),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Shaper(ShaperDestination::Attack),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Shaper(ShaperDestination::Hold),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Shaper(ShaperDestination::Decay),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Shaper(ShaperDestination::Sustain),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Shaper(ShaperDestination::Release),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Shaper(ShaperDestination::Sense),
                )),
                _ => None,
            },
            2 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Frequency),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Portamento),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Arp),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Delay),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Pan),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Width),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::Send),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Voice(VoiceDestination::DelaySend),
                )),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::Oscillator)) => match page {
            0 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc1(OscillatorDestination::Frequency),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc1(OscillatorDestination::Phase),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc1(OscillatorDestination::Delay),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc1(OscillatorDestination::Asymmetry),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc1(OscillatorDestination::Width),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc1(OscillatorDestination::Gap),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc1(OscillatorDestination::Cushion),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc1(OscillatorDestination::Melt),
                )),
                _ => None,
            },
            1 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc2(OscillatorDestination::Frequency),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc2(OscillatorDestination::Phase),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc2(OscillatorDestination::Delay),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc2(OscillatorDestination::Asymmetry),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc2(OscillatorDestination::Width),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc2(OscillatorDestination::Gap),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc2(OscillatorDestination::Cushion),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc2(OscillatorDestination::Melt),
                )),
                _ => None,
            },
            2 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc3(OscillatorDestination::Frequency),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc3(OscillatorDestination::Phase),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc3(OscillatorDestination::Delay),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc3(OscillatorDestination::Asymmetry),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc3(OscillatorDestination::Width),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc3(OscillatorDestination::Gap),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc3(OscillatorDestination::Cushion),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Osc3(OscillatorDestination::Melt),
                )),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::TunedDelay)) => match page {
            0 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::Frequency),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::Track),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::Speed),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::Feedback),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::FilterFrequency),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::Sampling),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::Blend),
                )),
                _ => None,
            },
            1 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::Osc1),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::Osc2),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::Osc3),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::Noise),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::TunedDelay(TunedDelayDestination::Voice),
                )),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::Filter)) => match page {
            0 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt1(FilterDestination::Frequency),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt1(FilterDestination::Lowpass),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt1(FilterDestination::Highpass),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt1(FilterDestination::Bandpass),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt1(FilterDestination::Resonance),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt1(FilterDestination::Headroom),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt1(FilterDestination::Decay),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt1(FilterDestination::Track),
                )),
                _ => None,
            },
            1 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt2(FilterDestination::Frequency),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt2(FilterDestination::Lowpass),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt2(FilterDestination::Highpass),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt2(FilterDestination::Bandpass),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt2(FilterDestination::Resonance),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt2(FilterDestination::Headroom),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt2(FilterDestination::Decay),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Flt2(FilterDestination::Track),
                )),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::Waveshaper)) => match page {
            0 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Wshp(WaveshaperDestination::Pregain),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Wshp(WaveshaperDestination::Asymmetry),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Wshp(WaveshaperDestination::Gate),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Wshp(WaveshaperDestination::Cushion),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Wshp(WaveshaperDestination::Hardclip),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Wshp(WaveshaperDestination::Crossover),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Wshp(WaveshaperDestination::Crush),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Wshp(WaveshaperDestination::Sampling),
                )),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::Envelope)) => match page {
            0 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env1(EnvelopeDestination::Attack),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env1(EnvelopeDestination::Hold),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env1(EnvelopeDestination::Decay),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env1(EnvelopeDestination::Sustain),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env1(EnvelopeDestination::Slope),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env1(EnvelopeDestination::Scatter),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env1(EnvelopeDestination::Echo),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env1(EnvelopeDestination::Release),
                )),
                _ => None,
            },
            1 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env2(EnvelopeDestination::Attack),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env2(EnvelopeDestination::Hold),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env2(EnvelopeDestination::Decay),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env2(EnvelopeDestination::Sustain),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env2(EnvelopeDestination::Slope),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env2(EnvelopeDestination::Scatter),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env2(EnvelopeDestination::Echo),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Env2(EnvelopeDestination::Release),
                )),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::Lfo)) => match page {
            0 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Lfo1(LfoDestination::Frequency),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Lfo1(LfoDestination::Phase),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Lfo1(LfoDestination::Asymmetry),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Lfo1(LfoDestination::Cushion),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Lfo1(LfoDestination::Slew),
                )),
                _ => None,
            },
            1 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Lfo2(LfoDestination::Frequency),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Lfo2(LfoDestination::Phase),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Lfo2(LfoDestination::Asymmetry),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Lfo2(LfoDestination::Cushion),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Lfo2(LfoDestination::Slew),
                )),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::Accumulator)) => match page {
            0 => match idx {
                0 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Acc1(AccumulatorDestination::Value),
                )),
                1 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Acc1(AccumulatorDestination::Slew),
                )),
                2 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Acc1(AccumulatorDestination::Depth),
                )),
                3 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Acc1(AccumulatorDestination::Rate),
                )),
                4 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Acc2(AccumulatorDestination::Value),
                )),
                5 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Acc2(AccumulatorDestination::Slew),
                )),
                6 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Acc2(AccumulatorDestination::Depth),
                )),
                7 => Some(Parameter::Voice(
                    channel,
                    VoiceParameter::Acc2(AccumulatorDestination::Rate),
                )),
                _ => None,
            },
            _ => None,
        },

        // Master menus
        Some(MenuMode::Param(ParamMenu::Mixer)) => match page {
            0 => match idx {
                0 => Some(Parameter::Master(MasterParameter::Master(
                    MasterDestination::Vocal,
                ))),
                1 => Some(Parameter::Master(MasterParameter::Master(
                    MasterDestination::Clean,
                ))),
                2 => Some(Parameter::Master(MasterParameter::Master(
                    MasterDestination::Delay,
                ))),
                3 => None,
                4 => None,
                5 => Some(Parameter::Master(MasterParameter::Master(
                    MasterDestination::TimeScale,
                ))),
                6 => Some(Parameter::Master(MasterParameter::Master(
                    MasterDestination::Filter,
                ))),
                7 => Some(Parameter::Master(MasterParameter::Master(
                    MasterDestination::Gain,
                ))),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::Vocal)) => match page {
            0 => match idx {
                0 => Some(Parameter::Master(MasterParameter::VocalProcessor(
                    VocalProcessorDestination::Lowpass,
                ))),
                1 => Some(Parameter::Master(MasterParameter::VocalProcessor(
                    VocalProcessorDestination::Highpass,
                ))),
                2 => Some(Parameter::Master(MasterParameter::VocalProcessor(
                    VocalProcessorDestination::Gate,
                ))),
                3 => Some(Parameter::Master(MasterParameter::VocalProcessor(
                    VocalProcessorDestination::Threshold,
                ))),
                4 => Some(Parameter::Master(MasterParameter::VocalProcessor(
                    VocalProcessorDestination::Gain,
                ))),
                5 => Some(Parameter::Master(MasterParameter::VocalProcessor(
                    VocalProcessorDestination::Feedback,
                ))),
                6 => Some(Parameter::Master(MasterParameter::VocalProcessor(
                    VocalProcessorDestination::Time,
                ))),
                7 => Some(Parameter::Master(MasterParameter::VocalProcessor(
                    VocalProcessorDestination::DelaySend,
                ))),
                _ => None,
            },
            1 => match idx {
                0 => Some(Parameter::Master(MasterParameter::PitchTracker(
                    PitchTrackerDestination::Threshold,
                ))),
                1 => Some(Parameter::Master(MasterParameter::PitchTracker(
                    PitchTrackerDestination::NoiseLevel,
                ))),
                2 => Some(Parameter::Master(MasterParameter::PitchTracker(
                    PitchTrackerDestination::RelativeFreqTolerance,
                ))),
                3 => Some(Parameter::Master(MasterParameter::PitchTracker(
                    PitchTrackerDestination::Decay,
                ))),
                4 => Some(Parameter::Master(MasterParameter::PitchTracker(
                    PitchTrackerDestination::SignalThreshold,
                ))),
                5 => Some(Parameter::Master(MasterParameter::PitchTracker(
                    PitchTrackerDestination::MinFreq,
                ))),
                6 => Some(Parameter::Master(MasterParameter::PitchTracker(
                    PitchTrackerDestination::MaxFreq,
                ))),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::Delay)) => match page {
            0 => match idx {
                0 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Balance,
                ))),
                1 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Spread,
                ))),
                2 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Diffusion,
                ))),
                3 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::DiffusionLength,
                ))),
                4 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Shimmer,
                ))),
                5 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::ShimmerShift,
                ))),
                6 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Feedback,
                ))),
                7 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Cutoff,
                ))),
                _ => None,
            },
            1 => match idx {
                0 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Delay1,
                ))),
                1 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Time1,
                ))),
                2 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Delay2,
                ))),
                3 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Time2,
                ))),
                4 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Delay3,
                ))),
                5 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Time3,
                ))),
                6 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Delay4,
                ))),
                7 => Some(Parameter::Master(MasterParameter::Delay(
                    DelayDestination::Time4,
                ))),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::Compressor)) => match page {
            0 => match idx {
                0 => Some(Parameter::Master(MasterParameter::Compressor(
                    CompressorDestination::Drive,
                ))),
                1 => Some(Parameter::Master(MasterParameter::Compressor(
                    CompressorDestination::Attack,
                ))),
                2 => Some(Parameter::Master(MasterParameter::Compressor(
                    CompressorDestination::Release,
                ))),
                3 => Some(Parameter::Master(MasterParameter::Compressor(
                    CompressorDestination::Threshold,
                ))),
                4 => Some(Parameter::Master(MasterParameter::Compressor(
                    CompressorDestination::Gain,
                ))),
                5 => Some(Parameter::Master(MasterParameter::Compressor(
                    CompressorDestination::Knee,
                ))),
                6 => Some(Parameter::Master(MasterParameter::Compressor(
                    CompressorDestination::Ratio,
                ))),
                7 => Some(Parameter::Master(MasterParameter::Compressor(
                    CompressorDestination::PostGain,
                ))),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::Sidechain)) => match page {
            0 => match idx {
                0 => Some(Parameter::Master(MasterParameter::Sidechain(
                    SidechainDestination::Amount,
                ))),
                1 => Some(Parameter::Master(MasterParameter::Sidechain(
                    SidechainDestination::Attack,
                ))),
                2 => Some(Parameter::Master(MasterParameter::Sidechain(
                    SidechainDestination::Hold,
                ))),
                3 => Some(Parameter::Master(MasterParameter::Sidechain(
                    SidechainDestination::Decay,
                ))),
                4 => Some(Parameter::Master(MasterParameter::Sidechain(
                    SidechainDestination::Sense,
                ))),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::GlobalLfo)) => match page {
            0 => match idx {
                0 => Some(Parameter::Master(MasterParameter::Lfo1(
                    LfoDestination::Frequency,
                ))),
                1 => Some(Parameter::Master(MasterParameter::Lfo1(
                    LfoDestination::Phase,
                ))),
                2 => Some(Parameter::Master(MasterParameter::Lfo1(
                    LfoDestination::Asymmetry,
                ))),
                3 => Some(Parameter::Master(MasterParameter::Lfo1(
                    LfoDestination::Cushion,
                ))),
                4 => Some(Parameter::Master(MasterParameter::Lfo1(
                    LfoDestination::Slew,
                ))),
                _ => None,
            },
            1 => match idx {
                0 => Some(Parameter::Master(MasterParameter::Lfo1(
                    LfoDestination::Frequency,
                ))),
                1 => Some(Parameter::Master(MasterParameter::Lfo1(
                    LfoDestination::Phase,
                ))),
                2 => Some(Parameter::Master(MasterParameter::Lfo1(
                    LfoDestination::Asymmetry,
                ))),
                3 => Some(Parameter::Master(MasterParameter::Lfo1(
                    LfoDestination::Cushion,
                ))),
                4 => Some(Parameter::Master(MasterParameter::Lfo1(
                    LfoDestination::Slew,
                ))),
                _ => None,
            },
            _ => None,
        },
        Some(MenuMode::Param(ParamMenu::GlobalAccumulator)) => match page {
            0 => match idx {
                0 => Some(Parameter::Master(MasterParameter::Acc1(
                    AccumulatorDestination::Value,
                ))),
                1 => Some(Parameter::Master(MasterParameter::Acc1(
                    AccumulatorDestination::Slew,
                ))),
                2 => Some(Parameter::Master(MasterParameter::Acc1(
                    AccumulatorDestination::Depth,
                ))),
                3 => Some(Parameter::Master(MasterParameter::Acc1(
                    AccumulatorDestination::Rate,
                ))),
                4 => Some(Parameter::Master(MasterParameter::Acc2(
                    AccumulatorDestination::Value,
                ))),
                5 => Some(Parameter::Master(MasterParameter::Acc2(
                    AccumulatorDestination::Slew,
                ))),
                6 => Some(Parameter::Master(MasterParameter::Acc2(
                    AccumulatorDestination::Depth,
                ))),
                7 => Some(Parameter::Master(MasterParameter::Acc2(
                    AccumulatorDestination::Rate,
                ))),
                _ => None,
            },
            _ => None,
        },
        _ => None,
    }
}
