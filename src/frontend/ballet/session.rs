use crossbeam::channel::Sender;

use serde::{Deserialize, Serialize};

use std::collections::HashMap;

use crate::ballet::{MasterPatch, Parameter, Patch};
use crate::messages::{Action, BalletConfig, BalletMasterConfig};
use crate::paths;
use crate::state::State;
use crate::synth::{Bounds, Modulation, ModulationParams};

use super::menu::*;

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct SessionMetadata {
    #[serde(default)]
    pub color: [u8; 3],
}

impl State for SessionMetadata {
    fn new() -> Self {
        Self {
            color: [255, 255, 255],
        }
    }

    fn from_string(s: String) -> Self {
        match ron::from_str(&s) {
            Ok(state) => state,
            Err(_) => Self::new(),
        }
    }
}

impl SessionMetadata {
    pub fn from_color(color: [u8; 3]) -> Self {
        Self { color }
    }
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct GlobalState {
    #[serde(default)]
    pub session_idx: usize,

    #[serde(default)]
    pub locked: bool,
}

impl State for GlobalState {
    fn new() -> Self {
        Self {
            session_idx: 0,
            locked: false,
        }
    }

    fn from_string(s: String) -> Self {
        match ron::from_str(&s) {
            Ok(state) => state,
            Err(_) => Self::new(),
        }
    }
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Session {
    #[serde(default)]
    pub base_octave: i16,
    #[serde(default)]
    pub piano_octaves: u8,
    #[serde(default)]
    pub shift_piano: i16,
    #[serde(default)]
    pub master_patch: MasterPatch,
    #[serde(default)]
    pub channel_patches: [Patch; 16],
    #[serde(default)]
    pub voice_channels: [u8; 16],
    #[serde(default)]
    pub channel_patch_idx: [usize; 16],
    #[serde(default)]
    pub channel: u8,
    #[serde(default)]
    pub base_color: [u8; 3],
    #[serde(default)]
    pub grid: HashMap<[isize; 2], Pad>,
    #[serde(default)]
    pub offset: [isize; 2],
    #[serde(default)]
    pub menu_pages: HashMap<MenuMode, u8>,
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize)]
pub struct Param {
    pub parameter: Parameter,
    pub modulation_source: Option<[usize; 4]>,
    pub bounds_mode: Option<Bounds>,
}

#[derive(Debug, Deserialize, Clone, Copy, Serialize)]
pub struct DisplayValue {
    pub x: f32,
    pub initial: f32,
    pub relative: f32,
    pub relative_default: f32,
    pub low: f32,
    pub high: f32,
    pub group: u8,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Pad {
    pub latch: bool,
    pub mute: bool,

    #[serde(skip)]
    pub group_id: usize,

    #[serde(skip)]
    pub active: bool,

    pub color: [u8; 3],
    pub notes: Vec<(u8, i16)>,
    #[serde(default)]
    pub scale_notes: Vec<(i16, bool)>,
    #[serde(default)]
    pub params: Vec<(Param, f32)>,
    #[serde(default)]
    pub sidechain: bool,
}

impl Session {
    pub fn set_from(&mut self, other: Self) {
        self.master_patch.set_from(&other.master_patch);

        for channel in 0..16 {
            self.channel_patches[channel].set_from(&other.channel_patches[channel]);
        }

        self.base_octave = other.base_octave;
        self.piano_octaves = other.piano_octaves;
        self.shift_piano = other.shift_piano;
        self.voice_channels = other.voice_channels;
        self.channel_patch_idx = other.channel_patch_idx;
        self.channel = other.channel;
        self.base_color = other.base_color;
        self.grid = other.grid;
        self.offset = other.offset;
        self.menu_pages = other.menu_pages;
    }

    pub fn get_channel_patch(&self, channel: u8) -> Patch {
        if channel == 0 || channel > 16 {
            Patch::new(0, 1, 255)
        } else {
            self.channel_patches[(channel - 1) as usize].clone()
        }
    }

    pub fn set_channel_patch(&mut self, channel: u8, patch: Patch) {
        if channel > 0 && channel <= 16 {
            self.channel_patches[(channel - 1) as usize].set_from(&patch);
        }
    }

    pub fn get_param(&self, param: Option<Param>) -> Option<DisplayValue> {
        param?;
        let param = param.unwrap();

        let parameter = match param.parameter {
            Parameter::Master(dest) => self.master_patch.get_param(dest),
            Parameter::Voice(channel, dest) => {
                if channel > 0 && channel <= 16 {
                    self.channel_patches[(channel - 1) as usize].get_param(dest)
                } else {
                    None
                }
            }
        };

        parameter?;
        let parameter = parameter.unwrap();

        match param.modulation_source {
            None => Some(DisplayValue {
                x: parameter.x,
                initial: parameter.initial,
                relative: parameter.get_relative(),
                relative_default: parameter.get_relative_default(),
                low: parameter.low,
                high: parameter.high,
                group: parameter.group,
            }),
            Some(idx) => {
                let modulation_params = match param.parameter {
                    Parameter::Master(dest) => self.master_patch.get_modulation_params(idx, dest),
                    Parameter::Voice(channel, dest) => {
                        if channel > 0 && channel <= 16 {
                            self.channel_patches[(channel - 1) as usize]
                                .get_modulation_params(idx, dest)
                        } else {
                            ModulationParams::new()
                        }
                    }
                };

                match param.bounds_mode {
                    None => Some(DisplayValue {
                        x: modulation_params.amount,
                        initial: 0.0,
                        relative: (modulation_params.amount + 1.0) / 2.0,
                        relative_default: 0.5,
                        low: -1.0,
                        high: 1.0,
                        group: parameter.group,
                    }),
                    Some(Bounds::Lower) => Some(DisplayValue {
                        x: modulation_params.lower,
                        initial: 0.0,
                        relative: modulation_params.lower,
                        relative_default: 0.0,
                        low: 0.0,
                        high: 1.0,
                        group: parameter.group,
                    }),
                    Some(Bounds::Upper) => Some(DisplayValue {
                        x: modulation_params.upper,
                        initial: 1.0,
                        relative: modulation_params.upper,
                        relative_default: 1.0,
                        low: 0.0,
                        high: 1.0,
                        group: parameter.group,
                    }),
                    Some(Bounds::Offset) => Some(DisplayValue {
                        x: modulation_params.offset,
                        initial: 1.0,
                        relative: modulation_params.offset,
                        relative_default: 1.0,
                        low: 0.0,
                        high: 1.0,
                        group: parameter.group,
                    }),
                }
            }
        }
    }

    pub fn get_note(&self, idx: i16) -> bool {
        self.master_patch.get_note(idx)
    }

    pub fn toggle_note(&mut self, idx: i16) -> bool {
        self.master_patch.toggle_note(idx)
    }

    pub fn get_modulation(&self, param: Param) -> Option<Modulation<Parameter>> {
        param.modulation_source?;
        let idx = param.modulation_source.unwrap();

        match param.parameter {
            Parameter::Master(dest) => Some(Modulation {
                idx,
                destination: Parameter::Master(dest),
                params: self.master_patch.get_modulation_params(idx, dest),
            }),
            Parameter::Voice(channel, dest) => Some(Modulation {
                idx,
                destination: Parameter::Voice(channel, dest),
                params: if channel > 0 && channel <= 16 {
                    self.channel_patches[(channel - 1) as usize].get_modulation_params(idx, dest)
                } else {
                    ModulationParams::new()
                },
            }),
        }
    }

    pub fn shift_param(&mut self, param: Option<Param>, value: f32) {
        if param.is_none() {
            return;
        }

        let param = param.unwrap();

        match param.modulation_source {
            None => match param.parameter {
                Parameter::Master(dest) => {
                    self.master_patch.shift_param(dest, value);
                }
                Parameter::Voice(channel, dest) => {
                    if channel > 0 && channel <= 16 {
                        self.channel_patches[(channel - 1) as usize].shift_param(dest, value);
                    }
                }
            },
            Some(idx) => {
                let mut modulation_params = match param.parameter {
                    Parameter::Master(dest) => self.master_patch.get_modulation_params(idx, dest),
                    Parameter::Voice(channel, dest) => {
                        if channel > 0 && channel <= 16 {
                            self.channel_patches[(channel - 1) as usize]
                                .get_modulation_params(idx, dest)
                        } else {
                            ModulationParams::new()
                        }
                    }
                };

                match param.bounds_mode {
                    None => {
                        modulation_params.amount =
                            (modulation_params.amount + value).min(1.0).max(-1.0);
                    }
                    Some(Bounds::Lower) => {
                        modulation_params.lower =
                            (modulation_params.lower + value).min(1.0).max(0.0);
                    }
                    Some(Bounds::Upper) => {
                        modulation_params.upper =
                            (modulation_params.upper + value).min(1.0).max(0.0);
                    }
                    Some(Bounds::Offset) => {
                        modulation_params.offset =
                            (modulation_params.offset + value).min(1.0).max(0.0);
                    }
                }

                match param.parameter {
                    Parameter::Master(dest) => {
                        self.master_patch
                            .set_modulation_params(idx, dest, modulation_params);
                    }
                    Parameter::Voice(channel, dest) => {
                        if channel > 0 && channel <= 16 {
                            self.channel_patches[(channel - 1) as usize].set_modulation_params(
                                idx,
                                dest,
                                modulation_params,
                            )
                        }
                    }
                }
            }
        }
    }

    pub fn set_param(&mut self, param: Option<Param>, value: f32) {
        if param.is_none() {
            return;
        }

        let param = param.unwrap();

        match param.modulation_source {
            None => match param.parameter {
                Parameter::Master(dest) => {
                    self.master_patch.set_param(dest, value);
                }
                Parameter::Voice(channel, dest) => {
                    if channel > 0 && channel <= 16 {
                        self.channel_patches[(channel - 1) as usize].set_param(dest, value);
                    }
                }
            },
            Some(idx) => {
                let mut modulation_params = match param.parameter {
                    Parameter::Master(dest) => self.master_patch.get_modulation_params(idx, dest),
                    Parameter::Voice(channel, dest) => {
                        if channel > 0 && channel <= 16 {
                            self.channel_patches[(channel - 1) as usize]
                                .get_modulation_params(idx, dest)
                        } else {
                            ModulationParams::new()
                        }
                    }
                };

                match param.bounds_mode {
                    None => {
                        modulation_params.amount = value.min(1.0).max(-1.0);
                    }
                    Some(Bounds::Lower) => {
                        modulation_params.lower = value.min(1.0).max(0.0);
                    }
                    Some(Bounds::Upper) => {
                        modulation_params.upper = value.min(1.0).max(0.0);
                    }
                    Some(Bounds::Offset) => {
                        modulation_params.offset = value.min(1.0).max(0.0);
                    }
                }

                match param.parameter {
                    Parameter::Master(dest) => {
                        self.master_patch
                            .set_modulation_params(idx, dest, modulation_params);
                    }
                    Parameter::Voice(channel, dest) => {
                        if channel > 0 && channel <= 16 {
                            self.channel_patches[(channel - 1) as usize].set_modulation_params(
                                idx,
                                dest,
                                modulation_params,
                            );
                        }
                    }
                }
            }
        }
    }

    pub fn get_config(&self, channel: u8, dest: BalletConfig) -> usize {
        if channel > 0 && channel <= 16 {
            self.channel_patches[(channel - 1) as usize].get_config(dest)
        } else {
            0
        }
    }

    pub fn set_config(&mut self, channel: u8, dest: BalletConfig, value: usize) {
        if channel > 0 && channel <= 16 {
            self.channel_patches[(channel - 1) as usize].set_config(dest, value);
        }
    }

    pub fn get_master_config(&self, dest: BalletMasterConfig) -> usize {
        self.master_patch.get_config(dest)
    }

    pub fn set_master_config(&mut self, dest: BalletMasterConfig, value: usize) {
        self.master_patch.set_config(dest, value);
    }

    pub fn get_page(&mut self, menu_mode: MenuMode) -> u8 {
        match self.menu_pages.get(&menu_mode) {
            Some(page) => *page,
            None => 0,
        }
    }

    pub fn set_page(&mut self, menu_mode: MenuMode, value: u8) {
        self.menu_pages.insert(menu_mode, value);
    }

    pub fn read_ballet_with_logger<'a>(filename: &str, action_tx: &Sender<Action>) -> Self
    where
        Self: Sized + Deserialize<'a>,
    {
        match std::fs::read_to_string(paths::ballet_path(filename)) {
            Ok(s) => Self::from_string_with_logger(s, action_tx),
            Err(_) => Self::new(),
        }
    }

    pub fn from_string_with_logger(s: String, action_tx: &Sender<Action>) -> Self {
        let mut session = Self::new();

        match ron::from_str(&s) {
            Ok(state) => session.set_from(state),
            Err(msg) => {
                action_tx.send(Action::Log(format!("{}", msg))).unwrap();
            }
        }

        session
    }
}

impl State for Session {
    fn new() -> Self {
        Self {
            master_patch: MasterPatch::new(),
            channel_patches: [
                Patch::new(0, 1, 1),
                Patch::new(0, 1, 2),
                Patch::new(0, 1, 3),
                Patch::new(0, 1, 4),
                Patch::new(0, 1, 5),
                Patch::new(0, 1, 6),
                Patch::new(0, 1, 7),
                Patch::new(0, 1, 8),
                Patch::new(2, 3, 9),
                Patch::new(2, 3, 10),
                Patch::new(2, 3, 11),
                Patch::new(2, 3, 12),
                Patch::new(2, 3, 13),
                Patch::new(2, 3, 14),
                Patch::new(2, 3, 15),
                Patch::new(2, 3, 16),
            ],
            voice_channels: [1, 1, 1, 1, 1, 1, 1, 1, 9, 10, 11, 12, 13, 14, 15, 16],
            channel_patch_idx: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            channel: 1,
            base_color: [32, 8, 32],
            base_octave: 4,
            piano_octaves: 0,
            shift_piano: 0,
            grid: HashMap::new(),
            offset: [0, 0],
            menu_pages: HashMap::new(),
        }
    }

    fn from_string(s: String) -> Self {
        match ron::from_str(&s) {
            Ok(state) => state,
            Err(_) => Self::new(),
        }
    }
}
