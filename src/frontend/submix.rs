// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Submix {
    pub idx: usize,
    pub master_gain: f32,
    pub master_pan: f32,
    pub track_gains: [f32; 8],
    pub track_pans: [f32; 8],
    pub track_mutes: [bool; 8],
    pub track_solos: [bool; 8],
    pub fader: f32,
}

impl Submix {
    pub fn from_idx(idx: usize) -> Self {
        Self {
            idx,
            master_gain: 0.0,
            master_pan: 0.0,
            track_gains: [-f32::INFINITY; 8],
            track_pans: [0.0; 8],
            track_mutes: [false; 8],
            track_solos: [false; 8],
            fader: 0.0,
        }
    }
}
