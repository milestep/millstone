// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Value {
    pub x: f32,
    pub initial: f32,

    #[serde(skip)]
    _x: f32,
    modulation: f32,

    #[serde(skip)]
    pub low: f32,
    #[serde(skip)]
    pub default: f32,
    #[serde(skip)]
    pub high: f32,
    #[serde(skip)]
    pub slope: u8, // 0 = linear, 1 = log
    #[serde(skip)]
    pub group: u8,
}

impl Default for Value {
    fn default() -> Self {
        Self {
            x: 0.0,
            _x: 0.0,
            modulation: 0.0,
            default: 0.0,
            low: 0.0,
            high: 1.0,
            initial: 0.0,
            slope: 0,
            group: 0,
        }
    }
}

impl Value {
    pub fn linear(x: f32, default: f32, low: f32, high: f32, group: u8) -> Self {
        Self {
            x,
            _x: x,
            modulation: 0.0,
            default,
            low,
            high,
            initial: x,
            slope: 0,
            group,
        }
    }

    pub fn logarithmic(x: f32, default: f32, low: f32, high: f32, group: u8) -> Self {
        Self {
            x,
            _x: x,
            modulation: 0.0,
            default,
            low,
            high,
            initial: x,
            slope: 1,
            group,
        }
    }

    #[inline(always)]
    pub fn set(&mut self, value: f32) {
        self._x = value;

        self.x = if self.slope == 0 {
            self._x + self.modulation * (self.high - self.low)
        } else {
            self._x * (self.high / self.low).powf(self.modulation)
        };

        self.x = self.x.max(self.low).min(self.high);
    }

    #[inline(always)]
    pub fn modulate(&mut self, amount: f32) {
        self.modulation += amount;
        self.set(self._x);
    }

    #[inline(always)]
    pub fn reset(&mut self) {
        self.modulation = 0.0;
        self.set(self._x);
    }

    pub fn set_from(&mut self, other: &Self) {
        self.set(other.x);
    }

    pub fn get_relative(&self) -> f32 {
        if self.high <= self.low {
            return -1.0;
        }

        if self.slope == 0 {
            (self.x - self.low) / (self.high - self.low)
        } else if self.slope == 1 {
            (self.x / self.low).log2() / (self.high / self.low).log2()
        } else {
            -1.0
        }
    }

    pub fn get_relative_default(&self) -> f32 {
        if self.high <= self.low {
            return -1.0;
        }

        if self.slope == 0 {
            (self.default - self.low) / (self.high - self.low)
        } else {
            (self.default / self.low).log2() / (self.high / self.low).log2()
        }
    }

    pub fn shift_relative(&mut self, value: f32) {
        if self.slope == 0 {
            self.set(self.x + value * (self.high - self.low));
        } else {
            self.set(self.x * (self.high / self.low).powf(value));
        }
    }
}

#[derive(Debug, Deserialize, Clone, Copy, Serialize)]
pub struct Modulation<T> {
    pub idx: [usize; 4],
    pub destination: T,
    pub params: ModulationParams,
}

impl<T> Modulation<T> {
    pub fn with_destination<S>(&self, destination: S) -> Modulation<S> {
        Modulation {
            idx: self.idx,
            destination,
            params: self.params,
        }
    }

    #[inline(always)]
    pub fn get(&self, axes: &[f32]) -> f32 {
        self.params.get(
            self.idx
                .iter()
                .filter(|&idx| *idx != usize::MAX)
                .fold(1.0, |a, &idx| a * axes[idx]),
        )
    }
}

#[derive(Debug, Deserialize, Clone, Copy, Serialize)]
pub struct ModulationParams {
    pub amount: f32,
    pub lower: f32,
    pub upper: f32,
    pub offset: f32,
}

impl ModulationParams {
    pub fn new() -> Self {
        Self {
            amount: 0.0,
            lower: 0.0,
            upper: 1.0,
            offset: 1.0,
        }
    }

    pub fn get(&self, mut axis: f32) -> f32 {
        if self.upper <= self.lower {
            if axis >= self.lower {
                axis = 1.0;
            } else {
                axis = 0.0;
            }
        } else {
            axis = ((axis - self.lower) / (self.upper - self.lower))
                .min(1.0)
                .max(0.0);
        }

        ((self.offset - 1.0) + axis) * self.amount
    }
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Serialize)]
pub enum Bounds {
    Lower,
    Upper,
    Offset,
}
