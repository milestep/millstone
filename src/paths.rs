// This file is part of Millstone.

// Millstone is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// Millstone is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along with
// Millstone. If not, see <https://www.gnu.org/licenses/>.

use std::env;
use std::fs::create_dir_all;
use std::path::Path;

pub fn is_dir(p: &str) -> bool {
    Path::new(&p).is_dir()
}

pub fn join_paths(p1: &str, p2: &str) -> String {
    Path::new(p1)
        .join(Path::new(&p2))
        .to_str()
        .unwrap()
        .to_string()
}

pub fn depot_path(p: &str) -> String {
    let root: String = match env::var("MILLSTONE_DEPOT") {
        Ok(val) => val,
        Err(_) => dirs::home_dir()
            .unwrap()
            .join(Path::new("Millstone"))
            .to_str()
            .unwrap()
            .to_string(),
    };
    let path = join_paths(&root, p);
    create_dir_all(Path::new(&path).parent().unwrap()).unwrap();

    path
}

pub fn project_path(p: &str) -> String {
    let root: String = match env::var("_MILLSTONE_PROJECT_ROOT") {
        Ok(val) => val,
        Err(_) => env::current_dir().unwrap().to_str().unwrap().to_string(),
    };

    join_paths(&root, p)
}

pub fn home_path(p: &str) -> String {
    let root: String = match env::var("MILLSTONE_HOME") {
        Ok(val) => val,
        Err(_) => dirs::home_dir()
            .unwrap()
            .join(Path::new(".config/millstone"))
            .to_str()
            .unwrap()
            .to_string(),
    };

    join_paths(&root, p)
}

#[cfg(feature = "ballet")]
pub fn ballet_path(p: &str) -> String {
    let root: String = match env::var("MILLSTONE_BALLET") {
        Ok(val) => val,
        Err(_) => dirs::home_dir()
            .unwrap()
            .join(Path::new(".config/millstone/ballet"))
            .to_str()
            .unwrap()
            .to_string(),
    };

    join_paths(&root, p)
}
