% Millstone - the rock solid portable digital audio recorder

# Release 0.5 (The ergonomic project release)
## Done features since last release
- Parametric mid EQ
- Saturator
- RMS measurement
- Nice VU meters
- Ine parameter is improved
- Better reverb
- Full screen logger without borders
- Zero influence effect defaults
- Reverb have by-pass at zero posistion
- Splash screen
- VU on Performance mode
- Logger should be a sub mode (like script execution)
- Right justify the effect slots
- SetFader removed, was never constructed
- "q" for save and quit
- Script running open logger at execution
- Jog, scroll, scrub should scroll in logger window
- Improved track vertical visibility zoom
- Transport visible on performance page
- Loop mode is just af pink logo
- Submix for monitoring in live situation
- Menu complete internal overhall
- Menu safety check required width before rendering and adapts to screen size
- Dimming non-active menu columns
- Scripts as menu column and actions
- Trash system is working
- Project names and trash are inforced to be deduplicated
- Rename project name, editable from menu + save
- Copy project
- New from current project
- New from selected project
- All modes accessible from Menu Mode
- Fader controls master levels
- Mute and solo for submixes
- All modes should be in rotation for navigation (no sub-modes)
- New project, New from this at main
- Script: Copy wav file project to sync folder for mixind at home
- No modes menu in Base menu
- Effects, Tanh function should be default for saturation
- Even higher VU master meters using better layout
- Remove marker stripe color
- Fader controls master levels
- New project should have new date name
- Effects, Reverb mix should change room size as well
- For demo purposes Projects menu column contain Projects - remove again
- Reverb should be send effect only

## Fixed bugs since last release
- Sound disappears when entering loop mode (outside buffer)
- Mid eq seems to be buggy, frequency follows gain
- Numeric effect cycling does not take focus on effect slot
- Activation of loop mode does not wait for preload before looping
- Loop lengths bigger than wheel-buffer will not be able to (re)load -> no sound!
- Bug at Cancel action basemain menu
- No markers at new from this
- Trashbin with no elements have wrong select offset
- New from current seems to be buggy
- Playhead time should be reset when making new project
- Stop can be acivated after longpress
- Play state is out of sync with backend when switching project while Play is on

# Release 0.4 (The extended usable release)

## General features
- Master section with filters, reverb and compressor
- Analyzer spectrum page - 'Performance' mode
- Less verbose kernel logging
- General cleanup of top level dir names
- Export event logged
- Improved log location for millstone
- No click when adjusting fader levels

## Core
- Auto reconnect if sound card dies

## Frontend
- Long press for logger and project view
- Vertical zoom button
- Input monitor button
- Global project folder variable
- Project selector menu
- Refactor of generalized input actions
- Output to LED and fader depending on track select
- Implement all features from keyboard to midi device
- No markers for underruns, only overruns
- Removed autorreturn display "button"
- No pages required
- Rec + rev/ff set export region
- Loop button with cycle set functinality, left, riht, disable
- Stop modifier
  - Stop + Play: play with autoreturn
  - Stop + Rewind: jump to start
  - Stop + Forward: jump to end
- Rec + play -> start recording
- Project selector
- Project menu with quit button

# Release 0.3 (The minimal lovable release)

## General features
- Frontend and backend have no shared state anymore
- All communication between frontend and backend happens through channels
- All tracks are now stereo
- Input monitor is now implicitly enabled for a track, when record arm is on

## Core
- Mixer setup and track loading are now handled as events
- Complete overhaul of disk I/O from all-in-memory to wheel buffers
- The above feature gives predictable/near-constant memory use during runtime
- A single, asynchronous thread now handles all I/O in chunks
- Several minor threading optimisations to improve performance
- Kernel now supports all bit depths (tested with Behringer 1820)
- Loudness map is now generated as part of I/O and passed to the frontend asynchronously
- Backend now supports (seamless) looping

## Frontend features
- Support of the Mackie Control protocol via MIDI, including two-way communication
- Cycle through physically available inputs
- Dynamically draw the loudness map
- Set loop points and enable/disable looping
- Auto-return
- Coloured peak meters for input and output
- Graphical faders
- Logging

# Release 0.2 (The minimal usable release)

## General features
- New State trait for unified disk persistence handling
- Track state mutex
- Mixer state mutex
- App state mutex
- Transport state mutex
- Frontend to backend communication channel
- Add and remove track

## Core
- Load/save real WAV files with correct header
- Persistent transport state
- Bus setup for master track
- Export 'vinyl' function
- Auto revision handling
- Start / End export region functionality
- Track state
- Tape abstraction for tracks
- Loudness map
- Start/stop/continue playback
- Change position (while playing/stopped)
- Config toml file with specification of sound card

## Frontend features
- Markers over the waveform view~~
- Follow track selection focus (autoscroll)~~
- Track fader adjusting with number indicator
- Pan adjusting with number indicator
- Add and remove markers
- Cycle marker names
- Move between timeline markers
- Transport indicator with play/pause/record status
- Time indicator
- Show current time code of play-head
- Display tracks visually and navigate in pages, remeber between restarts
- Indicate if track is record armed, muted, solo og input monitoring
- Show play-head
- Display vital messages from backend
- Display master fader
- Colorstripe on active region between ordinary markers

## Frontend stabilization
- Terminal max size live testing
- Persitent app state
- Move non persistant parameters from App state
- Remove all unwraps in frontend code
- Enums for max size
- Safe draw inside app
- Isolate all calls to colors and styles
- Refactor marker drawing code
- Main layout build functions
- Inputs on track status instead of side panel
- Move layout spliting to layout file
- Marker zero removed as default
- Track height toggle implemented across App
- Refactor basic draw functions

## Frontend bugs
- Marker labels are not always remembered
- Height and track position weird sometimes when loading

## Artwork
- SVG logo
- ASCII logo


# Release 0.1 (The dictaphone release)

## Core
- Start/stop record function
- Read/write uncompressed file from memory into file
- Record enable/disable (arm)
- Start/stop playback
- Jump to start when stopping

## Frontend
- Show basic canvas with transport area
- Show play state (Playing, Recording, Stopped)
- Change play/stop/record state on distinct keystrokes
