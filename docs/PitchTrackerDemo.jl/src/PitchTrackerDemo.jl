module PitchTrackerDemo

using Plots: Plots
using WAV: wavread

export Filter, Integrator, IntervalTracker, PitchTracker
export integrate!, take_samples!, wave_from_file, wave_generator
export advance_and_plot!, plot_intervals!
export complex1, complex2, noise, sawtooth, sine, square, triangle

const F32_2PI = Float32(2π)
const F32_FRAC_PI_2 = Float32(π/2)
const DT = inv(44100.0f0)

mutable struct Integrator
    abs::Float32
    sin::Float32
    cos::Float32
    freq::Float32
    affinity::Float32
    min_freq::Float32
    max_freq::Float32
    target_freq::Float32
    noise_level::Float32
    relative_freq_tolerance::Float32
    x::Float32
end

function Integrator(
    min_freq::Float32,
    max_freq::Float32,
    noise_level::Float32,
    relative_freq_tolerance::Float32,
)
    return Integrator(
        1.0f0,
        0.0f0,
        0.0f0,
        min_freq,
        0.0f0,
        min_freq,
        max_freq,
        min_freq,  # Initial target frequency
        noise_level,
        relative_freq_tolerance,
        0.0f0,
    )
end

function set_target_freq!(integrator::Integrator, freq::Float32)
    if freq < integrator.min_freq
        for k in 1:4
            if freq*k >= integrator.min_freq
                freq = freq*k
                break
            end
        end
    end

    if freq > integrator.max_freq
        for k in 1:4
            if freq/k <= integrator.max_freq
                freq = freq/k
                break
            end
        end
    end

    integrator.target_freq = min(max(freq, integrator.min_freq), integrator.max_freq)

    return integrator
end

function integrate!(integrator::Integrator, sample::Float32, dt::Float32=DT)
    integrator.x = (integrator.x + integrator.target_freq*dt) % 1.0f0

    alpha = exp(-dt*integrator.target_freq)

    integrator.abs = alpha*integrator.abs + (1 - alpha)*abs(sample)
    integrator.cos = alpha*integrator.cos + (1 - alpha)*sample*cos(F32_2PI*integrator.x)
    integrator.sin = alpha*integrator.sin + (1 - alpha)*sample*sin(F32_2PI*integrator.x)
    integrator.freq = alpha*integrator.freq + (1 - alpha)*integrator.target_freq

    scale = 0.6337*(integrator.abs^2 + integrator.noise_level)
    affinity = (integrator.sin^2 + integrator.cos^2)/scale
    integrator.affinity = alpha*integrator.affinity + (1 - alpha)*affinity

    freq_tolerance = min(integrator.target_freq, integrator.freq)*integrator.relative_freq_tolerance

    return if abs(integrator.target_freq - integrator.freq) < freq_tolerance
        integrator.affinity
    else
        0.0f0
    end
end

mutable struct Filter
    lowpass_alpha::Float32
    lowpass_y::Float32
    highpass_alpha::Float32
    highpass_y::Float32
    highpass_x::Float32
end

function Filter(freq_low::Float32, freq_high::Float32, dt::Float32=DT)
    return Filter(
        F32_2PI*dt*freq_high/(F32_2PI*dt*freq_high + 1),
        0.0f0,
        1/(F32_2PI*dt*freq_low + 1),
        0.0f0,
        0.0f0,
    )
end

function apply_filter(flt::Filter, sample::Float32)
    flt.highpass_y = flt.highpass_alpha*(flt.highpass_y + sample - flt.highpass_x)
    flt.highpass_x = sample
    flt.lowpass_y = flt.lowpass_y + flt.lowpass_alpha*(flt.highpass_y  - flt.lowpass_y)
    return flt.lowpass_y
end

mutable struct IntervalTracker
    decay::Float32
    threshold::Float32
    prev_sample::Float32
    t::Float32
    x::Float32
    intervals::Vector{Float32}
    cumulated_intervals::Vector{Float32}
    idx::Int
end

function IntervalTracker(
    n::Int,
    decay::Float32,
    threshold::Float32
)
    return IntervalTracker(
        decay,
        threshold,
        0.0f0,
        1.0f0,
        0.0f0,
        fill(inv(40.0f0), n),
        fill(1.0f0, Int((n^2 + n)/2)),
        1,
    )
end

function update_intervals!(
    interval_tracker::IntervalTracker,
    sample::Float32,
    dt::Float32=DT,
)
    if interval_tracker.prev_sample < 0.0f0 && sample >= 0.0f0
        slope = (sample - interval_tracker.prev_sample)/dt
        correction = sample/slope
        interval_tracker.x = interval_tracker.t - correction

        if interval_tracker.x < interval_tracker.decay
            interval_tracker.x = 0.0f0
        end
    end

    trigger = false

    if interval_tracker.x > 0.0f0 && sample > interval_tracker.threshold
        trigger = true

        interval_tracker.intervals[interval_tracker.idx] = interval_tracker.x
        interval_tracker.t -= interval_tracker.x
        interval_tracker.x = 0.0f0

        N = length(interval_tracker.intervals)
        cumulated_idx = 0

        for idx1 in 1:N, idx2 in idx1:N
            cumulated_idx += 1
            interval_idx = interval_tracker.idx - (idx2 - 1)

            while interval_idx < 1
                interval_idx += length(interval_tracker.intervals)
            end

            interval_tracker.cumulated_intervals[cumulated_idx] =
                interval_tracker.intervals[interval_idx]

            if idx1 != idx2
                interval_tracker.cumulated_intervals[cumulated_idx] +=
                    interval_tracker.cumulated_intervals[cumulated_idx - 1]
            end
        end

        interval_tracker.idx += 1

        if interval_tracker.idx > length(interval_tracker.intervals)
            interval_tracker.idx = 1
        end
    end

    interval_tracker.t += dt
    interval_tracker.prev_sample = sample

    return trigger
end

mutable struct RateReducer
    n::Int
    counter::Int
    sum::Float32
    x0::Float32
    x1::Float32
    flt::Filter
end

function RateReducer(n::Int, freq_low::Float32=200.0f0, freq_high::Float32=400.0f0)
    return RateReducer(
        n,
        0,
        0.0f0,
        0.0f0,
        0.0f0,
        Filter(freq_low, freq_high),
    )
end

function update!(rate_reducer::RateReducer, sample::Float32)
    filtered_sample = apply_filter(rate_reducer.flt, sample)

    rate_reducer.sum += filtered_sample
    rate_reducer.counter += 1

    if rate_reducer.counter >= rate_reducer.n
        rate_reducer.x0 = rate_reducer.x1
        rate_reducer.x1 = rate_reducer.sum/Float32(rate_reducer.n)
        rate_reducer.counter = 0
        rate_reducer.sum = 0.0f0
    end

    a = Float32(rate_reducer.counter/rate_reducer.n)
    interpolated_sample = (1 - a)*rate_reducer.x0 + a*rate_reducer.x1

    return sample, filtered_sample, interpolated_sample
end

mutable struct PitchTracker
    integrators::Vector{Integrator}
    interval_tracker::IntervalTracker
    rate_reducer::RateReducer
    freq::Float32
    affinity::Float32
    threshold::Float32
end

function PitchTracker(
    n::Int;
    damping::Float32=0.0f0,
    decay::Float32=inv(2000.0f0),
    freq_high::Float32=400.0f0,
    freq_low::Float32=200.0f0,
    max_freq::Float32=2000.0f0,
    min_freq::Float32=40.0f0,
    noise_level::Float32=1.0f-5,
    rate_reduction::Int=1,
    relative_freq_tolerance::Float32=0.0297f0,  # Roughly half-way between two notes.
    signal_threshold::Float32=10.0f0^(-60.0f0/20.0f0),
    threshold::Float32=0.5f0,
)
    N = Int((n^2 + n)/2)

    return PitchTracker(
        map(_ -> Integrator(min_freq, max_freq, noise_level, relative_freq_tolerance), 1:N),
        IntervalTracker(n, decay, signal_threshold),
        RateReducer(rate_reduction, freq_low, freq_high),
        0.0f0,
        0.0f0,
        threshold,
    )
end

function detect_pitch!(pitch_tracker::PitchTracker, sample::Float32, dt::Float32=DT)
    if pitch_tracker.rate_reducer.counter > 0
        return pitch_tracker.freq, pitch_tracker.affinity
    end

    reduced_dt = pitch_tracker.rate_reducer.n*dt

    if update_intervals!(pitch_tracker.interval_tracker, sample, reduced_dt)
        taken_interval_indices = Int[]

        for integrator in sort(pitch_tracker.integrators; by=x -> -x.affinity)
            integrator_interval = 1/integrator.freq
            best_interval = Inf32
            best_idx = 0

            for (idx, interval) in enumerate(
                pitch_tracker.interval_tracker.cumulated_intervals,
            )
                idx in taken_interval_indices && continue

                if abs(integrator_interval - interval) < abs(integrator_interval - best_interval)
                    best_interval = interval
                    best_idx = idx
                end
            end

            push!(taken_interval_indices, best_idx)

            set_target_freq!(integrator, inv(best_interval))
        end
    end

    pitch_tracker.affinity = 0.0f0

    for integrator in pitch_tracker.integrators
        affinity = integrate!(integrator, sample, reduced_dt)

        if affinity > pitch_tracker.affinity
            pitch_tracker.affinity = affinity

            if affinity > pitch_tracker.threshold
                pitch_tracker.freq = integrator.freq
            end
        end
    end

    return pitch_tracker.freq, pitch_tracker.affinity
end

function plot_intervals!(
    f,
    pitch_tracker::PitchTracker,
    time_start::Number,
    time_stop::Number,
    dt::Float32=DT;
    kwargs...,
)
    samples = Float32[]
    filtered_samples = Float32[]
    interpolated_samples = Float32[]
    freq0s = Float32[]
    crossings = Float32[]
    frequencies = Vector{Float32}[]
    affinities = Vector{Float32}[]
    prev_t = 0.0f0

    for (idx, (sample, freq0)) in enumerate(take_samples!(f, ceil(Int, time_stop/dt), dt))
        idx*dt < time_start && continue

        sample, filtered_sample, interpolated_sample =
            update!(pitch_tracker.rate_reducer, sample)

        detect_pitch!(pitch_tracker, interpolated_sample, dt)

        push!(samples, sample)
        push!(filtered_samples, filtered_sample)
        push!(interpolated_samples, interpolated_sample)
        push!(freq0s, freq0)

        push!(
            frequencies,
            map(
                integrator -> integrator.freq,
                pitch_tracker.integrators,
            ),
        )

        push!(
            affinities,
            map(
                integrator -> integrator.affinity,
                pitch_tracker.integrators,
            ),
        )

        if pitch_tracker.interval_tracker.t < prev_t
            push!(crossings, idx*dt - pitch_tracker.interval_tracker.x)
        end

        prev_t = pitch_tracker.interval_tracker.t
    end

    time_range = range(time_start, time_stop; length=length(samples))

    plt = Plots.plot(
        time_range,
        mapreduce(transpose, vcat, affinities);
        ylabel="affinity",
        color=[:green :orange :purple :black :red :teal],
        label=nothing,
        line=(:line, 1),
        xlabel="time [s]",
        ylims=(0.0, 1.0),
        kwargs...,
    )

    Plots.hline!(
        plt,
        [pitch_tracker.threshold];
        color=:black,
        label="threshold",
    )

    Plots.plot!(
        Plots.twinx(plt),
        time_range,
        samples;
        color=:blue,
        ylabel="",
        label="waveform",
        ticks=nothing,
        ylims=(-1.0, 1.0),
    )

    Plots.plot!(
        Plots.twinx(plt),
        time_range,
        filtered_samples;
        color=:gray,
        ylabel="",
        label="filtered waveform",
        ticks=nothing,
        ylims=(-1.0, 1.0),
    )

    Plots.plot!(
        Plots.twinx(plt),
        time_range,
        interpolated_samples;
        color=:black,
        ylabel="",
        label="interpolated waveform",
        ticks=nothing,
        ylims=(-1.0, 1.0),
    )

    Plots.hline!(
        plt,
        [0.5];
        color=:gray,
        label=nothing,
    )


    if length(crossings) < 200
        Plots.vline!(
            plt,
            crossings;
            color=:gray,
            label=nothing,
        )
    end

    Plots.plot!(
        Plots.twinx(plt),
        time_range,
        freq0s;
        color=:blue,
        ylabel="",
        label="wave frequency",
        ticks=nothing,
        line=(:line, 3),
        ylims=(0.0, 1000.0),

    )

    return Plots.scatter!(
        Plots.twinx(plt),
        time_range,
        mapreduce(transpose, vcat, frequencies);
        ylabel="frequency [1/s]",
        markercolor=[:green :orange :purple :black :red :teal],
        label=nothing,
        markersize=3,
        markeralpha=mapreduce(
            row -> transpose(
                map(x -> 0.01*min(1.0, (x/pitch_tracker.threshold)^2), row),
            ),
            vcat,
            affinities,
        ),
        markerstrokewidth=0,
        ylims=(0.0, 1000.0),
    )
end

function advance_and_plot!(
    f,
    pitch_tracker::PitchTracker,
    time_start::Number,
    time_stop::Number,
    dt::Float32=DT;
    kwargs...,
)
    freq0s = Float32[]
    freqs = Float32[]
    affinities = Float32[]

    for (idx, (sample, freq0)) in enumerate(take_samples!(f, ceil(Int, time_stop/dt), dt))
        idx*dt < time_start && continue

        sample, filtered_sample, interpolated_sample =
            update!(pitch_tracker.rate_reducer, sample)

        freq, affinity = detect_pitch!(pitch_tracker, interpolated_sample, dt)

        push!(freq0s, freq0)
        push!(freqs, freq)
        push!(affinities, affinity)
    end

    time_range = range(time_start, time_stop; length=length(freqs))

    plt = Plots.scatter(
        time_range,
        freqs;
        markercolor=map(x -> x > pitch_tracker.threshold ? :blue : :red, affinities),
        label=nothing, # "tracked",
        xlabel="time [s]",
        ylabel="frequency [1/s]",
        markersize=1,
        markeralpha=map(x -> x > pitch_tracker.threshold ? 0.1 : 0.01, affinities),
        markerstrokewidth=0,
        kwargs...,
    )

    return Plots.plot!(
        time_range,
        freq0s;
        label=nothing,
        color=:gray,
    )
end

function wave_generator(
    f,
    freq::Float32;
    sweep_rate::Float32=0.0f0,
    osc_amp::Float32=0.0f0,
    osc_freq::Float32=4.0f0,
)
    x = 0.0f0
    t = 0.0f0

    return function (dt::Float32)
        y = f(x)

        freq0 = (freq + osc_amp*sin(Float32(2π)*t))
        x += freq0*dt
        t += osc_freq*dt
        freq += sweep_rate*dt

        if x >= 1.0f0
            x -= 1.0f0
        end

        return y, freq0
    end
end

function wave_from_file(file::AbstractString, channel::Int=1)
    wave_data, sample_rate = wavread(file; format="native")
    samples = wave_data[:, channel]
    idx = 1

    return function (dt::Float32)
        isapprox(sample_rate, inv(dt)) || error("incompatible sample rate $sample_rate != $(inv(dt))")

        if idx <= length(samples)
            sample = samples[idx]
        else
            sample = 0.0f0
        end

        idx += 1

        return sample, 0.0f0
    end
end

function take_samples!(f, n::Int, dt::Float32=DT)
    return map(_ -> f(dt), 1:n)
end

wraparound(x::Float32) = x % 1.0f0

sine(x::Float32) = sin(Float32(2π)*x)
triangle(x::Float32) = x < 0.5f0 ? -1.0f0 + 4*x : 1.0f0 - (4*(x - 0.5f0))
sawtooth(x::Float32) = -1.0f0 + 2*x
square(x::Float32) = sign(sine(x))
noise(x::Float32) = -1.0f0 + 2*rand(Float32)

function complex1(x::Float32)
    return (
        0.5f0*sawtooth(x)
        + 0.2f0*sine(wraparound(0.1f0 + 2x))
        - 0.3f0*triangle(wraparound(0.8f0 + 3x))
        + 0.05f0*noise(x)
    )
end

function complex2(x::Float32)
    return (
        0.6f0*sawtooth(x)
        - 0.1f0*sawtooth(0.4f0 + x)
        + 0.2f0*sine(wraparound(0.1f0 + 2x))
        + 0.1f0*sine(wraparound(0.3f0 + 4x))
        + 0.1f0*triangle(wraparound(0.8f0 + 3x))
        + 0.05f0*noise(x)
    )
end

end # module
