# Setting up Millstone on a Raspberry Pi 4 Model B

## Setting up the partitions

For maximum performance, we need to set up a FAT32 partition to host Millstone projects.
Since Millstone uses the standard WAV format to save files, audio files are limited to 4GB
in size, so using e.g. exFAT or will provide no benefit. The most important part, however,
is that we want to avoid journaling, as both the jbd2 journaling process of ext4 partitions,
and the equivalent one for F2FS can interfere with IRQ messages from the USB bus in some
circumstances. The lack of journaling also helps maximise the lifetime of the SD card, and
since Millstone is mostly doing large, sequential writes, we do not need any of the
optimisations found in filesystems that do random read/writes.

You will likely need around 14GiB for the root system, just to be on the safe side, since we
need the full Rust compiler + other dependencies. So it is recommended that you resize the
default root partition down to that size, if you use a default NOOBS SD card that usually
ships with a Raspberry Pi. On a Linux machine with the SD card inserted and partitions
unmounted, launch GParted and downsize the `root` partition. Then, create a new FAT32
partition with the label `Projects` and set it to fill the remaining space. Take note of
the partition name. On a standard NOOBS SD card, it will likely be `/dev/sdX8`.

Then, mount the `root` partition and edit `/etc/fstab`. Assuming you used a default NOOBS SD card, the new partition will be on `/dev/sdX8`, in which case you should append the line:

```
/dev/mmcblk0p8  /home/pi/Projects       vfat    rw,user,exec,umask=0000 0       0
```

If the partition ends in something other than `8`, replace the corresponding last number in: `/dev/mmcblk0p8`.

Now you should be set to install the kernel.

## Realtime preempt kernel

In order for Millstone to run properly, you need to install the latest realtime preempt
Linux kernel. Images and instructions can be found at:

- https://github.com/kdoren/linux/releases

## Disable unnecessary services

To limit unnecessary I/O and CPU load, disable services:

```
$ sudo systemctl disable rsyslog
$ sudo systemctl disable syslog.socket
$ sudo systemctl disable triggerhappy.service
$ sudo systemctl disable cups.service
$ sudo systemctl disable bluetooth.service
$ sudo systemctl disable alsa-state.service
$ sudo systemctl disable cups-browsed.service
$ sudo systemctl disable exim4.service
$ sudo systemctl disable hciuart.service
$ sudo systemctl disable gldriver-test.service
$ sudo systemctl disable rng-tools.service
$ sudo systemctl disable alsa-restore.service
$ sudo systemctl disable keyboard-setup.service
$ sudo systemctl disable systemd-backlight@backlight:rpi_backlight.service
```

## Use no-op IO scheduler

Edit `/boot/cmdline.txt` and change `elevator=deadline` to `elevator=none`. Add `mitigations=off` to disable security vulnerability mitigations, and add `irqaffinity=0` to ensure that IRQ's are bound to CPU0.  Additionally, set `nohz=off`, `nohz_full=1-3`, and `rcu_nocbs=1-3` to reduce OS jitter on CPUs 1-3.

## Remove superfluous packages

```
$ sudo apt remove pulseaudio cups xcompmgr
$ sudo apt autoremove
```

## Optimise `config.txt`

Add the following to the top of `/boot/config.txt`

```
disable_splash=1
boot_delay=0
```

## Overclocking the CPU

The Raspberry Pi allows overclocking up to 2.1GHz, but we set it to 1.75GHz for an extra 20% boost. Add the following to `/boot/config.txt`:

```
over_voltage=2
arm_freq=1750
```

## Reduce swappiness

At the end of `/etc/sysctl.conf`, add:

```
vm.swappiness = 10
```

## udev rules

Create a file in `/etc/udev/rules.d/80-audio.rules` with the following content:

```
KERNEL=="rtc0", GROUP="audio"
KERNEL=="hpet", GROUP="audio"
```

## Install Rust

`curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`

## Build Millstone

Install ALSA dependencies:

```
$ sudo apt install libasound2-dev
```

Install hwloc

```
sudo apt install hwloc libhwloc-dev
```

Then, from the Millstone git directory, build:

```
$ cargo build --release
```

The first time this is run, it may take a while, since Rust needs to compile all of the
dependencies.

## Install Sakura

`sudo apt install sakura`

## Install JuliaMono

Download the latest tarball from:

- https://github.com/cormullion/juliamono/releases

Extract its contents and move all `*.ttf` files into `$HOME/.local/share/fonts`.

Configure Sakura, by editing `$HOME/.config/sakura/sakura.conf` and setting:

```
...
font=JuliaMono,monospace 12
...
```

## Hide mouse cursor

Install unclutter:

```
$ sudo apt install unclutter
```

## Automatically start at boot

To start Millstone on boot, create `$HOME/run_millstone.sh` with the contents:

```
#!/bin/bash

sudo chrt -f -p 95 `pgrep xhci_hcd`

echo performance | sudo tee /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo performance | sudo tee /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
echo performance | sudo tee /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor
echo performance | sudo tee /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor

unclutter -idle 0 &
export MILLSTONE_PROJECT_ROOT=$HOME/Projects/default
$HOME/millstone/target/release/millstone
```

Then create `$HOME/.config/lxsession/LXDE-pi/autostart` with the contents:

```
#!/bin/bash

sakura -x /home/pi/run_millstone.sh
```
