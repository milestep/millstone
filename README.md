# Millstone

<p align="center">![logo](artwork/logo-v2.svg)</p>

The rock-solid, portable digital audio recorder

- [Release history](./docs/HISTORY.md)

- [Raspberry Pi installation guide](./docs/GUIDE.md)

- [Future ideas and known bugs](./docs/FUTURE.md)
