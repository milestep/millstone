#!/usr/bin/env python3
# coding: utf-8

import glob
import json
import os
import sys

from subprocess import run

depot_path = os.environ.get(
    "MILLSTONE_DEPOT", os.path.join(os.path.expanduser("~"), "Millstone")
)
mirror_path = os.environ.get("MILLSTONE_SYNC", os.path.join(depot_path, ".sync.d"))

project_path = os.environ["_MILLSTONE_PROJECT_ROOT"]

print("Project path:", project_path)

# Copy TOML files

toml_files = glob.glob(os.path.join(project_path, "*.toml"))

for toml_file in toml_files:
    rel_file = os.path.relpath(toml_file, depot_path)
    mirror_file = os.path.join(mirror_path, rel_file)

    if os.path.exists(mirror_file):
        mirror_modified_time = os.path.getmtime(mirror_file)
        toml_modified_time = os.path.getmtime(toml_file)
        if mirror_modified_time > toml_modified_time:
            continue

    os.makedirs(os.path.dirname(mirror_file), exist_ok=True)

    if run(["cp", toml_file, mirror_file]):
        print("Done copying:", toml_file, "->", mirror_file)
    else:
        print("Failed to copy:", toml_file, "->", mirror_file)

# Compress and copy audio files

wav_files = glob.glob(os.path.join(project_path, "export", "*.wav"))

for wav_file in wav_files:
    rel_file = os.path.relpath(wav_file, depot_path)
    ogg_file = os.path.join(mirror_path, os.path.splitext(rel_file)[0] + ".ogg")

    size = round(os.path.getsize(wav_file) / 1024 / 1024, 1)
    print(f"File found: {wav_file} [{size} MiB]")

    os.makedirs(os.path.dirname(ogg_file), exist_ok=True)

    result = run(
        ["ffmpeg", "-v", "-8", "-y", "-i", wav_file, "-q", "8", ogg_file],
        capture_output=False,
        shell=False,
    )

    if result.returncode == 0:
        print(f"Done exporting: {ogg_file}")
    else:
        print(f"[{result}] Error while exporting: {ogg_file}")
