#!/bin/bash
set -e
cd $HOME/millstone
git pull origin master
$HOME/.cargo/bin/cargo build -r --features "ballet"
sudo systemctl restart lightdm.service
