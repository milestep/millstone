#!/usr/bin/env python3
# coding: utf-8

import glob
import json
import os
import sys

from subprocess import run

depot_path = os.environ.get(
    "MILLSTONE_DEPOT", os.path.join(os.path.expanduser("~"), "Millstone")
)
mirror_path = os.environ.get("MILLSTONE_SYNC", os.path.join(depot_path, ".sync.d"))

project_path = os.environ["_MILLSTONE_PROJECT_ROOT"]

print("Project path:", project_path)

# Copy project files
toml_files = glob.glob(os.path.join(project_path, "*.toml"))
wav_files = glob.glob(os.path.join(project_path, "audio/*.wav"))

all_files = toml_files + wav_files

for project_file in all_files:
    rel_file = os.path.relpath(project_file, depot_path)
    mirror_file = os.path.join(mirror_path, rel_file)

    if os.path.exists(mirror_file):
        mirror_modified_time = os.path.getmtime(mirror_file)
        modified_time = os.path.getmtime(project_file)
        if mirror_modified_time > modified_time:
            continue

    os.makedirs(os.path.dirname(mirror_file), exist_ok=True)

    max_len = 23
    if run(["cp", project_file, mirror_file]):
        print("Done copying:", project_file[-max_len:], "->", mirror_file[-max_len:])
    else:
        print("Failed to copy:", project_file[-max_len:], "->", mirror_file[-max_len:])
