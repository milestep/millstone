#!/usr/bin/env bash

sudo chrt -f -p 95 `pgrep xhci_hcd`
sudo taskset -pc 0 `pgrep sakura`
sudo taskset -pc 0 `pgrep rcu_preempt`
sudo taskset -pc 1-3 `pgrep xhci_hcd`

echo performance | sudo tee /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo performance | sudo tee /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
echo performance | sudo tee /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor
echo performance | sudo tee /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor

unclutter -idle 0 &

MILLSTONE_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

if [ -e $MILLSTONE_DIR/config.sh ];
then
    source $MILLSTONE_DIR/config.sh
fi;

$MILLSTONE_DIR/target/release/millstone && poweroff
